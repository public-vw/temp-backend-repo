#!/bin/bash

DEBUG=false
ARGS=""
XDEBUG_IDEKEY="INDOCKER"

# Parse command-line arguments for debug flag
for arg in "$@"
do
  case $arg in
    -d|--debug)
      DEBUG=true
      shift
      ;;
    *)
      ARGS="$ARGS $arg"
      shift
      ;;
  esac
done

(
  source ./.env

  # Set environment variables for Xdebug if debug is true
  if [ "$DEBUG" = true ]; then
    $DOCKER_COMPOSE exec -e XDEBUG_MODE=debug -e XDEBUG_CONFIG="idekey=$XDEBUG_IDEKEY" \
     -u $(id -u):$(id -g) phpfpm php artisan $ARGS
  else
    $DOCKER_COMPOSE exec -u $(id -u):$(id -g) phpfpm php artisan $ARGS
  fi
)