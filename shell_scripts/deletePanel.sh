#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "USAGE: ./deletePanel.sh ThePanel"
    echo "EXAMPLE: ./deletePanel.sh Admin Agency"
    echo
    echo "Panel -> Admin, Sysadmin, ... (role name, capital case)"
    exit
fi

lower=$(echo "$1" | sed -r 's/([A-Z])/_\L\1/g' | sed 's/^_//')

#change current directory
cd ..

# remove routes/
rm -fr routes/"$lower"

# remove resources/lang/en/
rm -fr resources/lang/en/"$lower"

# remove resources/assets/
rm -fr resources/assets/"$lower"

# remove resources/views/
rm -fr resources/views/"$lower"

# remove Controllers
rm -fr app/Http/Controllers/"$1"

# remove DataTables
rm -fr app/DataTables/"$1"

## Some manually performs: ##

echo "in RouteServiceProvider:"
echo "... remove function newPanelRoutes()"
echo "... remove the newPanelRoutes() inside boot()"
echo
echo "remove <NewPanel>'s routes to config\routes.php"
echo
echo "remove route prefixes in config/routes.php"
echo
echo "remove sidebar seeder and remove <newPanel>Composer at ViewComposers"
echo
echo "clear <newPanel>Composer in ComposerServiceProvider"
echo
echo "remove new role <NewPanel>"
