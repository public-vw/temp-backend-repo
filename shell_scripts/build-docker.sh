#!/bin/bash

./shell_scripts/stop-docker.sh

OS='UNKNOWN'

if [[ $(uname) == "Darwin" ]]; then
    OS='macOS'
elif [[ $(uname) == "Linux" ]]; then
    OS='linux'
else
    echo "Unsupported OS"
    exit 1
fi

echo "Building in $OS"

#--------------------------------------------------------------------------------

(
  source ./.env

  if [ "$MODE" == "prod" ]; then
    echo "Builds in PRODUCTION mode"
    $DOCKER_COMPOSE -f docker-compose.prod.yaml build $@
  else
    echo "Builds in DEVELOPMENT mode"
    if [[ $OS == "linux" ]]; then
      echo "Builds in LINUX mode"
      $DOCKER_COMPOSE -f docker-compose.linux-dev.yaml build $@
    else
      echo "Builds in non-linux [$OS] mode"
      $DOCKER_COMPOSE build $@
    fi
  fi
)


