#!/bin/bash

if [[ $(pwd) == *"shell_scripts"* ]]; then
  echo "Please run this script from root folder, using bash ./shell_scripts/make_localhost_ssl.sh"
  exit 1
fi

# shellcheck disable=SC2046
docker run -it --rm -v $(pwd)/configs/certs:/certs alpine sh -c "apk add openssl && openssl req -x509 -newkey rsa:4096 -keyout /certs/key.pem -out /certs/cert.pem -days 365 -nodes -subj '/CN=localhost'"
