#!/bin/bash

show_logs=false

# shellcheck disable=SC2214
# shellcheck disable=SC2213
while getopts ":l--logs:" opt; do
  case $opt in
    l|--logs)
      show_logs=true
      shift
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done

./shell_scripts/stop-docker.sh
./shell_scripts/run-docker.sh $@

(
    source ./.env
    if [ "$show_logs" = true ]; then
      $DOCKER_COMPOSE logs -f
    fi
)
