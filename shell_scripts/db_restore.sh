#!/bin/bash

# Global Variables
RCLONE_CONFIG_NAME='myblog_backup'
BACKUP_FOLDER_BASE="./DB_Backups/"

# Help Message
show_help() {
    echo "Usage: db_restore.sh [options] [fileterm]"
    echo "  -y, --yes       Confirm restoring without asking"
    echo "  -d, --drop      Drop all tables before restore"
    echo "  -x, --nobackup  Skip backup when dropping tables"
    echo "  -r, --remote    Use remote backup, skips local backup"
    echo "  -h, --help      Show help message"
}

# Drop Tables
drop_tables() {
    if [ $NOBACKUP -eq 0 ]; then
        echo "Backing up before dropping tables"
        pushd ".." > /dev/null || exit 1
        ./shell_scripts/db_backup.sh -s -l
        popd > /dev/null || exit 1
    fi

    docker_service="db_$1"

    echo "Dropping all tables"
    $DOCKER_COMPOSE exec -T $docker_service mariadb -u "$MARIADB_USER" -p"$MARIADB_PASSWORD" -Nse 'show tables' "$MARIADB_DATABASE" | \
    while read -r table; do
        $DOCKER_COMPOSE exec -T $docker_service mariadb -u "$MARIADB_USER" -p"$MARIADB_PASSWORD" \
            -e "SET FOREIGN_KEY_CHECKS = 0; drop table $table;" "$MARIADB_DATABASE"
    done
    echo "DB 'Truncate All' Process Finished"
}

# Main Function
main() {
    source ./.env
    pushd $BACKUP_FOLDER >/dev/null || exit 1

    if [ $REMOTE -eq 1 ]; then
        echo "Fetching remote backup with term: $FILETERM"
        REMOTE_FILES=$(COMPOSE_PROFILES="$DOCKER_NAME"_backup $DOCKER_COMPOSE run rclone lsf $RCLONE_CONFIG_NAME: --include "db_*${FILETERM}*.tar.gz")
        FILENAME=$(echo "$REMOTE_FILES" | tail -n1)
        COMPOSE_PROFILES="$DOCKER_NAME"_backup $DOCKER_COMPOSE run rclone copy -P "$RCLONE_CONFIG_NAME:$FILENAME" /backup
    else
        FILES=$(find . -name "db_*${FILETERM}*.gz" -type f -print | sort -n | tail -3)
        FILENAME=$(echo "$FILES" | tail -n1 | cut -d'/' -f 2)
    fi

    if [ $YES -eq 0 ]; then
        read -r -p "Are you sure you want to restore $FILENAME? [y/N] " response
        if [[ ! "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then
            echo "Canceled by user"
            popd >/dev/null || exit 1
            exit 1
        fi
    fi

    if [ $DROP -eq 1 ]; then
        drop_tables $1
    fi

    docker_service="db_$1"

    echo "Restoring [$FILENAME] in DB"
    tar xzf "$FILENAME"
    SQL_FILE="${FILENAME%.tar.gz}.sql"
    $DOCKER_COMPOSE exec --workdir=/backup -T $docker_service mariadb -u$MARIADB_USER -p"$MARIADB_PASSWORD" $MARIADB_DATABASE < $SQL_FILE

    rm -f "$SQL_FILE"
    popd >/dev/null || exit 1
}

# Parse options
YES=0
DROP=0
NOBACKUP=0
REMOTE=0

# No options provided
if [ "$#" -eq 0 ]; then
    show_help
    exit 0
fi

if [ "$#" -eq 1 ]; then
    echo "At least one option is required"
    echo
    show_help
    exit 0
fi

database=$1
shift

if [ "$database" != "main" ] && [ "$database" != "bots" ]; then
    echo "Invalid database: $database"
    show_help
    exit 1
fi

BACKUP_FOLDER="$BACKUP_FOLDER_BASE$database"
ENV_FILE="./configs/db/$database.env"

if [ ! -f "$ENV_FILE" ]; then
    echo "Error: Environment file not found for $database."
    exit 1
fi

source "$ENV_FILE"

while (("$#")); do
    case "$1" in
    -y | --yes)
        YES=1
        shift
        ;;
    -d | --drop)
        DROP=1
        shift
        ;;
    -x | --nobackup)
        NOBACKUP=1
        shift
        ;;
    -r | --remote)
        REMOTE=1
        shift
        ;;
    -h | --help)
        show_help
        exit 0
        ;;
    *)
        echo "Invalid option: $1" >&2
        exit 1
        ;;
    esac
done

FILETERM=$1
main $database
