#!/bin/bash

source .env

$DOCKER_COMPOSE --profile pma up -d phpmyadmin

echo "phpMyAdmin has been started."
