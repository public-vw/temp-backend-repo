#!/bin/bash

show_logs=false

# shellcheck disable=SC2214
# shellcheck disable=SC2213
while getopts ":l--logs:" opt; do
  case $opt in
    l|--logs)
      show_logs=true
      shift
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done


OS='UNKNOWN'

if [[ $(uname) == "Darwin" ]]; then
    OS='macOS'
elif [[ $(uname) == "Linux" ]]; then
    OS='linux'
else
    echo "Unsupported OS"
    exit 1
fi

echo "Running in $OS"

#--------------------------------------------------------------------------------

(
  source ./.env

  if [ "$MODE" == "prod" ]; then
    echo "RUNS in PRODUCTION mode"
    $DOCKER_COMPOSE -f docker-compose.prod.yaml up -d $@
  else
    echo "RUNS in DEVELOPMENT mode"
    if [[ $OS == "linux" ]]; then
      echo "RUNS in LINUX mode"
      $DOCKER_COMPOSE -f docker-compose.linux-dev.yaml up -d $@
    else
      echo "RUNS in non-linux [$OS] mode"
      $DOCKER_COMPOSE up -d $@
    fi
  fi

  if [ "$show_logs" = true ]; then
    $DOCKER_COMPOSE logs -f
  fi
)



