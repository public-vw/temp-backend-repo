#!/bin/bash

# Current configuration values
CURRENT_SERVER_NAME="myblogs.datarivers.org"
CURRENT_FOLDER="myblogs"
CURRENT_INTERNAL_DOMAIN="myblogs.local"
CURRENT_FRONTEND_PORT="750"
CURRENT_BACKEND_PORT="150"

# New configuration values
NEW_SERVER_NAME="parfumes.datarivers.org"
NEW_FOLDER="parfumes"
NEW_INTERNAL_DOMAIN="parfumes.local"
NEW_FRONTEND_PORT="760"
NEW_BACKEND_PORT="160"

# File to modify
CONFIG_FILE="./default.conf"

# Function to update the configuration with new values
update_config() {
    local current_value=$1
    local new_value=$2
    sed -i "s|$current_value|$new_value|g" "$CONFIG_FILE"
}

update_ports() {
    local current_port=$1
    local new_port=$2
    sed -i "s|proxy_pass http://$CURRENT_INTERNAL_DOMAIN:\s*$current_port;|proxy_pass http://$NEW_INTERNAL_DOMAIN:$new_port;|g" "$CONFIG_FILE"
}

# Update configuration values
update_config "$CURRENT_SERVER_NAME" "$NEW_SERVER_NAME"
update_config "/domains/$CURRENT_FOLDER/" "/domains/$NEW_FOLDER/"
update_config "$CURRENT_FOLDER.htpasswd" "$NEW_FOLDER.htpasswd"

update_ports "$CURRENT_FRONTEND_PORT" "$NEW_FRONTEND_PORT"
update_ports "$CURRENT_BACKEND_PORT" "$NEW_BACKEND_PORT"
update_config "$CURRENT_INTERNAL_DOMAIN" "$NEW_INTERNAL_DOMAIN"

echo "Nginx configuration for $CURRENT_SERVER_NAME has been updated to $NEW_SERVER_NAME."

nginx -t
