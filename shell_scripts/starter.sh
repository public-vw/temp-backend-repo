#!/bin/bash

if [[ $(pwd) == *"shell_scripts"* ]]; then
  echo "Please run this script from root folder, using bash ./shell_scripts/starter.sh"
  exit 1
fi

mkdir -p data/db/{main,bots}
chmod 777 -R data/db

if [ "$MODE" == "local" ]; then
  echo "Generates localhost ssl certificates"
  bash ./shell_scripts/make_localhost_ssl.sh
fi

chmod 777 -R ./lara_root/storage

(
  source .env

  ./shell_scripts/run-docker.sh

  ./composer.sh install

  $DOCKER_COMPOSE exec minio sh -c "mc alias set myminio http://localhost:9000 $MINIO_ADMIN_USERNAME $MINIO_ADMIN_PASSWORD"
  $DOCKER_COMPOSE exec minio sh -c "mc mb myminio/temporary"
  $DOCKER_COMPOSE exec minio sh -c "mc mb myminio/permanent"
  $DOCKER_COMPOSE exec minio sh -c "mc mb myminio/data"

  if [ "$MODE" == "prod" ]; then
    echo "Regenerates key in production mode"
    ./artisan.sh build-token:generate
    ./artisan.sh revalidate-token:generate
#    ./artisan.sh api-token:generate
    ./artisan.sh key:generate
  fi

  ./artisan.sh migrate --seed
)