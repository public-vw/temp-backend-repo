#!/bin/bash

# Global Variables
RCLONE_CONFIG_NAME='myblog_backup'
BACKUP_FOLDER_BASE='./DB_Backups/'

# Help Message
show_help() {
    echo "Usage: script.sh [database] [OPTION]"
    echo "  database          The database to backup: 'main' or 'bots'"
    echo "  -l, --local        Just a local backup"
    echo "  -u, --upload       Upload the backup"
    echo "  -n, --nolocal      Do not keep local backup"
    echo "  -s, --silence      Disable Telegram alert"
    echo "  -h, --help         Display this help and exit"
}

# Function for the actual backup
perform_backup() {
    current_date=$(date +"%Y%m%d")
    current_time=$(date +"%H%M%S")
    random_code=$(LC_ALL=C tr </dev/urandom -dc 'A-Za-z' | fold -w 3 | head -n 1)
    filename="db_$1_"$current_date"_"$current_time"_"$random_code

    source ./.env
    backup_file=$filename".sql"

    command1="mariadb-dump -u$MARIADB_USER -p\"$MARIADB_PASSWORD\" --default-character-set=utf8 --add-drop-table --add-locks --extended-insert --quick --quote-names $MARIADB_DATABASE > $backup_file"
    command2="tar czf $filename.tar.gz $backup_file; rm -f $backup_file;"

    # Determine the Docker service name based on the database argument
    docker_service="db_$1"

    COMPOSE_PROFILES="$DOCKER_NAME"_backup $DOCKER_COMPOSE exec -u $(id -u):$(id -g) --workdir=/backup $docker_service /bin/bash -c "$command1"
    COMPOSE_PROFILES="$DOCKER_NAME"_backup $DOCKER_COMPOSE exec -u $(id -u):$(id -g) --workdir=/backup $docker_service /bin/bash -c "$command2"

    echo "DB Backup Created Successfully: $filename"
}


# Function to upload the backup
upload_backup() {
    echo "Uploading to remote drive"
    pushd "$BACKUP_FOLDER" || exit 1
    COMPOSE_PROFILES="$DOCKER_NAME"_backup $DOCKER_COMPOSE run rclone copy "/backup/$filename.tar.gz" "$RCLONE_CONFIG_NAME":\/ -v
    popd || exit 1
}

# Function to remove local backup
remove_local_backup() {
    echo "Removing local backup"
    pushd "$BACKUP_FOLDER" || exit 1
    rm -f "$filename.tar.gz" -v
    popd || exit 1
}

# Initialize options
database=""
upload=false
nolocal=false
silence=false

# No options provided
if [ "$#" -eq 0 ]; then
    show_help
    exit 0
fi

if [ "$#" -eq 1 ]; then
    echo "At least one option is required"
    echo
    show_help
    exit 0
fi

database=$1
shift

# Validate database argument
if [ "$database" != "main" ] && [ "$database" != "bots" ]; then
    echo "Invalid database: $database"
    show_help
    exit 1
fi

BACKUP_FOLDER="$BACKUP_FOLDER_BASE$database"
ENV_FILE="./configs/db/$database.env"

if [ ! -f "$ENV_FILE" ]; then
    echo "Error: Environment file not found for $database."
    exit 1
fi

source "$ENV_FILE"

# Process options
while (("$#")); do
    case "$1" in
    -u | --upload)
        upload=true
        shift
        ;;
    -n | --nolocal)
        nolocal=true
        shift
        ;;
    -l | --local)
        echo "Local backup mode enabled"
        nolocal=false
        shift
        ;;
    -s | --silence)
        silence=true
        shift
        ;;
    -h | --help)
        show_help
        exit 0
        ;;
    *)
        echo "Invalid option: $1" >&2
        exit 1
        ;;
    esac
done

# Perform the backup
perform_backup $database

# Upload the backup if specified
if [ "$upload" = true ]; then
    upload_backup
fi

# Remove the local backup if specified
if [ "$nolocal" = true ]; then
    remove_local_backup
fi

# Send Telegram alert unless silenced
if [ "$silence" = false ]; then
    telegram_message="💾 DB Backup created"
    telegram_message+="\n🏁️ YourApp"
    telegram_message+="\n-------------------"
    telegram_message+="\ndate: $(date +%Y-%m-%d\ %A)"
    telegram_message+="\ntime: $(date +%H:%M:%S)"
    telegram_message+="\ncode: $random_code"
    # Replace alertAdminTelegram with your Telegram API call
    # alertAdminTelegram "$telegram_message"
fi
