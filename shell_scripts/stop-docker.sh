#!/bin/bash

(
    source ./.env

  if [ "$MODE" == "prod" ]; then
    echo "RUNS in PRODUCTION mode"
    $DOCKER_COMPOSE -f docker-compose.prod.yaml down --remove-orphans $@
  else
    echo "RUNS in DEVELOPMENT mode"
    if [[ $OS == "linux" ]]; then
      echo "RUNS in LINUX mode"
      $DOCKER_COMPOSE -f docker-compose.linux-dev.yaml down --remove-orphans $@
    else
      echo "RUNS in non-linux [$OS] mode"
      $DOCKER_COMPOSE down --remove-orphans $@
    fi
  fi
)
