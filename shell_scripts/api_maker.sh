#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "USAGE: ./api_maker.sh Panel Model"
    echo "EXAMPLE: ./api_maker.sh InterfaceZone Specialty"
    echo
    echo "ATT: InterfaceZone route should change to BasicInterface with Basic Interface On"
    echo
    echo "Panel -> Admin, Sysadmin, ... (role name, capital case)"
    echo "Model -> Article, Specialty, ... (model name, capital case)"
    exit
fi


./artisan.sh structure:api:controller $1 $2
./artisan.sh structure:transformer $1 $2
./artisan.sh structure:api:route $1 $2
./artisan.sh structure:api:request $1 $2
./artisan.sh structure:policy $2
