<?php
return [
    'layout' => [
        'header' => [
            'profile' => [
                'menu' => [
                    'articles' => 'Artikelen',
                    'categories' => 'Categorieën',
                    'staticPages' => "Statische Pagina's",
                    'signin' => 'Inloggen',
                    'signout' => 'Uitloggen',
                ],
            ],
            'theme' => [
                'light' => 'Licht',
                'dark' => 'Donker',
                'system' => 'Systeem',
            ],
        ],
        'footer' => [
            'newsletter' => [
                'title' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit',
                'email' => 'Uw e-mailadres',
            ],
        ],
    ],
    'auth' => [
        'pages' => [
            'signin' => [
                'title' => 'Inloggen',
                'form' => [
                    'email' => [
                        'label' => 'E-mail',
                        'placeholder' => 'Voer uw e-mail in',
                    ],
                    'password' => [
                        'label' => 'Wachtwoord',
                        'placeholder' => 'Voer uw wachtwoord in',
                    ],
                    'remember' => 'Onthoud mij',
                    'forgetPassword' => 'Wachtwoord vergeten?',
                    'submit' => 'Inloggen',
                    'signup' => [
                        'text' => 'Nog geen account?',
                        'link' => 'Registreren',
                    ],
                ],
            ],
            'signup' => [
                'title' => 'Registreren',
                'form' => [
                    'firstName' => [
                        'label' => 'Voornaam',
                        'placeholder' => 'Voer uw voornaam in',
                    ],
                    'email' => [
                        'label' => 'E-mail',
                        'placeholder' => 'Voer uw e-mail in',
                    ],
                    'password' => [
                        'label' => 'Wachtwoord',
                        'placeholder' => 'Voer uw wachtwoord in',
                    ],
                    'confirmPassword' => [
                        'label' => 'Bevestig wachtwoord',
                        'placeholder' => 'Bevestig uw wachtwoord',
                    ],
                    'agree' => 'Ik ga akkoord met de',
                    'terms' => 'Algemene Voorwaarden',
                    'submit' => 'Registreren',
                    'signin' => [
                        'text' => 'Heeft u al een account?',
                        'link' => 'Inloggen',
                    ],
                ],
            ],
            'forgotPassword' => [
                'title' => 'Wachtwoord vergeten',
                'description' => 'Voer uw e-mailadres in en we sturen u een link om uw wachtwoord opnieuw in te stellen.',
                'form' => [
                    'email' => [
                        'label' => 'E-mail',
                        'placeholder' => 'example@example.com',
                    ],
                    'submit' => 'Verstuur link',
                    'signin' => [
                        'text' => 'Heeft u al een account?',
                        'link' => 'Inloggen',
                    ],
                    'succes' => [
                        'message' => 'Een link om uw wachtwoord opnieuw in te stellen is verzonden naar uw e-mailadres',
                    ],
                ],
            ],
            'forgotPasswordSuccess' => [
                'title' => 'Wachtwoord vergeten',
                'description' => 'Een link om uw wachtwoord opnieuw in te stellen is verzonden naar uw e-mailadres',
                'check' => 'Controleer uw e-mail',
                'contact' => [
                    'text' => 'Heeft u nog steeds problemen?',
                    'link' => 'Neem contact met ons op',
                ],
            ],
            'resetPassword' => [
                'title' => 'Wachtwoord opnieuw instellen',
                'description' => 'Voer uw nieuwe wachtwoord in',
                'form' => [
                    'password' => [
                        'label' => 'Nieuw wachtwoord',
                        'placeholder' => 'Voer uw nieuwe wachtwoord in',
                    ],
                    'confirmPassword' => [
                        'label' => 'Bevestig wachtwoord',
                        'placeholder' => 'Bevestig uw nieuwe wachtwoord',
                    ],
                    'submit' => 'Wachtwoord opnieuw instellen',
                    'signin' => [
                        'text' => 'Heeft u al een account?',
                        'link' => 'Inloggen',
                    ],
                ],
            ],
        ],
    ],
    'date' => [
        'formatDistance' => 'geleden',
    ],
];
