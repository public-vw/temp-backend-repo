<?php
return [
    'admin' => [
        'back' => 'Terug',
        'cancel' => 'Annuleren',
        'next' => 'Volgende',
        'save' => 'Opslaan',
        'article' => [
            'articleCard' => [
                'editArticleContent' => 'Inhoud bewerken',
                'editArticleMetadata' => 'Metadata bewerken',
            ],
            'toolbox' => [
                'viewPublic' => 'Publieke pagina bekijken',
                'articleStatus' => 'Artikelstatus',
                'updateArticleStatus' => 'Bijwerken',
                'articleVersion' => 'Artikelversie',
                'newArticleVersion' => 'Nieuwe versie',
                'editArticle' => 'Bewerken',
                'editArticleContent' => 'Inhoud bewerken',
                'editArticleMetadata' => 'Metadata bewerken',
            ],
            'pages' => [
                'articles' => [
                    'all' => 'Alle artikelen',
                    'new' => 'Nieuw artikel',
                ],
                'new' => [
                    'title' => 'Titel',
                    'category' => 'Categorie',
                    'noCategory' => 'Geen categorie',
                ],
                'editContent' => [
                    'preview' => 'Voorbeeld',
                    'editMetadata' => 'Metadata bewerken',
                    'content' => 'Inhoud',
                    'form' => [
                        'title' => 'Titel',
                        'readTime' => 'Leestijd',
                        'uri' => 'URI',
                        'category' => 'Categorie',
                        'noCategory' => 'Geen categorie',
                        'summary' => 'Samenvatting',
                        'inspiration' => 'Auteur Inspiratie',
                    ],
                ],
            ],
        ],
        'category' => [
            'categoryCard' => [
                'editCategoryContent' => 'Inhoud bewerken',
                'editCategoryMetadata' => 'Metadata bewerken',
                'articles' => 'Artikelen',
                'active' => 'Actief',
            ],
            'toolbox' => [
                'viewPublic' => 'Publieke pagina bekijken',
                'categoryStatus' => 'Categoriestatus',
                'updateCategoryStatus' => 'Bijwerken',
                'categoryVersion' => 'Categorieversie',
                'newCategoryVersion' => 'Nieuwe versie',
                'editCategory' => 'Bewerken',
                'editCategoryContent' => 'Inhoud bewerken',
                'editCategoryMetadata' => 'Metadata bewerken',
            ],
            'pages' => [
                'categories' => [
                    'all' => 'Alle categorieën',
                    'new' => 'Nieuwe categorie',
                ],
                'new' => [
                    'title' => 'Titel',
                    'category' => 'Categorie',
                    'noCategory' => 'Geen categorie',
                ],
                'editContent' => [
                    'preview' => 'Voorbeeld',
                    'editMetadata' => 'Metadata bewerken',
                    'form' => [
                        'title' => 'Titel',
                        'heading' => 'Kop',
                        'uri' => 'URI',
                        'category' => 'Categorie',
                        'noCategory' => 'Geen categorie',
                        'description' => 'Auteur Inspiratie',
                        'summary' => 'Samenvatting',
                    ],
                ],
            ],
        ],
        'staticPage' => [
            'staticPageCard' => [
                'editStaticPageContent' => 'Inhoud bewerken',
                'editStaticPageMetadata' => 'Metadata bewerken',
            ],
            'toolbox' => [
                'viewPublic' => 'Publieke pagina bekijken',
                'staticPageStatus' => 'Status statische pagina',
                'updateStaticPageStatus' => 'Bijwerken',
                'staticPageVersion' => 'Versie statische pagina',
                'newStaticPageVersion' => 'Nieuwe versie',
                'editStaticPage' => 'Bewerken',
                'editStaticPageContent' => 'Inhoud bewerken',
                'editStaticPageMetadata' => 'Metadata bewerken',
            ],
            'pages' => [
                'staticPages' => [
                    'all' => "Alle statische pagina's",
                    'new' => 'Nieuwe statische pagina',
                ],
                'new' => [
                    'title' => 'Titel',
                ],
                'editContent' => [
                    'preview' => 'Voorbeeld',
                    'editMetadata' => 'Metadata bewerken',
                    'content' => 'Inhoud',
                    'form' => [
                        'title' => 'Titel',
                        'heading' => 'Kop',
                        'uri' => 'URI',
                    ],
                ],
            ],
        ],
        'metadata' => [
            'pages' => [
                'editMetadata' => [
                    'advancedMode' => 'Geavanceerde modus',
                    'form' => [
                        'clear' => 'Wissen',
                        'fields' => [
                            'primary' => [
                                'heading' => 'Metadata',
                                'title' => 'Titel',
                                'description' => 'Beschrijving',
                                'keywords' => 'Trefwoorden',
                                'authors' => 'Auteurs',
                                'creator' => 'Maker',
                                'publisher' => 'Uitgever',
                                'colorScheme' => 'Kleurenschema',
                                'noColorScheme' => 'Geen kleurenschema',
                            ],
                            'og' => [
                                'heading' => 'Open Graph',
                                'title' => 'OG Titel',
                                'description' => 'OG Beschrijving',
                                'siteName' => 'OG Sitenaam',
                                'url' => 'OG URL',
                                'authors' => 'OG Auteurs',
                                'type' => 'OG Type',
                                'images' => 'OG Afbeeldingen',
                            ],
                            'twitter' => [
                                'heading' => 'Twitter',
                                'title' => 'Twitter Titel',
                                'siteId' => 'Twitter Site Id',
                                'card' => 'Twitter Kaart',
                                'noCard' => 'Geen Twitter Kaart',
                                'description' => 'Twitter Beschrijving',
                                'images' => 'Twitter Afbeeldingen',
                            ],
                            'robots' => [
                                'heading' => 'Robots',
                                'index' => 'Robots Index',
                                'noIndex' => 'Geen Index',
                                'follow' => 'Robots Volgen',
                                'noFollow' => 'Niet Volgen',
                                'noCache' => 'Robots Geen Cache',
                                'noNoCache' => 'Geen Cache',
                                'googleBotIndex' => 'Robots Google Bot Index',
                                'noGoogleBotIndex' => 'Geen Google Bot Index',
                            ],
                        ],
                        'tooltips' => [
                            'primary' => [
                                'title' => 'Dit is de titel die verschijnt in zoekmachine resultaten voor het artikel',
                                'description' => 'Dit is een korte samenvatting of beschrijving van het artikel, gebruikt in zoekmachine resultaten',
                                'keywords' => 'Dit zijn relevante trefwoorden of zinnen geassocieerd met het artikel voor SEO doeleinden',
                                'authors' => 'Dit zijn de individuen of entiteiten verantwoordelijk voor het creëren van het artikel.',
                                'creator' => 'Dit is de primaire maker of auteur van het artikel.',
                                'publisher' => 'Dit is de entiteit of organisatie verantwoordelijk voor het publiceren van het artikel.',
                                'colorScheme' => 'Dit vertegenwoordigt het kleurenschema of thema gebruikt voor het artikel of de pagina.',
                            ],
                            'og' => [
                                'title' => 'Dit is de Open Graph titel die verschijnt bij het delen van het artikel op sociale media.',
                                'description' => 'Dit is de Open Graph beschrijving die verschijnt bij het delen van het artikel op sociale media.',
                                'siteName' => 'Dit is de Open Graph sitenaam die verschijnt bij het delen van het artikel op sociale media.',
                                'url' => 'Dit is de Open Graph URL die verschijnt bij het delen van het artikel op sociale media.',
                                'authors' => 'Dit zijn de Open Graph auteurs geassocieerd met het artikel wanneer gedeeld op sociale media.',
                                'type' => 'Dit is het Open Graph type dat het type inhoud van het artikel specificeert.',
                                'images' => 'Dit zijn de Open Graph afbeeldingen die verschijnen bij het delen van het artikel op sociale media',
                            ],
                            'twitter' => [
                                'title' => 'Dit is de titel die verschijnt bij het delen van het artikel op Twitter.',
                                'siteId' => 'Dit is de Twitter Site ID geassocieerd met het artikel wanneer gedeeld op Twitter.',
                                'card' => 'Dit specificeert het type Twitter kaart gebruikt bij het delen van het artikel op Twitter.',
                                'description' => 'Dit is de beschrijving die verschijnt bij het delen van het artikel op Twitter.',
                                'images' => 'Dit zijn de Twitter afbeeldingen die verschijnen bij het delen van het artikel op sociale media',
                            ],
                            'robots' => [
                                'index' => 'Deze richtlijn instrueert zoekmachines of ze de pagina moeten indexeren of niet. Wanneer ingesteld op waar, staat het zoekmachines toe de pagina op te nemen in hun zoekresultaten.',
                                'follow' => "Deze richtlijn vertelt zoekmachines of ze de links op de pagina moeten volgen. Wanneer ingesteld op waar, staat het zoekmachines toe de gelinkte pagina's te crawlen en te indexeren",
                                'noCache' => "Deze richtlijn geeft aan dat zoekmachines de inhoud van de pagina niet moeten cachen. Het is nuttig voor pagina's die altijd vers van de server moeten worden opgehaald.",
                                'googleBotIndex' => 'Deze richtlijn is specifiek voor Googlebot en instrueert het of het de pagina moet indexeren. Wanneer ingesteld op waar, staat het Google toe de pagina op te nemen in zijn zoekresultaten',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'components' => [
        'admin' => [
            'categories' => [
                'categorySelector' => [
                    'select' => 'Selecteer een categorie',
                    'empty' => 'Geen categorieën gevonden',
                    'placeholder' => 'Zoek naar een categorie',
                    'noCategory' => 'Geen categorie',
                    'published' => 'Gepubliceerd',
                    'unpublished' => 'Niet gepubliceerd',
                    'articles' => 'Artikelen',
                    'active' => 'Actief',
                ],
            ],
        ],
    ],
    'date' => [
        'formatDistance' => 'geleden',
    ],
];
