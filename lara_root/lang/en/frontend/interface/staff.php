<?php
return [
    'admin' => [
        'back' => 'Back',
        'cancel' => 'Cancel',
        'next' => 'Next',
        'save' => 'Save',
        'article' => [
            'articleCard' => [
                'editArticleContent' => 'Edit Content',
                'editArticleMetadata' => 'Edit Metadata',
            ],
            'toolbox' => [
                'viewPublic' => 'View Public Page',
                'articleStatus' => 'Article Status',
                'updateArticleStatus' => 'Update',
                'articleVersion' => 'Article Version',
                'newArticleVersion' => 'New Version',
                'editArticle' => 'Edit',
                'editArticleContent' => 'Edit Content',
                'editArticleMetadata' => 'Edit Metadata',
            ],
            'pages' => [
                'articles' => [
                    'all' => 'All Articles',
                    'new' => 'New Article',
                ],
                'new' => [
                    'title' => 'Title',
                    'category' => 'Category',
                    'noCategory' => 'No Category',
                ],
                'editContent' => [
                    'preview' => 'Preview',
                    'editMetadata' => 'Edit Metadata',
                    'content' => 'Content',
                    'form' => [
                        'title' => 'Title',
                        'readTime' => 'Reading Time',
                        'uri' => 'URI',
                        'category' => 'Category',
                        'noCategory' => 'No Category',
                        'summary' => 'Summary',
                        'inspiration' => 'Author Inspiration',
                    ],
                ],
            ],
        ],
        'category' => [
            'categoryCard' => [
                'editCategoryContent' => 'Edit Content',
                'editCategoryMetadata' => 'Edit Metadata',
                'articles' => 'Articles',
                'active' => 'Active',
            ],
            'toolbox' => [
                'viewPublic' => 'View Public Page',
                'categoryStatus' => 'Category Status',
                'updateCategoryStatus' => 'Update',
                'categoryVersion' => 'Category Version',
                'newCategoryVersion' => 'New Version',
                'editCategory' => 'Edit',
                'editCategoryContent' => 'Edit Content',
                'editCategoryMetadata' => 'Edit Metadata',
            ],
            'pages' => [
                'categories' => [
                    'all' => 'All Categories',
                    'new' => 'New Category',
                ],
                'new' => [
                    'title' => 'Title',
                    'category' => 'Category',
                    'noCategory' => 'No Category',
                ],
                'editContent' => [
                    'preview' => 'Preview',
                    'editMetadata' => 'Edit Metadata',
                    'form' => [
                        'title' => 'Title',
                        'heading' => 'Heading',
                        'uri' => 'URI',
                        'category' => 'Category',
                        'noCategory' => 'No Category',
                        'description' => 'Author Inspiration',
                        'summary' => 'Summary',
                    ],
                ],
            ],
        ],
        'staticPage' => [
            'staticPageCard' => [
                'editStaticPageContent' => 'Edit Content',
                'editStaticPageMetadata' => 'Edit Metadata',
            ],
            'toolbox' => [
                'viewPublic' => 'View Public Page',
                'staticPageStatus' => 'Static Page Status',
                'updateStaticPageStatus' => 'Update',
                'staticPageVersion' => 'Static Page Version',
                'newStaticPageVersion' => 'New Version',
                'editStaticPage' => 'Edit',
                'editStaticPageContent' => 'Edit Content',
                'editStaticPageMetadata' => 'Edit Metadata',
            ],
            'pages' => [
                'staticPages' => [
                    'all' => 'All Static Pages',
                    'new' => 'New Static Page',
                ],
                'new' => [
                    'title' => 'Title',
                ],
                'editContent' => [
                    'preview' => 'Preview',
                    'editMetadata' => 'Edit Metadata',
                    'content' => 'Content',
                    'form' => [
                        'title' => 'Title',
                        'heading' => 'Heading',
                        'uri' => 'URI',
                    ],
                ],
            ],
        ],
        'metadata' => [
            'pages' => [
                'editMetadata' => [
                    'advancedMode' => 'Advanced Mode',
                    'form' => [
                        'clear' => 'Clear',
                        'fields' => [
                            'primary' => [
                                'heading' => 'Metadata',
                                'title' => 'Title',
                                'description' => 'Description',
                                'keywords' => 'Keywords',
                                'authors' => 'Authors',
                                'creator' => 'Creator',
                                'publisher' => 'Publisher',
                                'colorScheme' => 'Color Scheme',
                                'noColorScheme' => 'No Color Scheme',
                            ],
                            'og' => [
                                'heading' => 'Open Graph',
                                'title' => 'OG Title',
                                'description' => 'OG Description',
                                'siteName' => 'OG Site Name',
                                'url' => 'OG URL',
                                'authors' => 'OG Authors',
                                'type' => 'OG Type',
                                'images' => 'OG Images',
                            ],
                            'twitter' => [
                                'heading' => 'Twitter',
                                'title' => 'Twitter Title',
                                'siteId' => 'Twitter Site Id',
                                'card' => 'Twitter Card',
                                'noCard' => 'No Twitter Card',
                                'description' => 'Twitter Description',
                                'images' => 'Twitter Images',
                            ],
                            'robots' => [
                                'heading' => 'Robots',
                                'index' => 'Robots Index',
                                'noIndex' => 'No Index',
                                'follow' => 'Robots Follow',
                                'noFollow' => 'No Follow',
                                'noCache' => 'Robots No Cache',
                                'noNoCache' => 'No Cache',
                                'googleBotIndex' => 'Robots Google Bot Index',
                                'noGoogleBotIndex' => 'No Google Bot Index',
                            ],
                        ],
                        'tooltips' => [
                            'primary' => [
                                'title' => 'This is the title that appears in search engine results for the article',
                                'description' => 'This is a brief summary or description of the article, used in search engine results',
                                'keywords' => 'These are relevant keywords or phrases associated with the article for SEO purposes',
                                'authors' => 'These are the individuals or entities responsible for creating the article.',
                                'creator' => 'This is the primary creator or author of the article.',
                                'publisher' => 'This is the entity or organization responsible for publishing the article.',
                                'colorScheme' => 'This represents the color scheme or theme used for the article or page.',
                            ],
                            'og' => [
                                'title' => 'This is the Open Graph title that appears when sharing the article on social media.',
                                'description' => 'This is the Open Graph description that appears when sharing the article on social media.',
                                'siteName' => 'This is the Open Graph site name that appears when sharing the article on social media.',
                                'url' => 'This is the Open Graph URL that appears when sharing the article on social media.',
                                'authors' => 'These are the Open Graph authors associated with the article when shared on social media.',
                                'type' => 'This is the Open Graph type that specifies the type of content the article represents.',
                                'images' => 'This is the Open Graph images that appears when sharing the article on social media',
                            ],
                            'twitter' => [
                                'title' => 'This is the title that appears when sharing the article on Twitter.',
                                'siteId' => 'This is the Twitter Site ID associated with the article when shared on Twitter.',
                                'card' => 'This specifies the type of Twitter card used when sharing the article on Twitter.',
                                'description' => 'This is the description that appears when sharing the article on Twitter.',
                                'images' => 'This is the Twitter images that appears when sharing the article on social media',
                            ],
                            'robots' => [
                                'index' => 'This directive instructs search engines whether to index the page or not. When set to true it allows search engines to include the page in their search results.',
                                'follow' => 'This directive tells search engines whether they should follow the links on the page. When set to true it allows search engines to crawl and index the linked pages',
                                'noCache' => "This directive indicates that search engines should not cache the page's content. It's useful for pages that should always be fetched fresh from the server.",
                                'googleBotIndex' => 'This directive is specific to Googlebot and instructs it on whether to index the page. When set to true it allows Google to include the page in its search results',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'components' => [
        'admin' => [
            'categories' => [
                'categorySelector' => [
                    'select' => 'Select Category',
                    'empty' => 'No Categories Found',
                    'placeholder' => 'Search Categories',
                    'noCategory' => 'No Category',
                    'published' => 'Published',
                    'unpublished' => 'Unpublished',
                    'articles' => 'Articles',
                    'active' => 'Active',
                ],
            ],
        ],
    ],
    'date' => [
        'formatDistance' => 'ago',
    ],
];
