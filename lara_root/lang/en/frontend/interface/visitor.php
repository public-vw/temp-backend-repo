<?php
return [
    'layout' => [
        'header' => [
            'profile' => [
                'menu' => [
                    'articles' => 'Articles',
                    'categories' => 'Categories',
                    'staticPages' => 'Static Pages',
                    'signin' => 'Sign In',
                    'signout' => 'Signout',
                ],
            ],
            'theme' => [
                'light' => 'Light',
                'dark' => 'Dark',
                'system' => 'System',
            ],
        ],
        'footer' => [
            'newsletter' => [
                'title' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit',
                'email' => 'Your Email Address',
            ],
        ],
    ],
    'auth' => [
        'pages' => [
            'signin' => [
                'title' => 'Sign in',
                'form' => [
                    'email' => [
                        'label' => 'Email',
                        'placeholder' => 'Enter your email',
                    ],
                    'password' => [
                        'label' => 'Password',
                        'placeholder' => 'Enter your password',
                    ],
                    'remember' => 'Remember me',
                    'forgetPassword' => 'Forgot Password?',
                    'submit' => 'Sign in',
                    'signup' => [
                        'text' => "Don't have an account?",
                        'link' => 'Sign up',
                    ],
                ],
            ],
            'signup' => [
                'title' => 'Sign up',
                'form' => [
                    'firstName' => [
                        'label' => 'First name',
                        'placeholder' => 'Enter your first name',
                    ],
                    'email' => [
                        'label' => 'Email',
                        'placeholder' => 'Enter your email',
                    ],
                    'password' => [
                        'label' => 'Password',
                        'placeholder' => 'Enter your password',
                    ],
                    'confirmPassword' => [
                        'label' => 'Confirm password',
                        'placeholder' => 'Enter confirm password',
                    ],
                    'agree' => 'I agree to the',
                    'terms' => 'Terms & Conditions',
                    'submit' => 'Sign up',
                    'signin' => [
                        'text' => 'Already have an account?',
                        'link' => 'Sign in',
                    ],
                ],
            ],
            'forgotPassword' => [
                'title' => 'Forgot Password',
                'description' => 'Please enter your email address below. We will send you a link to reset your password.',
                'form' => [
                    'email' => [
                        'label' => 'Email Address',
                        'placeholder' => 'example@example.com',
                    ],
                    'submit' => 'Reset Password',
                    'signin' => [
                        'text' => 'Remembered your password?',
                        'link' => 'Sign in',
                    ],
                    'success' => [
                        'message' => 'We have emailed your password reset link!',
                    ],
                ],
            ],
            'forgotPasswordSuccess' => [
                'title' => 'Reset Password',
                'description' => 'An email has been sent to reset your password.',
                'check' => 'Please check your email and follow the instructions to reset your password.',
                'contact' => [
                    'text' => "Didn't receive an email?",
                    'link' => 'Contact us',
                ],
            ],
            'resetPassword' => [
                'title' => 'Reset Password',
                'description' => 'Please enter your new password, then confirm it.',
                'form' => [
                    'password' => [
                        'label' => 'New password',
                        'placeholder' => 'Enter your new password',
                    ],
                    'confirmPassword' => [
                        'label' => 'Confirm password',
                        'placeholder' => 'Enter confirm password',
                    ],
                    'submit' => 'Reset Password',
                    'signin' => [
                        'text' => 'Back to Login?',
                        'link' => 'Sign in',
                    ],
                ],
            ],
        ],
    ],
    'date' => [
        'formatDistance' => 'ago',
    ],
];
