<?php
return [
    'layout' => [
        'header' => [
            'profile' => [
                'menu' => [
                    'articles' => 'مقاله ها',
                    'categories' => 'دسته بندی ها',
                    'staticPages' => 'صفحات استاتیک',
                    'signin' => 'ورود',
                    'signout' => 'خروج',
                ],
            ],
            'theme' => [
                'light' => 'روشن',
                'dark' => 'تاریک',
                'system' => 'سیستم',
            ],
        ],
        'footer' => [
            'newsletter' => [
                'title' => 'لورم ایپسوم متن ساختگی , لابلا بل بلا',
                'email' => 'آدرس ایمیل',
            ],
        ],
    ],
    'auth' => [
        'pages' => [
            'signin' => [
                'title' => 'ورود',
                'form' => [
                    'email' => [
                        'label' => 'ایمیل',
                        'placeholder' => 'ایمیل خود را وارد کنید',
                    ],
                    'password' => [
                        'label' => 'رمز',
                        'placeholder' => 'رمز خود را وارد کنید',
                    ],
                    'remember' => 'مرا به خاطر داشته باش',
                    'forgetPassword' => 'بازیابی رمز عبور؟',
                    'submit' => 'ورود',
                    'signup' => [
                        'text' => 'اکانت ندارید؟',
                        'link' => 'ثبت نام',
                    ],
                ],
            ],
            'signup' => [
                'title' => 'ثبت نام',
                'form' => [
                    'firstName' => [
                        'label' => 'نام',
                        'placeholder' => 'نام خود را وارد کنید',
                    ],
                    'email' => [
                        'label' => 'ایمیل',
                        'placeholder' => 'ایمیل خود را وارد کنید',
                    ],
                    'password' => [
                        'label' => 'رمز',
                        'placeholder' => 'رمز خود را وارد کنید',
                    ],
                    'confirmPassword' => [
                        'label' => 'رمز تایید',
                        'placeholder' => 'رمز تایید خود را ورد کنید',
                    ],
                    'agree' => 'من موافقت دارم با',
                    'terms' => 'قوانین و مقررات',
                    'submit' => 'ثبت نام',
                    'signin' => [
                        'text' => 'آیا قبلا ثبت نام کرده اید؟',
                        'link' => 'ورود',
                    ],
                ],
            ],
            'forgotPassword' => [
                'title' => 'بازیابی رمز عبور',
                'description' => 'لطفا ایمیل خود را وارد کنید تا لینک بازیابی رمز عبور برای شما ارسال شود',
                'form' => [
                    'email' => [
                        'label' => 'ایمیل',
                        'placeholder' => 'example@example.com',
                    ],
                    'submit' => 'ارسال لینک بازیابی',
                    'signin' => [
                        'text' => 'آیا قبلا ثبت نام کرده اید؟',
                        'link' => 'ورود',
                    ],
                    'succes' => [
                        'message' => 'لینک بازیابی رمز عبور برای شما ارسال شد',
                    ],
                ],
            ],
            'forgotPasswordSuccess' => [
                'title' => 'بازیابی رمز عبور',
                'description' => 'لینک بازیابی رمز عبور برای شما ارسال شد',
                'check' => 'لطفا ایمیل خود را بررسی کنید',
                'contact' => [
                    'text' => 'در صورتی که ایمیلی دریافت نکردید با ما در تماس باشید',
                    'link' => 'تماس با ما',
                ],
            ],
            'resetPassword' => [
                'title' => 'بازیابی رمز عبور',
                'description' => 'لطفا رمز عبور جدید خود را وارد کنید',
                'form' => [
                    'password' => [
                        'label' => 'رمز عبور جدید',
                        'placeholder' => 'رمز عبور جدید خود را وارد کنید',
                    ],
                    'confirmPassword' => [
                        'label' => 'تایید رمز عبور جدید',
                        'placeholder' => 'تایید رمز عبور جدید خود را وارد کنید',
                    ],
                    'submit' => 'تغییر رمز عبور',
                    'signin' => [
                        'text' => 'آیا قبلا ثبت نام کرده اید؟',
                        'link' => 'ورود',
                    ],
                ],
            ],
        ],
    ],
    'date' => [
        'formatDistance' => 'پیش',
    ],
];
