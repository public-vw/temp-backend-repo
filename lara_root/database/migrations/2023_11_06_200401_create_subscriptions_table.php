<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->nullableMorphs('subscribable');
            $table->enum('type',config_keys_all('enums.subscriptions.types'))->default(config('enums.subscriptions.type_default'));

            $table->timestamps();

            $table->index('user_id','type');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('subscriptions');
    }
};
