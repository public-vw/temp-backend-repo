<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('seo_details', function (Blueprint $table) {
            $table->id();

            $table->morphs('seoable');

            $table->enum('type', config_keys_all('enums.seo_details.types'))->default(config('enums.seo_details.type_default'));
            $table->text('data')->default(null);

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('seo_details');
    }
};
