<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstname');
            $table->string('lastname')->nullable();

            $table->string('username')->nullable();
            $table->string('nickname',100)->nullable()->unique();
            $table->string('password');
            $table->string('ref_code',20)->nullable()->unique();

            $table->string('mobile')->unique()->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();

            $table->enum('random_color', config_keys_all('enums.random_colors'));

            $table->string('telegram_uid',20)->nullable()->default(null);
            $table->unsignedInteger('referer_id')->nullable()->index();
//            $table->foreign('referer_id')->nullable()->index()->refrences('id')->on('users')->onDelete('set null');

            $table->string('google_secret_key',20)->nullable();
            $table->boolean('use_two_factor_auth')->nullable()->default(null);

            $table->enum('status', config_keys_all('enums.users.status'))->default(config('enums.users.status_default'));

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
