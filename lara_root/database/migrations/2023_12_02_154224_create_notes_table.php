<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->nullOnDelete();
            $table->foreignId('parent_id')->nullable()->default(NULL)
                ->constrained('notes')->cascadeOnUpdate()->nullOnDelete();

            $table->morphs('notable');

            $table->string('platejs_id',100)->nullable()->default(NULL); //TODO:customize platejs to use id instead of this
            $table->string('platejs_parent_id',100)->nullable()->default(NULL); //TODO:customize platejs to use id instead of this

            $table->longText('content')->nullable()->default(null);
            $table->longText('raw_content')->nullable()->default(null)->comment('keeps text to search');

            $table->enum('status',config_keys_all('enums.notes.status'))->default(config('enums.notes.status_default'));

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('notes');
    }
};
