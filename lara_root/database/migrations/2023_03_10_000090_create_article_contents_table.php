<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('article_contents', function (Blueprint $table) {
            $table->id();

            $table->foreignId('article_id')->constrained('articles')->cascadeOnDelete();
            $table->unsignedSmallInteger('version')->default(0);
            $table->string('uri', 150)->nullable();

            $table->string('heading')->nullable()->default(null);
            $table->unsignedTinyInteger('reading_time')->nullable()->default(null)->comment('reading duration time in minute');

            $table->longText('content')->nullable()->default(null);
            $table->longText('raw_content')->nullable()->default(null)->comment('keeps just text to search');

            $table->text('summary')->nullable()->default(null);
            $table->text('inspiration')->nullable()->default(null);

            $table->enum('status', config_keys_all('enums.contents.status'))->default(config('enums.contents.status_default'));

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('article_contents');
    }
};
