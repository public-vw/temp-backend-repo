<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('menu_placeholders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('panel_id')->constrained('panels')->cascadeOnDelete();
            $table->string('slug',100);# sidebar, topmenu, footer, ...
            $table->timestamps();

            $table->unique(['panel_id' , 'slug']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('menu_placeholders');
    }
};
