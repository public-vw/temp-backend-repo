<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {

    public function up(): void
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('origin_id')->nullable()->default(null)->constrained('attachments')->nullOnDelete();//non-null = copied from another record
            $table->foreignId('user_id')->nullable()->default(null)->constrained('users')->nullOnDelete();

            $table->enum('media', config_keys_all('enums.attachments.medias'))->nullable()->default(null);//null = Unknown

            $table->enum('storage_disk', ['permanent', 'temporary', 'data'])->default('temporary');
            $table->string('uri', 150)->nullable();

            $table->string('file_name', 255)->nullable()->default(null);
            $table->string('file_ext', 5)->nullable()->default(null);
            $table->string('file_size', 20)->nullable()->default(null);

            $table->text('properties')->nullable()->default(null);
            // Json String, associative array
            // Image: width, height, blur, alt_text
            // Video: width, height, duration, cover(attachment:image), meta, title
            // Sound: duration, transcription

            $table->string('md5', 32);
            $table->double('quality_rank', 4, 2)->unsigned()->nullable()->default(null);
            $table->string('mimetype', 30)->nullable()->default(null);
            $table->text('original_name')->nullable();
            $table->string('original_ext', 5)->nullable();

            $table->boolean('noindex')->default(false);

            $table->timestamps();

            $table->unique(['uri', 'media']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('attachments');
    }
};
