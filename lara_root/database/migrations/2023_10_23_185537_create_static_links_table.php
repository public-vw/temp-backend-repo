<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('static_links', function (Blueprint $table) {
            $table->id();
            $table->string('title',80);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('static_links');
    }
};
