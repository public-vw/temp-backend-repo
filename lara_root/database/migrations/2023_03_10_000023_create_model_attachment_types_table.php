<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('model_attachment_types', function (Blueprint $table) {
            $table->id();
            $table->foreignId('attachment_type_id');
            $table->string('model_name');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('model_attachment_types');
    }
};
