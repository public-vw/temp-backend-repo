<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('bot_connections', function (Blueprint $table) {
            $table->id();
            $table->string('title',120)->comment('administration title');
            $table->string('username',120)->unique();
            $table->enum('type',config_keys_all('enums.bot_connections.platforms'))->default(config('enums.bot_connections.default'));
            $table->string('robot_token',50)->nullable()->comment('token to send messages');
            $table->text('parameters')->nullable()->default(null);//telegrambot: offset:1,limit:5,...
            $table->string('webhook_token',50)->nullable()->default(null)->comment('token attached to webhook url');
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('bot_connections');
    }
};
