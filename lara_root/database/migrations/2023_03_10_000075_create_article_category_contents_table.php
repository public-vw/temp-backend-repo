<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('article_category_contents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('article_category_id')->constrained('article_categories')->cascadeOnDelete();
            $table->unsignedSmallInteger('version')->default(0);
            $table->string('uri', 150)->nullable();

            $table->string('title', 150)->nullable()->default(null)->comment('usage: in breadcrumb');
            $table->string('heading')->nullable()->default(null);
            $table->text('summary')->nullable()->default(null);
            $table->mediumText('description')->nullable()->default(null);

            $table->enum('status', config_keys_all('enums.contents.status'))->default(config('enums.contents.status_default'));

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('article_category_contents');
    }
};
