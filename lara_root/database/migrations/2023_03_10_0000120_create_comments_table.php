<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->nullOnDelete();
            $table->morphs('commentable');
            $table->text('content');
            $table->foreignId('parent_id')->nullable()->default(NULL)
                ->constrained('comments')->cascadeOnUpdate()->nullOnDelete();
            $table->enum('status',config_keys_all('enums.comments.status'))->default(config('enums.comments.status_default'));

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('comments');
    }
};
