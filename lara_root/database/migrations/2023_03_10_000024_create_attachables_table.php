<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {

    public function up(): void
    {
        Schema::create('attachables', function (Blueprint $table) {
            $table->id();

            $table->foreignId('attachment_id')->constrained('attachments')->cascadeOnDelete();
            $table->foreignId('attachable_id');
            $table->string('attachable_type');
            $table->foreignId('type_id')->nullable()->constrained('attachment_types','id','type_id_foreign')->nullOnDelete();
            $table->text('attributes')->nullable()->default(null);

            $table->timestamps();

            $table->index(['attachment_id', 'attachable_type']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('attachables');
    }
};
