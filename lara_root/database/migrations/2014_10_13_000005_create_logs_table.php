<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignId('user_id')->nullable()->constrained('users')->nullOnDelete();

            $table->ipAddress('ip');
            $table->string('type',20)->nullable(); //error,warning,info,attack,quick,develop,postman
            $table->longText('message');
            $table->text('curr_url')->nullable()->default(NULL);
            $table->text('prev_url')->nullable()->default(NULL);
            $table->integer('milli')->default(false);

            $table->timestamp('created_at');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('logs');
    }
};
