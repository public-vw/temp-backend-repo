<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{

    public function up(): void
    {
        Schema::create('setting_groups', function (Blueprint $table) {
            $table->engine = "MyIsam";

            $table->id();
            $table->foreignId('parent_id')->nullable()->constrained('setting_groups')->cascadeOnDelete();
            $table->string('title',120);
            $table->string('slug',80)->unique();
            $table->unsignedSmallInteger('order')->default(100);
            $table->boolean('hidden')->default(false);
            $table->boolean('for_frontend')->default(false);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('setting_groups');
    }
};
