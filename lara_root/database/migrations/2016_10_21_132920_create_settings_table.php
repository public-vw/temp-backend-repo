<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{

    public function up(): void
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->string('title', 120);
            $table->foreignId('group_id')->constrained('setting_groups')->cascadeOnDelete();
            $table->string('slug', 80);
            $table->text('value')->nullable();
            $table->unsignedSmallInteger('order')->default(100);
            $table->string('lang', 3)->nullable()->default('en');// null = All Other Languages that has no value
            $table->enum('direction', config_keys_all('enums.settings.directions'))->default(config('enums.settings.direction_default'));
            $table->enum('mode', config_keys_all('enums.settings.modes'))->default(config('enums.settings.mode_default'));
            $table->enum('type', config('enums.settings.types'));
            $table->text('description')->nullable();
            $table->boolean('hidden')->default(false);
            $table->boolean('for_frontend')->default(false);
            $table->timestamps();

            $table->unique(['group_id', 'slug']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('settings');
    }
};
