<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {

    public function up(): void
    {
        Schema::create('attachment_types', function (Blueprint $table) {
            $table->id();
            $table->string('title', 120);

            $table->string('slug', 125)->unique()->comment('be used for hard-coding');
            $table->text('properties_limit')->nullable()->default(null);
            # Minimum & Maximum sizes, durations
            # [dur|10,100]                 // video and sound
            # [width|10]                   // fix width
            # [height|10]                  // fix height
            # [width|10,100]               // width between 10px and 100px
            # [width|10,~]                 // no upper bound
            # [width|~,10]                 // no lower bound
            # [box|10,100|20,-|-,300]      // smart boxing
            # [size|100]                   // max filesize in KB

            $table->string('size',8)->default('1M')->comment('file size limit: K M G');
            $table->string('folder', 50)->default('other');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('attachment_types');
    }
};
