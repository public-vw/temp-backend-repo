<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('seo_rich_snippets', function (Blueprint $table) {
            $table->id();

            $table->nullableMorphs('richsnippetable'); // null = whole website (every pages)

            $table->enum('type', config_keys_all('enums.rich_snippets.types'))->default(config('enums.rich_snippets.type_default'));
            $table->text('data')->default(null);

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('seo_rich_snippets');
    }
};
