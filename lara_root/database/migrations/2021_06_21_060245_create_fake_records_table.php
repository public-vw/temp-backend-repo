<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('fake_records', function (Blueprint $table) {
            $table->id();
            $table->string('model');
            $table->foreignId('model_id');
            $table->string('parent_model')->nullable()->default(null);
            $table->foreignId('parent_id')->nullable()->default(null);
            $table->timestamp('created_at');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('fake_records');
    }
};
