<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs('menuable');

            $table->foreignId('placeholder_id')->nullable()->index()->constrained('menu_placeholders')->nullOnDelete();
            $table->string('slug',100)->nullable();

            $table->foreignId('parent_id')->nullable()->default(NULL)->constrained('menus')->nullOnDelete();
            $table->string('title',100)->nullable();
            $table->foreignId('icon_id')->nullable()->constrained('icons')->nullOnDelete();
            $table->unsignedTinyInteger('order')->default(0);

            $table->boolean('heading')->default(false)->comment('true: click has no action');
            $table->boolean('new_tab')->default(false);
            $table->boolean('nofollow')->default(false);

            $table->boolean('active')->default(false);
            $table->timestamps();

            $table->unique(['placeholder_id','slug']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('menus');
    }
};
