<?php

namespace Database\Factories\Traits;

use App\Models\FakeRecord;
use Illuminate\Support\Str;

trait FakeRecordable
{
    protected ?int $parent_id = null;

    public function configure()
    {
        return $this->afterCreating(function ($item, $parent) {
            $this->storeFakeData($item, $parent);
        });
    }

    function manyToOne($child, $related_class, $related_method, array $additional_params = []): mixed
    {
        $parent = (new $related_class)::factory()->create([
            ...$additional_params
        ]);
        $child->{$related_method}()->associate($parent);
        return $parent;
    }

    function oneToMany($parent, $related_class, $related_field, $count, $related_value = null, array $additional_params = []): void
    {
        (new $related_class)::factory()
            ->count($count)
            ->create([
                $related_field => $related_value ?? $parent->id,
                ...$additional_params
            ], $parent);
    }

    function oneToMorph($parent, $related_class, $morph_field, $count = 1, array $additional_params = []): void
    {
        (new $related_class)::factory()
            ->count($count)
            ->create([
                "{$morph_field}_type" => get_class($parent),
                "{$morph_field}_id" => $parent->id,
                ...$additional_params
            ], $parent);
    }

    function manyToMany($parent, $related_class, $count, array $additional_params = []): void
    {
        $related_method = Str::lower(Str::plural(last(explode('\\',$related_class))));
        $items = (new $related_class)::factory()
            ->count($count)
            ->create([
                ...$additional_params
            ], $parent);
        $parent->{$related_method}()->attach($items);
    }

    public function storeFakeData($model, $parent = null)
    {
        FakeRecord::create([
            'model'        => get_class($model),
            'model_id'     => $model->id,
            'parent_model' => $parent ? get_class($parent) : null,
            'parent_id'    => $parent?->id,
        ]);
    }

    // here, we are using 'type_id' field of model to move conditions to state function
    //      then read it and act based on that before creation
    // this function is about accessing that value before creation, which is not a good practice
    // usage: $this->oneToMorph($articleContent, Attachment::class, 'attachmentable', 1, [
    //            'type_id' => $attachment_type->id
    //        ]);
    protected function getAdditionalParameter(string $field_name)
    {
        $field_value = null;

        foreach ($this->states as $stateClosure) {
            $reflection = new \ReflectionFunction($stateClosure);
            $staticVariables = $reflection->getStaticVariables();

            if (isset($staticVariables['state'][$field_name])) {
                $field_value = $staticVariables['state'][$field_name];
                break;
            }
        }

        return $field_value;
    }
}
