<?php

namespace Database\Factories;

use App\Models\ArticleContent;
use App\Models\Attachable;
use App\Models\Attachment;
use App\Models\AttachmentType;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class AttachableFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        $attachable = !ArticleContent::count() ? null : ArticleContent::all()->random();

        return [
            'attachment_id' => Attachment::all()->random()->id,

            'attachable_type' => !$attachable || get_class($attachable),
            'attachable_id'   => !$attachable || $attachable->id,

            'type_id'    => AttachmentType::all()->random()->id,
            'attributes' => null,
        ];
    }
}
