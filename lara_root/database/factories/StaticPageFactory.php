<?php

namespace Database\Factories;

use App\Models\SeoDetail;
use App\Models\StaticPage;
use App\Models\StaticPageContent;
use App\Models\Url;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class StaticPageFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'title' => fake()->text(10),
        ];
    }

    public function configure(): StaticPageFactory
    {
        return $this->afterCreating(function ($staticPage) {
            $this->makeContents($staticPage);
            !$staticPage->published ?: $this->oneToMorph($staticPage, Url::class, 'urlable', 1, [
                'url' => $staticPage->generateUrl(),
            ]);
            $this->storeFakeData($staticPage);
        });
    }

    function makeContents($staticPage): void
    {
        $versions = fake()->numberBetween(1, 10);
        fake()->boolean?:$this->oneToMany($staticPage, StaticPageContent::class, 'static_page_id', 1, additional_params:[
            'status' => config_key('enums.contents.status','active')
        ]);
        $this->oneToMany($staticPage, StaticPageContent::class, 'static_page_id', $versions);
    }
}
