<?php

namespace Database\Factories;

use App\Models\AttachmentType;
use App\Models\Url;
use App\Models\User;
use App\Services\FakeImageService\FakeImageService;
use App\Services\ImageProcessor\ImageProcessor;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Intervention\Image\Facades\Image;

#TODO: we have serious issues in this seeder. redundant code
class AttachmentFactory extends Factory
{
    use FakeRecordable;

    public UploadedFile $original;
    public array $sizes;
    public string $blur;
    public string $extension;

    public function __construct($count = null, ?Collection $states = null, ?Collection $has = null, ?Collection $for = null, ?Collection $afterMaking = null, ?Collection $afterCreating = null, $connection = null, ?Collection $recycle = null)
    {
        parent::__construct($count, $states, $has, $for, $afterMaking, $afterCreating, $connection, $recycle);

        $this->extension = fake()->randomElement(['jpg', 'webp']);
        $image = FakeImageService::getImageByUrl(640, 480, $this->extension, config('app.fake_local_image', true));

        $processor = new ImageProcessor($image);
        [$this->original, $this->sizes, $this->blur] = array_values($processor->process());

    }

    public function definition(): array
    {
        return [
            'storage_disk' => 'permanent',
            'file_ext'     => config('enums.attachments.image_ext'),
            'properties'   => ['blur' => $this->blur],
            'md5'          => md5($this->original->get()),
            'media'        => fake()->randomElement(config_keys_all('enums.attachments.medias')),
            'file_name'    => null,

            'user_id' => fake()->boolean ? User::all()->random()->id : null,

            'uri'           => fake()->boolean ? fake()->slug(4) : null,
            'noindex'       => fake()->boolean,
            'original_name' => fake()->boolean ? basename(fake()->filePath()) : __('attachments.raw_data'),
            'original_ext'  => $this->extension,

            'file_size' => $this->original->getSize(),
            'mimetype'  => $this->original->getMimeType(),

            'quality_rank' => fake()->randomElement(range(1, 10)),
        ];
    }

    public function configure(): AttachmentFactory
    {
        return $this->afterCreating(function ($attachment) {
            $this->oneToMorph($attachment, Url::class, 'urlable', 1, [
                'url' => $attachment->generateUrl(),
            ]);
            $this->storeFakeData($attachment);
            $this->putToStorage($attachment);
        });


    }

    public function putToStorage($attachment)
    {
        if (!$attachment->putToStorage($this->original->get(), extension: $this->extractFileExtension($this->original))) {
            $attachment->delete();
        }

        foreach ($this->sizes as $label => $size) {
            if (!$attachment->putToStorage($size->encode(), $label, config('enums.attachments.image_ext'))) {
                $attachment->delete();
            }
        }
    }

    protected function extractFileExtension(array|\Illuminate\Http\UploadedFile|Image|null $image): mixed
    {
        $file_ext = $image->getMimeType();
        preg_match('/image\/(\w+)/', $file_ext, $file_ext);
        $file_ext = $file_ext[1];
        return $file_ext;
    }

}
