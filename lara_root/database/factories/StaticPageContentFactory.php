<?php

namespace Database\Factories;

use App\Models\SeoDetail;
use App\Services\EditorFakers\PlatejsFaker;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class StaticPageContentFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'uri'          => fake()->slug(4),
            'heading'      => fake()->realText(150),
            'reading_time' => fake()->boolean ? null : fake()->numberBetween(1, 10),
            'content'      => '{}',
            'data'         => '{}',
            'status'       => (string)fake()->numberBetween(0, count(config_keys_all('enums.contents.status')) - 1),
        ];
    }

    public function active(): StaticPageContentFactory
    {
        return $this->state([
            'status' => config_key('enums.contents.status', 'active'),
        ]);
    }

    public function configure(): StaticPageContentFactory
    {
        return $this->afterCreating(function ($staticPageContent, $parent) {
            $staticPageContent->update([
                'content' => PlatejsFaker::setContentObject($staticPageContent)->generateRandomBlocks(15),
                'version' => $staticPageContent->container->getNextNewVersionNumber()
            ]);
            $this->makeSeoDetails($staticPageContent);
            $this->storeFakeData($staticPageContent, $parent);
        });
    }

    function makeSeoDetails($article): void
    {
        $this->oneToMorph($article, SeoDetail::class, 'seoable', rand(1, 5));
    }

}
