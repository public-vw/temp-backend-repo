<?php

namespace Database\Factories;

use App\Models\Like;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class LikeFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        $model = fake()->randomElement(config('morphs.likable'));
        $class = model2class($model);

        return [
            'user_id'      => fake()->title,
            'likable_type' => $model,
            'likable_id'   => (new $class)->random()->id,
        ];
    }
}
