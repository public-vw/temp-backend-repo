<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\User;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    use FakeRecordable;

    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'content' => fake()->realText(300),
            'status'  => (string)fake()->numberBetween(0, count(config_keys_all('enums.comments.status')) - 1),
        ];

        #TODO: enable parent_id for comments in faker mode

        /*        $ret['parent_id'] = fake()->boolean ? null
                    : (Comment::where('commentable_type',$ret['commentable_type'])
                        ->where('commentable_id',$ret['commentable_id'])
                        ->get()
                        ->random()->id ?? null);*/
    }
}
