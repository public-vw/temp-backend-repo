<?php

namespace Database\Factories;

use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class SeoDetailFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'type' => (string)fake()->numberBetween(0, count(config_keys_all('enums.seo_details.types')) - 1),
            'data' => $this->fakeArray(10)
        ];
    }

    public function configure(): SeoDetailFactory
    {
        return $this->afterCreating(function ($seo_details, $parent) {

            switch (config('enums.seo_details.types.' . $seo_details->type)) {

                case "basic-fields":
                    $seo_details->data = [
                        'title'       => fake()->text(10),
                        'keywords'    => implode(',', $this->fakeArray(10)),
                        'description' => fake()->text(10),
                        "colorScheme" => fake()->randomElement(['light', 'dark']),
                        "creator"     => "John Doe",
                        "publisher"   => "Jane Doe",
                    ];
                    break;
                case "open-graph-article":
                    $seo_details->data = [
                        'title'       => fake()->text(10),
                        'description' => fake()->text(10),
                        'siteName'    => fake()->text(5),
                        'url'         => fake()->url(),
                    ];
                    break;

                case "icons":
                    $seo_details->data = $this->fakeArray(10);
                    break;

                case "theme-color":
                    $seo_details->data = $this->fakeArray(10);
                    break;

                case "twitter":
                    $seo_details->data = [
                        'title'       => fake()->text(10),
                        'description' => fake()->text(10),
                        'siteId'      => fake()->text(5),
                        'card'        => fake()->randomElement(['summary_large_image']),
                    ];
                    break;

                case "app-link":
                    $seo_details->data = $this->fakeArray(10);
                    break;


                case 'open-graph-static-page':
                    //
                    break;
                case 'robots':
                    //
                    break;
                case 'apple-web-app':
                    //
                    break;
                case 'alternatives':
                    //
                    break;
                case 'bookmarks':
                    //
                    break;
                case 'other':
                    //
                    break;

                /*

                                case 'google':
                                    $seo_details->data = [
                                        'title'       => fake()->text(10),
                                        'keywords'    => $this->fakeArray(10),
                                        'description' => fake()->text(10),
                                    ];
                                    break;
                                case 'open-graph-website':
                                    $seo_details->data = [
                                        'og:type'        => 'website',
                                        'og:title'       => fake()->text(10),
                                        'og:description' => fake()->text(10),
                                        'og:image'       => fake()->imageUrl(),
                                        'og:url'         => $this->fakeArray(10),
                                    ];
                                    break;
                                case 'open-graph-article':
                                    $seo_details->data = [
                                        'og:type'                => 'article',
                                        'og:title'               => fake()->text(10),
                                        'og:site-name'           => fake()->text(10),
                                        'og:description'         => fake()->text(10),
                                        'og:image'               => fake()->imageUrl(),
                                        'og:url'                 => $this->fakeArray(10),
                                        'article:section'        => fake()->text(10),
                                        'article:published_time' => fake()->date(),
                                    ];
                                    break;
                                case 'twitter':
                                    $seo_details->data = [
                                        'twitter:title'       => fake()->text(10),
                                        'twitter:url'         => $this->fakeArray(10),
                                        'twitter:description' => fake()->text(10),
                                        'twitter:creator'     => $this->fakeArray(10),
                                        'twitter:image'       => fake()->imageUrl(),
                                        'twitter:card'        => fake()->randomElement([
                                            'Summary',              // Basic card with a title, description, and small thumbnail image, good for most blog posts
                                            'Summary Large Image',  // Similar to Summary, but features a larger, more prominent image
                                            'App',                  // Promotes a mobile application with a direct download link
                                            'Player',               // Embeds a video/audio player directly into the tweet
                                            'Gallery',              // Showcases a collection of images in a gallery format (Note: Twitter native support might be limited)
                                            'Product',              // Designed to showcase a product, includes price, and availability (Note: Twitter native support might be limited)
                                            'Photo',                // Focuses primarily on a large photo, useful for image-heavy posts (Note: Twitter native support might be limited)
                                            'Video',                // Designed to showcase a video, may include title and description (Note: Twitter native support might be limited)
                                            'Audio',                // Features an audio player for sharing music or podcasts (Note: Twitter native support might be limited)
                                            'Lead Generation',      // Includes a lead capture form for collecting user information (Note: Twitter native support might be limited)
                                        ]),
                                    ];
                                    break;
                            }*/
            }
            $seo_details->save();
            $this->storeFakeData($seo_details, $parent);
        });
    }

    function fakeArray($count, $maxNbChars = 10): array
    {
        return collect(range(1, fake()->numberBetween(1, $count)))->map(function () use ($maxNbChars) {
            return trim(fake()->text($maxNbChars), '.');
        })->toArray();
    }

}
