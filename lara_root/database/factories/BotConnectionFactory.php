<?php

namespace Database\Factories;

use App\Models\BotConnection;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class BotConnectionFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'title' => fake()->title,
        ];
    }
}
