<?php

namespace Database\Factories;

use App\Models\Panel;
use App\Models\User;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'firstname'          => $this->fakeFirstName(),
            'lastname'           => $this->fakeLastName(),
            'username'           => $this->fakeUniqueUsername(),
            'nickname'           => $this->fakeNickname(),
            'ref_code'           => $this->fakeUniqueRefCode(),
            'referer_id'         => $this->fakeReferer(),
            'email'              => $this->fakeUniqueEmail(),
            'email_verified_at'  => $this->fakeDateTime(),
            'mobile'             => $this->fakeMobileNumber(),
            'mobile_verified_at' => $this->fakeDateTime(),
            'random_color'       => $this->fakeRandomColor(),
            'password'           => $this->fakePassword('password'),
            'remember_token'     => $this->fakeToken(),
            'status'             => $this->fakeStatus(),
        ];
    }

    protected function fakeFirstName(): string
    {
        return fake()->firstName();
    }

    protected function fakeLastName(): ?string
    {
        return fake()->boolean ? fake()->lastName() : null;
    }

    protected function fakeUniqueUsername(): string
    {
        return fake()->unique()->userName();
    }

    protected function fakeNickname(): string
    {
        return Str::slug(fake()->unique()->name());
    }

    protected function fakeUniqueRefCode(): ?string
    {
        return fake()->boolean ? fake()->unique()->text(20) : null;
    }

    protected function fakeReferer(): ?string
    {
        return fake()->boolean ? User::all()->random()->id : null;
    }

    protected function fakeUniqueEmail(): string
    {
        return fake()->unique()->safeEmail();
    }

    protected function fakeDateTime(): ?\Illuminate\Support\Carbon
    {
        return fake()->boolean ? now() : null;
    }

    protected function fakeRandomColor(): string
    {
        return fake()->numberBetween(0, count(config_keys_all('enums.random_colors')) - 1);
    }

    protected function fakeMobileNumber(): ?string
    {
        return fake()->boolean ? fake()->unique()->e164PhoneNumber : null;
    }

    protected function fakePassword(string $pass): string
    {
        return Hash::make($pass);
    }

    protected function fakeToken(): ?string
    {
        return fake()->boolean ? Str::random(10) : null;
    }

    protected function fakeStatus(): string
    {
        return (string)fake()->numberBetween(0, count(config_keys_all('enums.users.status')) - 1);
    }

    public function unverified(): static
    {
        return $this->state(fn(array $attributes) => [
            'email_verified_at' => null,
        ]);
    }

    public function configure(): UserFactory
    {
        return $this->afterCreating(function ($user, $parent) {
            if (!$parent) {
                $user->assignRole($this->getRandomActivePanel());
            } else {
                $user->assignRole('client');
            }
            $this->storeFakeData($user);
        });
    }

    function getRandomActivePanel()
    {
        return Panel::where('active', true)->whereNot('title', 'admin')->get()->random()->title;
    }
}
