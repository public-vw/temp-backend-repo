<?php

namespace Database\Factories;

use App\Models\ArticleCategory;
use App\Models\ArticleCategoryContent;
use App\Models\SeoDetail;
use App\Models\Url;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;


class ArticleCategoryFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'parent_id' => null,
        ];
    }

    public function configure(): ArticleCategoryFactory
    {
        return $this->afterCreating(function ($category) {
            $category->update(['parent_id' => ($category->id > 1) && fake()->boolean ? ArticleCategory::where('id', '<', $category->id)->get()->random()->id : null]);
            $this->makeContents($category);
            !$category->published ?: $this->oneToMorph($category, Url::class, 'urlable', 1, [
                'url' => $category->generateUrl(),
            ]);
            $this->storeFakeData($category);
        });
    }

    function makeContents($category): void
    {
        $versions = fake()->numberBetween(1, 3);
        fake()->boolean?:$this->oneToMany($category, ArticleCategoryContent::class, 'article_category_id', 1, additional_params:[
            'status' => config_key('enums.contents.status','active')
        ]);
        $this->oneToMany($category, ArticleCategoryContent::class, 'article_category_id', $versions);
    }

}
