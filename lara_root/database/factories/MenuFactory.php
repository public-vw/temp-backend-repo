<?php

namespace Database\Factories;

use App\Models\Icon;
use App\Models\Menu;
use App\Models\MenuPlaceholder;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class MenuFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'placeholder_id' => fake()->boolean ? null : fake()->randomElement(MenuPlaceholder::all()->pluck('id')),

            'parent_id' => null,
            'title' => implode(' ', fake()->words(3)),

            'icon_id' => fake()->boolean? Icon::all()->random()->id : null,

            'heading'=> false,
            'new_tab' => fake()->boolean,
            'nofollow' => fake()->boolean,

            'active' => fake()->boolean,
        ];
    }
}
