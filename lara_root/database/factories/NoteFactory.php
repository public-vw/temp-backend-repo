<?php

namespace Database\Factories;

use App\Models\Note;
use App\Models\User;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class NoteFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'user_id'     => User::all()->random()->id,
            'content'     => fake()->realText(300),
            'raw_content' => fake()->realText(300),
            'status'      => (string)fake()->numberBetween(0, count(config_keys_all('enums.notes.status')) - 1),
        ];

        #TODO: enable parent_id for notes in faker mode

        /*        $ret['parent_id'] = fake()->boolean ? null
                    : (Note::where('notable_type',$ret['notable_type'])
                        ->where('notable_id',$ret['notable_id'])
                        ->get()
                        ->random()->id ?? null);*/
    }
}
