<?php

namespace Database\Factories;

use App\Models\Subscription;
use App\Models\User;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubscriptionFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'user_id' => User::all()->random()->id,
        ];
    }

    public function generalType(): SubscriptionFactory
    {
        return $this->state([
            'subscribable_type' => null,
            'subscribable_id'   => null,
            'type'              => config_key('enums.subscriptions.types', 'general'),
        ]);
    }

    public function followAuthorType(): SubscriptionFactory
    {
        return $this->state([
            'subscribable_type' => User::class,
            'subscribable_id'   => User::all()->random()->id,
            'type'              => config_key('enums.subscriptions.types', 'follow-author'),
        ]);
    }
}
