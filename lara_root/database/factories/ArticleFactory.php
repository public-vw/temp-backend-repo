<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleContent;
use App\Models\Attachment;
use App\Models\Comment;
use App\Models\SeoDetail;
use App\Models\Url;
use App\Models\User;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'author_id'   => User::all()->random()->id,
            'category_id' => ArticleCategory::all()->random()->id,
        ];
    }

    public function configure(): ArticleFactory
    {
        return $this->afterCreating(function ($article) {
            $user = $this->manyToOne($article, User::class, 'author');
            $this->makeArticleContents($article);
//            $this->makeComments($article);
            !$article->published ?: $this->oneToMorph($article, Url::class, 'urlable', 1, [
                'url' => $article->generateUrl(),
            ]);
            $this->storeFakeData($article, $user);
        });
    }

    function makeArticleContents($article): void
    {
        $versions = fake()->numberBetween(1, 10);
        fake()->boolean?:$this->oneToMany($article, ArticleContent::class, 'article_id', 1, additional_params:[
            'status' => config_key('enums.contents.status','active')
        ]);
        $this->oneToMany($article, ArticleContent::class, 'article_id', $versions);
    }

    function makeComments($article): void
    {
        $this->oneToMorph($article, Comment::class, 'commentable', fake()->numberBetween(0, 20));
    }
}
