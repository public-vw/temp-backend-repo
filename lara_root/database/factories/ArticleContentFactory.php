<?php

namespace Database\Factories;

use App\Models\Attachable;
use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\SeoDetail;
use App\Services\EditorFakers\PlatejsFaker;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleContentFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'heading'      => fake()->realText(150),
            'uri'          => fake()->slug(4),
            'reading_time' => fake()->boolean ? null : fake()->numberBetween(1, 10),
            'summary'      => fake()->realText(100),
            'inspiration'  => fake()->realText(100),
            'content'      => '{}',
            'raw_content'  => '',
            'status'       => (string)fake()->numberBetween(0, count(config_keys_all('enums.contents.status')) - 1),
        ];
    }

    public function configure(): ArticleContentFactory
    {
        return $this->afterCreating(function ($articleContent, $parent) {
            $editorContentFaker = PlatejsFaker::setContentObject($articleContent);
            $content = $editorContentFaker->generateRandomBlocks(15);
            $rawContent = $editorContentFaker->getRawContent();
            $articleContent->update([
                'content'     => $content,
                'raw_content' => $rawContent,
                'version'     => $articleContent->container->getNextNewVersionNumber()
            ]);
            $this->makeSeoDetails($articleContent);
            $this->createCoverImageAttachment($articleContent);
            $this->storeFakeData($articleContent, $parent);
        });
    }

    function makeSeoDetails($article): void
    {
        $this->oneToMorph($article, SeoDetail::class, 'seoable', rand(1, 5));
    }

    protected function createCoverImageAttachment($articleContent): void
    {
        $attachment_type = AttachmentType::where('slug', 'article-cover-image')->first();
        $attachment = Attachment::factory()->create([
            'media' => config_key('enums.attachments.medias', 'image'),
        ]);
        $this->oneToMorph($articleContent, Attachable::class, 'attachable', 1, [
            'attachment_id' => $attachment->id,
            'type_id'       => $attachment_type->id,
        ]);
    }


    public function active(): ArticleContentFactory
    {
        return $this->state([
            'status' => config_key('enums.contents.status', 'active'),
        ]);
    }

}
