<?php

namespace Database\Factories;

use App\Models\Article;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

class ArticleRelationFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'article_id' => Article::published()->get()->random()->id,
            'order'      => $this->faker->numberBetween(1, 100),
        ];
    }

    public function withRelatable(Model $relatable): ArticleRelationFactory
    {
        return $this->state([
            'relatable_type' => get_class($relatable),
            'relatable_id'   => $relatable->id,
        ]);
    }

    public function withRelation($relation_type): ArticleRelationFactory
    {
        return $this->state([
            'relation_type' => config_key('enums.article_relations.types',$relation_type),
        ]);
    }

}
