<?php

namespace Database\Factories;

use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class ColorFactory extends Factory
{
    use FakeRecordable;

    public function definition()
    {
        return [
            'foreground' => fake()->hexColor(),
            'background' => fake()->safeHexColor(),
        ];
    }
}
