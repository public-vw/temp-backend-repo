<?php

namespace Database\Factories;

use App\Models\SeoRichSnippet;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class SeoRichSnippetFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'type' => (string)fake()->numberBetween(0, count(config_keys_all('enums.rich_snippets.types')) - 1),
            'data' => $this->fakeArray(10)
        ];
    }


    public function configure(): SeoRichSnippetFactory
    {
        return $this->afterCreating(function ($rich_snippets, $parent) {

            switch (config('enums.rich_snippets.types.' . $rich_snippets->type)) {

                case "website":
                    $rich_snippets->data = [
                        'title'       => fake()->text(10),
                        'keywords'    => implode(',', $this->fakeArray(10)),
                        'description' => fake()->text(10),
                        "colorScheme" => fake()->randomElement(['light', 'dark']),
                        "creator"     => "John Doe",
                        "publisher"   => "Jane Doe",
                    ];
                    break;
            }
            $rich_snippets->save();
            $this->storeFakeData($rich_snippets, $parent);
        });
    }

    function fakeArray($count, $maxNbChars = 10): array
    {
        return collect(range(1, fake()->numberBetween(1, $count)))->map(function () use ($maxNbChars) {
            return trim(fake()->text($maxNbChars), '.');
        })->toArray();
    }

}
