<?php

namespace Database\Factories;

use App\Models\Url;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class UrlFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [];
    }
}
