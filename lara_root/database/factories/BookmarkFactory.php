<?php

namespace Database\Factories;

use App\Models\Bookmark;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookmarkFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        $model = fake()->randomElement(config('morphs.bookmarkable'));
        $class = model2class($model);

        return [
            'user_id'           => fake()->title,
            'bookmarkable_type' => $model,
            'bookmarkable_id'   => (new $class)->random()->id,
        ];
    }
}
