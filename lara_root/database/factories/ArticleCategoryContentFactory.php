<?php

namespace Database\Factories;

use App\Models\ArticleCategory;
use App\Models\Attachable;
use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\SeoDetail;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleCategoryContentFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'title'       => $this->makeTitle(1, 5),
            'uri'         => fake()->slug(4),
            'heading'     => fake()->realText(120),
            'summary'     => fake()->realText(100),
            'description' => fake()->realText(1000),
            'status'      => (string)fake()->numberBetween(0, count(config_keys_all('enums.contents.status')) - 1),
        ];
    }

    public function makeTitle($min, $max): string
    {
        return implode(' ', fake()->words(fake()->randomNumber($min, $max)));
    }

    public function active(): ArticleCategoryContentFactory
    {
        return $this->state([
            'status' => config_key('enums.contents.status', 'active'),
        ]);
    }

    public function configure(): ArticleCategoryContentFactory
    {
        return $this->afterCreating(function ($articleCategoryContent, $parent) {
            $articleCategoryContent->update(['version' => $articleCategoryContent->container->getNextNewVersionNumber()]);
            $this->createCoverImageAttachment($articleCategoryContent);
            $this->makeSeoDetails($articleCategoryContent);
            $this->storeFakeData($articleCategoryContent, $parent);
        });
    }

    protected function createCoverImageAttachment($articleCategoryContent): void
    {
        $attachment_type = AttachmentType::where('slug', 'article-category-cover-image')->first();
        $attachment = Attachment::factory()->create([
            'media' => config_key('enums.attachments.medias', 'image'),
        ]);
        $this->oneToMorph($articleCategoryContent, Attachable::class, 'attachable', 1, [
            'attachment_id' => $attachment->id,
            'type_id'       => $attachment_type->id,
        ]);
    }

    function makeSeoDetails($article): void
    {
        $this->oneToMorph($article, SeoDetail::class, 'seoable', rand(1, 5));
    }

}
