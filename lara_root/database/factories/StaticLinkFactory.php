<?php

namespace Database\Factories;

use App\Models\Url;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class StaticLinkFactory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'title' => implode(' ', fake()->words(3)),
        ];
    }
    public function configure(): StaticLinkFactory
    {
        return $this->afterCreating(function ($static_link) {
            $this->oneToMorph($static_link, Url::class, 'urlable', 1, [
                'url' => $static_link->generateUrl(),
            ]);
            $this->storeFakeData($static_link, null);
        });
    }

}
