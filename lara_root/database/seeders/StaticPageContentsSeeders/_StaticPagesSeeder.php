<?php

namespace Database\Seeders\StaticPageContentsSeeders;

class _StaticPagesSeeder
{
    protected array $data = [];
    protected function add(string $key, $value, $isArray = false): void
    {
        $keys = explode('.', $key);

        if($isArray) {
            $lastKey = array_pop($keys); // Remove the last key
            $key = implode('.', $keys); // Rejoin the keys
            $keys = explode('.', $key); // Re-split for recursion
            $this->addRecursive($this->data, $keys, [$lastKey => [$value]]);
        } else {
            $this->addRecursive($this->data, $keys, $value);
        }
    }


    protected function addRecursive(&$array, $keys, $value): void
    {
        $key = array_shift($keys);

        if (count($keys) > 0) {
            if (!isset($array[$key]) || !is_array($array[$key])) {
                $array[$key] = [];
            }
            $this->addRecursive($array[$key], $keys, $value);
        } else {
            if (!isset($array[$key])) {
                $array[$key] = $value;
            } elseif (is_array($array[$key])) {
                $array[$key][] = $value;
            } else {
                $array[$key] = [$array[$key], $value];
            }
        }
    }
}
