<?php

namespace Database\Seeders\StaticPageContentsSeeders;

use App\Models\Article;
use App\Models\ArticleCategory;

class HomePage extends _StaticPagesSeeder
{
    public function call(): array
    {
        $this->createData();
        return $this->data;
    }

    protected function createData(): void
    {
        $this->createHeading();

        $this->createMediaTiles();

        $this->createTopArticles();

        $this->createTopCategories();

        $this->createSubscription();

        $this->createSocialSection();
    }

    private function createHeading(): void
    {
        $this->add('heading.title', 'MyBlogs Homepage');
        $this->add('heading.description', fake()->text(300));
    }

    private function createMediaTiles(): void
    {
        $articles = Article::published()->get();
        if (!$articles->isEmpty()) {
            $count = min($articles->count(), 4);
            $articles->random($count)->each(
                fn($object) => $this->add('media_tile.article_id', $object->id)
            );
        }
    }

    private function createTopArticles(): void
    {
        $this->add('top_articles.header.title', 'Top Articles');

        $tabs = [];
        $articles = Article::published()->get();

        if (!$articles->isEmpty()) {
            $count = min($articles->count(), 5);

            $articles->random($count)->each(
                function ($object) use (&$tabs) {
                    $tabs[0][] = $object->id;
                }
            );
            $articles->random($count)->each(
                function ($object) use (&$tabs) {
                    $tabs[1] [] = $object->id;
                }
            );

            $this->add('top_articles.tabs', [
                [
                    'title'    => 'Tab 1',
                    'articles' => $tabs[0],
                ],
                [
                    'title'    => 'Tab 2',
                    'articles' => $tabs[1],
                ]
            ]);
        }
    }

    private function createTopCategories(): void
    {
        $this->add('top_categories.header.title', 'Top Categories');
        $categories = ArticleCategory::published()->get();
        if (!$categories->isEmpty()) {
            $count = min($categories->count(), 6);
            $categories->random($count)->each(
                fn($object) => $this->add('top_categories.categories', $object->id, true)
            );
        }
    }

    private function createSubscription(): void
    {
        $this->add('subscribe.title', 'Subscribe Here!');
        $this->add('subscribe.input.place_holder', 'Enter your email address');
        $this->add('subscribe.button.caption', 'Subscribe Me');
        $this->add('subscribe.description', '*you will receive the latest news and updates on your favorite celebrities');
        $this->add('subscribe.success_message', 'Thanks for your subscription');
    }

    private function createSocialSection(): void
    {
        $this->add('socials.title', 'Find Me in Socials');

        $this->add('socials.medias',
            [
                [
                    'type'      => 'instagram',
                    'followers' => '335',
                    'cta'       => 'Follow Me',
                    'url'       => 'https://instagram.com'
                ], [
                    'type'      => 'twitter',
                    'followers' => '231K',
                    'cta'       => 'Twit Me',
                    'url'       => 'https://x.com'
                ], [
                    'type'      => 'linkedin',
                    'followers' => '5.1K',
                    'cta'       => 'Link with Me',
                    'url'       => 'https://linkedin.com'
                ], [
                    'type'      => 'email',
                    'followers' => '',
                    'cta'       => 'Email to Me',
                    'url'       => 'mailto:info@myblogs.com'
                ]
            ]);
    }


}

