<?php

namespace Database\Seeders;

use Database\Seeders\FactoringSeeders\ArticleCategorySeeder;
use Database\Seeders\FactoringSeeders\ArticleRelationSeeder;
use Database\Seeders\FactoringSeeders\ArticleSeeder;
use Database\Seeders\FactoringSeeders\ColorSeeder;
use Database\Seeders\FactoringSeeders\IconSeeder;
use Database\Seeders\FactoringSeeders\SubscriptionSeeder;
use Database\Seeders\FactoringSeeders\UserSeeder;
use Illuminate\Database\Seeder;

class FakeDataSeeder extends Seeder
{
    public function run(): void
    {
        $product_mode = config('app.env') === 'local' ? 'local' : 'prod';

        $this->call(ColorSeeder::class, false, ['count' => config('fake-records.color.' . $product_mode)]);
        $this->call(IconSeeder::class, false, ['count' => config('fake-records.icon.' . $product_mode)]);

        $this->call(UserSeeder::class, false, ['count' => config('fake-records.user.' . $product_mode)]);
        $this->call(ArticleCategorySeeder::class, false, ['count' => config('fake-records.article_category.' . $product_mode)]);
        $this->call(ArticleSeeder::class, false, ['count' => config('fake-records.article.' . $product_mode)]);

        $this->call(StaticLinkSeeder::class, false, ['count' => config('fake-records.static_link.' . $product_mode)]);
        $this->call(SubscriptionSeeder::class, false, ['count' => config('fake-records.subscription.' . $product_mode)]);

        $this->call(ArticleRelationSeeder::class, false, ['production_mode' => $product_mode]);
    }
}
