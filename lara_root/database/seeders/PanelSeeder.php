<?php

namespace Database\Seeders;

use App\Models\Panel;
use Illuminate\Database\Seeder;

class PanelSeeder extends Seeder
{
    public function run()
    {
        $panels = [
            'admin'     => true,
            'author'    => true,
            'seo'       => true,
            'reader'    => true,
            'interface' => false,
        ];

        foreach ($panels as $panel => $active) {
            Panel::create([
                'title'  => $panel,
                'active' => $active,
            ]);
        }
    }
}
