<?php

namespace Database\Seeders;

use App\Models\MenuPlaceholder;
use App\Models\Panel;
use Illuminate\Database\Seeder;

class MenuPlaceholderSeeder extends Seeder
{
    public function run(): void
    {
        $items = [
            'interface' => [
                'sidebar',
                'footer'
            ],
            'admin'     => [
                'sidebar',
            ],
        ];

        foreach ($items as $panelTitle => $slugs) {
            foreach ($slugs as $slug) {
                try {
                    $panel = Panel::where('title', $panelTitle)->first();
                    MenuPlaceholder::create([
                        'panel_id' => $panel->id,
                        'slug'     => $slug,
                    ]);
                }
                catch (\Exception $e){
                    $this->command->info('Panel Title: ' . $panelTitle . '| Err: ' . $e->getMessage());
                }
            }
        }
    }
}
