<?php

namespace Database\Seeders;

use App\Models\AttachmentType;
use App\Models\ModelAttachmentType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AttachmentTypeSeeder extends Seeder
{
    protected $images = [
//        'Logo'   => [
//            [
//              'width'=>[',50'],
//              'height=>[',50'],
//            ],
//            '50K',
//            "SiteBrand",
//        ],

        'User Avatar' => [
            [
                'box' => ['40,44', '30,32'],
            ],
            '30K',
            "User",
        ],

        'Article Cover' => [
            [
                'box' => ['1280,560', '1024,', ',320',]
            ],
            '1M',
            "ArticleContent",
        ],

        'Article Content' => [
            [
                'box' => [
                    '690,320',
                    '500,',
                    '340,',
                    '250,',
                ],
            ],
            '720K',
            "ArticleContent",
        ],

        'Article Content Meta' => [
            [
                'box' => ['1280,560', '1024,', ',320',]
            ],
            '30K',
            "ArticleContent"
        ],

        'Article Category Cover' => [
            [
                'box' => [
                    '1280,560',
                    '1024,',
                    ',320',
                ],
            ],
            '1M',
            "ArticleCategoryContent",
        ],

        'Article Category Meta' => [
            [
                'box' => ['1280,560', '1024,', ',320',]
            ],
            '30K',
            "ArticleCategoryContent"
        ],

        'Static Page Content' => [
            [
                'box' => [
                    '690,320',
                    '500,',
                    '340,',
                    '250,',
                ],
            ],
            '720K',
            "StaticPageContent",
        ],

        'Static Page Cover' => [
            [
                'box' => [
                    '1280,560',
                    '1024,',
                    ',320',
                ],
            ],
            '1M',
            "StaticPageContent",
        ],

        'Static Page Meta' => [
            [
                'box' => ['1280,560', '1024,', ',320',]
            ],
            '30K',
            "StaticPageContent"
        ],

    ];
    protected $videos = [
        'Article Content' => [
            [
                'box' => ['1920,940'],
            ],
            '2M',
            "ArticleContent"
        ],
        'Static Page Content' => [
            [
                'box' => ['1920,940'],
            ],
            '2M',
            "StaticPageContent"
        ],
    ];

    public function run(): void
    {
        $this->extractConfig($this->images, 'image');
        $this->extractConfig($this->videos, 'video');
    }

    protected function extractConfig(array $config, string $media): void
    {
        foreach ($config as $title => $props) {
            $title .= ' ' . Str::ucfirst($media);
            $att = AttachmentType::create([
                'title'            => $title,
                'slug'             => Str::slug($title),
                'properties_limit' => $props[0],
                'size'             => $props[1],
            ]);
            $this->createRecord($props[2], $att);
        }
    }

    protected function createRecord($props, $att): void
    {
        $models = explode('|', $props);
        foreach ($models ?? [] as $model)
            ModelAttachmentType::create([
                'attachment_type_id' => $att->id,
                'model_name'         => $model,
            ]);
    }
}
