<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    public function run(): void
    {
        $items = [
            ['Euro', '€', 'EUR',],
            ['Dollar', '$', 'USD',],
        ];

        foreach ($items as $item) {
            Currency::create([
                'name'     => $item[0],
                'symbol'   => $item[1],
                'iso_code' => $item[2],
            ]);
        }
    }
}
