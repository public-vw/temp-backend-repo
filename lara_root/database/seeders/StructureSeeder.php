<?php

namespace Database\Seeders;

use App\Models\Structure;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class StructureSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('structures')->truncate();

        $models = $this->getModels(app_path('Models'));

        $structures = [];
        foreach ($models as $model) {
            $structures[] = Structure::create([
                'parent_id' => null,
                'type'      => 'model',
                'title'     => $model,
            ]);
        }
        foreach($structures as $structure){
            $model = $structure->title;
            $modelClass = '\\App\\Models\\'.$model;
            $fields = Schema::getColumnListing((new $modelClass)->getTable());
            foreach($fields as $field){
                Structure::create([
                    'parent_id' => $structure->id,
                    'type'      => 'field',
                    'title'     => $field,
                ]);
            }
        }
    }

    protected function getModels($path = null, $exceptions = []): array
    {
        if (is_null($path)) $path = app_path('Models');

        $out = [];
        $results = scandir($path);

        foreach ($results as $result) {
            if (!str_ends_with($result, '.php')) continue;
            $tmp = basename($result, '.php');
            if (in_array($tmp, $exceptions)) continue;
            $out[] = $tmp;
        }
        return $out;
    }
}
