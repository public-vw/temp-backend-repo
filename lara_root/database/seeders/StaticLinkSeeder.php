<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\Panel;
use App\Models\StaticLink;
use Illuminate\Database\Seeder;

class StaticLinkSeeder extends Seeder
{
    public function run(): void
    {
        $items = [
//            'Categories',//TODO: this is for development only. remove on production
        ];

        foreach ($items as $item) {
            $link = StaticLink::factory()->create([
                'title'    => $item,
            ]);

            Menu::factory()->create([
                'placeholder_id' => Panel::where('title','interface')->first()->menuPlaceholders()->where('slug','sidebar')->first()->id,
                'parent_id' => null,
                'title' => $item,

                'heading' => false,
                'nofollow' => true,
                'active' => true,

                'menuable_type' => get_class($link),
                'menuable_id' => $link->id,
            ], $link);
        }

        $this->stepLevelSample(10);
    }

    public function stepLevelSample($count = 1): void
    {
        $created_menus = [];

        for($i = 0; $i < $count; $i++){
            $link = StaticLink::factory()->create();
            $menu = Menu::factory()->create([
                'placeholder_id' => Panel::where('title','interface')->first()->menuPlaceholders()->where('slug','sidebar')->first()->id,
                'parent_id' => $i == 0? null: fake()->randomElement($created_menus),

                'heading' => $i == 0,
                'nofollow' => true,
                'active' => true,

                'menuable_type' => get_class($link),
                'menuable_id' => $link->id,
            ], $link);
            $created_menus[] = $menu->id;
        }
    }

}
