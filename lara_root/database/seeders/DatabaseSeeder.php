<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call(StructureSeeder::class);

        $this->call(CurrencySeeder::class);
        $this->call(PanelSeeder::class);
        $this->call(RolePermissionSeeder::class);

        $this->call(AttachmentTypeSeeder::class);
        $this->call(AdminsSeeder::class);

        $this->call(SettingsSeeder::class);

        $this->call(MenuPlaceholderSeeder::class);
        $this->call(MenuSeeder::class);

        $this->call(StaticPageSeeder::class);

        if ((bool)config('app.test_users', false) === true) {
            $this->call(TestUsersSeeder::class);
        }

        if ((bool)config('app.fake_data', false) === true) {
            $this->call(FakeDataSeeder::class);
        }

        $this->call(StaticPageContentSeeder::class);
    }
}
