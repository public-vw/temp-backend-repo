<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminsSeeder extends Seeder
{
    public function run(): void
    {
        $this->createSysadmin();
    }

    protected function createSysadmin(): void
    {
        $u = User::create([
            'firstname' => 'Armin',
            'lastname'  => 'Ghasemi',

            'username'           => 'sysadmin',
            'ref_code'           => 'armin',
            'mobile'             => '+31645396996',
            'email'              => 'armin.ghassemi@gmail.com',
            'password'           => Hash::make('12345678'),
            'mobile_verified_at' => \Carbon\Carbon::now(),
            'email_verified_at'  => \Carbon\Carbon::now(),

            'random_color' => (string)rand(0, count(config('enums.random_colors')) - 1),
            'telegram_uid' => '115724262',

            'use_two_factor_auth' => false,
            'google_secret_key'   => 'OX7K2OYA3ROGPTHW',
            'status'              => config_key('enums.users.status', 'active'),
        ]);

        setRoles($u, ['sysadmin', 'admin', 'author', 'seo', 'reader', 'staff']);
    }

}
