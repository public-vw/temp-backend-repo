<?php

namespace Database\Seeders;

use App\Models\Setting;
use App\Models\SettingGroup;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SettingsSeeder extends Seeder
{

    protected array $settings = [];

    public function run(): void
    {
        SettingGroup::truncate();
        Setting::truncate();

        $this->makeSettings();
        $this->seedSettings($this->settings);
    }

    protected function makeSettings(): void
    {
        $this->settings = [
            'brand'  => [
                'titles'   => [
                    'short_title' => ['string', config('app.short_title')],
                    'long_title'  => ['string', config('app.title')],
                    'description' => ['string', config('app.description')],
                ],
                'users.0'  => [//.0 (meaning) is not for frontend
                               'pending_after_register' => ['bool', false, false],
                ],
                'manifest' => [
                    'start_url'        => ['string', '/'],
                    'display'          => ['string', 'standalone'],
                    'background_color' => ['string', '#fff'],
                    'theme_color'      => ['string', '#000'],
                    'icons'            => [
                        'image_list',
                        [
                            [
                                'src'   => "assets/icons/icon-48x48.png",
                                'sizes' => "48x48",
                                'type'  => "image/png",
                            ],
                            [
                                'src'   => "assets/icons/icon-72x72.png",
                                'sizes' => "72x72",
                                'type'  => "image/png",
                            ],
                            [
                                'src'   => "assets/icons/icon-96x96.png",
                                'sizes' => "96x96",
                                'type'  => "image/png",
                            ],
                            [
                                'src'   => "assets/icons/icon-128x128.png",
                                'sizes' => "128x128",
                                'type'  => "image/png",
                            ],
                            [
                                'src'   => "assets/icons/icon-144x144.png",
                                'sizes' => "144x144",
                                'type'  => "image/png",
                            ],
                            [
                                'src'   => "assets/icons/icon-152x152.png",
                                'sizes' => "152x152",
                                'type'  => "image/png",
                            ],
                            [
                                'src'   => "assets/icons/icon-192x192.png",
                                'sizes' => "192x192",
                                'type'  => "image/png",
                            ],
                            [
                                'src'   => "assets/icons/icon-384x384.png",
                                'sizes' => "384x384",
                                'type'  => "image/png",
                            ],
                            [
                                'src'   => "assets/icons/icon-512x512.png",
                                'sizes' => "512x512",
                                'type'  => "image/png",
                            ],
                            [
                                'src'     => "assets/icons/icon-512x512.png",
                                'sizes'   => "512x512",
                                'type'    => "image/png",
                                'purpose' => "maskable",
                            ],
                        ],
                    ],
                ],
                'site_url' => ['string', config('app.frontend_url')],
            ],
            'languages' => [
                'interface' => [
                    'visitor' => [
                        'lang' => ['string', 'nl'],
                        'lang_name' => ['string', 'Netherlands'],
                        'direction' => ['string', 'ltr'],
                    ],
                    'staff' => [
                        'lang' => ['string', 'en'],
                        'lang_name' => ['string', 'English'],
                        'direction' => ['string', 'ltr'],
                    ],
                ]
            ],
            'social' => [
                'links' => [
                    'link_list',
                    [
                        ['label' => 'instagram', 'url' => 'https://instagram.com'],
                        ['label' => 'linkedin', 'url' => 'https://linkedin.com'],
                        ['label' => 'youtube', 'url' => 'https://youtube.com'],
                        ['label' => 'facebook', 'url' => 'https://facebook.com'],
                        ['label' => 'twitter', 'url' => 'https://twitter.com'],
                    ]
                ],
            ],

            'contact.0' => [
                'crisp_chat' => [
                    'enable'     => ['bool', false],
                    'website_id' => ['string', null],
                ],
            ],

            'api.0' => [
                'namespace' => [
                    'auth_by_session'    => ['string', 'auth/session'],
                    'auth_by_token'      => ['string', 'auth/token'],
                    'sysadmin'           => ['string', "sys"],
                    'admin'              => ['string', "admin"],
                    'developer'          => ['string', "dev"],
                    'author'             => ['string', "author"],
                    'seo'                => ['string', "seo"],
                    'reader'             => ['string', "reader"],
                    'secure_interface'   => ['string', "sec"],
                    'public_interface'   => ['string', "face"],
                    'frontend_buildtime' => ['string', "front/build"],
                ],
            ],

            'ui' => [
                'themes'      => ['string_list', ['light', 'dark']],

                'themeColor' => [
                    'theme_color_list', [
                        ['media' => '(prefers-color-scheme: light)', 'color' => '#fff'],
                        ['media' => '(prefers-color-scheme: dark)', 'color' => '#000'],
                    ]
                ],
            ],

            'layout' => [
                'sidebar' => [
                    'sign' => [
                        'alt'    => ['string', 'Sign Alt'],
                        'slogan' => ['string', 'Sign Slogan']
                    ]
                ],
                'header'  => [
                    'logo' => [
                        'alt'    => ['string', 'Header Logo Alt'],
                        'height' => ['int', 25],
                    ],
                ],
                'footer'  => [
                    'slogan'    => [
                        'text'  => ['string', 'Lorem ipsum...'],
                        'image' => [
                            'alt' => ['string', 'Alt Text ...'],
                        ],
                    ],
                    'copyright' => [
                        'text'   => ['string', 'myBlogs.com'],
                        'madeBy' => ['string', "<p>Design & Maintenance by <span class='text-red-500'>&hearts;</span> in <b>myblogs.com</b></p>"],
                    ]
                ],
            ],

            'contents' => [
                'languages' => [
                    'language_list', [
                        ['label' => 'English', 'locale' => 'en', 'sign' => 'GB', 'url' => 'https://myblogs.com/en/'],
                        ['label' => 'Dutch', 'locale' => 'nl', 'sign' => 'NL', 'url' => 'https://myblogs.com/nl/'],
                    ]
                ],

                'share' => [
                    'share_on_socials' => ['string_list', ['twitter', 'facebook', 'linkedin', 'email']],
                ],

                'article_types'    => [
                    'image' => ['string', 'your-image-icon'],
                    'video' => ['string', 'your-video-icon'],
                    'audio' => ['string', 'your-audio-icon'],
                    'text'  => ['string', 'your-text-icon'],
                ],
                'status_variation' => [
                    'key_value_list', [
                        ['label' => "Seo Review", 'value' => "seo-review"],
                        ['label' => "Seo Ready", 'value' => "seo-ready"],
                        ['label' => "Checking", 'value' => "checking"],
                        ['label' => "Active", 'value' => "active"],
                        ['label' => "Ready", 'value' => "ready"],
                        ['label' => "Draft", 'value' => "draft"],
                    ],
                ],
                'cta'              => [
                    'postLG' => ['string', 'Read Now!'],
                ],
                'segments'         => [
                    'static_page' => ['string', '/info'],
                    'blog_folder' => ['string', '/blog'],
                ]
            ],

            'server.0' => [
                'cronjob' => ['int','0'],   // 0 = disable, 1 = checking, 10 = active
                'queue' => ['int','0'],     // 0 = disable, 1 = checking, 10 = active
            ],
            'publish_manager.0' => [
                'active' => ['bool',false],
                'new_post' => [
                    'date_time_list',[
                        'Tue 12:00',
                        'Wed 18:00',
                    ]
                ],
                'new_version' => [
                    'date_time_list',[
                        'Thr 01:00',
                        'Fri 13:30',
                        'Sat 10:15',
                    ]
                ],
            ]
        ];
    }

    protected function seedSettings(array $settings, $parentGroup = null): void
    {
        $types = config('enums.settings.types');
        foreach ($settings as $key => $value) {
            if (is_array($value) && isset($value[0]) && in_array($value[0], $types)) {
                $this->createSetting($key, $value, $parentGroup);
            } else {
                $newGroup = $this->createGroup($key, $parentGroup);
                $this->seedSettings($value, $newGroup);
            }
        }
    }

    protected function createSetting(string $slug, array $value, SettingGroup $parentGroup): void
    {
        Setting::create([
            'title'        => ucfirst(str_replace('_', ' ', $slug)),
            'group_id'     => $parentGroup->id,
            'slug'         => $slug,
            'type'         => $value[0],
            'value'        => (strpos($value[0],'_list') > 0) ? json_encode($value[1], true) : $value[1],
            'order'        => 100,
            'lang'         => 'en',
            'hidden'       => false,
            'for_frontend' => $value[2] ?? true
        ]);
    }

    protected function createGroup(string $slug, ?SettingGroup $parentGroup = null): SettingGroup
    {
        [$slug, $for_frontend] = array_merge(explode('.', $slug), [1]);

        return SettingGroup::create([
            'parent_id'    => optional($parentGroup)->id,
            'title'        => ucfirst(str_replace('_', ' ', $slug)),
            'slug'         => $slug,
            'order'        => 100,
            'hidden'       => false,
            'for_frontend' => !(isset($for_frontend)) || (bool)$for_frontend,
        ]);
    }
}
