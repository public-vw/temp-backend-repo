<?php

namespace Database\Seeders;

use App\Models\Panel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use \Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    protected $cruds = ['view', 'edit', 'create', 'delete'];
    protected $territories = ['all', 'branch', 'own'];

    protected $roles = [];
    protected $default_role_map = [
        'admin' => [
            'ArticleCategory' => 'aaaa',
            'Article'         => 'aaaa',
            'Comment'         => 'aaaa',
            'Log'             => 'a---',
            'Panel'           => 'a---',//not for sysadmin
            'Role'            => '----',
            'SeoDetail'       => 'aaaa',
            'Setting'         => '----',
            'User'            => 'aaaa',//not for sysadmins and other admins
        ],
        'author' => [
            'ArticleCategory' => 'a-o-',
            'Article'         => 'aoo-',
            'Comment'         => 'aooo',
            'Log'             => '----',
            'Panel'           => '----',
            'Role'            => '----',
            'SeoDetail'       => 'o---',
            'Setting'         => '----',
            'User'            => 'oo--',
        ],
    ];


    protected function truncateAll(): void
    {
        $tableNames = config('permission.table_names');
        if (empty($tableNames)) {
            throw new \Exception('Error: config/permission.php not loaded. Run [php artisan config:clear] and try again.');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        foreach ($tableNames as $key => $tableName) {
            if ($key == 'model_has_roles') continue;
            DB::table($tableName)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function run(): void
    {
        $this->truncateAll();

        $panels = Panel::where('active',true)->get();
        foreach($panels as $panel) {
            Role::create([
                'name'     => $panel->title,
                'panel_id' => $panel->id,
            ]);
        }

        Role::create(['name' => "sysadmin"]);
        Role::create(['name' => "staff"]);

//            $this->roles['author'][$guard] = Role::create(['guard_name' => $guard, 'name' => "author@{$guard}"]);
//            $this->roles['author'][$guard]->update(['panel_id' => Panel::where('title', 'author')->first()->id]);

//TODO: we need more realistic permissions
//        $this->addPermissions();

    }

    protected function addPermissions(): void
    {
        $models = getModels();
        foreach ($models as $model) {
            $table = model2table($model);

            $this->command->comment("table: {$table}");

            foreach ($this->cruds as $crud) {
                foreach ($this->territories as $territory) {
                    $perm = "{$table}.{$crud}.{$territory}";
                    Permission::create(['name' => $perm]);
                    $this->connectPermissionToRoles($perm);
                }
            }
        }
    }

    protected function connectPermissionToRoles($perm): void
    {
        list($table, $crud, $territory) = explode('.', $perm);
        $crud_index = array_search($crud, $this->cruds);
        $territory = $territory[0];

        foreach ($this->roles as $name => $role) {
            if ($name == 'sysadmin') {
                $role->givePermissionTo($perm);
                continue;
            }
            if (!isset($this->default_role_map[$name][table2model($table)])) continue;
            $signs = str_split($this->default_role_map[$name][table2model($table)]);
            if ($signs[$crud_index] == $territory) {
                $role->givePermissionTo($perm);
                $this->command->comment("Role [{$role->name}] granted Permission [{$perm}]");
            }
        }
    }
}
