<?php

namespace Database\Seeders;

use App\Models\BotConnection;
use Illuminate\Database\Seeder;

class BotConnectionSeeder extends Seeder
{
    public function run(): void
    {
        BotConnection::truncate();

        BotConnection::create([
            'title'         => 'Alert Sender Robot',
            'username'      => 'WebAlertRobot',
            'type'          => config_key('enums.bot_connections.platforms', 'telegram'),
            'robot_token'   => '2018720703:AAEqQurNbj063U0WhbbpQtw-RC2wkJryqqE',
            'parameters'    => null,
            'webhook_token' => null,
            'active'        => true,
        ]);

        BotConnection::create([
            'title'         => 'MyBlogs Assistant',
            'username'      => 'myblogs_robot',
            'type'          => config_key('enums.bot_connections.platforms', 'telegram'),
            'robot_token'   => '5515728704:AAG50gJU2e-4URuS3NvfrWc1bS0707sBxCM',
            'parameters'    => null,
            'webhook_token' => 'o7siGofhooC8ieh',
            'active'        => true,
        ]);
    }
}
