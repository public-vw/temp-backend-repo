<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Note;
use Illuminate\Database\Seeder;

class NoteSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Note::factory()->count($count)->create();
    }
}
