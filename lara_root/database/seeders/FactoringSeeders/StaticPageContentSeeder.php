<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\StaticPageContent;
use Illuminate\Database\Seeder;

class StaticPageContentSeeder extends Seeder
{
    public function run($count = 1): void
    {
        StaticPageContent::factory()->count($count)->create();
    }
}
