<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Color;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Color::factory()->count($count)->create();
    }
}
