<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\SeoRichSnippet;
use Illuminate\Database\Seeder;

class SeoRichSnippetSeeder extends Seeder
{
    public function run($count = 1): void
    {
        SeoRichSnippet::factory()->count($count)->create();
    }
}
