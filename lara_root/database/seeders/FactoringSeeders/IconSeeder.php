<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Icon;
use Illuminate\Database\Seeder;

class IconSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Icon::factory()->count($count)->create();
    }
}
