<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\StaticPage;
use Illuminate\Database\Seeder;

class StaticPageSeeder extends Seeder
{
    public function run($count = 1): void
    {
        StaticPage::factory()->count($count)->create();
    }
}
