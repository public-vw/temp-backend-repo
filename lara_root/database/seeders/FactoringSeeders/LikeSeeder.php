<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Like;
use Illuminate\Database\Seeder;

class LikeSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Like::factory()->count($count)->create();
    }
}
