<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Url;
use Illuminate\Database\Seeder;

class UrlSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Url::factory()->count($count)->create();
    }
}
