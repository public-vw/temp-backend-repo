<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Article;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Article::factory()->count($count)->create();
    }
}
