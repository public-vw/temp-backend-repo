<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run($count = 1): void
    {
        User::factory()->count($count)->create();
    }

}
