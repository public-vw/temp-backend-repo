<?php

namespace Database\Seeders\FactoringSeeders;

use \App\Models\Attachment;
use Illuminate\Database\Seeder;

class AttachmentSeeder extends Seeder
{
    public function run($count = 1)
    {
        Attachment::factory()->count($count)->create();
    }
}
