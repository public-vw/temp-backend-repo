<?php


namespace Database\Seeders\FactoringSeeders;

use App\Models\FakeRecord;
use Illuminate\Database\Seeder;

abstract class FactoringSeeder extends Seeder
{
    protected string $model;
    protected bool $cache_ids = false;

    public function run($count = 1, $faker_id = null, $dependency = null): void
    {
        if($this->cache_ids) {
            cache()->forget('faker_records_ids');
        }

        $faker_ids = [];

        for ($i = 0; $i < $count; $i++) {
            $item = (new $this->model)->factory()->create($dependency ?? null);

            $faker_ids[] = $item->id;
            $fakeRecord = $this->createFakeTableRecord($item, $faker_id);

            if(method_exists(static::class,'postRun')){
                $this->postRun($fakeRecord);
            }
        }

        if($this->cache_ids) {
            cache()->put('faker_records_ids', [
                'class' => static::class,
                'ids'   => $faker_ids,
            ]);
        }
    }

    protected function createFakeTableRecord(mixed $item, mixed $faker_id)
    {
        $fakeRecord = FakeRecord::create([
            'model'     => $this->model,
            'record_id' => $item->id,
            'parent_id' => $faker_id,
        ]);
        return $fakeRecord;
    }

}
