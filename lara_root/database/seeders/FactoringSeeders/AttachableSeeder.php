<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Attachable;
use Illuminate\Database\Seeder;

class AttachableSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Attachable::factory()->count($count)->create();
    }
}
