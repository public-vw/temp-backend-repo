<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Subscription::factory()->count($count)->create();
    }
}
