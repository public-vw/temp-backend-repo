<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleRelation;
use App\Models\StaticPage;
use Illuminate\Database\Seeder;

class ArticleRelationSeeder extends Seeder
{
    protected string $production_mode = 'local';

    public function run($production_mode = 'local'): void
    {
        $this->production_mode = $production_mode;

        ArticleRelation::truncate();

        $this->makeArticleRelations();
        $this->makeCategoryRelations();
        $this->makeStaticPageRelations();
    }

    private function makeArticleRelations(): void
    {
        $objects = Article::published()->get();

        foreach ($objects as $object) {
            foreach (config('enums.article_relations.types') as $relation_type) {
                $this->createFakeRelation($relation_type, $object);
            }
        }
    }

    protected function createFakeRelation($relation_type, $object): void
    {
        $count = config("fake-records.article-relation.{$this->production_mode}.{$relation_type}");

        ArticleRelation::factory($count)
            ->withRelatable($object)
            ->withRelation($relation_type)
            ->create();
    }

    private function makeCategoryRelations(): void
    {
        $objects = ArticleCategory::published()->get();

        foreach ($objects as $object) {
            $this->createFakeRelation('related', $object);
        }
    }

    private function makeStaticPageRelations(): void
    {
        $objects = StaticPage::published()->get();

        foreach ($objects as $object) {
            $this->createFakeRelation('related', $object);
        }
    }
}
