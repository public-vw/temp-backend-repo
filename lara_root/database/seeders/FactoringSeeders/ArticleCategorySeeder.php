<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\ArticleCategory;
use Illuminate\Database\Seeder;

class ArticleCategorySeeder extends Seeder
{
    public function run($count = 1): void
    {
        ArticleCategory::factory()->count($count)->create();
    }
}
