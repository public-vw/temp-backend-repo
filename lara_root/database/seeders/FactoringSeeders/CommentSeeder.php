<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Comment;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Comment::factory()->count($count)->create();
    }
}
