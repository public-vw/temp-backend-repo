<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\Bookmark;
use Illuminate\Database\Seeder;

class BookmarkSeeder extends Seeder
{
    public function run($count = 1): void
    {
        Bookmark::factory()->count($count)->create();
    }
}
