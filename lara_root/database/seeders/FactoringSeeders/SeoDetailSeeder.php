<?php

namespace Database\Seeders\FactoringSeeders;

use \App\Models\SeoDetail;
use Illuminate\Database\Seeder;

class SeoDetailSeeder extends Seeder
{
    public function run($count = 1): void
    {
        SeoDetail::factory()->count($count)->create();
    }
}
