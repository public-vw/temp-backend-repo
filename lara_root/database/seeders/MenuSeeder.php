<?php

namespace Database\Seeders;

use App\Models\MenuPlaceholder;
use App\Models\Panel;
use App\Models\StaticPage;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    const HEADING = true;

    public function run(): void
    {
        $items = [
            'About Us'   => [
                'menuable' => StaticPage::find(2),
            ],
            'Contact Us' => [
                'menuable' => StaticPage::find(3),
            ],
        ];

        $this->creator('interface', 'sidebar', $items);

        //----------------------------------------

        $items = [
            'Main Pages' => [
                'is_heading' => self::HEADING,
                'children'   => [
                    'About Us'   => [
                        'menuable' => StaticPage::find(2),
                    ],
                    'Contact Us' => [
                        'menuable' => StaticPage::find(3),
                    ],
                ],
            ],
            'Technical'  => [
                'is_heading' => self::HEADING,
                'children'   => [
                    'Terms'   => [
                        'menuable' => StaticPage::find(4),
                    ],
                    'Privacy' => [
                        'menuable' => StaticPage::find(5),
                    ],
                    'FAQ'     => [
                        'menuable' => StaticPage::find(6),
                    ],
                ],
            ],
        ];

        $this->creator('interface', 'footer', $items);
    }

    //---------------------------------------------------------------------

    protected function creator($panel, $placeholder, $items): void
    {
        $panel = Panel::where('title', $panel)->first();
        $placeholder = $panel->menuPlaceholders()->where(['slug' => $placeholder])->first();
        $this->loopCreation($placeholder, $items);
    }

    protected function loopCreation(MenuPlaceholder $menuPlaceholder, array $items, int $parent_id = null): void
    {
        foreach ($items as $title => $details) {
            if (in_array($title, ['children', 'menuable'])) continue;
            $menu = $menuPlaceholder->menus()->create([
                'parent_id' => $parent_id,
                'heading'   => $details['is_heading'] ?? false,
                'title'     => $title,
                'active'    => true,
            ]);

            if (!$menu->heading) {
                $menu->menuable()->associate($details['menuable'])->save();
            }

            if (isset($details['children']) && count($details['children']) > 0) {
                $this->loopCreation($menuPlaceholder, $details['children'], $menu->id);
            }
        }
    }
}
