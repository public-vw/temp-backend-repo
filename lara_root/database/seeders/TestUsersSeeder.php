<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TestUsersSeeder extends Seeder
{
    public function run(): void
    {
        $this->createTestAdmin();
        $this->createTestAuthor();
        $this->createTestSeo();
        $this->createTestReader();
    }

    protected function createTestAdmin(): void
    {
        $u = User::factory()->create([
            'username'          => 'admin',
            'email'             => 'admin@example.com',
            'password'          => Hash::make('12345678'),
            'email_verified_at' => \Carbon\Carbon::now(),
            'status'            => config_key('enums.users.status', 'active'),
        ]);

        setRoles($u, ['admin', 'staff']);
    }

    protected function createTestAuthor(): void
    {
        $u = User::factory()->create([
            'username'          => 'author',
            'email'             => 'author@example.com',
            'password'          => Hash::make('12345678'),
            'email_verified_at' => \Carbon\Carbon::now(),
            'status'            => config_key('enums.users.status', 'active'),
        ]);

        setRoles($u, ['author', 'staff']);
    }

    protected function createTestSeo(): void
    {
        $u = User::factory()->create([
            'username'          => 'seo',
            'email'             => 'seo@example.com',
            'password'          => Hash::make('12345678'),
            'email_verified_at' => \Carbon\Carbon::now(),
            'status'            => config_key('enums.users.status', 'active'),
        ]);

        setRoles($u, ['seo', 'staff']);
    }

    protected function createTestReader(): void
    {
        $u = User::factory()->create([
            'username'          => 'reader',
            'email'             => 'reader@example.com',
            'password'          => Hash::make('12345678'),
            'email_verified_at' => \Carbon\Carbon::now(),
            'status'            => config_key('enums.users.status', 'active'),
        ]);

        setRoles($u, ['reader', 'staff']);
    }

}
