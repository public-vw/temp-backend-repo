<?php

namespace Database\Seeders;

use App\Models\StaticPageContent;
use Database\Seeders\StaticPageContentsSeeders\HomePage;
use Illuminate\Database\Seeder;

class StaticPageContentSeeder extends Seeder
{
    protected array $items = [
        [
            'static_page_id' => 1,
            'heading'        => 'Home Page',
            'seeder_class'   => HomePage::class,
        ],
        [
            'static_page_id' => 2,
            'heading'        => 'About Me',
        ],
        [
            'static_page_id' => 3,
            'heading'        => 'Contact Me',
        ],
        [
            'static_page_id' => 4,
            'heading'        => 'Terms & Conditions',
        ],
        [
            'static_page_id' => 5,
            'heading'        => 'Privacy Policy',
        ],
        [
            'static_page_id' => 6,
            'heading'        => 'FAQ',
        ],
        [
            'static_page_id' => 7,
            'heading'        => 'Error 401',
        ],
        [
            'static_page_id' => 8,
            'heading'        => 'Error 403',
        ],
        [
            'static_page_id' => 9,
            'heading'        => 'Error 404',
        ],
        [
            'static_page_id' => 10,
            'heading'        => 'Error 500',
        ],
    ];


    public function run(): void
    {
        StaticPageContent::truncate();

        foreach ($this->items as $item) {
            $content = StaticPageContent::create([
                'static_page_id' => $item['static_page_id'],
                'version'        => 1,
                'heading'        => $item['heading'],
                'data'           => isset($item['seeder_class']) ? $this->callDataSeeder($item['seeder_class']) : null,
                'status'         => config_key('enums.contents.status', 'active'),
            ]);
//TODO: add seoDetails seeder
//            $content->seoDetails()->create([
//                'type',
//                'data',
//            ]);
        }
    }

    private function callDataSeeder(mixed $seeder_class): array
    {
        return (new $seeder_class)->call();

    }
}
