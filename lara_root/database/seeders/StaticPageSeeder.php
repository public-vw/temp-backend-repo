<?php

namespace Database\Seeders;

use App\Models\StaticPage;
use App\Models\Url;
use Illuminate\Database\Seeder;

class StaticPageSeeder extends Seeder
{
    public function run(): void
    {
        $items = [
            ['title' => 'home', 'uri' => null],
            ['title' => 'about', 'uri' => 'about-us'],
            ['title' => 'contact', 'uri' => 'contact-us'],
            ['title' => 'terms', 'uri' => 'terms-and-conditions'],
            ['title' => 'privacy', 'uri' => 'privacy'],
            ['title' => 'faq', 'uri' => 'faq'],
            ['title' => 'e401', 'uri' => '401'],
            ['title' => 'e403', 'uri' => '403'],
            ['title' => 'e404', 'uri' => '404'],
            ['title' => 'e500', 'uri' => '500'],
        ];

        foreach ($items as $item) {
            $page = StaticPage::create([
                'title'             => $item['title'],
            ]);

            $page->contents()->create([
                'uri'               => $item['uri'],
            ]);

            $url = $page->addUrl();

            # home is the only static page that has no url
            if($item['title'] === 'home'){
                $url->update(['url'=>'/']);
            }
        }
    }
}
