<?php

namespace Database\Factories;

use App\Models\_Model_;
use Database\Factories\Traits\FakeRecordable;
use Illuminate\Database\Eloquent\Factories\Factory;

class _Model_Factory extends Factory
{
    use FakeRecordable;

    public function definition(): array
    {
        return [
            'title' => fake()->title,
        ];
    }
}
