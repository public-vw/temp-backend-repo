<?php

namespace Database\Seeders;

use App\Models\_Model_;
use Illuminate\Database\Seeder;

class _Model_Seeder extends Seeder
{
    public function run(): void
    {
        $items = [
        ];

        foreach ($items as $item) {
            _Model_::create([
                'title'    => $item,
            ]);
        }
    }
}
