<?php

namespace App\Transformers\_Panel_\_Model_;

use App\Models\_Model_;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        //
    ];

    protected array $availableIncludes = [
        //
    ];

    public function transform(_Model_ $_model_): array
    {
        return [
            'id'    => $_model_->id,
            'title' => $_model_->title,
        ];
    }
}
