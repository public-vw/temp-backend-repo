<?php
## Replace these to make controller operable
# _Panel_    -> Admin, Sysadmin, ... (role name, capital case)
# _Model_ -> Agency, PropertyGroup, ... (model name)
# _model_ -> agency, property_group, ... (model name, snake case)
#

namespace App\Requests\_Panel_;

use App\Models\_Model_;
use App\Requests\_CustomWebRequest;

class _Model_Request extends _CustomWebRequest
{
    protected function prepareProperties(): void
    {
        $this->model = _Model_::class;
        $this->mainEntity = $this->_model_;
    }
}
