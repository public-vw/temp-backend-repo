<?php

namespace App\Requests\_Panel_\API\V_Version_\_Model_;

use App\Requests\_CustomApiRequest;

class CreateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'field1' => 'required|string',
        ];
    }
}
