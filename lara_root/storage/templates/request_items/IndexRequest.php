<?php

namespace App\Requests\_Panel_\API\V_Version_\_Model_;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'parent_id' => 'required|exists:parents,id',
        ];
    }
}
