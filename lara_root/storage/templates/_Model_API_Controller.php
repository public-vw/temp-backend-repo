<?php
## Replace these to make controller operable
# _Panel_    -> Admin, Sysadmin, ... (role name, capital case)
# _panel_    -> admin, sysadmin, ... (role name)
# _Model_ -> Agency, PropertyGroup, ... (model name)
# _model_ -> agency, property_group, ... (model name, snake case)
# _Version_  -> 1, 1.2, ... (version number)
#

namespace App\Http\Controllers\_Panel_;

use App\Http\Controllers\ApiController;
use App\Transformers\_Panel_\_Model_\IndexTransformer;
use App\Transformers\_Panel_\_Model_\ShowTransformer;
use App\Models\_Model_;
use App\Requests\_Panel_\_Model_\IndexRequest;
use App\Requests\_Panel_\_Model_\CreateRequest;
use App\Requests\_Panel_\_Model_\UpdateRequest;

class _Model_Controller extends ApiController
{
    protected string $model = _Model_::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $query = new $this->model;
        $query = $query->newQuery();

        return $this->responseByCursorFractal($query, new IndexTransformer());
    }

    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', $this->model);

        $query = _Model_::create($request->validated());

        return self::responseJson(['id' => $query->id]);
    }

    public function show(_Model_ $_model_): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('view', $_model_);

        return $this->responseByOneFractal($_model_, new ShowTransformer());
    }

    public function update(UpdateRequest $request, _Model_ $_model_): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $_model_);

        $_model_->update($request->validated());

        return self::success(__('_panel_._model_.update.success'));
    }

    public function destroy(_Model_ $_model_): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $_model_);

        $_model_->delete();

        return self::success(__('_panel_._model_.delete.success'));
    }
}
