<?php
## Replace these to make policy operable
# _Model_ -> Agency, PropertyGroup, ... (model name)
# _model_ -> agency, property_group, ... (model name, snake case)
#

namespace App\Policies;

use App\Models\_Model_;
use App\Models\User;

class _Model_Policy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, _Model_ $_model_): bool
    {
        if(!$user->hasState('active')) return false;

        return true;
    }

    public function create(User $user): bool
    {
        if(!$user->hasState('active')) return false;

        return true;
    }

    public function update(User $user, _Model_ $_model_): bool
    {
        if(!$user->hasState('active')) return false;

        return true;
    }

    public function delete(User $user, _Model_ $_model_): bool
    {
        if(!$user->hasState('active')) return false;

        return true;
    }

    public function restore(User $user, _Model_ $_model_): bool
    {
        if(!$user->hasState('active')) return false;

        return true;
    }

    public function forceDelete(User $user, _Model_ $_model_): bool
    {
        if(!$user->hasState('active')) return false;

        return true;
    }
}
