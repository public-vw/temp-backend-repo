<?php
## Replace these to make controller operable
# _Panel_    -> Admin, Sysadmin, ... (role name, capital case)
# _panel_    -> admin, sysadmin, ... (role name)
# _Model_ -> Agency, PropertyGroup, ... (model name)
# _model_ -> agency, property_group, ... (model name, snake case)
#

namespace App\Http\Controllers\_Panel_;

use Illuminate\Http\Request;
use App\DataTables\_Panel_\_Model_DataTable;
use App\Requests\_Panel_\_Model_Request;
use App\Models\_Model_;

class _Model_Controller extends _Controller
{
    # Uses database listing
    # use DbList;

    protected $model = _Model_::class;

    public function index(_Model_DataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(_Model_Request $request)
    {
        $territory = $this->checkPermission();

        $item = _Model_::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('_panel_/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(_Model_ $_model_)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->with_Model_($_model_);
    }

    public function edit(Request $request, _Model_ $_model_)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->with_Model_($_model_);
    }

    public function update(_Model_Request $request, _Model_ $_model_)
    {
        $territory = $this->checkPermission();

        $_model_->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $_model_)
            ->withResult(self::success(
                __('_panel_/controllers.update.success', ['model' => $this->modelName, 'title' => $_model_->id])
            ));
    }

    public function destroy(_Model_ $_model_)
    {
        $territory = $this->checkPermission();

        $_model_->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('_panel_/controllers.destroy.success', ['model' => $this->modelName, 'title' => $_model_->id])
            ));
    }
}
