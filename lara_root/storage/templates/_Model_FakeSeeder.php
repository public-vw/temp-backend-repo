<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\_Model_;
use Illuminate\Database\Seeder;

class _Model_Seeder extends Seeder
{
    public function run($count = 1): void
    {
        _Model_::factory()->count($count)->create();
    }
}
