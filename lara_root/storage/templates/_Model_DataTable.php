<?php
## Replace these to make controller operable
# _Panel_    -> Admin, Sysadmin, ... (role name, capital case)
# _Model_ -> Agency, PropertyGroup, ... (model name)
#

namespace App\DataTables\_Panel_;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\_Panel_\Traits\RoleJoint;
use App\Models\_Model_;
use Illuminate\Http\Request;


class _Model_DataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = _Model_::class;

    public function query(_Model_ $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
