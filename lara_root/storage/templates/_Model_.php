<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class _Model_ extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $validation = [
        '*'             => 'bail|string',
        'title'         => 'required',
    ];

    protected $fillable = [
        'title',
    ];

    # -----------------------------------

}
