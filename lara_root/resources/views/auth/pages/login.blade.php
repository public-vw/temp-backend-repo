<x-auth.guest-layout>
    <!-- Session Status -->
    <x-auth.auth-session-status class="mb-4" :status="session('status')" />

    <form method="POST" action="{{ route('auth.login.post') }}">
        @csrf

        <div>
            <x-auth.input name="email" autocomplete="username" required autofocus >{{ __('Email') }}</x-auth.input>
        </div>

        <div class="mt-4">
            <x-auth.input name="password" type="password" autocomplete="password" required >{{ __('Password') }}</x-auth.input>
        </div>

        <!-- Remember Me -->
        <div class="block mt-4">
            <label for="remember_me" class="inline-flex items-center">
                <input id="remember_me" type="checkbox" class="rounded dark:bg-gray-900 border-gray-300 dark:border-gray-700 text-indigo-600 shadow-sm focus:ring-indigo-500 dark:focus:ring-indigo-600 dark:focus:ring-offset-gray-800" name="remember">
                <span class="ml-2 text-sm text-gray-600 dark:text-gray-400">{{ __('Remember me') }}</span>
            </label>
        </div>

        <div class="flex items-center justify-end mt-4">
            @if (Route::has('auth.password.request'))
                <a class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800" href="{{ route('auth.password.request') }}">
                    {{ __('Forgot your password?') }}
                </a>
            @endif

            <x-auth.primary-button class="ml-3">
                {{ __('Log in') }}
            </x-auth.primary-button>
        </div>
    </form>
</x-auth.guest-layout>
