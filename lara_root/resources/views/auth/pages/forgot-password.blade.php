<x-auth.guest-layout>
    <div class="mb-4 text-sm text-gray-600 dark:text-gray-400">
        {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
    </div>

    <!-- Session Status -->
    <x-auth.auth-session-status class="mb-4" :status="session('status')" />

    <form method="POST" action="{{ route('auth.password.email') }}">
        @csrf

        <div>
            <x-auth.input name="email" autocomplete="username" required autofocus >{{ __('Email') }}</x-auth.input>
        </div>

        <div class="flex items-center justify-end mt-4">
            <x-auth.primary-button>{{ __('Email Password Reset Link') }}</x-auth.primary-button>
        </div>
    </form>
</x-auth.guest-layout>
