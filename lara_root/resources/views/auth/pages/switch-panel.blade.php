<x-auth.guest-layout>
    <div class="mb-4 text-sm text-gray-600 dark:text-gray-400">
        {{ __('You have multiple panels.') }}
    </div>
    <div class="mb-4 text-sm text-gray-600 dark:text-gray-400">
        {{ __('Choose the one you want to enter now.') }}
    </div>

    <div class="mt-4 flex items-center justify-between">
        <div>
            @role('sysadmin')
            <x-auth.responsive-nav-link :href="route('sysadmin.home')" >
                {{ __('System Administration Panel') }}
            </x-auth.responsive-nav-link>
            @endrole

            @foreach($panels as $panel)
            <x-auth.responsive-nav-link :href="route($panel->title .'.home')" >
                {{ __($panel->title . ' Panel') }}
            </x-auth.responsive-nav-link>
            @endforeach
        </div>

    </div>
</x-auth.guest-layout>
