<x-auth.guest-layout>
    <div class="mb-4 text-sm text-gray-600 dark:text-gray-400">
        {{ __('This is a secure area of the application. Please confirm your password before continuing.') }}
    </div>

    <form method="POST" action="{{ route('auth.password.confirm') }}">
        @csrf

        <div>
            <x-auth.input name="password" type="password" autocomplete="password" required autofocus >{{ __('Password') }}</x-auth.input>
        </div>

        <div class="flex justify-end mt-4">
            <x-auth.primary-button>{{ __('Confirm') }}</x-auth.primary-button>
        </div>
    </form>
</x-auth.guest-layout>
