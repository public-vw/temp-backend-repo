<x-auth.guest-layout>
    <form method="POST" action="{{ route('auth.password.store') }}">
        @csrf

        <!-- Password Reset Token -->
        <input type="hidden" name="token" value="{{ $request->route('token') }}">

        <div>
            <x-auth.input name="email" autocomplete="username" required autofocus >{{ __('Email') }}</x-auth.input>
        </div>

        <div class="mt-4">
            <x-auth.input name="password" type="password" required >{{ __('Password') }}</x-auth.input>
        </div>

        <div class="mt-4">
            <x-auth.input name="password_confirmation" type="password" required>{{ __('Confirm Password') }}</x-auth.input>
        </div>

        <div class="flex items-center justify-end mt-4">
            <x-auth.primary-button>{{ __('Reset Password') }}</x-auth.primary-button>
        </div>
    </form>
</x-auth.guest-layout>
