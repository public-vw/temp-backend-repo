<x-auth.guest-layout>
    <form method="POST" action="{{ route('auth.register.post') }}">
        @csrf

        <div>
            <x-auth.input name="firstname" autocomplete required autofocus >{{ __('First Name') }}</x-auth.input>
        </div>

        <div>
            <x-auth.input name="lastname" autocomplete >{{ __('Last Name') }}</x-auth.input>
        </div>

        <div class="mt-4">
            <x-auth.input name="email" autocomplete="username" required >{{ __('Email') }}</x-auth.input>
        </div>

        <div class="mt-4">
            <x-auth.input name="password" type="password" required >{{ __('Password') }}</x-auth.input>
        </div>

        <div class="mt-4">
            <x-auth.input name="password_confirmation" type="password" required>{{ __('Confirm Password') }}</x-auth.input>
        </div>

        <div class="flex items-center justify-end mt-4">
            <a class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800" href="{{ route('auth.login') }}">
                {{ __('Already registered?') }}
            </a>

            <x-auth.primary-button class="ml-4">{{ __('Register') }}</x-auth.primary-button>
        </div>
    </form>
</x-auth.guest-layout>
