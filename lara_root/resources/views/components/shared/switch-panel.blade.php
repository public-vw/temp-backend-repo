@props(['current_panel'])

@php($showed = false)

@role('sysadmin')
    @if($current_panel !== 'sysadmin')
    <x-shared.dropdown-link :href="route('sysadmin.home')" >
        {{ __('System Administration Panel') }}
    </x-shared.dropdown-link>
    @php($showed = true)
    @endif
@endrole

@foreach(auth()->user()->panels() as $panel)
@continue($panel->title == $current_panel)
<x-shared.dropdown-link :href="route($panel->title .'.home')" >
    {{ __($panel->title . ' Panel') }}
</x-shared.dropdown-link>
@php($showed = true)
@endforeach

@if($showed)
<hr />
@endif