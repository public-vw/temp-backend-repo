@props(['name','title', 'default'])

<x-sysadmin.input-label :for="$name" :value="$slot" :required="$attributes->has('required')"/>
<x-sysadmin.text-input
        :id="$name"
        class="block mt-1 w-full"
        type="text"
        :name="$name"
        :value="old($name, $default ?? '')"
        {{ $attributes }}
/>
<x-sysadmin.input-error
        :messages="$errors->get($name)"
        class="mt-2"
/>
