<?php

use Illuminate\Support\Facades\Route;

Route::get('ping', 'TestController@ping')->name('test.ping');
