<?php

use Illuminate\Support\Facades\Route;

Route::controller('OtpController')->group(function () {
    Route::post('otp/get', 'getOtp')->name('otp.get');
});
