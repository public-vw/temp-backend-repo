<?php

use Illuminate\Support\Facades\Route;

Route::controller('EmailController')->group(function () {
    Route::post('email/unverify', 'unverifyEmail')->name('email.unverify');
});

Route::controller('PasswordController')->group(function () {
    Route::post('password/token/get', 'getResetPasswordToken')->name('password.token.get');
});
