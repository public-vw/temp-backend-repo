<?php

use Illuminate\Support\Facades\Route;

# ArticleCategory ------------------------
Route::apiResource('article_categories', 'ArticleCategoryController');
# ArticleCategoryContent -----------------
Route::patch('article_category_contents/{article_category_content}/change_status', 'ArticleCategoryContentController@changeStatus')
    ->name('article_category_contents.change_status');

Route::apiResource('article_category_contents', 'ArticleCategoryContentController');

# Article --------------------------------
Route::apiResource('articles', 'ArticleController');
# ArticleContent -------------------------
Route::patch('article_contents/{article_content}/change_status', 'ArticleContentController@changeStatus')
    ->name('article_contents.change_status');

Route::apiResource('article_contents', 'ArticleContentController');

# StaticPage ------------------------------
Route::apiResource('static_pages', 'StaticPageController');
# StaticPageContent -----------------------
Route::patch('static_page_contents/{static_page_content}/change_status', 'StaticPageContentController@changeStatus')
    ->name('static_page_contents.change_status');

Route::apiResource('static_page_contents', 'StaticPageContentController');

# Attachment ------------------------------
Route::apiResource('attachments', 'AttachmentController')->only(['destroy']);

# Attachable ------------------------------
Route::apiResource('attachables', 'AttachableController')->only(['store', 'update', 'destroy']);

# SeoDetail ------------------------------
Route::post('seo_details', 'SeoDetailController@show')->name('seo_details.show');
Route::put('seo_details/by_content', 'SeoDetailController@updateByContent')->name('seo_details.update.by_content');
Route::put('seo_details/by_parent', 'SeoDetailController@updateByParent')->name('seo_details.update.by_parent');

# SeoRichSnippet ------------------------------
Route::post('rich_snippets', 'SeoRichSnippetController@show')->name('rich_snippets.show');
Route::put('rich_snippets/by_content', 'SeoRichSnippetController@updateByContent')->name('rich_snippets.update.by_content');
Route::put('rich_snippets/by_parent', 'SeoRichSnippetController@updateByParent')->name('rich_snippets.update.by_parent');

# User -----------------------------------
Route::apiResource('users', 'UserController')->only(['index']);

# Setting --------------------------------
Route::apiResource('settings', 'SettingController');

# Test -----------------------------------
Route::get('ping', 'TestController@ping')->name('test.ping');

# Note -----------------------------------
Route::apiResource('notes', 'NoteController')->except(['show']);

//TODO: check below routes
# Dashboard
Route::get('/', 'DashboardController@index')->name('home');

# Profile
Route::controller('ProfileController')->group(function () {
    Route::get('/profile', 'edit')->name('profile.edit');
    Route::patch('/profile', 'update')->name('profile.update');
    Route::delete('/profile', 'destroy')->name('profile.destroy');
});
