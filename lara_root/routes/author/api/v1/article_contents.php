<?php

use Illuminate\Support\Facades\Route;

Route::patch('article_contents/{article_content}/change_status', 'ArticleContentController@changeStatus')
    ->name('article_contents.change_status');

Route::apiResource('article_contents', 'ArticleContentController');
