<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('articles', 'ArticleController')->except(['delete']);
