<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('attachables', 'AttachableController')->only(['store', 'update', 'destroy']);
