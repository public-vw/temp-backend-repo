<?php

use Illuminate\Support\Facades\Route;

//TODO: store should be implemented as a request of creation
Route::apiResource('article_categories', 'ArticleCategoryController')->except(['store','update', 'destroy']);
