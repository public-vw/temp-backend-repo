<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('article_category_contents', 'ArticleCategoryContentController')->only(['index','show']);
