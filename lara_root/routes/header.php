<?php

use Illuminate\Support\Facades\Route;

Route::get('manage/error/{code}', function (int $code) {
    mylog("HTTP {$code}", 'error', "HTTP Error {$code}", false);
    throw new \Symfony\Component\HttpKernel\Exception\HttpException((int) $code);
});

Route::get('users/impersonate/stop', 'GeneralController@stopImpersonate')->name('impersonate.stop');

/*
//[google fixer]================================================
$redirects = [];
cache()->forget('url_redirect');
if(!cache()->has('url_redirect') || cache()->get('url_redirect')!=='true'){
    if(\Schema::hasTable('url_redirects'))
        cache()->forever('url_redirect','true');
}
else
    $redirects = \App\Models\UrlRedirect::where('active',1)->get();

if(count($redirects)>0){
    foreach($redirects as $redirect)
        if($redirect->start_with){
            Route::get($redirect->from.'/{etc?}', function($etc = '') use ($redirect) {
                return redirect($redirect->to.'/'.trim($etc,'/'),$redirect->state_code);
            })->where('etc','(.*)');
        }
//        elseif($redirect->external){
//            Route::get($redirect->from, function() use ($redirect) {
//                return redirect($redirect->to,$redirect->state_code);
//            });
//        }
        else{
            Route::get($redirect->from, function() use ($redirect) {
                return redirect($redirect->to,$redirect->state_code);
            });
//            Route::redirect($redirect->from,
//                            $redirect->to,
//                            $redirect->state_code);
        }
}
//[google fixer - END]==========================================*/
