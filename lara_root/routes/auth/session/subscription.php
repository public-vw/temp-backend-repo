<?php

use Illuminate\Support\Facades\Route;

Route::post('/subscribe', 'SubscribedUserController@store')
    ->name('subscription');
