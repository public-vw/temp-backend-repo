<?php

use Illuminate\Support\Facades\Route;

Route::get('/verify-email/{id}/{hash}', 'VerifyEmailController')
    ->middleware(['signed', 'throttle:6,1'])
    ->name('verification.verify');

Route::post('/email/verification-notification', 'EmailVerificationNotificationController@store')
    ->middleware(['auth', 'throttle:6,1'])
    ->name('verification.send');
