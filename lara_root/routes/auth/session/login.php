<?php

use Illuminate\Support\Facades\Route;

Route::post('/check', 'AuthenticatedSessionController@show')
    ->name('check');

Route::post('/login', 'AuthenticatedSessionController@store')
    ->middleware('guest')
    ->name('login');

Route::post('/logout', 'AuthenticatedSessionController@destroy')
    ->middleware('auth')
    ->name('logout');
