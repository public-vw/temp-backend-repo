<?php

use Illuminate\Support\Facades\Route;

Route::post('/register', 'RegisteredUserController@store')
    ->middleware('guest')
    ->name('register');
