<?php

use Illuminate\Support\Facades\Route;

Route::post('/forgot-password', 'PasswordResetLinkController@store')
    ->middleware('guest')
    ->name('password.email');

Route::post('/reset-password', 'NewPasswordController@store')
    ->middleware('guest')
    ->name('password.store');
