<?php

use Illuminate\Support\Facades\Route;

Route::controller('StructureController')->group(function () {
    Route::post('structures/api/controller', 'createApiController')->name('structures.controller.api');
    Route::post('structures/api/route', 'createApiRoute')->name('structures.route.api');
    Route::post('structures/api/request', 'createApiRequest')->name('structures.request.api');
//    Route::get('structures/request','createRequest')->name('structures.request');
    Route::post('structures/policy', 'createPolicy')->name('structures.policy');
    Route::post('structures/transformer', 'createTransformer')->name('structures.transformer');

    Route::post('structures/api', 'createApi')->name('structures.api');

    Route::post('structures/model', 'createModel')->name('structures.model');
    Route::post('structures/migration', 'createMigration')->name('structures.migration');
    Route::post('structures/factory', 'createFactory')->name('structures.factory');
    Route::post('structures/seeder/fake', 'createFakeSeeder')->name('structures.seeder.fake');
    Route::post('structures/route', 'createRoute')->name('structures.route');
    Route::post('structures/seeder', 'createSeeder')->name('structures.seeder');
    Route::post('structures/permissions', 'createPermission')->name('structures.permission');

    Route::post('structures/entity', 'createEntity')->name('structures.entity');

//    Route::get('structures/panel','panel')->name('structures.panel');
//    Route::get('structures/controller','createController')->name('structures.controller');
//    Route::get('structures/views','views')->name('structures.views');
});
