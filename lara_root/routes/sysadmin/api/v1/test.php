<?php

use Illuminate\Support\Facades\Route;

Route::get('ping', 'TestController@ping')->name('test.ping');
Route::get('test', 'TestController@test')->name('test.test');
