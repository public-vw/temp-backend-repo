<?php
use Illuminate\Support\Facades\Route;

Route::apiResource('static_pages', 'StaticPageController')->only(['show']);
