<?php
use Illuminate\Support\Facades\Route;

Route::apiResource('menus', 'MenuController')->only(['index']);

Route::apiResource('footer', 'FooterController')->only(['index']);
