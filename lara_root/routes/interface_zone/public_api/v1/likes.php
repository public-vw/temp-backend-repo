<?php

use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function (){
    Route::post('likes/check','LikeController@check')->name('likes.check');
    Route::put('likes/toggle','LikeController@toggle')->name('likes.toggle');
    Route::apiResource('likes', 'LikeController')->only(['index']);
});

