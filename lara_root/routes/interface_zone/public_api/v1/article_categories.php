<?php
use Illuminate\Support\Facades\Route;

Route::apiResource('article_categories', 'ArticleCategoryController')->shallow()->only(['index','show']);
