<?php

use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function (){
    Route::post('bookmarks/check','BookmarkController@check')->name('bookmarks.check');
    Route::put('bookmarks/toggle','BookmarkController@toggle')->name('bookmarks.toggle');
    Route::apiResource('bookmarks', 'BookmarkController')->only(['index']);
});

