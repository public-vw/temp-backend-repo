<?php
use Illuminate\Support\Facades\Route;

Route::apiResource('article_relations', 'ArticleRelationController')->only(['index']);
