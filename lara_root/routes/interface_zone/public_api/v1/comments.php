<?php
use Illuminate\Support\Facades\Route;

Route::apiResource('articles.comments', 'CommentController')->only(['index']);
