<?php
use Illuminate\Support\Facades\Route;

Route::apiResource('article_categories.articles', 'ArticleController')->shallow()->only(['index','show']);
