<?php
use Illuminate\Support\Facades\Route;

Route::post('static_pages', 'StaticPageController@showByUrl')->name('urls.static_page.show');
Route::post('article_categories', 'ArticleCategoryController@showByUrl')->name('urls.article_category.show');
Route::post('articles', 'ArticleController@showByUrl')->name('urls.article.show');

