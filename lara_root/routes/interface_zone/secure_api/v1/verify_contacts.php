<?php

use Illuminate\Support\Facades\Route;

Route::controller('VerifyController')->group(function () {
    Route::post('verification/email', 'email')->name('verify.email');
    Route::post('verification/mobile', 'mobile')->name('verify.mobile');

    Route::post('verify', 'verify')->name('verify.verification');
});
