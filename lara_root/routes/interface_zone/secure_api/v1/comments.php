<?php

use Illuminate\Support\Facades\Route;

Route::get('articles/{article}/comments/yours', 'CommentController@getYoursComment')->name('articles.comments.yours');
Route::resource('articles.comments', 'CommentController')->shallow()->only(['store','update']);
