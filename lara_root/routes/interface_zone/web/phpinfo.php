<?php

use Illuminate\Support\Facades\Route;

if (config('app.env', 'prod') === 'local') {
    Route::get('phpinf', function () {
        phpinfo();
    })->name('phpinfo');
}
