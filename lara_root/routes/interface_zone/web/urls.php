<?php

use Illuminate\Support\Facades\Route;

Route::get('files/{url}', 'AttachmentController@showByUrl')
    ->where('url', '^[a-z\/\-0-9]+\.[a-z0-9]+$')
    ->name('urls.attachment.show');
