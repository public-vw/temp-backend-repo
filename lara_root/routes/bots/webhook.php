<?php

use Illuminate\Support\Facades\Route;

Route::post('webhook/{token}', 'WebhookController@index')->name('webhook');
