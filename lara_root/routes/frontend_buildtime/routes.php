<?php

use Illuminate\Support\Facades\Route;

Route::post('seo_details', 'SeoDetailController@show')->name('seo_details.show');

Route::apiResource('all-settings', 'SettingGroupController')->only(['index']);
Route::post('locales', 'LocaleController@index')->name('locales.index');

Route::apiResource('urls', 'UrlController')->only(['index']);
