<?php
return [
    'telegram_api_path'       => env('TBOT_API_PATH', 'https://api.telegram.org') . '/bot',
    'token'                   => env('TBOT_TOKEN', ''),
    'username'                => env('TBOT_USERNAME', ''),
    'webhook'                 => [
        'token' => env('TBOT_WEBHOOK_TOKEN', ''),
    ],
    'debug'                   => [
        'log' => [
            'request'  => env('TBOT_LOG_REQUEST', false),
            'response' => env('TBOT_LOG_RESPONSE', false),
            'webhook'  => env('TBOT_LOG_WEBHOOK', false),
        ],
        'url' => env('TBOT_DEBUG_URL', false),
    ],
    'supervisor_telegram_uid' => env('TBOT_SUPERVISOR_UID', ''),

    'listen_to' =>
        [ //listens and send telegram message to the supervisor | kebab-case of event class name
            'revalidation-requested' => true,
            'url-created'            => true,
            'error-occurred'         => true,
            'record-update-occurred' => true,
            'content-status-changed' => true,
            'article-created'        => true,

            'errors' => [
                'levels'  => ['error', 'critical'], // lower case | * opens all levels. use wisely and limited (not recommended in the production)
                'exclude' => '',
                'include' => '',
            ]
        ]
];
