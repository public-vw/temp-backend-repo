<?php
return [
    'enable'     => env('GOOGLE_RECAPTCHAV3_ENABLE', false),
    'origin'     => env('GOOGLE_RECAPTCHAV3_ORIGIN', 'https://www.google.com/recaptcha'),
    'site_key'   => env('GOOGLE_RECAPTCHAV3_SITE_KEY', ''),
    'secret_key' => env('GOOGLE_RECAPTCHAV3_SECRET_KEY', ''),
    'locale'     => env('GOOGLE_RECAPTCHAV3_LOCALE', 'en')
];
