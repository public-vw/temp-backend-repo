<?php

return [
    'article_category',
    'article',
    'static_page',
    'attachment',
    'static_link',
];