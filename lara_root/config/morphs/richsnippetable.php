<?php

return [
    'website',
    'article_category_content',
    'article_content',
    'static_page_content',
    'attachment',
];