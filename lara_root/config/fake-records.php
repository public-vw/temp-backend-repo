<?php

return [
    'user' =>
        [
            'local' => 10,
            'prod'  => 10,
        ],

    'article_category' =>
        [
            'local' => 2,
            'prod'  => 10,
        ],

    'article' =>
        [
            'local' => 5,
            'prod'  => 35,
        ],

    'article-relation' =>
        [
            'local' => [
                'prev_next'     => 2,
                'other_readers' => 10,
                'recommended'   => 8,
                'top_rated'     => 8,
                'related'       => 8,
            ],
            'prod'  => [
                'prev_next'     => 2,
                'other_readers' => 10,
                'recommended'   => 8,
                'top_rated'     => 8,
                'related'       => 8,
            ],
        ],

    'comment' =>
        [
            'local' => 20,
            'prod'  => 1,
        ],

    'color' =>
        [
            'local' => 30,
            'prod'  => 30,
        ],

    'icon' =>
        [
            'local' => 100,
            'prod'  => 100,
        ],

    'static_link' =>
        [
            'local' => 10,
            'prod'  => 10,
        ],

    'subscription' =>
        [
            'local' => 10,
            'prod'  => 30,
        ],
];
