<?php

return [
    'api_secret_token' => env('API_SECRET_TOKEN', ''),
    'namespaces' => [
        'frontend_buildtime' => 'front/build',
        'bots' => 'bots',

        'auth' => [
            'session' => 'auth/session',
            'token'   => 'auth/token',
            'api'     => 'api/v1/auth',
            'view'    => 'auth',
        ],

        'sysadmin' => [
            'web'  => 'sys',
            'api'  => 'api/v1/sys',
            'view' => 'sysadmin',
        ],

        'dev' => [
            'web'  => 'dev',
            'api'  => 'api/v1/dev',
            'view' => 'dev',
        ],

        'staff' => [
            'api'  => 'api/v1/staff',
        ],

        'interface' => [
            'web'        => '', # unchangeable
            'public_api' => 'api/v1/face',
            'secure_api' => 'api/v1/sec',
            'view'       => 'interface_dark',
        ],
    ],
    'appearance'  => [
        'static_page' => [
            'prefix'        => '/info/',
            'trailing_html' => true
        ],
        'pdp'         => [
            'trailing_html' => true
        ]
    ]

];
