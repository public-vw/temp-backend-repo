<?php

return [
    'status_default' => 3,
    'status' => [
        'pending',
        'rejected',
        'suspended',
        'accepted',
    ],
];
