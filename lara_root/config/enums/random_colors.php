<?php
return [ // background_color => text_color
         [
             'foreground' => '#453939',
             'background' => '#eeeeee',
             'theme_name' => 'Masala'
         ],

         [
             'foreground' => '#87837A',
             'background' => '#eeeeee',
             'theme_name' => 'Friar Gray'
         ],

         [
             'foreground' => '#F0E5C9',
             'background' => '#333333',
             'theme_name' => 'Wheatfield'
         ],

         [
             'foreground' => '#006490',
             'background' => '#eeeeee',
             'theme_name' => 'Bahama Blue'
         ],

         [
             'foreground' => '#00A1D5',
             'background' => '#eeeeee',
             'theme_name' => 'Cerulean'
         ],

         [
             'foreground' => '#FF207B',
             'background' => '#eeeeee',
             'theme_name' => 'Wild Strawberry'
         ],

         [
             'foreground' => '#FE5552',
             'background' => '#eeeeee',
             'theme_name' => 'Persimmon'
         ],

         [
             'foreground' => '#FDD526',
             'background' => '#eeeeee',
             'theme_name' => 'Candlelight'
         ],

         [
             'foreground' => '#00844B',
             'background' => '#eeeeee',
             'theme_name' => 'Tropical Rain Forest'
         ],

         [
             'foreground' => '#8700B9',
             'background' => '#eeeeee',
             'theme_name' => 'Purple'
         ],

         [
             'foreground' => '#714BAE',
             'background' => '#eeeeee',
             'theme_name' => 'Studio'
         ],

         [
             'foreground' => '#4AE326',
             'background' => '#eeeeee',
             'theme_name' => 'Lima'
         ],

];