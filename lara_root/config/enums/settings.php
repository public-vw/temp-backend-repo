<?php
return [
    'mode_default' => 0,
    'modes'         => [
	    'bool',
        'text',
        'html',
        'textarea',
        'checkbox',
        'image',
    ],

    'direction_default' => 0,
    'directions'         => [
        'ltr',
        'rtl',
    ],

    'types' => [
        'bool',
        'int',
        'string',
        'string_list',
        'period',               // hourly, daily, 3days, weekly, monthly, yearly
        'time',                 // 18:00
        'date_time',            // WEB 18:00
        'date_time_list',       // WED 18:30    (day hh:mm)
        'theme_color_list',
        'link',
        'link_list',
        'image',
        'image_list',
        'language',
        'language_list',
        'key_value',
        'key_value_list',
    ]
];
