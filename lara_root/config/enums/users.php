<?php

return [
    'status_default' => 0,
    'status'         => [
        'draft',      # incomplete registration
        'pending',  # registered, waiting for activation
        'active',   # active!
        'suspended',  # suspended!
        'storage',  # no login, just for storing information.
        'deleted',  # soft deleted
    ],
];