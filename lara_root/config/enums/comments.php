<?php

return [
    'status_default' => 0,
    'status' => [
        'pending',
        'rejected',
        'suspended',
        'accepted',
    ],
];