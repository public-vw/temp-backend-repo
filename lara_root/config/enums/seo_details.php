<?php
return [
    'type_default' => 0,
    'types'        => [
        'basic-fields',
        'open-graph-static-page',
        'open-graph-article',
        'robots',
        'icons',
        'theme-color',
        'twitter',
        'apple-web-app',
        'alternatives',
        'app-links',
        'bookmarks',
        'other'
    ],
];
