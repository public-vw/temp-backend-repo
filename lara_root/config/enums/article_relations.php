<?php
return [
    'types' => [
        'prev_next',
        'other_readers',
        'recommended',
        'top_rated',
        'related',
    ],
];
