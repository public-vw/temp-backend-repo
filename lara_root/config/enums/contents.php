<?php
return [
    'status_default' => 0,
    'status' => [
        'draft',            // WRITER starting new content, or new version of latest content
        'review',           // WRITER finished the content, asks SEO and READER to make notes
        'ready',            // ALL made discussions and find peace that the content is ready to publish | all roles should request this
        'request-review',   // WRITER, SEO or READER have a NOTE that there is a need for more review | Admin reads the notes and makes final decision
        'request-delete',   // WRITER, requests deletion, for test contents | Admin checks the content and makes final decision
        'active',           // PUBLISHER, published the content and almost no-body can change it. just some notes.
    ],
];


