<?php
return [
    'type_default' => 0,
    'types'        => [
        'website',
        'person',
        'organization',
        'article',
        'author',
        'faq',
    ],
];
