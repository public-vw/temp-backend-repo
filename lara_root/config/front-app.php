<?php

return [
    'domain'                 => env('FRONTEND_URL', 'localhost'),
    'ssl'                    => env('FRONTEND_HAS_SSL', false),
    'reset-password'         => env('FRONTEND_RESET_PASS_URI', '/password/reset'),
    'build-token'            => env('FRONTEND_BUILD_TOKEN'),
    'white-ips'              => explode(',', env('FRONTEND_WHITE_IPS','')),
    'email-verification-url' => env('FRONTEND_EMAIL_VERIFICATION_URL'),
];
