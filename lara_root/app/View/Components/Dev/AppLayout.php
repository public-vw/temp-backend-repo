<?php

namespace App\View\Components\Dev;

use Illuminate\View\Component;
use Illuminate\View\View;

class AppLayout extends Component
{
    public function render(): View
    {
        return view('dev.layouts.app');
    }
}
