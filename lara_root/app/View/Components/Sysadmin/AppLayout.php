<?php

namespace App\View\Components\Sysadmin;

use Illuminate\View\Component;
use Illuminate\View\View;

class AppLayout extends Component
{
    public function render(): View
    {
        return view('sysadmin.layouts.app');
    }
}
