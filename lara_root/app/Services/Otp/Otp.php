<?php

namespace App\Services\Otp;

use App\Models\User;
use Illuminate\Support\Str;

class Otp
{
    protected $prefix = 'VERIFY';

    public function verification($traching_code, $otp)
    {
        $track = $this->extractTrackingCode($traching_code);

        abort_if(! $track, 401, 'Invalid data.');

        abort_if(! $this->isOtpCorrect($otp, $track['otp']), 401, 'Invalid OTP code.');

//        $this->flushOtp($request->tracking_code);

        return $track;
    }

    public function makeOtp(User $user): string
    {
        $cache_key = "{$this->prefix}_{$user->id}";

        if (($otp = $this->retrieveOtp($cache_key)) !== null) {
            return $otp;
        }

        $otp = $this->createRandomCombination();
        cache()->put(
            $cache_key,
            $otp,
            config('auth.passwords.' . config('auth.defaults.passwords') . '.expire')
        );
        return $otp;
    }

    public function retrieveOtp(string $cache_key): ?string
    {
        return cache()->get($cache_key, null);
    }

    public function makeTrackingCode(string $field, string $otp, User $user): string
    {
        $tracking_code = Str::random(config('auth.tracking_code.length'));

        cache()->put(
            $tracking_code,
            [
                'field' => $field,
                'otp' => $otp,
                'user' => $user,
            ],
            config('auth.passwords.' . config('auth.defaults.passwords') . '.expire')
        );

        return $tracking_code;
    }

    public function extractTrackingCode(string $tracking_code): ?array
    {
        return cache()->get($tracking_code, null);
    }

    public function isOtpCorrect(string $requested_otp, ?string $cached_otp): bool
    {
        return $requested_otp === $cached_otp;
    }

    public function createRandomCombination(int $len = 4): string
    {
        $ret = '';

        $i = 0;
        do {
            $ret .=
                $i % 2 === 0 ?
                  (string) random_int(1, 9)
                  : array_rand(['0', '5']);
        } while (++$i < $len);

        return $ret;
    }

    protected function flushOtp(string $cache_key)
    {
        cache()->forget($cache_key);
    }
}
