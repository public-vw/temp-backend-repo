<?php

namespace App\Services\FakeImageService;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class FakeImageService
{
    public static function getImageByUrl(int $width, int $height, string $file_ext = 'webp', $local = false): UploadedFile
    {
        $random_tmp_filename = md5(time());

        $url = PicsumRandomImage::imageUrl($width, $height, blur: false, imageExtension: $file_ext);
        $arrContextOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ];

        $image = null;
        $error = false;

        if (! $local) {
            $content = '';

            try {
                $content = file_get_contents($url, false, stream_context_create($arrContextOptions));
            } catch (\Exception $e) {
                $error = true;
            }

            $image = self::makeFakeUploadedFile($random_tmp_filename, $content, $file_ext);
        }
        if ($local || $error) {
            $image = self::makeLocalImage($width, $height, $random_tmp_filename, $file_ext);
        }

        return $image;
    }
    public static function makeFakeUploadedFile(string $random_tmp_filename, false|string $content, string $file_ext): UploadedFile
    {
        $temp_file = tempnam(sys_get_temp_dir(), 'uploaded_' . $random_tmp_filename);
        file_put_contents($temp_file, $content);
        return new UploadedFile($temp_file, "{$random_tmp_filename}.{$file_ext}", test: true);
    }

    /**
     * @param int $width
     * @param int $height
     * @param string $random_tmp_filename
     * @param string $file_ext
     *
     * @return UploadedFile
     */
    public static function makeLocalImage(int $width, int $height, string $random_tmp_filename, string $file_ext): UploadedFile
    {
        $temp_file = self::createOfflineRandomTextureImage($width, $height);
        return new UploadedFile($temp_file, "{$random_tmp_filename}.{$file_ext}", test: true);
    }

    protected static function createOfflineRandomTextureImage($width, $height): string
    {
        // Create a blank canvas
        $img = Image::canvas($width, $height, fake()->hexColor);

        // Number of shapes to draw
        $numShapes = rand(5, 20);

        // Draw random shapes
        for ($i = 0; $i < $numShapes; $i++) {
            $x = rand(0, $width);
            $y = rand(0, $height);
            $size = rand(20, 100);

            $shapeType = rand(1, 2);

            if ($shapeType === 1) {
                // Draw rectangles
                $img->rectangle($x, $y, $x + $size, $y + $size, function ($draw) {
                    $draw->background('#'.dechex(rand(0, 255)).dechex(rand(0, 255)).dechex(rand(0, 255)));
                });
            } else {
                // Draw circles
                $img->circle($size, $x, $y, function ($draw) {
                    $draw->background('#'.dechex(rand(0, 255)).dechex(rand(0, 255)).dechex(rand(0, 255)));
                });
            }
        }

        // Save to a temporary path
        $path = sys_get_temp_dir(). '/uploaded_' . uniqid() . '.jpg';
        $img->save($path);

        return $path;
    }
}
