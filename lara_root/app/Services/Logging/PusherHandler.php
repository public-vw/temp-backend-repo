<?php

namespace App\Services\Logging;

use App\Events\DebugMessageBroadcast;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\LogRecord;

class PusherHandler extends AbstractProcessingHandler
{
    protected function write(LogRecord $record): void
    {
        $level = $record->level->getName();
        $time = $record->datetime->format('Y-m-d H:i:s');
        $message = $record->message;
        $context = json_encode($record->context);
        $extra = json_encode($record->extra);

        DebugMessageBroadcast::dispatch($level, $time, $message, $context, $extra);
    }
}
