<?php

namespace App\Services\EditorFakers;

use App\Models\Attachable;
use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Models\Contracts\AbsContentModel;

class EditorjsFaker
{
    public static array $TYPES = [
        'paragraph',
        'heading',
        'quote',
        'image',
        'list',
        'video',
        //        "code",
    ];
    protected AbsContentModel $contentObject;

    protected string $rawContent = '';

    public static function setContentObject(AbsContentModel $contentObject): static
    {
        $obj = new static();
        $obj->contentObject = $contentObject;
        return $obj;
    }

    public function generateRandomBlocks(int $number_of_blocks): array
    {
        $output = [];
        for ($i = 0; $i < $number_of_blocks; $i++) {
            $output[] = $this->createFakeBlock(fake()->randomElement(self::$TYPES));
        }
        return [
            'time' => fake()->unixTime,
            'version' => fake()->randomDigit() . '.' . fake()->numberBetween(1, 80) . '.' . fake()->randomDigit(),
            'blocks' => $output,
        ];
    }

    public function createFakeBlock(string $type): array
    {
        $operator = 'createFake' . ucfirst($type);
        return $this->$operator();
    }

    public function getRawContent(): string
    {
        return $this->rawContent;
    }

    protected function createFakeParagraph(): array
    {
        $text = fake()->text(100);
        $this->rawContent .= $text . '|';
        return [
            'id' => fake()->uuid,
            'type' => 'paragraph',
            'data' => [
                'text' => $text,
            ],
        ];
    }

    protected function createFakeQuote(): array
    {
        $text = fake()->text(100);
        $this->rawContent .= $text . '|';
        return [
            'id' => fake()->uuid,
            'type' => 'quote',
            'data' => [
                'text' => $text,
                'caption' => fake()->boolean(75) ? fake()->name() : null,
                'alignment' => 'left',
            ],
        ];
    }

    protected function createFakeHeading(): array
    {
        $text = fake()->text(100);
        $this->rawContent .= $text . '|';
        return [
            'id' => fake()->uuid,
            'type' => 'header',
            'data' => [
                'text' => $text,
                'level' => rand(2, 6),
            ],
        ];
    }

    protected function createFakeImage(): array
    {
        $attachment_type = AttachmentType::where('slug', 'article-content-image')->first();

        $attachment = Attachment::factory()->create([
            'media' => config_key('enums.attachments.medias', 'image'),
        ]);

        $attachable = (new Attachable())::factory()
            ->count(1)
            ->create([
                'attachment_id' => $attachment->id,
                'type_id' => $attachment_type->id,
                'attachable_type' => get_class($this->contentObject),
                'attachable_id' => $this->contentObject->id,

            ], $this->contentObject)->first();

        $text = fake()->boolean ? fake()->words(4, true) : null;
        $this->rawContent .= (string) $text . '|';

        return [
            'id' => fake()->uuid,
            'type' => 'image',
            'data' => [
                'file' => [
                    'id' => $attachment->id,
                ],
                'caption' => $text,
                'withBorder' => false,
                'stretched' => false,
                'withBackground' => false,
            ],
        ];
    }

    protected function createFakeVideo(): array
    {
//        TODO: [low priority] generate dynamic video link
        $text = fake()->boolean ? fake()->words(4, true) : null;
        $this->rawContent .= (string) $text . '|';
        return [
            'id' => fake()->uuid,
            'type' => 'embed',
            'data' => [
                'service' => 'youtube',
                'source' => 'https://www.youtube.com/watch?v=N76198i_VBY',
                'embed' => 'https://www.youtube.com/embed/N76198i_VBY',
                'width' => 580,
                'height' => 320,
                'caption' => $text,
            ],
        ];
    }

    protected function createFakeList(): array
    {
        $fakeArray = self::fakeArray(rand(4, 12), rand(6, 14));
        $this->rawContent .= implode('|', $fakeArray) . '|';
        return [
            'id' => fake()->uuid,
            'type' => 'list',
            'data' => [
                'style' => fake()->boolean ? 'ordered' : 'unordered',
                'items' => $fakeArray,
            ],
        ];
    }

    protected static function fakeArray($count, $maxNbChars = 10): array
    {
        return collect(range(1, fake()->numberBetween(1, $count)))->map(function () use ($maxNbChars) {
            return trim(fake()->text($maxNbChars), '.');
        })->toArray();
    }
}
