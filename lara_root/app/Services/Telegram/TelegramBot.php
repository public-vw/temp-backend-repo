<?php

namespace Telegram;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Telegram\Core\Command;
use Telegram\Customs\CustomResponse;
use TelegramRobot\RobotHandler;

class TelegramBot
{
    public Command $commands;
    public string $chat_id;
    protected $url;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->url = $this->makeUrl();
        $this->commands = new Command($this);
    }

    public function __call(string $name, array $arguments)
    {
        return $this->commands->$name(...$arguments);
    }

    public function handleWebhook(Request $request): Response
    {
        $request_arr = $request->all();
        $this->chat_id = $request_arr['message']['chat']['id'];

        if ((bool) config('tbot.debug.log.webhook')) {
            Log::info('tbot.webhook', $request->all() ?? []);
        }

        $handler = new RobotHandler($this, $request);
        return $handler->run()->getResponse();
    }

    public function call($telegram_method_name, $params = [], $http_method = 'post'): CustomResponse
    {
        abort_if(! in_array($http_method, ['get', 'post']), 503);

        $url = $this->url . $telegram_method_name;

        if (config('tbot.debug.url')) {
            dd($url);
        }

        $response = Http::beforeSending(function ($req) use ($telegram_method_name) {
            if (! (bool) config('tbot.debug.log.request')) {
                return;
            }
            Log::debug('tbot.request', [
                'method' => $req->method(),
                'url' => $req->url(),
                'headers' => $req->headers(),
                'data' => $req->data(),
                'telegram_method' => $telegram_method_name,
            ]);
        })->$http_method($url, $params);

        return new CustomResponse($response);
    }

    private function makeUrl(): string
    {
        if (config('tbot.telegram_api_path', '') === '') {
            throw new \Exception('<telegram API path> does not exists');
        }

        if (config('tbot.token', '') === '') {
            throw new \Exception('<telegram bot token> does not exists');
        }

        return config('tbot.telegram_api_path') . config('tbot.token') . '/';
    }
}
