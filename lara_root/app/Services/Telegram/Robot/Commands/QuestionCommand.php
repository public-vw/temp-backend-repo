<?php

namespace TelegramRobot\Commands;

use Telegram\Customs\_RobotCommands;

class QuestionCommand extends _RobotCommands
{
    public function execute()
    {
        $message = $this->getMessage();

        $chat = $message->getChat();
        $user = $message->getFrom();
        $text = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();

        // Preparing response
        $data = [
            'chat_id' => $chat_id,
            // Remove any keyboard by default
            'reply_markup' => Keyboard::remove(['selective' => true]),
        ];

        if ($chat->isGroupChat() || $chat->isSuperGroup()) {
            // Force reply is applied by default, so it can work with privacy on
            $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
        }

        // Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        // Load any existing notes from this conversation
        $notes = &$this->conversation->notes;
        ! is_array($notes) && $notes = [];

        // Load the current state of the conversation
        $state = $notes['state'] ?? 0;

        $result = Request::emptyResponse();

        // State machine
        // Every time a step is achieved the state is updated
        switch ($state) {
            // No break!
            case 0:
                if ($message->getContact() === null) {
                    $notes['state'] = 0;
                    $this->conversation->update();

                    $data['reply_markup'] = (new Keyboard(
                        (new KeyboardButton('ثبت اطلاعات تماس'))->setRequestContact(true)
                    ))
                        ->setOneTimeKeyboard(true)
                        ->setResizeKeyboard(true)
                        ->setSelective(true);

                    $data['text'] = 'برای احراز هویت کلیک کن';

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['phone_number'] = $message->getContact()->getPhoneNumber();
                $notes['db_user_id'] = $this->getUserDbRecord($notes['phone_number']);

                // no break
            case 1:
                if ($text === '') {
                    $notes['state'] = 1;
                    $this->conversation->update();

                    $data['text'] = 'سوال خودت رو وارد کن';

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['name'] = $text;
                $text = '';

                // no break!
            case 2:
                $this->conversation->update();
                $this->saveConversationDbRecord();

                $out_text = 'سوال در فهرست سوالات این هفته ثبت شد' . PHP_EOL;
                $out_text .= PHP_EOL;
                $out_text .= 'اگر سوال دیگری هست برروی /question کلیک کن' . PHP_EOL;
                unset($notes['state']);

                $data['photo'] = $notes['photo_id'];
                $data['caption'] = $out_text;

                $this->conversation->stop();

                $result = Request::sendPhoto($data);
                break;
        }

        return $result;
    }

    protected function saveConversationDbRecord()
    {
        $QUERY = 'INSERT INTO `questions`';
        $QUERY .= 'SET (`question`, `user_id`, `status`)';
        $QUERY .= "VALUES ('', '', 'accepted');";
    }

    protected function getUserDbRecord($phonenumber)
    {
        $QUERY = 'SELECT `my_users` WHERE `phonenumber` = :phonenumber;';

        if (0) { //no user returned
            $user_db_id = $this->saveUserDbRecord();
        }

        return $user_db_id;
    }

    protected function saveUserDbRecord()
    {
        $QUERY = 'INSERT INTO `my_users`';
        $QUERY .= 'SET (`phonenumber`, `telegram_userid`, `firstname`, `lastname`)';
        $QUERY .= "VALUES ('', '', '', '');";
    }
}
