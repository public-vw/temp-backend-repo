<?php

namespace TelegramRobot\Commands;

use Telegram\Customs\_RobotCommands;

class StartCommand extends _RobotCommands
{
    public function execute()
    {
        return $this->bot->sendReply(
            'به کمک من میتونی سوالات خودت رو در مورد مهاجرت کاری بپرسی' . PHP_EOL
            . 'برای شروع روی /question کلیک کن'
        );
    }
}
