<?php

namespace TelegramRobot\Commands;

use Telegram\Customs\_RobotCommands;

class MarkdownCommand extends _RobotCommands
{
    public function execute()
    {
        return $this->replyToChat('
*bold* _italic_ `inline fixed width code`

```
preformatted code block
code block
```

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                [Best Telegram bot api!!](https://github.com/php-telegram-bot/core)', [
            'parse_mode' => 'markdown',
        ]);
    }
}
