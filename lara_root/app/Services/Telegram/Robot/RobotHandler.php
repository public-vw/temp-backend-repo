<?php

namespace TelegramRobot;

use Telegram\Customs\_Handler;

class RobotHandler extends _Handler
{
    public function run(): static
    {
        if ($command = $this->getCommand()) {
        }

        $this->response = response('done', 200);
        return $this;
    }

    protected function getCommand(): ?string
    {
        if ($command = $this->getProperty('command')) {
            return $command;
        }

        $full_command = $this->getFullCommand() ?? '';
        if (strpos($full_command, '/') !== 0) {
            return null;
        }
        $full_command = substr($full_command, 1);

        //check if command is followed by bot username
        $split_cmd = explode('@', $full_command);
        if (! isset($split_cmd[1])) {
            //command is not followed by name
            return $full_command;
        }

        if (strtolower($split_cmd[1]) === strtolower($this->getBotUsername())) {
            //command is addressed to me
            return $split_cmd[0];
        }

        return null;
    }
}
