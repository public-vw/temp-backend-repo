<?php

namespace App\Services\Telegram\Contracts;

abstract class ShouldTbot
{
    public Level $level = Level::Info;
    public string $title = 'Untitled Message';
    public array $payload = [];
}
