<?php

namespace App\Services\Telegram\Contracts;

enum Level: string
{
    case Info = 'ℹ️';
    case Debug = '🐛';
    case Warning = '⚠️';
    case Error = '❌';

    public static function fromString(string $level): self
    {
        return match (strtolower($level)) {
            'info' => self::Info,
            'debug' => self::Debug,
            'warning' => self::Warning,
            'error' => self::Error,
            default => throw new \InvalidArgumentException("Invalid level: {$level}")
        };
    }
}
