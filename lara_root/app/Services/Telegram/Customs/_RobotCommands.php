<?php

namespace Telegram\Customs;

use Telegram\TelegramBot;

abstract class _RobotCommands
{
    protected TelegramBot $bot;

    public function __construct(TelegramBot $bot)
    {
        $this->bot = $bot;
    }

    abstract public function execute();
}
