<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendEmailOtp extends Notification
{
    use Queueable;

    public $code;

    public function __construct($code)
    {
        $this->code = $code;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject('Your OTP code')
            ->line('Your OTP code is: ' . $this->code)
            ->line('This OTP code will expire in '.config('auth.passwords.' . config('auth.defaults.passwords') . '.expire').' minutes.')
            ->line('If you did not request this OTP code, please ignore this email.');
    }
}
