<?php

namespace App\Console;

use App\Jobs\JCheckJobService;
use App\Models\Setting;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected function schedule(Schedule $schedule): void
    {
        if (Setting::isSame('server.cronjob', '1')) { //checks if works, turns to 10 if yes
            $schedule->call(function () {
                Setting::put('server.cronjob', '10');
            });
        }

        if (Setting::isSame('server.queue', '1')) { //checks if works, turns to 10 if yes
            $schedule->call(function () {
                JCheckJobService::dispatch();
            });
        }

        if (Setting::isSame('publish_manager.active', true)) {
            $schedule->call(function () {
                // Publish New Post
            })->dailyAt(Setting::get('publish_manager.new_post'));
            $schedule->call(function () {
                // Publish New Version
            })->dailyAt(Setting::get('publish_manager.new_version'));
        }
    }

    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
