<?php

namespace App\Console\Commands\HealthCheck;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class RedisHealthCheck extends Command
{
    protected $signature = 'redis:health-check';

    protected $description = 'Check the health of Redis connection';

    public function handle(): int
    {
        try {
            Redis::connection()->set('testKey', 'testValue');
            $value = Redis::connection()->get('testKey');

            if ($value === 'testValue') {
                $this->info('Redis connection is healthy.');
            } else {
                $this->error('Redis connection is unhealthy.');
                return 1;
            }
        } catch (\Exception $e) {
            $this->error('Could not connect to Redis: ' . $e->getMessage());
            return 1;
        }

        return 0;
    }
}
