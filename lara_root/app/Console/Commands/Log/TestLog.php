<?php

namespace App\Console\Commands\Log;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class TestLog extends Command
{
    protected $signature = 'logs:test {level?}';
    protected $description = 'dummy log test';

    public function handle(): void
    {
        $level = $this->argument('level') ?? 'info';

        if (! in_array($level, ['info','debug','warning','error','critical','alert','emergency'])) {
            $this->error('log level `'.$level.'` is not supported!');
            return;
        }

        $this->comment('Sending test log to pusher');
        Log::write($level, 'This is a test log message', ['random_str' => Str::random()]);
    }
}
