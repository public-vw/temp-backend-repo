<?php

namespace App\Console\Commands\DB;

use App\Models\FakeRecord;
use Illuminate\Console\Command;

class RemoveFakeData extends Command
{
    protected $signature = 'fake:remove-all {model?}';

    protected $description = 'Removes Fake Data Safely';

    protected int $deleted = 0;
    protected int $norec = 0;
    protected int $errors = 0;

    public function handle(): void
    {
        $model = $this->argument('model') ?? null;

        $parents = new FakeRecord();
        if ($model) {
            $parents = $parents->where('model', 'like', "%{$model}");
        }
        $parents = $parents->whereNull('parent_model')->get(['id']);

        foreach ($parents as $parent) {
            $this->comment("REC {$parent->id}\tDeleting ...");

            $this->delete($parent->id);
        }

        $this->comment('Finished');
        $this->comment("Deleted records: {$this->deleted}");
        $this->comment("Deleted without record: {$this->norec}");
        $this->comment("Errors: {$this->errors}");
    }

    protected function delete($record_id): void
    {
        try {
            $fake_record = FakeRecord::find($record_id);

            $children = FakeRecord::where([
                'parent_model' => $fake_record->model,
                'parent_id' => $fake_record->model_id,
            ])->get();

            foreach ($children as $child) {
                $this->comment("CHLD REC {$child->id}\tDeleting ...");
                $this->delete($child->id);
            }

            $model = $fake_record->model;
            $query = new $model();
            $query = $query->where('id', $fake_record->model_id)->first();

            if (! $query) {
                $fake_record->delete();
                $this->norec++;
                return;
            }

            if (method_exists($model, 'hardDelete')) {
                $query->hardDelete();
            } else {
                $query->forceDelete();
            }
            $fake_record->delete();
            $this->deleted++;
        } catch (\Exception $e) {
            $this->error("REC: {$record_id} \t" . $e->getMessage());
            $this->errors++;
            return;
        }

        $this->comment('Deleted | ' .
            "Model {$model}\t" .
            "Record {$record_id}");
    }
}
