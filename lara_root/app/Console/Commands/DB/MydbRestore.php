<?php

namespace App\Console\Commands\DB;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class MydbRestore extends Command
{
    protected $signature = 'mydb:restore {fileterm?} {--y|yes} {--d|drop} {--x|nobackup}';

    protected $description = 'Restore last backup file of database';

    protected $rcloneConfigName = 'mr_backup_cryptobaz';

    protected string $backupFolder = 'DB_Backups';

    public function handle()
    {
        $dbName = config('database.connections.mysql.database');
        $dbUser = config('database.connections.mysql.username');
        $dbPass = config('database.connections.mysql.password');

        $this->comment('Restoring DB');
        $filename = null;

        if ($fileterm = $this->argument('fileterm')) {
            $lastFiles = runShell('cd ' . base_path($this->backupFolder) . ';' .
                "find . -name \"db_*{$fileterm}*.gz\" -type f -exec stat --format '%n' \"{}\" \; | sort -n | cut -d: -f2- | tail -3");
            if (empty(trim($lastFiles))) {
                $this->error("Backup file not found in LOCAL with term: {$fileterm}");

                $listRemoteFiles = runShell("rclone lsf {$this->rcloneConfigName}: --include \"db_*{$fileterm}*.gz\" | sort -n | cut -d: -f2- | tail -3");
                if (empty(trim($listRemoteFiles))) {
                    $this->error("Backup file not found in REMOTE with term: {$fileterm}");
                    return;
                }
                $filename = Arr::last(explode("\n", trim($listRemoteFiles)));
                $this->comment("Downloading backup file from REMOTE: {$filename}");

                runShell('cd ' . base_path($this->backupFolder) . ';' .
                    "rclone copy -P {$this->rcloneConfigName}:{$filename} .");
            }
        } else {
            $lastFiles = runShell('cd ' . base_path($this->backupFolder) . ';' .
                "find . -name \"db_*.gz\" -type f -exec stat --format '%n' \"{}\" \; | sort -n | cut -d: -f2- | tail -3");

            if (empty(trim($lastFiles))) {
                $this->error('Backup file not found');
                return;
            }
        }

        if (! $filename) {
            $filename = Arr::last(explode("\n", trim($lastFiles)));
            $filename = Str::substr($filename, 2, Str::length($filename) - 9);
        } else {
            $filename = Str::substr($filename, 0, Str::length($filename) - 7);
        }

        if (! $this->option('yes') && ! $this->confirm("Are you sure you want to restore {$filename} ?")) {
            $this->comment('Canceled by user');
            return;
        }

        if ($this->option('drop')) {
            $this->comment('Dropping All Tables');
            Artisan::call('mydb:drop -y ' . ($this->option('nobackup') ? '-x' : ''));
        }

        $this->comment("Restoring [{$filename}] in DB");

        $result = runShell(
            'cd ' . base_path($this->backupFolder) . ';' .
            "tar xzf {$filename}.tar.gz {$filename}.sql;" .
            "mysql -u {$dbUser} -p\"{$dbPass}\" {$dbName} < " . $filename . '.sql;' .
            "rm -f {$filename}.sql",
            true
        );
        $this->comment('output: '.$result['output']);
        $this->comment('errors: '.$result['errors']);
    }
}
