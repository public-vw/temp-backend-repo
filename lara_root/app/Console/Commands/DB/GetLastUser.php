<?php

namespace App\Console\Commands\DB;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class GetLastUser extends Command
{
    protected $signature = 'watson:last:user {role?} {user_state?}';

    protected $description = 'Return last user with specific role and user state';

    public function handle(): void
    {
        $role = $this->argument('role') ?? 'client';
        $user_state = $this->argument('user_state') ?? 'active';

        if (! $this->checkRoleExistance($role)) {
            return;
        }
        if (! $this->checkUserStateExistance($user_state)) {
            return;
        }

        $user = User::role($role)
            ->where('status', config_key('enums.users.status', $user_state))
            ->orderBy('created_at', 'desc')
            ->first();

        $this->sendOutput($user);
    }

    protected function checkRoleExistance(bool|array|string|null $role): bool
    {
        if (Role::where('name', $role)->first() === null) {
            $this->error('Invalid role');
            return false;
        }
        return true;
    }

    protected function checkUserStateExistance(bool|array|string $user_state): bool
    {
        if (! in_array($user_state, config('enums.users.status'))) {
            $this->error('Invalid user state');
            return false;
        }
        return true;
    }

    protected function sendOutput($user): void
    {
        if ($user) {
            $this->info(sprintf('[%d] %s', $user->id, $user->email));
        } else {
            $this->info('No migrants found');
        }
    }
}
