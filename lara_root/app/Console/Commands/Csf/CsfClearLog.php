<?php

namespace App\Console\Commands\Csf;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class CsfClearLog extends Command
{
    protected $signature = 'csf:clear';

    protected $description = 'Clears Log of CSF';

    public function handle()
    {
        $log_file = storage_path('logs/csf.log');
        File::put($log_file, '');
        return 0;
    }
}
