<?php

namespace App\Console\Commands\Frontend;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class GenerateApiSecretToken extends Command
{
    protected $signature = 'api-token:generate';
    protected $description = 'Generate a new api token and update .env file';

    public function handle(): void
    {
        $token = bin2hex(Str::random(18));

        $path = base_path('.env');

        if (File::exists($path)) {
            file_put_contents($path, str_replace(
                'API_SECRET_TOKEN=' . env('API_SECRET_TOKEN'),
                'API_SECRET_TOKEN=' . $token,
                file_get_contents($path)
            ));

            $this->info('api secret token set successfully.');
        } else {
            $this->error('`.env` file not found.');
        }
    }
}
