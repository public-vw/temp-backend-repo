<?php

namespace App\Console\Commands\Frontend;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class GenerateFrontendBuildToken extends Command
{
    protected $signature = 'build-token:generate';
    protected $description = 'Generate a new build token and update .env file';

    public function handle(): void
    {
        $token = bin2hex(Str::random(32));

        $path = base_path('.env');

        if (File::exists($path)) {
            file_put_contents($path, str_replace(
                'FRONTEND_BUILD_TOKEN=' . env('FRONTEND_BUILD_TOKEN'),
                'FRONTEND_BUILD_TOKEN=' . $token,
                file_get_contents($path)
            ));

            $this->info('Build token set successfully.');
        } else {
            $this->error('`.env` file not found.');
        }
    }
}
