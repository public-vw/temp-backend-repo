<?php

namespace App\Console\Commands\Frontend;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class GenerateFrontendRevalidateToken extends Command
{
    protected $signature = 'revalidate-token:generate';
    protected $description = 'Generate a new revalidate token and update .env file';

    public function handle(): void
    {
        $token = bin2hex(Str::random(18));

        $path = base_path('.env');

        if (File::exists($path)) {
            file_put_contents($path, str_replace(
                'FRONTEND_REVALIDATE_TOKEN=' . env('FRONTEND_REVALIDATE_TOKEN'),
                'FRONTEND_REVALIDATE_TOKEN=' . $token,
                file_get_contents($path)
            ));

            $this->info('Revalidate token set successfully.');
        } else {
            $this->error('`.env` file not found.');
        }
    }
}
