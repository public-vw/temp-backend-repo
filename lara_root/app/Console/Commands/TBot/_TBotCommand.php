<?php

namespace App\Console\Commands\TBot;

use Illuminate\Console\Command;
use Telegram\TelegramBot;

abstract class _TBotCommand extends Command
{
    protected $tbot;

    protected function fetchBot(): void
    {
        $this->tbot = new TelegramBot();
    }
}
