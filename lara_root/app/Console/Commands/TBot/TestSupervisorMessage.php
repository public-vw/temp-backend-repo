<?php

namespace App\Console\Commands\TBot;

use App\Events\TelegramAlertRequested;
use App\Models\User;
use App\Services\Telegram\Contracts\Level;
use Carbon\Carbon;
use Illuminate\Console\Command;

class TestSupervisorMessage extends Command
{
    protected $signature = 'tbot:supervisor:test {level?}';
    protected $description = 'sends a test message to the supervisor';

    public function handle(): int
    {
        $level = Level::fromString($this->argument('level') ?? 'info');

        $title = 'Test message';
        $message = 'This is a test message at '.Carbon::now()->toDateTimeString();

        $user = User::find(1);//TODO:make it dynamic by parameters
        auth()->login($user);

        TelegramAlertRequested::dispatch(
            config('tbot.supervisor_telegram_uid'),
            $level,
            $title,
            $message
        );
        auth()->logout();

        $this->comment('Test telegram message sent successfully');

        return Command::SUCCESS;
    }
}
