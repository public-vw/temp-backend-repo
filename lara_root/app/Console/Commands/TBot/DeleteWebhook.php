<?php

namespace App\Console\Commands\TBot;

use Illuminate\Console\Command;

class DeleteWebhook extends Command
{
    protected $signature = 'tbot:webhook:delete';
    protected $description = 'delete webhook';

    public function handle(): int
    {
        $this->comment('Token: '.config('tbot.webhook.token'));

        $url = route('bots.webhook', [
            'token' => config('tbot.webhook.token'),
        ]);
        $this->comment('Webhook URL: ' . $url);

        $this->call('tbot:call', ['method' => ['deleteWebhook'] ]);

        return Command::SUCCESS;
    }
}
