<?php

namespace App\Console\Commands\ApiStructure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateApiStructureController extends Command
{
    protected $signature = 'structure:api:controller {panel} {model} {version=1}';

    protected $description = 'Creates API Controller';

    public function handle(): void
    {
        $panel = $this->argument('panel');
        $panel = Str::ucfirst($panel);
        $panel = str_replace(' ', '', $panel);
        $snake_panel = Str::snake($panel);
        $panel = $panel !== 'Interface' ? $panel : 'InterfaceZone';

        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $camelModel = Str::camel($model);

        $version = $this->argument('version');
        $version = floatval($version);

        $dest = "Http/Controllers/{$panel}";
        $full_dest = app_path($dest);
        $dest .= "/{$model}Controller.php";

        # make folder
        runShell('mkdir -p ' . $full_dest);

        $content = file_get_contents(storage_path('templates/_Model_API_Controller.php'));

        ## change _Panel_
        $content = str_replace('_Panel_', $panel, $content);
        //## change _panel_
        $content = str_replace('_panel_', $snake_panel, $content);
        //## change _Model_
        $content = str_replace('_Model_', $model, $content);
        //## change _model_
        $content = str_replace('_model_', $camelModel, $content);
        ## change _Version_
        $content = str_replace('_Version_', $version, $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(app_path($dest), $content);

        $this->comment(
            "API Controller created successfully: {$dest}"
        );
    }
}
