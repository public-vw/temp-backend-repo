<?php

namespace App\Console\Commands\ApiStructure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateApiStructureRoute extends Command
{
    protected $signature = 'structure:api:route {panel} {model}';

    protected $description = 'Creates API Route File';

    public function handle()
    {
        $panel = $this->argument('panel');
        $panel = Str::ucfirst($panel);
        $panel = str_replace(' ', '', $panel);
        $panel = $panel !== 'Interface' ? $panel : 'InterfaceZone';
        $snake_panel = Str::snake($panel);

        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $table = model2table($model);

        $dest = "routes/{$snake_panel}";
        $full_dest = base_path($dest);
        $dest .= "/{$table}.php";

        # make folder
        runShell('mkdir -p ' . $full_dest);

        $content = file_get_contents(storage_path('templates/routes/api/_table_.php'));

        //## change _Model_
        $content = str_replace('_Model_', $model, $content);
        //## change _table_
        $content = str_replace('_table_', $table, $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(base_path($dest), $content);

        $this->comment(
            "API Route File created successfully: {$dest}"
        );
    }
}
