<?php

namespace App\Console\Commands\ApiStructure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateApiStructurePanel extends Command
{
    protected $signature = 'structure:api:panel {panel} {source_panel?}';

    protected $description = 'Creates New API Panel';

    public function handle()
    {
        $src_panel = $this->argument('source_panel') ?? 'Admin';
        $src_snake_panel = Str::snake($src_panel);

        $Panel = $this->argument('panel');
        $Panel = Str::ucfirst($Panel);
        $Panel = str_replace(' ', '', $Panel);
        $snake_panel = Str::snake($Panel);

        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $table = model2table($model);

        $base = app_path();

        # copy routes
        runShell("cp -a {$base}/routes/{{$src_snake_panel},{$snake_panel}}");

        # copy Controllers
        runShell("cp -a {$base}/app/Http/Controllers/{{$src_panel},{$Panel}}");
        ## change namespaces
        runShell("find {$base}/app/Http/Controllers/{$Panel} -type f -exec sed -i -e 's/{$src_panel}/{$Panel}/g' {} \;");

        $this->comment(
            "Panel created successfully: {$Panel}"
        );

        $this->doManual($Panel);
    }

    protected function doManual($Panel)
    {
        $panel = Str::snake($Panel);
        $camPanel = Str::camel($Panel);

        $this->comment('in RouteServiceProvider:');
        $this->comment("... copy function adminRoutes() -> {$camPanel}Routes()");
        $this->comment("... ... change admin to <{$panel}>");
        $this->comment("... add the {$camPanel}Routes() inside boot()");

        $this->comment("add <{$Panel}>'s routes to config/routes.php");

        $this->comment("define new role <{$panel}>");

        $this->comment('define route prefixes in config/routes.php');

        $this->comment("Note: this bash file only makes EN lang folder for <{$Panel}>");
    }
}
