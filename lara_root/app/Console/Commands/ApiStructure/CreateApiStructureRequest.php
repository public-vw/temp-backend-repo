<?php

namespace App\Console\Commands\ApiStructure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateApiStructureRequest extends Command
{
    protected $signature = 'structure:api:request {panel} {model} {version=1}';

    protected $description = 'Creates API Custom Request';

    public function handle()
    {
        $panel = $this->argument('panel');
        $panel = Str::ucfirst($panel);
        $panel = str_replace(' ', '', $panel);
        $panel = $panel !== 'Interface' ? $panel : 'InterfaceZone';

        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $snake = Str::snake($model);

        $version = $this->argument('version');
        $version = floatval($version);

        $dest = "Requests/{$panel}/{$model}";
        $full_dest = app_path($dest);

        # make folder
        runShell('mkdir -p ' . $full_dest);
        # copy View/Item
        runShell('cp -a ' . storage_path('templates/request_items/*') . ' ' . $full_dest . '/');
        ## change _ModelSpace_
        replaceInFiles($full_dest, '_Panel_', $panel, 'php');
        ## change _Model_
        replaceInFiles($full_dest, '_Model_', $model, 'php');
        ## change _model_
        replaceInFiles($full_dest, '_model_', $snake, 'php');
        ## change _Version_
        replaceInFiles($full_dest, '_Version_', $version, 'php');
        ## removes comments
        removeCommentsInFiles($full_dest, 'php');

        $this->comment(
            "Request Files created successfully: {$dest}"
        );
    }
}
