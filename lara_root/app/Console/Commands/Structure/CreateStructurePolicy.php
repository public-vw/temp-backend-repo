<?php

namespace App\Console\Commands\Structure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructurePolicy extends Command
{
    protected $signature = 'structure:policy {model}';

    protected $description = 'Creates Policy File';

    public function handle()
    {
        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $camelModel = Str::camel($model);

        $dest = "Policies/{$model}Policy.php";

        $content = file_get_contents(storage_path('templates/_Model_Policy.php'));

        //## change _Model_
        $content = str_replace('_Model_', $model, $content);
        //## change _model_
        $content = str_replace('_model_', $camelModel, $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(app_path($dest), $content);

        $this->comment(
            "Policy created successfully: {$dest}"
        );
    }
}
