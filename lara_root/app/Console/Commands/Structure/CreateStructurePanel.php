<?php

namespace App\Console\Commands\Structure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructurePanel extends Command
{
    protected $signature = 'structure:panel {panel} {source_panel?}';

    protected $description = 'Creates New Panel';

    public function handle()
    {
        $src_panel = $this->argument('source_panel') ?? 'Admin';
        $src_snake_panel = Str::snake($src_panel);

        $Panel = $this->argument('panel');
        $Panel = Str::ucfirst($Panel);
        $Panel = str_replace(' ', '', $Panel);
        $snake_panel = Str::snake($Panel);

        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $table = model2table($model);

        $base = app_path();

        # copy routes
        runShell("cp -a {$base}/routes/{{$src_snake_panel},{$snake_panel}}");

        # copy resources/lang/en/
        runShell("cp -a {$base}/resources/lang/en/{{$src_snake_panel},{$snake_panel}}");

        # copy resources/assets/
        runShell("cp -a {$base}/resources/assets/{{$src_snake_panel},{$snake_panel}}");

        # copy resources/views/panel
        runShell("cp -a {$base}/resources/views/{{$src_snake_panel},{$snake_panel}}");
        ## change titles
        runShell("find {$base}/resources/views/{$snake_panel} -type f -exec sed -i -e 's/{$src_panel}/{$Panel}/g' {} \;");
        ## change urls(assets paths)
        runShell("find {$base}/resources/views/{$snake_panel} -type f -exec sed -i -e 's/{$src_snake_panel}/{$snake_panel}/g' {} \;");

        # copy Controllers
        runShell("cp -a {$base}/app/Http/Controllers/{{$src_panel},{$Panel}}");
        ## change namespaces
        runShell("find {$base}/app/Http/Controllers/{$Panel} -type f -exec sed -i -e 's/{$src_panel}/{$Panel}/g' {} \;");
        ## change views root folder name
        runShell("find {$base}/app/Http/Controllers/{$Panel} -type f -exec sed -i -e 's/{$src_snake_panel}/{$snake_panel}/g' {} \;");

        # copy DataTables
        runShell("cp -a {$base}/app/DataTables/{{$src_panel},{$Panel}}");
        ## change namespaces
        runShell("find {$base}/app/DataTables/{$Panel} -type f -exec sed -i -e 's/{$src_panel}/{$Panel}/g' {} \;");
        ## change views root folder name
        runShell("find {$base}/app/DataTables/{$Panel} -type f -exec sed -i -e 's/{$src_snake_panel}/{$snake_panel}/g' {} \;");

        $this->comment(
            "Panel created successfully: {$Panel}"
        );

        $this->doManual($Panel);
    }

    protected function doManual($Panel)
    {
        $panel = Str::snake($Panel);
        $camPanel = Str::camel($Panel);

        $this->comment('in RouteServiceProvider:');
        $this->comment("... copy function adminRoutes() -> {$camPanel}Routes()");
        $this->comment("... ... change admin to <{$panel}>");
        $this->comment("... add the {$camPanel}Routes() inside boot()");

        $this->comment("add <{$Panel}>'s routes to config/routes.php");

        $this->comment("add <{$Panel}>'s assets to webpack.mix.js");

        $this->comment("define new role <{$panel}>");

        $this->comment('define route prefixes in config/routes.php');

        $this->comment("adds sidebar seeder and add <{$camPanel}>Composer in ViewComposers");

        $this->comment("define <{$camPanel}>Composer in ComposerServiceProvider");

        $this->comment("Note: this bash file only makes EN lang folder for <{$Panel}>");
    }
}
