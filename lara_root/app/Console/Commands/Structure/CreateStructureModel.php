<?php

namespace App\Console\Commands\Structure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureModel extends Command
{
    protected $signature = 'structure:model {model}';

    protected $description = 'Creates Model';

    public function handle()
    {
        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);

        $dest = "Models/{$model}.php";

        $content = file_get_contents(storage_path('templates/_Model_.php'));

        //## change _Model_
        $content = str_replace('_Model_', $model, $content);
        //## change _model_
        $content = str_replace('_model_', Str::lower($model), $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(app_path($dest), $content);

        $this->comment(
            "Model created successfully: {$dest}"
        );
    }
}
