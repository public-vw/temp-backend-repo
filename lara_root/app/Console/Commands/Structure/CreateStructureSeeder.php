<?php

namespace App\Console\Commands\Structure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureSeeder extends Command
{
    protected $signature = 'structure:seeder {model}';

    protected $description = 'Creates Seeder File';

    public function handle()
    {
        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);

        $dest = "seeders/{$model}Seeder.php";

        $content = file_get_contents(storage_path('templates/_Model_Seeder.php'));

        //## change _Model_
        $content = str_replace('_Model_', $model, $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(database_path($dest), $content);

        $this->comment(
            "Seeder created successfully: {$dest}"
        );
    }
}
