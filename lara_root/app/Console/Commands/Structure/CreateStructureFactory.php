<?php

namespace App\Console\Commands\Structure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureFactory extends Command
{
    protected $signature = 'structure:factory {model}';

    protected $description = 'Creates Factory Files';

    public function handle(): void
    {
        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);

        $this->createFactory($model);
    }

    protected function createFactory($model): void
    {
        $dest = "factories/{$model}Factory.php";

        $content = file_get_contents(storage_path('templates/_Model_Factory.php'));

        //## change _Model_
        $content = str_replace('_Model_', $model, $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(database_path($dest), $content);

        $this->comment(
            "Factory created successfully: {$dest}"
        );
    }
}
