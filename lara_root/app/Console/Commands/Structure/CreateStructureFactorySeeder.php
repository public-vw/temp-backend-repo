<?php

namespace App\Console\Commands\Structure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureFactorySeeder extends Command
{
    protected $signature = 'structure:fake:seeder {model}';

    protected $description = 'Creates Fake Seeder File';

    public function handle()
    {
        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);

        $dest = "seeders/FactoringSeeders/{$model}Seeder.php";

        $content = file_get_contents(storage_path('templates/_Model_FakeSeeder.php'));

        //## change _Model_
        $content = str_replace('_Model_', $model, $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(database_path($dest), $content);

        $this->comment(
            "Fake Seeder created successfully: {$dest}"
        );
    }
}
