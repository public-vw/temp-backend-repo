<?php

namespace App\Console\Commands\Structure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureRequest extends Command
{
    protected $signature = 'structure:request {panel} {model}';

    protected $description = 'Creates Custom Request';

    public function handle()
    {
        $panel = $this->argument('panel');
        $panel = Str::ucfirst($panel);
        $panel = str_replace(' ', '', $panel);

        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $snake = Str::snake($model);

        $dest = "Requests/{$panel}";
        $full_dest = app_path($dest);
        $dest = "{$dest}/{$model}Request.php";

        $content = file_get_contents(storage_path('templates/_Model_Request.php'));

        runShell('mkdir -p '.$full_dest);
        ## change _Panel_
        $content = str_replace('_Panel_', $panel, $content);
        ## change _Model_
        $content = str_replace('_Model_', $model, $content);
        ## change _model_
        $content = str_replace('_model_', $snake, $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(app_path($dest), $content);

        $this->comment(
            "Custom Request created successfully: {$dest}"
        );
    }
}
