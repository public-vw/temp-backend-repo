<?php

namespace App\Console\Commands\Structure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureTransformer extends Command
{
    protected $signature = 'structure:transformer {panel} {model}';

    protected $description = 'Creates Transformer';

    public function handle(): void
    {
        $panel = $this->argument('panel');
        $panel = str_replace(['-', '_'], ' ', $panel);
        $panel = Str::ucfirst($panel);
        $panel = str_replace(' ', '', $panel);
        $panel = $panel !== 'Interface' ? $panel : 'InterfaceZone';

        $model = $this->argument('model');
        $model = str_replace(['-', '_'], ' ', $model);
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $snake = Str::camel($model);

        $dest = "Transformers/{$panel}/{$model}";
        $full_dest = app_path($dest);

        # make folder
        runShell('mkdir -p ' . $full_dest);
        # copy View/Item
        runShell('cp -a ' . storage_path('templates/transform_items/*') . ' ' . $full_dest . '/');
        ## change _ModelSpace_
        replaceInFiles($full_dest, '_Panel_', $panel, 'php');
        ## change _Model_
        replaceInFiles($full_dest, '_Model_', $model, 'php');
        ## change _model_
        replaceInFiles($full_dest, '_model_', $snake, 'php');
        ## removes comments
        removeCommentsInFiles($full_dest, 'php');

        $this->comment(
            'Transformer Files created successfully: ' . $dest
        );
    }
}
