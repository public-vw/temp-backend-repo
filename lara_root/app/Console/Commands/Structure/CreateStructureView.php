<?php

namespace App\Console\Commands\Structure;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureView extends Command
{
    protected $signature = 'structure:views {panel} {model}';

    protected $description = 'Creates View Folder';

    public function handle()
    {
        $panel = $this->argument('panel');
        $panel = Str::ucfirst($panel);
        $panel = str_replace(' ', '', $panel);
        $snake_panel = Str::snake($panel);

        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $snake_model = Str::snake($model);
        $camelModel = Str::camel($model);
        $table = model2table($model);
        $space_model = ucwords(str_replace('_', ' ', $snake_model));

        $dest = "resources/views/{$snake_panel}/items/{$table}";
        $full_dest = base_path($dest);

        # copy View/Item
        runShell('cp -a '.storage_path('templates/view_items/_table_').' '.$full_dest);
        ## change _ModelSpace_
        runShell('find "'.$full_dest.'" -type f -exec sed -i -e "s/_ModelSpace_/'.$space_model.'/g" {} \;');
        ## change _model_
        runShell('find "'.$full_dest.'" -type f -exec sed -i -e "s/_model_/'.$camelModel.'/g" {} \;');
        ## removes comments
        runShell('find "'.$full_dest.'" -type f -exec sed -i -e "/^#/d" {} \;');

        $dest = "resources/lang/en/{$snake_panel}/datatables/{$table}";
        $full_dest = base_path($dest);

        # copy Language Items
        runShell('cp -a '.storage_path('templates/lang_datatables/_table_').' '.$full_dest);
        ## removes comments
        runShell('find "'.$full_dest.'" -type f -exec sed -i -e "/^#/d" {} \;');

        $this->comment(
            "View Files created successfully: {$dest}"
        );
    }
}
