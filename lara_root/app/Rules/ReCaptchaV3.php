<?php

namespace App\Rules;

use Closure;
use GuzzleHttp\Client;
use Illuminate\Contracts\Validation\ValidationRule;

class ReCaptchaV3 implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (config('recaptchav3.enable', true) === false) {
            return;
        }

        $client = new Client(['base_uri' => config('recaptchav3.origin')]);

        $response = $client->post('/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => config('recaptchav3.secret_key'),
                'response' => $value,
            ],
        ]);

        $body = json_decode((string) $response->getBody());

        if (! $body->success) {
            $fail('reCAPTCHA verification required.');
        }
    }
}
