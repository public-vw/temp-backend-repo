<?php

namespace App\Models;

use App\Models\Contracts\AbsContentModel;
use App\Models\Traits\Attachmentable;
use App\Models\Traits\HasContentContainer;
use App\Models\Traits\HasStatus;
use App\Models\Traits\Notable;
use App\Models\Traits\RichSnippetable;
use App\Models\Traits\Seoable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ArticleCategoryContent extends AbsContentModel
{
    use HasFactory;
    use Attachmentable;
    use HasStatus;
    use Seoable;
    use RichSnippetable;
    use Notable;
    use HasContentContainer;

    protected $fillable = [
        'article_category_id',
        'version',
        'uri',
        'title', //usage: breadcrumb
        'heading',
        'summary',
        'description',
        'status',
    ];
}
