<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuPlaceholder extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    protected $fillable = [
        'panel_id',
        'slug',
    ];

    # -----------------------------------

    public function panel(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Panel::class);
    }

    public function menus(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Menu::class, 'placeholder_id');
    }
}
