<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    protected $fillable = [
        'user_id',
        'likable_type',
        'likable_id',
        'liked',
    ];

    # -----------------------------------

    public function likable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function toggle(?bool $force = null): bool
    {
        $this->liked = $force !== null ? $force : ! $this->liked;
        $this->save();

        return (bool) $this->liked;
    }
}
