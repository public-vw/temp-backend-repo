<?php

namespace App\Models;

use App\Models\Contracts\IUrlable;
use App\Models\Traits\Menuable;
use App\Models\Traits\Urlable;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class StaticLink extends Model implements IUrlable
{
    use HasFactory;
    use CacheQueryBuilder;
    use Urlable;
    use Menuable;

    protected $fillable = [
        'title',
    ];

    # -----------------------------------

    public function generateUrl(): string
    {
        return '/link/'.Str::slug($this->title);
    }
}
