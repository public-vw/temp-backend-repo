<?php

namespace App\Models;

use App\Notifications\CustomResetPassword;
use App\Notifications\VerifyEmail;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable // implements MustVerifyEmail
{
    use CrudTrait;
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles;

    protected $fillable = [
        'firstname',
        'lastname',

        'username',
        'nickname',
        'password',
        'ref_code',

        'mobile',
        'email',

        'random_color',

        'telegram_uid',
        'referer_id',

        'google_secret_key',
        'use_two_factor_auth',

        'status',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'mobile_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    protected $appends = [
        'name',
    ];

//    _____________________

    public function sendEmailVerificationNotification(): void
    {
        $this->notify(new VerifyEmail());
    }

    public function subscriptions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Subscription::class);
    }

    public function likes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Like::class);
    }

    public function bookmarks(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Bookmark::class);
    }
    public function getNicknameAttribute($value): string
    {
        return $value ? $value : 'User' . $this->id;
    }

    public function panels()
    {
        $panels = Panel::where(['active' => true])->get();
        $user = $this;
        return $panels->filter(function ($panel) use ($user) {
            return $user->hasRole($panel->title);
        });
    }

    public function articles(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Article::class, 'author_id');
    }

    public function getNameAttribute(): string
    {
        return $this->firstname . ($this->lastname ? ' ' . $this->lastname : '');
    }

    public function hasState($state): bool
    {
        return $this->status === config_key('enums.users.status', $state);
    }

    public function sendPasswordResetNotification($token): void
    {
        $this->notify(new CustomResetPassword($token));
    }
}
