<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Panel extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'active',
    ];

    # -----------------------------------

    public function menus()
    {
        return $this->hasManyThrough(Menu::class, MenuPlaceholder::class, 'panel_id', 'placeholder_id', 'id')
            ->whereNull('parent_id')
            ->orderBy('order');
    }

    public function menuPlaceholders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(MenuPlaceholder::class, 'panel_id');
    }
}
