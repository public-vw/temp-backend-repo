<?php

namespace App\Models\Traits;

trait SlugAsRouteKey
{
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
