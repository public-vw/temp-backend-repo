<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait HasContent
{
    public const PUBLISHABLE_MODELS = ['Article', 'ArticleCategory', 'StaticPage'];

    public function contents(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        $model = static::class . 'Content';
        $key = Str::snake(class2config(static::class)) . '_id';
        return $this->hasMany($model, $key);
    }

    public function getFinalContentAttribute(): Model|\Illuminate\Database\Eloquent\Relations\HasMany|null
    {
        return $this->contents()->orderBy('version')->first();
    }

    public function scopePublished($query)
    {
        return $query->whereHas('contents', function ($q) {
            $q->where('version', 1)->where('status', config_key('enums.contents.status', 'active'));
        });
    }

    public function isPublishable(): bool
    {
        $model = class2model(static::class);
        return in_array($model, self::PUBLISHABLE_MODELS);
    }

    public function getLastVersion(): Model
    {
        return $this->contents()->orderBy('version', 'DESC')->first();
    }

    public function getPublishedAttribute(): bool
    {
        return $this->isPublishable() ? $this->final_content->checkStatus('active') : (bool) $this->final_content->id;
    }

    public function getNextNewVersionNumber(): int
    {
        return $this->getLastVersion()['version'] + 1;
    }
}
