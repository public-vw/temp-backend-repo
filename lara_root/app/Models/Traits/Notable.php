<?php

namespace App\Models\Traits;

use App\Models\Note;
use Illuminate\Database\Eloquent\Collection;

trait Notable
{
    public function notes(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Note::class, 'notable');
    }

    public function getAcceptedNotesAttribute(): Collection
    {
        return $this->notes()->where('status', config_key('enums.notes.status', 'accepted'))->get();
    }
}
