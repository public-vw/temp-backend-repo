<?php

namespace App\Models\Traits;

use App\Models\SeoDetail;
use Illuminate\Database\Eloquent\Collection;

trait Seoable
{
    public function seoDetails(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(SeoDetail::class, 'seoable');
    }

    public function getSeoAttribute(): Collection
    {
        $seo = [];
        foreach (config('enums.seo_details.types') as $type) {
            $record = $this->seoDetails()
                ->where('type', config_key('enums.seo_details.types', $type))
                ->orderByDesc('id')
                ->first();
            if (! $record) {
                continue;
            }
            $seo[$type] = $record['data'];
        }
        return new Collection($seo);
    }
}
