<?php

namespace App\Models\Traits;

trait GetNextId
{
    public function getNextId()
    {
        $statement = static::select('id')->orderBy('id', 'desc')->first();
        return ($statement['id'] ?? 0) + 1;
    }
}
