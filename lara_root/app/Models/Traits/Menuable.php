<?php

namespace App\Models\Traits;

use App\Models\Menu;

trait Menuable
{
    public function menu(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Menu::class, 'menuable');
    }
}
