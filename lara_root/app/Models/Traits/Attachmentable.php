<?php

namespace App\Models\Traits;

use App\Models\Attachable;
use App\Models\Attachment;

trait Attachmentable
{
    public function attachables(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Attachable::class, 'attachable');
    }

    public function attachments(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Attachment::class);
    }

    public function filterByMedia($media): \Illuminate\Database\Eloquent\Relations\MorphMany|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->attachables()
            ->whereHas('attachment', function ($query) use ($media) {
                $query->where('media', config_key('enums.attachments.medias', $media));
            });
    }
}
