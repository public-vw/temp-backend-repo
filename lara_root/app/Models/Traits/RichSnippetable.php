<?php

namespace App\Models\Traits;

use App\Models\SeoRichSnippet;
use Illuminate\Database\Eloquent\Collection;

trait RichSnippetable
{
    public function richSnippets(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(SeoRichSnippet::class, 'richsnippetable');
    }

    public function getRichSnippetsAttribute(): Collection
    {
        $seo = [];
        foreach (config('enums.rich_snippets.types') as $type) {
            $record = $this->richSnippets()
                ->where('type', config_key('enums.rich_snippets.types', $type))
                ->orderByDesc('id')
                ->first();
            if (! $record) {
                continue;
            }
            $seo[$type] = $record['data'];
        }
        return new Collection($seo);
    }
}
