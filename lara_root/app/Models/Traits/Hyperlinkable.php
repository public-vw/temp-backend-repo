<?php

namespace App\Models\Traits;

use App\Models\Hyperlink;

trait Hyperlinkable
{
    public function hyperlinks()
    {
        return $this->morphMany(Hyperlink::class, 'hyperlinkable');
    }
}
