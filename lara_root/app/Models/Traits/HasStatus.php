<?php

namespace App\Models\Traits;

trait HasStatus
{
    public static function getStatusList()
    {
        $statuses = static::select('status')
            ->orderBy('status')
            ->get()
            ->countBy('status')
            ->toArray();
        $statuses['all'] = static::select('id')->count();

        return $statuses;
    }

    // returns old and new status
    public function updateStatus(string $status): array
    {
        return [
            $this,
            $this->getStatus(),
            $this->setStatus($status)->getStatus(),
        ];
    }

    public function getStatus(): string
    {
        return config('enums.contents.status.'.$this->status);
    }

    public function setStatus(string $status): static
    {
        $this->status = config_key('enums.contents.status', $status);
        $this->save();
        return $this;
    }

    public function checkStatus(string $status): bool
    {
        return $this->status === config_key('enums.contents.status', $status);
    }
}
