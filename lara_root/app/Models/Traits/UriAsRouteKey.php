<?php

namespace App\Models\Traits;

trait UriAsRouteKey
{
    public function getRouteKeyName()
    {
        return 'uri';
    }
}
