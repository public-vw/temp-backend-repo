<?php

namespace App\Models\Traits;

use App\Models\Language;
use App\Models\Translate;

trait Translatable
{
    # how to make a field, translatable
    # 1. in model; add it to protected $translatable array
    # 2. in model; add it to protected $casts array as array
    # 3. in migration; the field should be big enough ( about 10 char for 1 language )
    # *. (under test | don't do this until causing issue) in migration; the field should not be unique ( not really sure, just faild because of this on one of tests, but after that I found and solved another bug that caused duplicate content. needs more test )

    #TODO: some improvements of performance maybe

    #in blade; check if a field is translatable or not
    //@if(isset($class) && in_array($NAME,('\\App\\Models\\'.$class)::$translatable ))
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if (isset($this->translatable)) {
            [$key, $code] = $this->extractKey($key);
            if (in_array($key, $this->translatable)) {
                $value = parent::getAttribute($key);
                $value = $this->getValueOfCurrentLocale($key, $value, $code);
            }
        }

        return $value;
    }

    /*    protected function isJsonCastable($key)
        {
            if (isset($this->translatable)){
                list($key, $code) = $this->extractKey($key);
                if(in_array($key, $this->translatable)) {
                    return true;
                }
            }

            return parent::isJsonCastable($key);
        }*/

    public function getValueOfCurrentLocale($key, $value, $code)
    {
        $values = explode('|', $value);
        $values_assoc = [];
        foreach ($values as $value) {
            [$k, $v] = explode(':', $value);
            $values_assoc[$k] = $v;
        }

        if (is_null($code)) {
            $codes = Language::codes();
            $code = $codes['currentLocale'];
            if (strpos($value, ":{$code}") === false) {
                $code = $codes['fallbackLocale'];
            }
        }

        $translation = Translate::find($values_assoc[$code] ?? null);

        if (! $translation) {
            return '';
        }

        return $translation->value;
    }

    public function setAttribute($key, $value)
    {
        if (isset($this->translatable)) {
            if (in_array($key, $this->translatable)) {
                $translations = $this->setValueOfCurrentLocale($key, $value);
                $value = implode('|', $translations);
            }
        }
        return parent::setAttribute($key, $value);
    }

    public function setValueOfCurrentLocale($key, $value)
    {
        $translations = [];
        $languages = Language::getList();
        foreach ($languages as $language) {
            $tmp = Translate::where(
                [
                    'language_id' => $language->id,
                    'translatable_type' => static::class,
                    'field' => $key,
                    'translatable_id' => $this->id,
                ]
            )->first();
            if (is_null($this->id) || ! $tmp) {
                $tmp = Translate::create(
                    [
                        'language_id' => $language->id,
                        'field' => $key,
                        'value' => $value[$language->code],
//                        'translatable_id'   => $this->getNextId(),
//                        'translatable_type' => get_class($this),
                    ]
                );
            } else {//Update
                $tmp->update(
                    [
                        'value' => $value[$language->code],
                    ]
                );
            }
            $tmp->translatable()->associate($this);
            $tmp->save();
            $translations[] = "{$language->code}:{$tmp->id}";
        }

        return $translations;
    }

    protected function extractKey($key): array
    {
        $key = preg_replace("/(.*)\[[\'\"]?(.*?)[\'\"]?\]/", '$1.$2', $key);
        $key = explode('.', $key);
        $code = $key[1] ?? null;
        $key = $key[0];
        return [$key, $code];
    }
}
