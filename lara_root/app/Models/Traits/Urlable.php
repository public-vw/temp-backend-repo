<?php

namespace App\Models\Traits;

use App\Models\Url;
use Illuminate\Support\Str;

trait Urlable
{
    //Use-case: sometimes, the Urlable comes to creation without URI value. this helps to generate a dummy one
    public function getUriAttribute($value)
    {
        return $value ? $value : Str::slug(class2config(static::class)) . '-uri-' . $this->id;
    }

    public function getUrlAttribute()
    {
        if ($this->urls()->count() === 0) {
            return null;
        }

        $latestUrl = $this->urls()->orderByDesc('updated_at')->first();
        return $latestUrl->url;
    }

    public function urls(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Url::class, 'urlable');
    }
    //TODO: check if disabling this cause any issue on update of urlables or not
//    public function setUriAttribute($value): void
//    {
//        $this->attributes['uri'] = $value;
//        $this->addUrl();
//    }

    public function addUrl($custom_url = null): ?\Illuminate\Database\Eloquent\Model
    {
        if ($this->urls->last('url') !== ($final_url = $this->generateUrl($custom_url))) {
            return $this->urls()->create([
                'url' => $final_url,
            ]);
        }
        return null;
    }
}
