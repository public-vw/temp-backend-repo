<?php

namespace App\Models\Traits;

use App\Models\StarRate;

trait Rankable
{
    public function starRates()
    {
        return $this->morphMany(StarRate::class, 'rankable');
    }

    public function getRankAttribute(): array
    {
        $weights = $this->starRates()->sum('weight');
        $sum = $this->starRates()->selectRaw('rate*weight as rank')->get(['rank'])->sum('rank');

        $rank = $weights ? round($sum / $weights, 1) : 0;
        return ['rank' => $rank, 'total' => $weights];
    }
}
