<?php

namespace App\Models\Traits;

use App\Models\Comment;

trait Commentable
{
    public function comments(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function getAcceptedCommentsAttribute(): \Illuminate\Database\Eloquent\Collection
    {
        return $this->comments()->where('status', config_key('enums.comments.status', 'accepted'))->get();
    }
}
