<?php

namespace App\Models\Traits;

trait SelfParentChild
{
    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function hasParent(): bool
    {
        return (bool) $this->parent_id;
    }
    public function hasChild(): bool
    {
        return count($this->children) !== 0;
    }
}
