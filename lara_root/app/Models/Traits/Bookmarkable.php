<?php

namespace App\Models\Traits;

use App\Models\Bookmark;

trait Bookmarkable
{
    public function bookmark(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Bookmark::class, 'bookmarkable');
    }

    public function isBookmarkedByCurrentUser(): bool
    {
        return (bool) auth()->user()?->bookmarks()->where('bookmarkable_id', $this->id)->exists();
    }
}
