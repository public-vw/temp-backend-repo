<?php

namespace App\Models\Traits;

use App\Models\Like;

trait Likable
{
    public function likes(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Like::class, 'likable');
    }

    public function isLikedByCurrentUser(): bool
    {
        return (bool) auth()->user()?->likes()->where('likable_id', $this->id)->exists();
    }
}
