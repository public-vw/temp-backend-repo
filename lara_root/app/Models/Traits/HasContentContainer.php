<?php

namespace App\Models\Traits;

use Illuminate\Support\Str;

trait HasContentContainer
{
    public function container(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        $class = Str::replace('Content', '', static::class);
        $field = Str::snake(class2config($class)) . '_id';
        return $this->belongsTo($class, $field);
    }
}
