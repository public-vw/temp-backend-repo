<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachable extends Model
{
    use HasFactory;

    protected $fillable = [
        'attachment_id',

        'attachable_type',
        'attachable_id',

        'type_id',

        'attributes',
    ];

    protected $casts = ['attributes' => 'array'];

    public function attachment(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Attachment::class);
    }

    public function type(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(AttachmentType::class, 'type_id');
    }
}
