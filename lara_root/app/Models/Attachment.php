<?php

namespace App\Models;

use App\Models\Contracts\IUrlable;
use App\Models\Traits\RichSnippetable;
use App\Models\Traits\Siblings;
use App\Models\Traits\Urlable;
use App\Support\Database\CacheQueryBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Attachment extends Model implements IUrlable
{
    use HasFactory;
    use CacheQueryBuilder;
    use Siblings;
    use Urlable;
    use RichSnippetable;

    protected $fillable = [
        'uri',
        'origin_id',

        'media',

        'user_id',
        'storage_disk',
        'file_name',
        'file_ext',
        'file_size',
        'properties',
        'md5',
        'quality_rank',
        'mimetype',
        'original_name',
        'original_ext',

        'noindex',
    ];

    protected $casts = [
        'quality_rank' => 'int',
        'properties' => 'array',
    ];

    # -----------------------------------

    public static function clearOldTemporaries($daysBefore = 7)
    {
        $temporaryRecords = self::where('temporary', true)->where('updated_at', '<', Carbon::now()->addDays(-1 * $daysBefore))->get();

        if ($temporaryRecords) {
            foreach ($temporaryRecords as $temporaryRecord) {
                $temporaryRecord->hardDelete();
            }
        }

        return $temporaryRecords->plunk('fullname')->toArray();
    }

    public function hardDelete(): ?bool
    {
        if (! $this->origin_id) {
            // it's not origin, then has no related file directly
            return $this->delete();
        }
        return $this->deleteFromStorage() && $this->delete();
    }

    public function deleteFromStorage(): bool
    {
        $result = true;
        foreach (config_keys_all('enums.attachments.image_sizes.16_9') as $key) {
            $result = $result && Storage::disk($this->storage_disk)->delete($this->getPath($key));
        }
        return $result && Storage::disk($this->storage_disk)->delete($this->getPath());
    }

    public static function saveAs(Attachment $srcAttachment)
    {
        return Attachment::create([
            'uri' => null,
            'origin_id' => $srcAttachment->id,

            // fills in the controller
            'attachmentable_type' => null,
            'attachmentable_id' => null,
            'user_id' => null,

            'media' => $srcAttachment->media,

            'storage_disk' => $srcAttachment->storage_disk,
            'file_name' => $srcAttachment->file_name,
            'file_ext' => $srcAttachment->file_ext,
            'file_size' => $srcAttachment->file_size,
            'properties' => $srcAttachment->properties,
            'md5' => $srcAttachment->md5,
            'quality_rank' => null,//should read from origin attachment
            'mimetype' => $srcAttachment->mimetype,
            'original_name' => $srcAttachment->original_name,
            'original_ext' => $srcAttachment->original_ext,

            'noindex' => $srcAttachment->noindex,
        ]);
    }

    public function attachables(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Attachable::class);
    }

    public function origin(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(self::class, 'origin_id', 'id');
    }

    public function mediaName(): string
    {
        return config('enums.attachments.medias.' . $this->media);
    }

    public function getQualityRankAttribute(): int
    {
        return $this->origin ? $this->origin->quality_rank : $this->quality_rank;
    }

    public function setFileNameAttribute($value): void
    {
        $this->attributes['file_name'] = $value ? $value : $this->generateFilename();
    }

    public function mediaFolder(): string
    {
        return Str::slug(Str::plural($this->mediaName()), '-');
    }

    public function generateFilename(): string
    {
        return $this->mediaFolder() . '/' . Carbon::now()->format('Y-m/d-His-') . rand(1001, 9999);
    }

    public function putToStorage(string $data, $size_label = null, $extension = null): bool
    {
        return Storage::disk($this->storage_disk)->put($this->getPath($size_label, $extension), $data);
    }

    public function getPath($size_label = null, $extension = null): string
    {
        $origin_attachment = $this->origin ?: $this;

        $size_ext = '';
        if ($size_label) {
            [$width, $height] = config('enums.attachments.image_sizes.16_9.' . $size_label);
            $size_ext .= '_' . $width;
            $size_ext .= '_' . $height;
        }

        return $origin_attachment->file_name . $size_ext . '.' . ($extension ?? $origin_attachment->file_ext);
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function generateUrl(?string $custom_url = null): string
    {
        return $custom_url ? $custom_url : '/' . $this->mediaFolder() . '/' . $this->uri . '.' . $this->file_ext;
    }

    public function getFromStorage($size_label = 'large'): ?string
    {
        return Storage::disk($this->storage_disk)->get($this->getPath($size_label));
    }
    // -----------------------------------

    public function makePermanent(string $destination_storage = 'permanent'): bool
    {
        if (! $this->temporary) {
            return true;
        }

        if ($this->storage_disk === $destination_storage) {
            $this->temporary = false;
            $this->save();
            return true;
        }

        if (
            Storage::disk($destination_storage)->writeStream(
                $this->fullname,
                Storage::disk($this->storage_disk)->readStream($this->fullname)
            )
        ) {
            Storage::disk($this->storage_disk)->delete($this->fullname);

            $this->temporary = false;
            $this->storage_disk = $destination_storage;
            $this->save();

            return true;
        }
        return false;
    }

    public function setOriginalNameAttribute($value)
    {
        #TODO fix $value = null
        $this->attributes['original_name'] = base64_encode($value);
    }
}
