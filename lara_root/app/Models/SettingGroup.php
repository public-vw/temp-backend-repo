<?php

namespace App\Models;

use App\Models\Traits\SelfParentChild;
use App\Support\Database\CacheQueryBuilder;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingGroup extends Model
{
    use CrudTrait;
    use HasFactory;
    use CacheQueryBuilder;
    use SelfParentChild;

    # TODO fix undefined variable id
//    use DeepCacheQuery;
//    static $deepCacheTTL = 0;

    protected $fillable = [
        'parent_id',
        'title',
        'slug',
        'order',
        'hidden',
        'for_frontend',
    ];

    protected $appends = ['full_slug'];

    # -----------------------------------

    public static function getAll($slug)
    {
        return self::where('slug', $slug)->first()->settings;
    }

    public static function get($slug): self
    {
        $slugs = explode('.', $slug);
        $group = null;

        while (! empty($slugs)) {
            $temp_slug = array_shift($slugs);
            $group = SettingGroup::where(
                is_null($group) ? [
                    'slug' => $temp_slug,
                ] : [
                    'slug' => $temp_slug,
                    'parent_id' => $group->id,
                ]
            )->first();
        }

        return $group;
    }

    public function settings(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Setting::class, 'group_id');
    }

    public function allSettings(): array
    {
        $output = [];

        foreach ($this->settings as $setting) {
            $output[$setting->slug] = $setting->value;
        }

        return $output;
    }

    public function getFullSlugAttribute()
    {
        $slug = $this->slug;

        $parent = $this->parent;
        while ($parent) {
            $slug = $parent->slug . '.' . $slug;
            $parent = $parent->parent;
        }

        return $slug;
    }
}
