<?php

namespace App\Models;

use App\Models\Contracts\AbsContentModel;
use App\Models\Traits\Attachmentable;
use App\Models\Traits\HasContentContainer;
use App\Models\Traits\HasStatus;
use App\Models\Traits\Notable;
use App\Models\Traits\RichSnippetable;
use App\Models\Traits\Seoable;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StaticPageContent extends AbsContentModel
{
    use HasFactory;
    use CacheQueryBuilder;
    use Attachmentable;
    use Seoable;
    use RichSnippetable;
    use HasStatus;
    use Notable;
    use HasContentContainer;

    protected $fillable = [
        'static_page_id',
        'version',
        'uri',
        'heading',
        'reading_time',
        'content',
        'raw_content',
        'data',
        'status',
    ];

    protected $casts = ['content' => 'array', 'data' => 'array'];

    # -----------------------------------

    public function staticPage(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(StaticPage::class);
    }
}
