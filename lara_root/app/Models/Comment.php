<?php

namespace App\Models;

use App\Models\Traits\Likable;
use App\Models\Traits\SelfParentChild;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory;
    use SoftDeletes;
    use SelfParentChild;
    use Likable;

    protected $fillable = [
        'user_id',
        'commentable',
        'content',
        'parent_id',
        'status',
    ];

    # -----------------------------------

    public function commentable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
