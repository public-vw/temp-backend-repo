<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BotConnection extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    protected $fillable = [
        'title',
        'username',
        'type',
        'robot_token',
        'parameters',
        'webhook_token',
        'active',
    ];
    protected $casts = ['parameters' => 'array'];

    # -----------------------------------

    public function getParamsAttribute(): array
    {
        $tmp = explode(',', $this->parameters);
        $ret = [];
        foreach ($tmp as $t) {
            $t = explode(':', $t);
            if (isset($t[0]) && isset($t[1])) {
                $ret[$t[0]] = $t[1];
            } else {
                Log::warning('Bot Parameters wrong structure. parameter will be ignored.');
            }
        }

        return $ret;
    }
}
