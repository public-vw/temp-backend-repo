<?php

namespace App\Models;

use App\Models\Contracts\AbsHasContentModel;
use App\Models\Contracts\IUrlable;
use App\Models\Traits\Bookmarkable;
use App\Models\Traits\Commentable;
use App\Models\Traits\HasContent;
use App\Models\Traits\Likable;
use App\Models\Traits\Urlable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends AbsHasContentModel implements IUrlable
{
    use HasFactory;
    use Urlable;
    use Commentable;
    use HasContent;
    use Bookmarkable;
    use Likable;

    protected $fillable = [
        'author_id',
        'category_id',
    ];

    protected $appends = ['final_content', 'published', 'published_category'];

    public function author(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ArticleCategory::class, 'category_id');
    }

    public function setPublishedCategoryAttribute(): ArticleCategory|null
    {
        $currentCategory = $this->category;

        while ($currentCategory !== null) {
            if ($currentCategory->published) {
                return $currentCategory;
            }
            $currentCategory = $currentCategory->parent;
        }

        return null;
    }

    public function relatedArticles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Article::class, 'article_relation', 'src_id', 'dest_id');
    }

    public function generateUrl(): string
    {
        return $this->category->url . '/' . $this->final_content->uri . (! config('routes.appearance.pdp.trailing_html', false) ?: '.html');
    }
}
