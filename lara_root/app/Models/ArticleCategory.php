<?php

namespace App\Models;

use App\Models\Contracts\AbsHasContentModel;
use App\Models\Contracts\IUrlable;
use App\Models\Traits\HasContent;
use App\Models\Traits\Hyperlinkable;
use App\Models\Traits\SelfParentChild;
use App\Models\Traits\Siblings;
use App\Models\Traits\Urlable;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ArticleCategory extends AbsHasContentModel implements IUrlable
{
    use HasFactory;
    use CacheQueryBuilder;
    use Siblings;
    use Urlable;
    use Hyperlinkable;
    use SelfParentChild;
    use HasContent;

    protected $fillable = [
        'parent_id',
        'order',
    ];

    protected $appends = ['final_content', 'published'];

    # -----------------------------------

    public function activeArticles(): \Illuminate\Database\Eloquent\Builder
    {
        return $this->articles()->published()->getQuery();
    }

    public function articles(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Article::class, 'category_id', 'id');
    }

    public function publishedChildren(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->children()->published();
    }

    public function publishedParent(): ArticleCategory|null
    {
        $currentParent = $this->parent;

        while ($currentParent !== null) {
            if ($currentParent->published) {
                return $currentParent;
            }
            $currentParent = $currentParent->parent;
        }

        return null;
    }

    public function generateUrl(): string
    {
        return $this->publishedParent()?->url . '/' . $this->final_content->uri;
    }

    # --------------- Prev Functions -----------------

    public function deepArticlesCount(): int
    {
        $count = 0;
        $this->countArticles($this, $count);
        return $count;
    }

    protected function countArticles(ArticleCategory $category, &$count): void
    {
        $count += $category->articles()->count();
        if ($category->hasChild()) {
            foreach ($category->children as $childCat) {
                $this->countArticles($childCat, $count);
            }
        }
    }
}
