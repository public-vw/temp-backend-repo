<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    protected $fillable = [
        'parent_id',
        'title',
        'type',
    ];

    protected $appends = ['count'];

    # -----------------------------------

    public function parent()
    {
        return $this->belongsTo(Structure::class, 'parent_id');
    }

    public function getCountAttribute()
    {
        if ($this->type !== 'model') {
            return null;
        }
        $model = model2class($this->title);
        return (new $model())->count();
    }

    public function migrations()
    {
        $term = "'" . model2table($this->title) . "', function";
        return filesContains(database_path('migrations'), $term, ['.php']);
    }

    public function models()
    {
        $term = $this->title;
        return folderContains(app_path('Models'), $term, ['.php']);
    }

    public function factories()
    {
        $term = "{$this->title}::class";
        return filesContains(database_path(), $term, ['.php'], 'database', 1);
    }

    public function routes()
    {
        $term = "('" . model2table($this->title);
        return filesContains(base_path('routes'), $term, ['.php'], null, 2);
    }

    public function controllers()
    {
        $term = model2table($this->title);
        return filesContains(app_path('Http'), $term, ['.php'], 'Controllers', 1);
    }

    public function datatables()
    {
        $term = model2table($this->title);
        return filesContains(app_path('DataTables'), $term, ['.php'], 'Datatables', 1);
    }

    public function requests()
    {
        $term = $this->title . ';';
        return filesContains(app_path('Http/Requests'), $term, ['.php']);
    }

    public function transformers()
    {
        $term = $this->title . ';';
        return filesContains(app_path('Http/Transformers'), $term, ['.php']);
    }

    public function views()
    {
        $term = model2table($this->title);
        return filesContains(resource_path('views'), $term, ['.php'], 'views', 1);
    }

    public static function dataTableQuery()
    {
        return (new static())->newQuery();

//        $query = new static;
//
//        $query = $query
//            ->leftJoin('structures as structures2', 'structures.parent_id', '=', 'structures2.id')
//            ->select(['structures.*', 'structures2.title as structures2.parent']);
//        return $query;
    }
}
