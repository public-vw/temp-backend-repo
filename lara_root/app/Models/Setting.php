<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class Setting extends Model
{
    use CrudTrait;
    use HasFactory;
    use CacheQueryBuilder;

//    use DeepCacheQuery;
//    static $deepCacheTTL = 0;

    #TODO create set value form
    protected $fillable = [
        'title',
        'group_id',
        'slug',
        'value',
        'order',
        'lang',
        'direction',
        'type',
        'mode',
        'description',
        'hidden',
        'for_frontend',
    ];

    protected $appends = ['full_slug'];
    # -----------------------------------

    public function setAttribute($key, $value)
    {
        Cache::forget('setting_group__as_array');
        return parent::setAttribute($key, $value);
    }

    public static function isSame($params, $checkValue): bool
    {
        $ret = self::get($params);
        return isset($ret) && ((is_array($ret) ? in_array($checkValue, $ret) : ($ret === $checkValue)));
    }

    public static function getSetting($full_slug): self|\Illuminate\Database\Eloquent\Relations\HasMany
    {
        $group_full_slug = Str::beforeLast($full_slug, '.');
        $setting_slug = Str::afterLast($full_slug, '.');
        $group = SettingGroup::get($group_full_slug);

        return $group->settings()->where([
            'slug' => $setting_slug,
        ])->first();
    }

    public static function get($full_slug, $default = null)
    {
        $setting = Setting::getSetting($full_slug);

        return $setting ? $setting->value : $default;
    }

    public static function put($full_slug, $value)
    {
        $setting = Setting::getSetting($full_slug);

        return $setting->update([
            'value' => $value,
        ]);
    }

    public static function has($param)
    {
        return ! empty(self::get($param));
    }

    public static function asArray()
    {
        if (Cache::has('setting_group__as_array')) {
            return Cache::get('setting_group__as_array');
        }

        $groups = Setting::groupBy('group_slug')->get(['group_slug']);
        $ret = [];
        foreach ($groups as $group) {
            $settings = SettingGroup::getAll($group->group_slug);
            foreach ($settings as $setting) {
                $ret[$group->group_slug][$setting->slug] = $setting->value;
            }
        }
        Cache::forever('setting_group__as_array', $ret);
        return $ret;
    }

    public function group(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(SettingGroup::class, 'group_id');
    }

    public function getFullSlugAttribute(): string
    {
        return $this->group->full_slug .'.'. $this->slug;
    }
}
