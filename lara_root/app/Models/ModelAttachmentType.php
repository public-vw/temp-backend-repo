<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelAttachmentType extends Model
{
    use HasFactory;

    protected $fillable = [
        'attachment_type_id',
        'model_name',
    ];

    # -----------------------------------

    public function attachmentType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(AttachmentType::class, 'attachment_type_id', 'id');
    }
}
