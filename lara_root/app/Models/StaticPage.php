<?php

namespace App\Models;

use App\Models\Contracts\AbsHasContentModel;
use App\Models\Contracts\IUrlable;
use App\Models\Traits\HasContent;
use App\Models\Traits\Urlable;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StaticPage extends AbsHasContentModel implements IUrlable
{
    use HasFactory;
    use CacheQueryBuilder;
    use Urlable;
    use HasContent;

    protected $fillable = [
        'title', //Administration title
    ];

    protected $appends = ['final_content'];

    # -----------------------------------

    public function generateUrl(): string
    {
        return $this->final_content->uri ?
            (config('routes.appearance.static_page.prefix', '/') .
                $this->final_content->uri .
                (! config('routes.appearance.static_page.trailing_html', false) ?: '.html'))
            : '';
    }
}
