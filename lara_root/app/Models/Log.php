<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'user_id',
        'ip',
        'type',
        'message',
        'curr_url',
        'prev_url',
        'milli',
    ];

    protected $appends = [
        'summary',
        'ip_info',
//        'ip_location',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    # -----------------------------------

    public static function warning($title, $message): void
    {
        Log::create([
            'title' => $title ?? '__ Untitled __',
            'user_id' => null,
            'ip' => null,
            'type' => 'warning',
            'message' => $message,
            'curr_url' => null,
            'prev_url' => null,
            'milli' => null,
        ]);
    }

    public static function error($title, $message): void
    {
        Log::create([
            'title' => $title ?? '__ Untitled __',
            'user_id' => null,
            'ip' => null,
            'type' => 'error',
            'message' => $message,
            'curr_url' => null,
            'prev_url' => null,
            'milli' => null,
        ]);
    }

    public static function logStructuredError($title, Exception $e): void
    {
        $ret = 'File: ' . $e->getFile() . ' | ' .
            'Line: ' . $e->getLine() . ' | ' .
            'Msg: ' . $e->getMessage();
        Log::error($title, $ret);
    }

    public function setMilliAttribute($value): void
    {
        $microtime = microtime(true);
        $this->attributes['milli'] = ($microtime - floor($microtime)) * 1000;
    }

    #TODO: activate after adding location info
    /*
        public function getIpLocationAttribute()
        {
            return ((!empty($this->ip) && $this->ip !== 'LOCAL') ? Location::get($this->ip) : null);
        }*/

    public function setIpAttribute($value): void
    {
        $this->attributes['ip'] = get_ip();
    }

    public function getIpInfoAttribute()
    {
        return ip2long($this->ip) !== false ?
                "<a href='https://ipinfo.io/{$this->ip}'>{$this->ip}</a>"
                : $this->ip;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setCurrUrlAttribute($value): void
    {
        $this->attributes['curr_url'] = url()->current();
    }

    public function setPrevUrlAttribute($value): void
    {
        $this->attributes['prev_url'] = url()->previous();
    }

    public function setUserIdAttribute($value): void
    {
        $this->attributes['user_id'] = auth()->id() ?? null;
    }

    public function getSummaryAttribute(): string
    {
        $string = strip_tags($this->message);
        $needle = ' ';
        $nth = Setting::get('summary_length', 50);

        $max = strlen($string);
        $n = 0;
        for ($i = 0; $i < $max; $i++) {
            if ($string[$i] === $needle) {
                $n++;
                if ($n >= $nth) {
                    break;
                }
            }
        }
        return $n < $nth ? $string : substr($string, 0, $i) . ' ...';
    }

    public static function boot(): void
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
}
