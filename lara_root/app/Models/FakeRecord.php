<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FakeRecord extends Model
{
    use HasFactory;

    public const UPDATED_AT = null;
    public $timestamps = ['created_at'];

    protected $fillable = [
        'model',
        'model_id',
        'parent_model',
        'parent_id',
    ];
}
