<?php

namespace App\Models;

use App\Models\Traits\SlugAsRouteKey;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttachmentType extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use SlugAsRouteKey;

    protected $fillable = [
        'title',
        'slug',
        'properties_limit',
        'folder',
    ];

    protected $casts = ['properties_limit' => 'array'];

    # -----------------------------------
    public function attachables(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Attachable::class);
    }

    public function attachments(): \Illuminate\Database\Eloquent\Relations\HasManyThrough
    {
        return $this->hasManyThrough(Attachment::class, Attachable::class, 'type_id', 'id', 'id', 'attachment_id');
    }

    public function modelAttachmentTypes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ModelAttachmentType::class, 'attachment_type_id', 'id');
    }

    public function getPropertiesAttribute()
    {
        $properties = $this->properties_limit ?? null;
        if (! $properties) {
            return null;
        }

        $main_size = [0, 0];

        foreach ($properties as &$options) {
            foreach ($options as &$option) {
                $option = array_map(function ($option) {
                    return floatval($option);
                }, explode(',', $option));
                $main_size = max($main_size, $option);
            }
        }

        foreach ($properties as &$options) {
            foreach ($options as &$option) {
                if ($option[0] === 0) {
                    $option[0] = ceil($main_size[0] * $option[1] / $main_size[1]);
                }
                if ($option[1] === 0) {
                    $option[1] = ceil($main_size[1] * $option[0] / $main_size[0]);
                }
            }
        }

        $properties['max'] = $main_size;

        return $properties;
    }

    public function getMaxWidthAttribute()
    {
        $properties = $this->properties['max'];
        if (! $properties) {
            return null;
        }

        return $properties[0];
    }

    public function getMaxHeightAttribute()
    {
        $properties = $this->properties['max'];
        if (! $properties) {
            return null;
        }

        return $properties[1] ?? $properties[0];
    }
}
