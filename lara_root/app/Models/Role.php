<?php

namespace App\Models;

class Role extends \Spatie\Permission\Models\Role
{
    public function panel()
    {
        return $this->BelongsTo(Panel::class);
    }

    public static function assignRoleByName(User $user, $name, $guard)
    {
        $r = Role::where(['guard_name' => $guard, 'name' => $name])->first();
        $user->assignRole($r);
        return true;
    }
}
