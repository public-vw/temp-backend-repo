<?php

namespace App\Models\Contracts;

interface IUrlable
{
    public function generateUrl(): string;
}
