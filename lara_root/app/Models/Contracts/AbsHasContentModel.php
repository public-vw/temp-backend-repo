<?php

namespace App\Models\Contracts;

use Illuminate\Database\Eloquent\Model;

abstract class AbsHasContentModel extends Model
{
}
