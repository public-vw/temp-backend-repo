<?php

namespace App\Models;

use App\Models\Traits\Attachmentable;
use App\Models\Traits\SelfParentChild;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use Attachmentable;
    use SelfParentChild;

    protected $fillable = [
        'placeholder_id',
        'menuable',

        'parent_id',
        'title',
        'icon_id',
        'order',

        'heading',
        'new_tab',
        'nofollow',

        'active',
    ];

    # -----------------------------------

    public function menuPlaceholder(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(MenuPlaceholder::class, 'placeholder_id');
    }

    public function menuable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function panel(): \Illuminate\Database\Eloquent\Relations\HasManyThrough
    {
        return $this->hasManyThrough('panels', 'menu_placeholders', 'placeholder_id', 'panel_id', 'id');
    }

    public function icon(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Icon::class);
    }
}
