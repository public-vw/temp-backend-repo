<?php

namespace App\Models;

use App\Models\Traits\SelfParentChild;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use SelfParentChild;

    protected $fillable = [
        'user_id',
        'notable',
        'platejs_id',
        'platejs_parent_id',
        'content',
        'raw_content',
        'parent_id',
        'status',
    ];

    protected $casts = ['content' => 'array'];

    # -----------------------------------

    public function notable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
