<?php

namespace App\Models;

use App\Models\Contracts\AbsContentModel;
use App\Models\Traits\Attachmentable;
use App\Models\Traits\HasContentContainer;
use App\Models\Traits\HasStatus;
use App\Models\Traits\Notable;
use App\Models\Traits\RichSnippetable;
use App\Models\Traits\Seoable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ArticleContent extends AbsContentModel
{
    use HasFactory;
    use Seoable;
    use RichSnippetable;
    use Notable;
    use Attachmentable;
    use HasStatus;
    use HasContentContainer;

    protected $fillable = [
        'article_id',
        'uri',
        'version',
        'heading',
        'reading_time',
        'content',
        'raw_content',
        'summary',
        'inspiration',
        'status',
    ];

    protected $casts = ['content' => 'array'];

    public function category()
    {
        return $this->container->category();
    }
}
