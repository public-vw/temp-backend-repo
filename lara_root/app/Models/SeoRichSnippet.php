<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoRichSnippet extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    protected $fillable = [
        'richsnippetable',
        'type',
        'data',
    ];

    protected $casts = ['data' => 'array'];

    # -----------------------------------

    public function richsnippetable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }
}
