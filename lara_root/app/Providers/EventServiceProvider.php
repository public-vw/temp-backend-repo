<?php

namespace App\Providers;

use App\Events\ArticleCreated;
use App\Events\ContentActivated;
use App\Events\ContentStatusChanged;
use App\Events\ErrorOccurred;
use App\Events\RecordUpdateOccurred;
use App\Events\RevalidationRequested;
use App\Events\SubscribedForEmail;
use App\Events\TelegramAlertRequested;
use App\Events\UrlCreated;
use App\Listeners\CreateUrlIfNeeded;
use App\Listeners\SendAdministrationMessage;
use App\Listeners\SendTelegramAlert;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SubscribedForEmail::class => [
            SendEmailVerificationNotification::class,
        ],
        ContentActivated::class => [
            CreateUrlIfNeeded::class,
        ],
        ArticleCreated::class => [
            SendAdministrationMessage::class,
        ],
        ContentStatusChanged::class => [
            SendAdministrationMessage::class,
        ],
        ErrorOccurred::class => [
            SendAdministrationMessage::class,
        ],
        RecordUpdateOccurred::class => [
            SendAdministrationMessage::class,
        ],
        RevalidationRequested::class => [
            SendAdministrationMessage::class,
        ],
        UrlCreated::class => [
            SendAdministrationMessage::class,
        ],
        TelegramAlertRequested::class => [
            SendTelegramAlert::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
