<?php

namespace App\Providers;

use App\Http\Controllers\Auth\Session\CsrfCookieController;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Http\Controllers\CsrfCookieController as SanctumCsrfCookieController;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
    }

    public function boot(): void
    {
        $this->app->bind(SanctumCsrfCookieController::class, CsrfCookieController::class);

        if (config('app.force_https')) {
            \URL::forceScheme('https');
        }

        ## Disables lazy loading on a model to find N+1 problem,
        # stops app just in non-production mode
        # Model::preventLazyLoading(! app()->isProduction());
    }
}
