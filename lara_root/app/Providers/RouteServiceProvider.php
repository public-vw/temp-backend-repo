<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    public const HOME = '/';

    protected $namespace = '\\App\\Http\\Controllers';

    public function boot(): void
    {
        $this->configureRateLimiting();

        $this->headerRoutes();

        $this->sysadminApiRoutes('Sysadmin');
        $this->sysadminWebRoutes('Sysadmin');

        $this->devApiRoutes('Dev');

        #copy this for <NewPanel>
        $this->staffApiRoutes('Staff');

        $this->sessionAuthRoutes();
//        $this->tokenAuthRoutes();

        $this->frontendBuildRoutes();
        $this->botsRoutes();

        $this->interfacePublicApiRoutes('InterfaceZone');
        $this->interfaceSecureApiRoutes('InterfaceZone');
        $this->interfaceWebRoutes('InterfaceZone');

        $this->footerRoutes();
    }

    protected function configureRateLimiting(): void
    {
        RateLimiter::for('web', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }

    protected function headerRoutes(): void
    {
        Route::middleware(['api_secret', 'web'])
            ->namespace($this->namespace)
            ->name('header.')
            ->group(base_path('routes/header.php'));
    }

    protected function footerRoutes(): void
    {
        Route::middleware(['api_secret', 'web'])
            ->namespace($this->namespace)
            ->name('footer.')
            ->group(base_path('routes/footer.php'));
    }

    private function sysadminWebRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware(['web', 'auth', 'role:sysadmin'])
            ->namespace($namespace)
            ->name('sysadmin.')
            ->prefix(config('routes.namespaces.sysadmin.web'))
            ->group(base_path('routes/sysadmin/web/web.php'));
    }
    private function sysadminApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware(['api_secret', 'web', 'auth:sanctum', 'role:sysadmin'])
            ->namespace($namespace . '\\API\\V1')
            ->name('sysadmin.api.v1.')
            ->prefix(config('routes.namespaces.sysadmin.api'))
            ->group(base_path('routes/sysadmin/api/v1/api.php'));
    }

    private function devApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware(['api_secret', 'web', 'auth:sanctum', 'role:admin'])
            ->namespace($namespace . '\\API\\V1')
            ->name('dev.api.v1.')
            ->prefix(config('routes.namespaces.dev.api'))
            ->group(base_path('routes/dev/api/v1/api.php'));
    }

    private function staffApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware(['api_secret', 'web', 'auth:sanctum', 'role:staff'])
            ->namespace($namespace)
            ->name('staff.api.v1.')
            ->prefix(config('routes.namespaces.staff.api'))
            ->group(base_path('routes/staff/api.php'));
    }

    private function interfaceWebRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('web')
            ->namespace($namespace)
            ->name('public.')
            ->group(base_path('routes/interface_zone/web/web.php'));
    }
    private function interfaceSecureApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware(['api_secret', 'web', 'auth:sanctum'])
            ->namespace($namespace . '\\SecureAPI\\V1')
            ->name('public.secure.api.v1.')
            ->prefix(config('routes.namespaces.interface.secure_api'))
            ->group(base_path('routes/interface_zone/secure_api/v1/api.php'));
    }
    private function interfacePublicApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware(['api_secret', 'web'])
            ->namespace($namespace . '\\API\\V1')
            ->name('public.api.v1.')
            ->prefix(config('routes.namespaces.interface.public_api'))
            ->group(base_path('routes/interface_zone/public_api/v1/api.php'));
    }

    private function sessionAuthRoutes(): void
    {
        $namespace = $this->namespace . '\\Auth';

        Route::middleware(['api_secret', 'web'])
            ->namespace($namespace . '\\Session')
            ->name('auth.session.')
            ->prefix(config('routes.namespaces.auth.session'))
            ->group(base_path('routes/auth/session/routes.php'));
    }
    private function tokenAuthRoutes(): void
    {
        $namespace = $this->namespace . '\\Auth';

        Route::middleware(['api'])
            ->namespace($namespace . '\\Token')
            ->name('auth.token.')
            ->prefix(config('routes.namespaces.auth.token'))
            ->group(base_path('routes/auth/token/routes.php'));
    }

    private function frontendBuildRoutes(): void
    {
        $namespace = $this->namespace . '\\FrontendBuildTime';

        Route::middleware(['api_secret', 'checkFrontendBuildToken'])
            ->namespace($namespace)
            ->name('frontend_build.')
            ->prefix(config('routes.namespaces.frontend_buildtime'))
            ->group(base_path('routes/frontend_buildtime/routes.php'));
    }

    private function botsRoutes(): void
    {
        $namespace = $this->namespace . '\\Bots';

        Route::middleware(['api'])
            ->namespace($namespace)
            ->name('bots.')
            ->prefix(config('routes.namespaces.bots'))
            ->group(base_path('routes/bots/api.php'));
    }
}
