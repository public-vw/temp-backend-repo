<?php

namespace App\Providers;

use App\Models\Article;
//use App\Models\ArticleCategoryContent;
use App\Models\ArticleCategory;
use App\Models\ArticleCategoryContent;
use App\Models\ArticleContent;
use App\Models\Attachable;
//use App\Models\AttachmentType;
//use App\Models\Color;
//use App\Models\Currency;
//use App\Models\FakeRecord;
//use App\Models\Icon;
//use App\Models\Log;
use App\Models\Attachment;
use App\Models\Bookmark;
use App\Models\Comment;
use App\Models\Like;
//use App\Models\ModelAttachmentType;
//use App\Models\Panel;
//use App\Models\Role;
//use App\Models\SeoDetail;
//use App\Models\Setting;
//use App\Models\SettingGroup;
use App\Models\Menu;
use App\Models\MenuPlaceholder;
use App\Models\Note;
use App\Models\SeoRichSnippet;
use App\Models\Setting;
//use App\Models\Structure;
use App\Models\StaticPage;
use App\Models\StaticPageContent;
use App\Models\Url;
use App\Models\User;
use App\Policies\ArticleCategoryContentPolicy;
use App\Policies\ArticleCategoryPolicy;
use App\Policies\ArticleContentPolicy;
use App\Policies\ArticlePolicy;
use App\Policies\AttachablePolicy;
use App\Policies\AttachmentPolicy;
use App\Policies\BookmarkPolicy;
use App\Policies\CommentPolicy;
use App\Policies\LikePolicy;
use App\Policies\MenuPlaceholderPolicy;
use App\Policies\MenuPolicy;
use App\Policies\NotePolicy;
use App\Policies\SeoRichSnippetPolicy;
// use Illuminate\Support\Facades\Gate;
use App\Policies\SettingPolicy;
use App\Policies\StaticPageContentPolicy;
use App\Policies\StaticPagePolicy;
use App\Policies\UrlPolicy;
use App\Policies\UserPolicy;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    #TODO: check if the commented policies are needed, then create them
    protected $policies = [
        // Articles
        Article::class => ArticlePolicy::class,
        ArticleCategory::class => ArticleCategoryPolicy::class,
        ArticleCategoryContent::class => ArticleCategoryContentPolicy::class,
        ArticleContent::class => ArticleContentPolicy::class,

        // Reactions
        Bookmark::class => BookmarkPolicy::class,
        Like::class => LikePolicy::class,

        // Miscellaneous
        Attachment::class => AttachmentPolicy::class,
        Attachable::class => AttachablePolicy::class,
        //        AttachmentType::class => AttachmentTypePolicy::class,
        //        Color::class => ColorPolicy::class,
        Comment::class => CommentPolicy::class,
        Note::class => NotePolicy::class,
        #        Currency::class => CurrencyPolicy::class,
        #        FakeRecord::class => FakeRecordPolicy::class,
        #        Icon::class => IconPolicy::class,
        #        Log::class => LogPolicy::class,
        Menu::class => MenuPolicy::class,
        MenuPlaceholder::class => MenuPlaceholderPolicy::class,
        #        ModelAttachmentType::class => ModelAttachmentTypePolicy::class,
        #        Panel::class => PanelPolicy::class,
        #        Role::class => RolePolicy::class,
        Setting::class => SettingPolicy::class,
        //        SettingGroup::class => SettingGroupPolicy::class,
        StaticPage::class => StaticPagePolicy::class,
        StaticPageContent::class => StaticPageContentPolicy::class,
        #        Structure::class => StructurePolicy::class,
        Url::class => UrlPolicy::class,
        SeoRichSnippet::class => SeoRichSnippetPolicy::class,

        // User
        User::class => UserPolicy::class,
    ];

    public function boot(): void
    {
        $this->registerPolicies();

        ResetPassword::createUrlUsing(function (object $notifiable, string $token) {
            return config('app.frontend_url')."/password-reset/{$token}?email={$notifiable->getEmailForPasswordReset()}";
        });
    }
}
