<?php

namespace App\Jobs;

use App\Events\RevalidationRequested;
use App\Libraries\Revalidation\AbsRevalidationRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class JRevalidateRequest implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(protected AbsRevalidationRequest $request)
    {
    }

    /**
     * @throws \Exception
     */
    public function handle(): void
    {
        ! $this->request->isEmpty() ?: $this->sendRequest();
    }

    protected function sendRequest(): void
    {
        $response = Http::withHeaders(
            [
                'token' => config('app.frontend_revalidate_token'),
                'Content-Type' => 'application/json',
            ]
        )
            ->post(config('app.frontend_url') . '/api/revalidate', [
                'tag' => $this->request->tag,
                'payload' => $this->request->payload,
            ]);

        RevalidationRequested::dispatch($this->request, $response);

        if ($response->status() !== 200) {
            throw new \Exception($response->json()['message']);
        }
    }
}
