<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Core\Command as TelegramCommand;

class JSendTelegramMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(
        protected string $chat_id,
        protected string $message
    ) {
    }

    public function handle(): void
    {
        TelegramCommand::callResponse('sendMessage', [
            $this->chat_id,
            $this->message,
        ]);
    }
}
