<?php

namespace App\Events;

use App\Models\Contracts\AbsContentModel;
use App\Services\Telegram\Contracts\ShouldTbot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ContentStatusChanged extends ShouldTbot
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    use InteractsWithQueue;

    public string $title = 'Content Status Changed';

    public function __construct(
        public AbsContentModel $content,
        public string $oldStatus,
        public string $newStatus,
    ) {
        $this->payload = [
            'Container' => ucfirst(get_class($this->content->container)) . '(' . $this->content->container->id . ')',
            'Content Id' => $this->content->id,
            'Content URI' => $this->content->uri,
            'From' => $this->oldStatus,
            'To' => $this->newStatus,
        ];

        ContentActivated::dispatchIf($this->newStatus === 'active', $this->content);
    }

//    public function broadcastOn(): array
//    {
//        return [
//            new PrivateChannel('channel-name'),
//        ];
//    }
}
