<?php

namespace App\Events;

use App\Models\Url;
use App\Services\Telegram\Contracts\ShouldTbot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UrlCreated extends ShouldTbot
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    use InteractsWithQueue;

    public string $title = 'URL Created';

    public function __construct(
        public Url $url,
    ) {
        $this->payload = [
            'URL id' => $this->url->id,
            'URL path' => $this->url->url,
            'Urlable' => ucfirst(get_class($this->urlable)) . '(' . $this->urlable->id . ')',
        ];
    }

//    public function broadcastOn(): array
//    {
//        return [
//            new PrivateChannel('channel-name'),
//        ];
//    }
}
