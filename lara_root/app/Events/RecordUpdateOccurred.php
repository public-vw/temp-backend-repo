<?php

namespace App\Events;

use App\Services\Telegram\Contracts\ShouldTbot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecordUpdateOccurred extends ShouldTbot implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    use InteractsWithQueue;

    public string $title = 'Record Update Occurred';

    public function __construct(
        public ?Model $record,
        public string $message = ''
    ) {
        $obj = $this->record ? ucfirst(get_class($this->record)) . '(' . $this->record->id . ')' : 'No Object';

        $this->payload = [
            'Object' => $obj,
            'Message' => $this->message,
        ];
    }

//    public function broadcastOn(): array
//    {
//        return [
//            new PrivateChannel('channel-name'),
//        ];
//    }
}
