<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class DebugMessageBroadcast implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels, Queueable;

    public function __construct(
        public $level,
        public $time,
        public $message,
        public $context,
        public $extra
    ) {
        ErrorOccurred::dispatchIf(
            $this->hasListeningConditions(),
            $this->time,
            $this->message,
            $this->context,
            $this->extra
        );
    }

    public function broadcastOn(): Channel
    {
        return new Channel('debug-channel');
    }

    public function broadcastAs(): string
    {
        return 'debug-event';
    }

    public function broadcastWith(): array
    {
        return [
            'website' => config('app.title'),
            'level' => $this->level,
            'time' => $this->time,
            'message' => $this->message,
            'context' => $this->context,
            'extra' => $this->extra,
        ];
    }

    protected function hasListeningConditions(): bool
    {
        $is_in_listening_levels = in_array('*', config('tbot.listen_to.errors.levels'));
        $is_in_listening_levels = $is_in_listening_levels || in_array(strtolower($this->level), config('tbot.listen_to.errors.levels'));

        $is_not_excluded = empty(config('tbot.listen_to.errors.exclude')) || ! str_contains($this->message, config('tbot.listen_to.errors.exclude'));

        $is_included = ! empty(config('tbot.listen_to.errors.include')) && str_contains($this->message, config('tbot.listen_to.errors.include'));

        return ($is_in_listening_levels && $is_not_excluded) || $is_included;
    }
}
