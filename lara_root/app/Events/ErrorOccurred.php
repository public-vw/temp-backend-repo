<?php

namespace App\Events;

use App\Services\Telegram\Contracts\Level;
use App\Services\Telegram\Contracts\ShouldTbot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ErrorOccurred extends ShouldTbot implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    use InteractsWithQueue;

    public string $title = 'Error Occurred';
    public Level $level = Level::Error;

    public function __construct(
        public string $time,
        public string $message,
        public string $context,
        public string $extra
    ) {
        $this->payload = [
            'Error Time' => $time,
            'Message' => $this->message,
            'Context' => $this->context,
            'Extra' => $this->extra,
        ];
    }

//    public function broadcastOn(): array
//    {
//        return [
//            new PrivateChannel('channel-name'),
//        ];
//    }
}
