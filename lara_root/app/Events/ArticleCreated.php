<?php

namespace App\Events;

use App\Models\Article;
use App\Services\Telegram\Contracts\ShouldTbot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ArticleCreated extends ShouldTbot implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public string $title = 'New Article Created';

    public function __construct(
        public Article $article,
    ) {
        $this->payload = [
            'article_id' => $this->article->id,
        ];
    }

//    public function broadcastOn(): array
//    {
//        return [
//            new PrivateChannel('channel-name'),
//        ];
//    }
}
