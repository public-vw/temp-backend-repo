<?php

namespace App\Events;

use App\Libraries\Revalidation\AbsRevalidationRequest;
use App\Services\Telegram\Contracts\ShouldTbot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Client\Response;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RevalidationRequested extends ShouldTbot
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    use InteractsWithQueue;

    public string $title = 'Revalidation Requested';

    public function __construct(
        public AbsRevalidationRequest $request,
        public Response $response,
    ) {
        $this->payload = [
            'Tag' => $this->request->tag,
            'Payload' => implode_assoc($this->request->payload),
            'Response' => implode_assoc(
                [
                    'status' => $this->response->status(),
                    'body' => $this->response->body(),
                ]
            ),
        ];
    }

//    public function broadcastOn(): array
//    {
//        return [
//            new PrivateChannel('channel-name'),
//        ];
//    }
}
