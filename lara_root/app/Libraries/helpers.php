<?php

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

function generalRichSnippets(): Collection
{
    $items = \App\Models\SeoRichSnippet::whereNull('richsnippetable_type');

    $seo = [];
    foreach (config('enums.rich_snippets.types') as $type) {
        $record = $items
            ->where('type', config_key('enums.rich_snippets.types', $type))
            ->orderByDesc('id')
            ->first();
        if (! $record) {
            continue;
        }
        $seo[$type] = $record['data'];
    }
    return new Collection($seo);
}

function implode_assoc(array $array, string $pairGlue = '=', string $itemGlue = ','): string
{
    return implode($itemGlue, array_map(
        function ($key, $value) use ($pairGlue) {
            return strpos($key, 'nokey_') === 0 ? $value : $key . $pairGlue . $value;
        },
        array_keys($array),
        $array
    ));
}
function setRoles(App\Models\User $user, $roles): void
{
    foreach ($roles as $role) {
        $r = App\Models\Role::where(['name' => $role])->first();
        $user->assignRole($r);
    }
}

function replaceInFiles($directory, $search, $replace, $extension = null): void
{
    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory));

    foreach ($iterator as $file) {
        if ($file->isDir()) {
            continue;
        }

        if ($extension && $file->getExtension() !== $extension) {
            continue;
        }

        $content = file_get_contents($file->getPathname());
        $newContent = str_replace($search, $replace, $content);

        // Uncomment the next line if you want to remove lines that start with '#'
        $newContent = preg_replace('/^#.*\n/m', '', $newContent);

        file_put_contents($file->getPathname(), $newContent);
    }
}

function removeCommentsInFiles($directory, $extension = null): void
{
    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory));

    foreach ($iterator as $file) {
        if ($file->isDir()) {
            continue;
        }

        if ($extension && $file->getExtension() !== $extension) {
            continue;
        }

        $content = file_get_contents($file->getPathname());
        $newContent = preg_replace('/^#.*\n/m', '', $content);

        file_put_contents($file->getPathname(), $newContent);
    }
}

function includeRouteFiles($dir, $recursive = true, $exclusion = [])
{
    $route_files = scandir($dir);
    foreach ($route_files as $route_file) {
        if (in_array($route_file, ['.', '..', 'web.php', 'api.php'])) {
            continue;
        }
        if (in_array($route_file, $exclusion)) {
            continue;
        }
        if (is_dir($dir . '/' . $route_file)) {
            if ($recursive) {
                includeRouteFiles($dir . '/' . $route_file);
            }
            continue;
        }
        require_once $dir . '/' . $route_file;
    }
}

function runShell($cmd, $stderr = false, $simulate = false)
{
    if ($simulate) {
        return $stderr ? ['output' => $cmd, 'errors' => ''] : $cmd;
    }

    if (! function_exists('proc_open')) {
        die('Error code: 20023');
    }

    $descriptorspec = [
        0 => ['pipe', 'r'],  // stdin is a pipe that the child will read from
        1 => ['pipe', 'w'],  // stdout is a pipe that the child will write to
        2 => $stderr ? ['pipe', 'w'] : ['file', storage_path('logs/shell-output.txt'), 'a'], // stderr
    ];

    $pipes = null;
    $process = proc_open($cmd, $descriptorspec, $pipes);

    if (is_resource($process)) {
        $ret = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        if ($stderr) {
            $err = stream_get_contents($pipes[2]);
            fclose($pipes[2]);
        }

        proc_close($process);
        return $stderr ? ['output' => $ret, 'errors' => $err] : $ret;
    }

    return false;
}

function config_keys_all($config, $values = null): array
{
    return array_keys($values ? array_intersect($values, config($config)) : config($config));
}

function config_key($config, $value)
{
    return (string) array_search($value, config($config));
}

function config_keys($config, array $values): array
{
    return array_map(function ($v) use ($config) {
        return config_key($config, $v);
    }, $values);
}

function config_trans($config, $key): \Illuminate\Foundation\Application|array|string|\Illuminate\Contracts\Translation\Translator|\Illuminate\Contracts\Foundation\Application|null
{
    return trans("{$config}." . config("{$config}.{$key}"));
}

function getModels($path = null): array
{
    if (is_null($path)) {
        $path = app_path('Models');
    }

    $out = [];
    $results = scandir($path);

    foreach ($results as $result) {
        if (substr($result, -4) !== '.php') {
            continue;
        }
        $out[] = substr($result, 0, -4);
    }
    return $out;
}

function alterArray(&$array, $function): void
{
    array_walk($array, function (&$value) use ($function) {
        $value = $function($value);
    });
}

// article_category -> ArticleCategory
function config2model($config_value): string
{
    $ret = \Illuminate\Support\Str::camel($config_value);
    return \Illuminate\Support\Str::ucfirst($ret);
}

//article_category -> App\Models\ArticleCategory
function config2class($config_value, $namespace = 'App\\Models\\'): string
{
    $ret = \Illuminate\Support\Str::plural($config_value);
    return model2class($ret, $namespace);
}

//ArticleCategory -> App\Models\ArticleCategory
function model2class($model_name, $namespace = 'App\\Models\\'): string
{
    $ret = \Illuminate\Support\Str::singular($model_name);
    $ret = \Illuminate\Support\Str::camel($ret);
    $ret = \Illuminate\Support\Str::ucfirst($ret);
    return $namespace . $ret;
}

// App\Models\ArticleCategory -> ArticleCategory
function class2model(string $class_name): string
{
    return last(explode('\\', $class_name));
}

// App\Models\ArticleCategory -> article_category
function class2config(string $class_name): string
{
    return Str::snake(class2model($class_name));
}

// ArticleCategory -> article_categories
function model2route($model_name): string
{
    $ret = \Illuminate\Support\Str::plural($model_name);
    return \Illuminate\Support\Str::snake($ret);
}

function model2table($model_name): string
{
    $ret = \Illuminate\Support\Str::plural($model_name);
    return \Illuminate\Support\Str::snake($ret);
}

function table2model($table_name): array|string
{
    $ret = \Illuminate\Support\Str::singular($table_name);
    $ret = \Illuminate\Support\Str::camel($ret);
//    $ret = str_replace('_',' ',$ret);
    $ret = \Illuminate\Support\Str::ucfirst($ret);
    return str_replace('_', '', $ret);
}

function modelObj($model_name, $namespace = 'App\\Models\\')
{
    $modelClass = model2class($model_name, $namespace);
    return new $modelClass();
}

function createBlurImage($fullpath)
{
    $image = Intervention\Image\Facades\Image::make($fullpath);
    $image->resize(10, 10);
//    $image->blur(5);

    return (string) $image->encode('data-url');
}

function get_ip($request = null)
{
    return $_SERVER['HTTP_CF_CONNECTING_IP'] ?? $_SERVER['REMOTE_ADDR'] ?? $request?->ip() ?? 'LOCAL';
}
