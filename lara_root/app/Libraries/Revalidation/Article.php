<?php

namespace App\Libraries\Revalidation;

class Article extends AbsRevalidationRequest
{
    public string $tag = 'article';

    public function __construct(string $url)
    {
        $this->payload = ['url' => $url];
    }
}
