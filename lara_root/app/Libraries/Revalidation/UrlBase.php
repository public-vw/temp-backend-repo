<?php

namespace App\Libraries\Revalidation;

use Illuminate\Support\Str;

class UrlBase extends AbsRevalidationRequest
{
    public function __construct(string $class, string $url)
    {
        $this->tag = Str::kebab(last(explode('\\', $class)));
        $this->payload = ['url' => $url];
    }
}
