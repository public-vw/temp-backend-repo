<?php

namespace App\Libraries\Revalidation;

abstract class AbsRevalidationRequest
{
    public string $tag;
    public array $payload;

    public function isEmpty(): bool
    {
        return empty($payload) || empty($payload['url'] ?? $payload['modelType']);
    }
}
