<?php

namespace App\Transformers\Staff\Setting;

use App\Models\Setting;
use App\Transformers\Staff\SettingGroup\IndexTransformer as GroupIndexTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
    ];

    protected array $availableIncludes = [
        'parent',
    ];

    public function transform(Setting $setting): array
    {
        return [
            'id' => $setting->id,
            'title' => $setting->title,
            'full_slug' => $setting->full_slug,
            'slug' => $setting->slug,
            'value' => $setting->type === 'json' ? json_decode($setting->value, true) : $setting->value,

            'lang' => $setting->lang,
            'direction' => config('enums.settings.directions.' . $setting->direction),
            'type' => $setting->type,
            'mode' => config('enums.settings.directions.' . $setting->mode),
            'description' => $setting->description,

        ];
    }

    public function includeParent(Setting $setting): \League\Fractal\Resource\Item
    {
        return $this->item($setting->group, new GroupIndexTransformer());
    }
}
