<?php

namespace App\Transformers\Staff\StaticPageContent;

use App\Models\StaticPageContent;
use App\Transformers\Staff\Image\IndexTransformer as ImageIndexTransformer;
use App\Transformers\Staff\StaticPage\ShowTransformer as StaticPageShowTransformer;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'images',
    ];

    protected array $availableIncludes = [
        'static_page',
    ];

    public function transform(StaticPageContent $staticPageContent): array
    {
        return [
            'id' => $staticPageContent->id,
            'version' => $staticPageContent->version,
            'heading' => $staticPageContent->heading,
            'reading_time' => $staticPageContent->reading_time,
            'uri' => $staticPageContent->uri,
            'content' => $staticPageContent->content,
            'data' => $staticPageContent->data,
            'status' => (string) config('enums.contents.status.' . (int) $staticPageContent->status),
            'updated_at' => $staticPageContent->updated_at->timestamp . Str::substr('000' . $staticPageContent->id, -3),
        ];
    }

    public function includeStaticPage(StaticPageContent $staticPageContent): \League\Fractal\Resource\Item
    {
        return $this->item($staticPageContent->container, new StaticPageShowTransformer());
    }

    public function includeImages(StaticPageContent $staticPageContent): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        return $this->collection($staticPageContent->attachables, new ImageIndexTransformer());
    }
}
