<?php

namespace App\Transformers\Staff\User;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(User $user): array
    {
        return [
            'id' => $user->id,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'fullname' => $user->name,

            'username' => $user->username,
            'nickname' => $user->nickname,

            'random_color' => config('enums.random_colors.' . $user->random_color),

            'status' => config('enums.users.status.' . $user->status),
        ];
    }
}
