<?php

namespace App\Transformers\Staff\User;

use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(User $user): array
    {
        return [
            'id' => $user->id,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'fullname' => $user->name,

            'username' => $user->username,
            'nickname' => $user->nickname,
            'ref_code' => $user->ref_code,

            'mobile' => $user->mobile,
            'mobile_verified_at' => $user->mobile_verified_at->timestamp . Str::substr('000' . $user->id, -3),
            'email' => $user->email,
            'email_verified_at' => $user->email_verified_at->timestamp . Str::substr('000' . $user->id, -3),

            'random_color' => config('enums.random_colors.' . $user->random_color),

            'telegram_uid' => $user->telegram_uid,
            'referer' => $user->referer,

            'use_two_factor_auth' => $user->use_two_factor_auth,

            'status' => config('enums.users.status.' . $user->status),
        ];
    }
}
