<?php

namespace App\Transformers\Staff\ArticleContent;

use App\Models\ArticleContent;
use App\Transformers\Staff\Article\ShowTransformer as ArticleShowTransformer;
use App\Transformers\Staff\Image\IndexTransformer as ImageIndexTransformer;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'images',
    ];

    protected array $availableIncludes = [
        'article',
    ];

    public function transform(ArticleContent $articleContent): array
    {
        return [
            'id' => $articleContent->id,
            'uri' => $articleContent->uri,
            'category' => [
                'id' => $articleContent->category?->id,
                'title' => $articleContent->category?->final_content->title,
                'url' => $articleContent->category?->url,
            ],
            'version' => $articleContent->version,
            'heading' => $articleContent->heading,
            'reading_time' => $articleContent->reading_time,
            'summary' => $articleContent->summary,
            'updated_at' => $articleContent->updated_at->timestamp . Str::substr('000' . $articleContent->id, -3),
            'status' => (string) config('enums.contents.status.' . (int) $articleContent->status),
        ];
    }

    public function includeArticle(ArticleContent $articleContent): \League\Fractal\Resource\Item
    {
        return $this->item($articleContent->container, new ArticleShowTransformer());
    }

    public function includeImages(ArticleContent $articleContent): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        return $this->collection($articleContent->attachables, new ImageIndexTransformer());
    }
}
