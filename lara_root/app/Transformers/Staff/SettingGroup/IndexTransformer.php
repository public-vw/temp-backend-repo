<?php

namespace App\Transformers\Staff\SettingGroup;

use App\Models\SettingGroup;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'parent',
    ];

    protected array $availableIncludes = [
    ];

    public function transform(SettingGroup $settingGroup): array
    {
        return [
            'id' => $settingGroup->id,
            'title' => $settingGroup->title,
            'slug' => $settingGroup->slug,
        ];
    }

    public function includeParent(SettingGroup $settingGroup): \League\Fractal\Resource\Item | \League\Fractal\Resource\NullResource
    {
        if (! $settingGroup->parent) {
            return $this->null();
        }
        return $this->item($settingGroup->parent, new self());
    }
}
