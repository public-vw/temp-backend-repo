<?php

namespace App\Transformers\Staff\Article;

use App\Models\Article;
use App\Transformers\Staff\ArticleCategory\ShowTransformer as CategoryShowTransformer;
use App\Transformers\Staff\ArticleContent\IndexTransformer as ContentIndexTransformer;
use App\Transformers\Staff\User\ShowTransformer as AuthorShowTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
    ];

    protected array $availableIncludes = [
        'author',
        'category',
        'final_content',
    ];

    public function transform(Article $article): array
    {
        return [
            'id' => $article->id,

            'version_counts' => $article->contents()->count(),
            'published' => (bool) $article->published,

            'features' => [
                'counts' => [
                    'likes' => $article->likes->count(), #TODO: add likes data structure
                    'comments' => $article->accepted_comments->count(),
                    'visits' => 25234,
                    //                "visits"   => $article->visits->count(),#TODO: add visits data structure
                ],
            ],
            'mode' => 'sm',         # sm | md | lg | xl
            //TODO:activate type
            //            "type"     => "image",      # image | video | audio | text
            'urlType' => 'external',   # default is internal
            'url' => $article->url,# if it is internal, start with /, if it is external, start with http:// or https://
            'label' => $article->label ?? 'no-label',#TODO: add label data structure
        ];
    }

    public function includeAuthor(Article $article): \League\Fractal\Resource\Item | \League\Fractal\Resource\NullResource
    {
        if (! $article->author) {
            return $this->null();
        }
        return $this->item($article->author, new AuthorShowTransformer());
    }

    public function includeCategory(Article $article): \League\Fractal\Resource\Item | \League\Fractal\Resource\NullResource
    {
        if (! $article->category) {
            return $this->null();
        }
        return $this->item($article->category, new CategoryShowTransformer());
    }

    public function includeFinalContent(Article $article): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $article->final_content) {
            return $this->null();
        }
        return $this->item($article->final_content, new ContentIndexTransformer());
    }
}
