<?php

namespace App\Transformers\Staff\ArticleCategory;

use App\Models\ArticleCategory;
use App\Transformers\Staff\ArticleCategoryContent\ShowTransformer as ContentShowTransformer;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
    ];

    protected array $availableIncludes = [
        'parent',
        'children',
        'final_content',
    ];

    public function transform(ArticleCategory $articleCategory): array
    {
        return [
            'id' => (int) $articleCategory->id,
            'parent_id' => $articleCategory->parent_id,
            'title' => (string) $articleCategory->title,
            'url' => $articleCategory->url,# if it is internal, start with /, if it is external, start with http:// or https://
            'order' => (int) $articleCategory->order,
            'version_counts' => $articleCategory->contents()->count(),
            'published' => (bool) $articleCategory->published,

            'features' => [
                'counts' => [
                    'articles' => $articleCategory->articles()->count(),
                    'active_articles' => $articleCategory->activeArticles()->count(),
                ],
            ],
        ];
    }

    public function includeChildren(ArticleCategory $articleCategory): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        $children = $articleCategory->children;
        if ($children->count() === 0) {
            return $this->null();
        }
        return $this->collection($children, new IndexTransformer());
    }

    public function includeParent(ArticleCategory $articleCategory): ?\League\Fractal\Resource\Item
    {
        $parent = $articleCategory->parent;
        return ! $parent ? null : $this->item($parent, new ShowTransformer());
    }

    public function includeFinalContent(ArticleCategory $articleCategory): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $articleCategory->final_content) {
            return $this->null();
        }
        return $this->item($articleCategory->final_content, new ContentShowTransformer());
    }
}
