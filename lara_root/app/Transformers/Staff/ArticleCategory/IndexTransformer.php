<?php

namespace App\Transformers\Staff\ArticleCategory;

use App\Models\ArticleCategory;
use App\Transformers\Staff\ArticleCategoryContent\IndexTransformer as ContentIndexTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'final_content',
    ];

    protected array $availableIncludes = [
        'parent',
        'published_parent',
    ];

    public function transform(ArticleCategory $articleCategory): array
    {
        return [
            'id' => (int) $articleCategory->id,
            'parent_id' => $articleCategory->parent?->id ?? null,
            'published_parent_id' => $articleCategory->publishedParent()?->id ?? null,
            'url' => (string) $articleCategory->url,
            'order' => (int) $articleCategory->order,
            'version_counts' => $articleCategory->contents()->count(),
            'published' => (bool) $articleCategory->published,

            'features' => [
                'counts' => [
                    'articles' => $articleCategory->articles()->count(),
                    'active_articles' => $articleCategory->activeArticles()->count(),
                ],
            ],
        ];
    }

    public function includeParent(ArticleCategory $articleCategory): ?\League\Fractal\Resource\Item
    {
        $parent = $articleCategory->parent;
        return ! $parent ? null : $this->item($parent, new ShowTransformer());
    }

    public function includePublishedParent(ArticleCategory $articleCategory): ?\League\Fractal\Resource\Item
    {
        $parent = $articleCategory->publishedParent();
        return ! $parent ? null : $this->item($parent, new ShowTransformer());
    }

    public function includeFinalContent(ArticleCategory $articleCategory): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $articleCategory->final_content) {
            return $this->null();
        }
        return $this->item($articleCategory->final_content, new ContentIndexTransformer());
    }
}
