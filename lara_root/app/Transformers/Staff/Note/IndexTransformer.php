<?php

namespace App\Transformers\Staff\Note;

use App\Models\Note;
use App\Transformers\Staff\User\ShowTransformer as UserShowTransformer;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [
        'user',
        'notable',
        'parent',
    ];

    public function transform(Note $note): array
    {
        // contains platejs note(comment) fields
        return [
            'id' => $note->id,
            'platejs_id' => $note->platejs_id,
            'platejs_parent_id' => $note->platejs_parent_id,
            'userId' => $note->user?->id,
            'createdAt' => $note->created_at->timestamp . Str::substr('000' . $note->id, -3),
            'isResolved' => false,//TODO:what is this?
            'content' => $note->content,
        ];
    }

    public function includeUser(Note $note): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $note->user) {
            return $this->null();
        }
        return $this->item($note->user, new UserShowTransformer());
    }

    public function includeParent(Note $note): ?\League\Fractal\Resource\Item
    {
        $parent = $note->parent;
        return ! $parent ? null : $this->item($parent, new ShowTransformer());
    }

    public function includeNotable(Note $note): ?\League\Fractal\Resource\Item
    {
        $notableClass = last(explode('\\', get_class($note->notable)));
        $transformer = 'App\\Transformers\\Staff\\' . $notableClass . '\\ShowTransformer';
        return $this->item($note->notable(), new $transformer());
    }
}
