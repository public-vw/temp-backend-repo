<?php

namespace App\Transformers\Staff\StaticPage;

use App\Models\StaticPage;
use App\Transformers\Staff\StaticPageContent\IndexTransformer as ContentIndexTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
    ];

    protected array $availableIncludes = [
        'final_content',
    ];

    public function transform(StaticPage $staticPage): array
    {
        return [
            'id' => $staticPage->id,
            'version_counts' => $staticPage->contents()->count(),
            'title' => $staticPage->title,
            'url' => $staticPage->url,# if it is internal, start with /, if it is external, start with http:// or https://
        ];
    }

    public function includeFinalContent(StaticPage $staticPage): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $staticPage->final_content) {
            return $this->null();
        }
        return $this->item($staticPage->final_content, new ContentIndexTransformer());
    }
}
