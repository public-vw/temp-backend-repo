<?php

namespace App\Transformers\Staff\StaticPage;

use App\Models\StaticPage;
use App\Transformers\Staff\Image\IndexTransformer as ImageIndexTransformer;
use App\Transformers\Staff\StaticPageContent\ShowTransformer as ContentShowTransformer;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
    ];

    protected array $availableIncludes = [
        'final_content',
    ];

    public function transform(StaticPage $staticPage): array
    {
        return [
            'id' => (int) $staticPage->id,
            'version_counts' => $staticPage->contents()->count(),
            'title' => $staticPage->title,
            'url' => $staticPage->url,# if it is internal, start with /, if it is external, start with http:// or https://
        ];
    }

    public function includeFinalContent(StaticPage $staticPage): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $staticPage->final_content) {
            return $this->null();
        }
        return $this->item($staticPage->final_content, new ContentShowTransformer());
    }

    public function includeImages(StaticPage $staticPage): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        if (! $staticPage->final_content) {
            return $this->null();
        }
        return $this->collection($staticPage->final_content->attachables, new ImageIndexTransformer());
    }
}
