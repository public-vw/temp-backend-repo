<?php

namespace App\Transformers\Staff\Attachable;

use App\Models\Attachable;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
    ];

    protected array $availableIncludes = [

    ];

    public function transform(Attachable $attachable): array
    {
        $attachment = $attachable->attachment;

        return [
            'incontent_id' => $attachment->id,
            'unique_id' => $attachable->id,
        ];
    }
}
