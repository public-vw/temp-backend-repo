<?php

namespace App\Transformers\Staff\ArticleCategoryContent;

use App\Models\ArticleCategoryContent;
use App\Transformers\Staff\ArticleCategory\ShowTransformer as CategoryShowTransformer;
use App\Transformers\Staff\Image\IndexTransformer as ImageIndexTransformer;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'images',
    ];

    protected array $availableIncludes = [
        'category',
    ];

    public function transform(ArticleCategoryContent $articleCategoryContent): array
    {
        return [
            'id' => $articleCategoryContent->id,
            'uri' => $articleCategoryContent->uri,
            'version' => $articleCategoryContent->version,
            'title' => $articleCategoryContent->title,
            'heading' => $articleCategoryContent->heading,
            'description' => $articleCategoryContent->description,
            'summary' => $articleCategoryContent->summary,
            'updated_at' => $articleCategoryContent->updated_at->timestamp . Str::substr('000' . $articleCategoryContent->id, -3),
            'status' => (string) config('enums.contents.status.' . (int) $articleCategoryContent->status),
        ];
    }

    public function includeCategory(ArticleCategoryContent $articleCategoryContent): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $articleCategoryContent->container) {
            return $this->null();
        }
        return $this->item($articleCategoryContent->container, new CategoryShowTransformer());
    }

    public function includeImages(ArticleCategoryContent $articleCategoryContent): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        return $this->collection($articleCategoryContent->attachables, new ImageIndexTransformer());
    }
}
