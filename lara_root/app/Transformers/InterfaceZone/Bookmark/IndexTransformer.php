<?php

namespace App\Transformers\InterfaceZone\Bookmark;

use App\Models\Bookmark;
use App\Transformers\InterfaceZone\Article\IndexTransformer as ArticleIndexTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'bookmarkable',
    ];

    protected array $availableIncludes = [

    ];

    public function transform(Bookmark $bookmark): array
    {
        return [
            'id' => $bookmark->id,
            'model_type' => class2config($bookmark->bookmarkable_type),
        ];
    }

    public function includeBookmarkable(Bookmark $bookmark): \League\Fractal\Resource\Item
    {
        return $this->item($bookmark->bookmarkable, new ArticleIndexTransformer());
    }
}
