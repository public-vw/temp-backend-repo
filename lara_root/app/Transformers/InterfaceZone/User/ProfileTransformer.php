<?php

namespace App\Transformers\InterfaceZone\User;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class ProfileTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(User $user): array
    {
        return [
            'id' => $user->id,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,

            'username' => $user->username,
            'nickname' => $user->nickname,
            'ref_code' => $user->ref_code,

            'mobile' => $user->mobile,
            'mobile_verified_at' => $user->mobile_verified_at,
            'email' => $user->email,
            'email_verified_at' => $user->email_verified_at,

            'random_color' => config('enums.random_colors.' . $user->random_color),

            'telegram_uid' => $user->telegram_uid,
            'referer' => $user->referer,

            'use_two_factor_auth' => $user->use_two_factor_auth,

            'status' => config('enums.users.status.' . $user->status),
        ];
    }
}
