<?php

namespace App\Transformers\InterfaceZone\User;

use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class SafeShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(User $user): array
    {
        return [
            'id' => $user->id,
            'fullname' => $user->name,
            'nickname' => $user->nickname,
            'avatar' => $user->avatar,
            'random_color' => config('enums.random_colors.' . $user->random_color),
            'created_at' => $user->created_at->timestamp . Str::substr('000' . $user->id, -3),
            'roles' => $user->roles->pluck('name')->toArray(),
            'status' => config('enums.users.status.' . $user->status),
        ];
    }
}
