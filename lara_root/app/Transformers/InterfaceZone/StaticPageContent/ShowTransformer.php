<?php

namespace App\Transformers\InterfaceZone\StaticPageContent;

use App\Models\StaticPageContent;
use App\Transformers\InterfaceZone\Image\IndexTransformer as ImageIndexTransformer;
use App\Transformers\InterfaceZone\StaticPage\ShowTransformer as StaticPageShowTransformer;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'images',
    ];

    protected array $availableIncludes = [
        'static_page',
    ];

    public function transform(StaticPageContent $staticPageContent): array
    {
        return [
            'id' => $staticPageContent->id,

            'seo_details' => $staticPageContent->seo,
            'heading' => $staticPageContent->heading,
            'reading_time' => $staticPageContent->reading_time,
            'content' => $staticPageContent->content,
            'data' => $staticPageContent->data,
            'updated_at' => $staticPageContent->updated_at->timestamp . Str::substr('000' . $staticPageContent->id, -3),
            //'rich_snippets' => $staticPageContent->rich_snippets,
        ];
    }

    public function includeStaticPage(StaticPageContent $staticPageContent): \League\Fractal\Resource\Item
    {
        return $this->item($staticPageContent->container, new StaticPageShowTransformer());
    }

    public function includeImages(StaticPageContent $staticPageContent): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        return $this->collection($staticPageContent->attachables, new ImageIndexTransformer());
    }
}
