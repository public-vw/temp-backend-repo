<?php

namespace App\Transformers\InterfaceZone\SeoRichSnippet;

use App\Models\SeoRichSnippet;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(SeoRichSnippet $seoRichSnippet): array
    {
        return [
            'id' => $seoRichSnippet->id,
            'type' => (string) config('enums.rich_snippets.types.' . (int) $seoRichSnippet->type),
            'data' => $seoRichSnippet->data,
        ];
    }
}
