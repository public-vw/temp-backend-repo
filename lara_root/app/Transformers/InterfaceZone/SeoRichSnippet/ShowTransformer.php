<?php

namespace App\Transformers\InterfaceZone\SeoRichSnippet;

use App\Models\SeoRichSnippet;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(SeoRichSnippet $seoRichSnippet): array
    {
        return [
            'richsnippetable' => [
                'id' => $seoRichSnippet->richsnippetable?->id,
                'type' => is_null($seoRichSnippet->richsnippetable) ? null : get_class($seoRichSnippet->richsnippetable),
            ],
            'id' => $seoRichSnippet->id,
            'type' => (string) config('enums.rich_snippets.types.' . (int) $seoRichSnippet->type),
            'data' => $seoRichSnippet->data, //TODO: check this with real data. this should be the last data with same type. for instance, if we have two google type, this should be the last id
        ];
    }
}
