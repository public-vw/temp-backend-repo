<?php

namespace App\Transformers\InterfaceZone\Menu;

use App\Models\Menu;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected static array $includedIds = [];

    protected array $defaultIncludes = [
        'children',
    ];

    protected array $availableIncludes = [

    ];

    public function transform(Menu $menu): array
    {
        return [
            'id' => $menu->id,
            'title' => $menu->title,
            'url' => $menu->heading ? null : $menu->menuable?->url,
            'icon' => $menu->icon?->snippet,
            'new_tab' => (bool) $menu->new_tab,
            'nofollow' => (bool) $menu->nofollow,
        ];
    }

    public function includeChildren(Menu $menu): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        if (in_array($menu->id, self::$includedIds) || $menu->children->isEmpty()) {
            return $this->null();
        }

        self::$includedIds[] = $menu->id;

        return $this->collection($menu->children, new ShowTransformer());
    }
}
