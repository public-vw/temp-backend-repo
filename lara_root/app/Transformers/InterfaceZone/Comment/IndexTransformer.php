<?php

namespace App\Transformers\InterfaceZone\Comment;

use App\Models\Comment;
use App\Transformers\InterfaceZone\User\SafeShowTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'user',
    ];

    protected array $availableIncludes = [
        'replies',
        'parent',
    ];

    public function transform(Comment $comment): array
    {
        return [
            'id' => $comment->id,
            'content' => $comment->content,
            'created_at' => $comment->created_at,
        ];
    }

    public function includeUser(Comment $comment): \League\Fractal\Resource\Collection
    {
        $user = $comment->user;
        return $this->collection([$user], new SafeShowTransformer());
    }

    public function includeReplies(Comment $comment): \League\Fractal\Resource\Collection
    {
        $replies = $comment->replies;
        return $this->collection($replies, new self());
    }

    public function includeParent(Comment $comment): \League\Fractal\Resource\Collection
    {
        $parent = $comment->parent;
        return $this->collection($parent, new self());
    }
}
