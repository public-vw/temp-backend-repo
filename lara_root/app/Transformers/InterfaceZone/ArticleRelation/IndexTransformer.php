<?php

namespace App\Transformers\InterfaceZone\ArticleRelation;

use App\Models\ArticleRelation;
use App\Transformers\InterfaceZone\Article\IndexTransformer as ArticleIndexTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'article',
    ];

    protected array $availableIncludes = [
    ];

    public function transform(ArticleRelation $articleRelation): array
    {
        return [
            'id' => $articleRelation->id,
        ];
    }

    public function includeArticle(ArticleRelation $articleRelation): \League\Fractal\Resource\Item
    {
        return $this->item($articleRelation->article, new ArticleIndexTransformer());
    }
}
