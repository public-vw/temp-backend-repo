<?php
//TODO: this is a front-end needed api and we must move it to sysadmin or delete it before lunch

namespace App\Transformers\InterfaceZone\All;

use App\Models\ArticleCategory;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    public function transform(ArticleCategory $articleCategory): array
    {
        return [
            'id' => $articleCategory->id,
            'parent_id' => $articleCategory->publishedParent()?->id,
            'title' => $articleCategory->final_content?->title ?? 'No Title Yet!',
            'url' => $articleCategory->url,# if it is internal, start with /, if it is external, start with http:// or https://
            'articles' => $articleCategory->activeArticles()->count(),
        ];
    }
}
