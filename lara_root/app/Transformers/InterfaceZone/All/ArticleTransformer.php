<?php
//TODO: this is a front-end needed api and we must move it to sysadmin or delete it before lunch

namespace App\Transformers\InterfaceZone\All;

use App\Models\Article;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{
    public function transform(Article $article): array
    {
        return [
            'id' => $article->id,
            'heading' => $article->final_content?->heading ?? 'No Heading Yet!',
            'url' => $article->url ?? 'No URL Yet!',
            'uri' => $article->final_content?->uri,
        ];
    }
}
