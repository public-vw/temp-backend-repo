<?php

namespace App\Transformers\InterfaceZone\StaticPage;

use App\Models\StaticPage;
use App\Transformers\InterfaceZone\StaticPageContent\ShowTransformer as ContentShowTransformer;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'final_content',
    ];

    protected array $availableIncludes = [
    ];

    public function transform(StaticPage $staticPage): array
    {
        return [
            'id' => $staticPage->id,
            'title' => $staticPage->title,
        ];
    }

    public function includeFinalContent(StaticPage $article): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $article->final_content) {
            return $this->null();
        }
        return $this->item($article->final_content, new ContentShowTransformer());
    }
}
