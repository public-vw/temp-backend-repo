<?php

namespace App\Transformers\InterfaceZone\Article;

use App\Models\Article;
use App\Transformers\InterfaceZone\ArticleContent\ShowTransformer as ContentShowTransformer;
use App\Transformers\InterfaceZone\Comment\IndexTransformer as CommentIndexTransformer;
use App\Transformers\InterfaceZone\User\SafeShowTransformer as AuthorShowTransformer;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'author',
        'final_content',
    ];

    protected array $availableIncludes = [
        'comments',
    ];

    public function transform(Article $article): array
    {
        return [
            'id' => $article->id,

            'category' => $article->published_category ? [
                'id' => $article->published_category->id,
                'title' => $article->published_category->final_content->title,
                'url' => $article->published_category->url,
            ] : [],

            'features' => [
                'counts' => [
                    'likes' => $article->likes->count(), #TODO: add likes data structure
                    'comments' => $article->accepted_comments->count(),
                    'visits' => 1152,
                    //                "visits"   => $article->visits->count(),#TODO: add visits data structure
                ],
            ],
            'mode' => 'sm',         # sm | md | lg | xl
            //TODO:activate type
            //            "type"     => "image",      # image | video | audio | text
            'urlType' => 'external',   # default is internal
            'url' => $article->url,# if it is internal, start with /, if it is external, start with http:// or https://
            'label' => $article->label ?? 'no-label',#TODO: add label data structure
        ];
    }

    public function includeComments(Article $article): \League\Fractal\Resource\Collection
    {
        return $this->collection($article->comments, new CommentIndexTransformer());
    }

    public function includeFinalContent(Article $article): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $article->final_content) {
            return $this->null();
        }
        return $this->item($article->final_content, new ContentShowTransformer());
    }

    public function includeAuthor(Article $article): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $article->author) {
            return $this->null();
        }
        return $this->item($article->author, new AuthorShowTransformer());
    }
}
