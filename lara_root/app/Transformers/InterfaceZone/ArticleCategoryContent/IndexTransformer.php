<?php

namespace App\Transformers\InterfaceZone\ArticleCategoryContent;

use App\Models\ArticleCategoryContent;
use App\Transformers\InterfaceZone\ArticleCategory\ShowTransformer as CategoryShowTransformer;
use App\Transformers\InterfaceZone\Image\IndexTransformer as ImageIndexTransformer;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'images',
    ];

    protected array $availableIncludes = [
        'category',
    ];

    public function transform(ArticleCategoryContent $articleCategoryContent): array
    {
        return [
            'id' => $articleCategoryContent->id,
            'title' => $articleCategoryContent->title,
            'heading' => $articleCategoryContent->heading,
            'summary' => $articleCategoryContent->summary,
            'updated_at' => $articleCategoryContent->updated_at->timestamp . Str::substr('000' . $articleCategoryContent->id, -3),
        ];
    }

    public function includeCategory(ArticleCategoryContent $articleCategoryContent): \League\Fractal\Resource\Item
    {
        return $this->item($articleCategoryContent->container, new CategoryShowTransformer());
    }

    public function includeImages(ArticleCategoryContent $articleCategoryContent): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        return $this->collection($articleCategoryContent->attachables, new ImageIndexTransformer());
    }
}
