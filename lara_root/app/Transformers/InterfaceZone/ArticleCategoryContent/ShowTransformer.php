<?php

namespace App\Transformers\InterfaceZone\ArticleCategoryContent;

use App\Models\ArticleCategoryContent;
use App\Transformers\InterfaceZone\ArticleCategory\ShowTransformer as CategoryShowTransformer;
use App\Transformers\InterfaceZone\Image\IndexTransformer as ImageIndexTransformer;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'images',
    ];

    protected array $availableIncludes = [
        'category',
    ];

    public function transform(ArticleCategoryContent $articleCategoryContent): array
    {
        return [
            'id' => $articleCategoryContent->id,
            'title' => $articleCategoryContent->title,
            'heading' => $articleCategoryContent->heading,
            'description' => $articleCategoryContent->description,
            'seo_details' => $articleCategoryContent->seo,
            'updated_at' => $articleCategoryContent->updated_at->timestamp . Str::substr('000' . $articleCategoryContent->id, -3),
         //   'rich_snippets' => $articleCategoryContent->rich_snippets,
        ];
    }

    public function includeCategory(ArticleCategoryContent $articleCategoryContent): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        return $this->item($articleCategoryContent->container, new CategoryShowTransformer());
    }

    public function includeImages(ArticleCategoryContent $articleCategoryContent): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        return $this->collection($articleCategoryContent->attachables, new ImageIndexTransformer());
    }
}
