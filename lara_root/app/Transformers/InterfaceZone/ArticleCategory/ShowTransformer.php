<?php

namespace App\Transformers\InterfaceZone\ArticleCategory;

use App\Models\ArticleCategory;
use App\Transformers\InterfaceZone\ArticleCategoryContent\ShowTransformer as ContentShowTransformer;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
    ];

    protected array $availableIncludes = [
        'parent',
        'final_content',
        'children',
    ];

    public function transform(ArticleCategory $articleCategory): array
    {
        return [
            'id' => $articleCategory->id,
            'url' => $articleCategory->url,# if it is internal, start with /, if it is external, start with http:// or https://
            'title' => $articleCategory->final_content?->title,

            'features' => [
                'counts' => [
                    'articles' => $articleCategory->activeArticles()->count(),
                ],
            ],
        ];
    }

    public function includeChildren(ArticleCategory $articleCategory): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        $published_children = $articleCategory->publishedChildren;
        if ($published_children->count() === 0) {
            return $this->null();
        }
        return $this->collection($published_children, new IndexTransformer());
    }

    public function includeParent(ArticleCategory $articleCategory): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        $published_parent = $articleCategory->publishedParent();
        if (! $published_parent) {
            return $this->null();
        }
        return $this->item($published_parent, new self());
    }

    public function includeFinalContent(ArticleCategory $articleCategory): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $articleCategory->published) {
            return $this->null();
        }
        return $this->item($articleCategory->final_content, new ContentShowTransformer());
    }
}
