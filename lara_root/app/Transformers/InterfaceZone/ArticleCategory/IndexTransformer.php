<?php

namespace App\Transformers\InterfaceZone\ArticleCategory;

use App\Models\ArticleCategory;
use App\Transformers\InterfaceZone\ArticleCategoryContent\IndexTransformer as ContentIndexTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'final_content',
    ];

    protected array $availableIncludes = [
        'parent',
    ];

    public function transform(ArticleCategory $articleCategory): array
    {
        return [
            'id' => $articleCategory->id,
            'url' => $articleCategory->url,# if it is internal, start with /, if it is external, start with http:// or https://

            'features' => [
                'counts' => [
                    'articles' => $articleCategory->activeArticles()->count(),
                ],
            ],
        ];
    }

    public function includeParent(ArticleCategory $articleCategory): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        $published_parent = $articleCategory->publishedParent();
        if (! $published_parent) {
            return $this->null();
        }
        return $this->item($published_parent, new ShowTransformer());
    }

    public function includeFinalContent(ArticleCategory $articleCategory): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $articleCategory->published) {
            return $this->null();
        }
        return $this->item($articleCategory->final_content, new ContentIndexTransformer());
    }
}
