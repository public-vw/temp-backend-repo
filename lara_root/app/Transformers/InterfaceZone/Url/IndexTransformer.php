<?php

namespace App\Transformers\InterfaceZone\Url;

use App\Models\Url;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    public function transform(Url $url): array
    {
        return [
            'id' => $url->urlable->id,
            'model' => last(explode('\\', $url->urlable_type)),
            'url' => $url->url,
        ];
    }
}
