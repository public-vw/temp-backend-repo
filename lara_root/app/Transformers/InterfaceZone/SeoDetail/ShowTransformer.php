<?php

namespace App\Transformers\InterfaceZone\SeoDetail;

use App\Models\SeoDetail;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(SeoDetail $seoDetail): array
    {
        return [
            'seoable' => [
                'id' => $seoDetail->seoable?->id,
                'type' => is_null($seoDetail->seoable) ? null : get_class($seoDetail->seoable),
            ],
            'id' => $seoDetail->id,
            'type' => (string) config('enums.seo_details.types.' . (int) $seoDetail->type),
            'data' => $seoDetail->data, //TODO: check this with real data. this should be the last data with same type. for instance, if we have two google type, this should be the last id
        ];
    }
}
