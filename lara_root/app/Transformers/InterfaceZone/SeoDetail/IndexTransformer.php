<?php

namespace App\Transformers\InterfaceZone\SeoDetail;

use App\Models\SeoDetail;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(SeoDetail $seoDetail): array
    {
        return [
            'id' => $seoDetail->id,
            'type' => (string) config('enums.seo_details.types.' . (int) $seoDetail->type),
            'data' => $seoDetail->data,
        ];
    }
}
