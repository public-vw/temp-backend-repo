<?php

namespace App\Transformers\InterfaceZone\Like;

use App\Models\Like;
use App\Transformers\InterfaceZone\Article\IndexTransformer as ArticleIndexTransformer;
use App\Transformers\InterfaceZone\Comment\IndexTransformer as CommentIndexTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'likable',
    ];

    protected array $availableIncludes = [

    ];

    public function transform(Like $like): array
    {
        return [
            'id' => $like->id,
            'model_type' => class2config($like->likable_type),
        ];
    }

    public function includeLikable(Like $bookmark): \League\Fractal\Resource\Item
    {
        //TODO: needs better implementation.
        //problems: add new entity in likables
        //solution: use a polymorphism implementation
        $class = get_class($bookmark->likable);
        return in_array($class, ['App\\Models\\Article','Article']) ?
            $this->item($bookmark->likable, new ArticleIndexTransformer())
            : $this->item($bookmark->likable, new CommentIndexTransformer());
    }
}
