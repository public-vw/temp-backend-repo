<?php

namespace App\Transformers\InterfaceZone\ArticleContent;

use App\Models\ArticleContent;
use App\Transformers\InterfaceZone\Article\ShowTransformer as ArticleShowTransformer;
use App\Transformers\InterfaceZone\Image\IndexTransformer as ImageIndexTransformer;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'images',
    ];

    protected array $availableIncludes = [
        'article',
    ];

    public function transform(ArticleContent $articleContent): array
    {
        return [
            'id' => $articleContent->id,

            'seo_details' => $articleContent->seo,
            'heading' => $articleContent->heading,
            'reading_time' => $articleContent->reading_time,
            'content' => $articleContent->content,
            'inspiration' => $articleContent->inspiration,
            'updated_at' => $articleContent->updated_at->timestamp . Str::substr('000' . $articleContent->id, -3),
            //'rich_snippets' => $articleContent->rich_snippets,
        ];
    }

    public function includeArticle(ArticleContent $articleContent): \League\Fractal\Resource\Item
    {
        return $this->item($articleContent->container, new ArticleShowTransformer());
    }

    public function includeImages(ArticleContent $articleContent): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        return $this->collection($articleContent->attachables, new ImageIndexTransformer());
    }
}
