<?php

namespace App\Transformers;

use App\Models\Icon;
use League\Fractal\TransformerAbstract;

class IconTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(Icon $icon): array
    {
        return [
            'snippet' => $icon->snippet,
        ];
    }
}
