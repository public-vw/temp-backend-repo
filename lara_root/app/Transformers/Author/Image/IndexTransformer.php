<?php

namespace App\Transformers\Author\Image;

use App\Models\Attachable;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(Attachable $attachable): array
    {
        $attachment = $attachable->attachment;

        return [
            'incontent_id' => $attachment->id,
            'unique_id' => $attachable->id,
            'url' => url('/files' . $attachment->url, secure: config('app.force_https')),
            'media' => $attachment->mediaName(),
            'type' => $attachable->type->slug,
            'attributes' => $attachable->attributes,
            'noindex' => $attachment->noindex,
            'blur' => $attachment->properties['blur'] ?? null,
        ];
    }
}
