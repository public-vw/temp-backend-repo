<?php

namespace App\Transformers\Author\ArticleContent;

use App\Models\ArticleContent;
use App\Transformers\Author\Article\ShowTransformer as ArticleShowTransformer;
use App\Transformers\Author\Image\IndexTransformer as ImageIndexTransformer;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'images',
    ];

    protected array $availableIncludes = [
        'article',
    ];

    public function transform(ArticleContent $articleContent): array
    {
        return [
            'id' => $articleContent->id,
            'category' => [
                'id' => $articleContent->category?->id,
                'title' => $articleContent->category?->final_content->title,
                'url' => $articleContent->category?->url,
            ],
            'version' => $articleContent->version,
            'heading' => $articleContent->heading,
            'reading_time' => $articleContent->reading_time,
            'summary' => $articleContent->summary,
            'updated_at' => $articleContent->updated_at->toDateTimeString(),
            'status' => (string) config('enums.contents.status.' . (int) $articleContent->status),
        ];
    }

    public function includeArticle(ArticleContent $articleContent): \League\Fractal\Resource\Item
    {
        return $this->item($articleContent->container, new ArticleShowTransformer());
    }

    public function includeImages(ArticleContent $articleContent): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        return $this->collection($articleContent->attachables, new ImageIndexTransformer());
    }
}
