<?php

namespace App\Transformers\Author\ArticleCategoryContent;

use App\Models\ArticleCategoryContent;
use App\Transformers\Author\ArticleCategory\ShowTransformer as CategoryShowTransformer;
use App\Transformers\Author\Image\IndexTransformer as ImageIndexTransformer;
use League\Fractal\TransformerAbstract;

class ShowTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [
        'images',
    ];

    protected array $availableIncludes = [
        'category',
    ];

    public function transform(ArticleCategoryContent $articleCategoryContent): array
    {
        return [
            'id' => $articleCategoryContent->id,
            'version' => $articleCategoryContent->version,
            'title' => $articleCategoryContent->title,
            'heading' => $articleCategoryContent->heading,
            'description' => $articleCategoryContent->description,
            'summary' => $articleCategoryContent->summary,
            'updated_at' => $articleCategoryContent->updated_at->toDateTimeString(),
            'status' => (string) config('enums.contents.status.' . (int) $articleCategoryContent->status),
        ];
    }

    public function includeCategory(ArticleCategoryContent $articleCategoryContent): \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
    {
        if (! $articleCategoryContent->container) {
            return $this->null();
        }
        return $this->item($articleCategoryContent->container, new CategoryShowTransformer());
    }

    public function includeImages(ArticleCategoryContent $articleCategoryContent): \League\Fractal\Resource\Collection|\League\Fractal\Resource\NullResource
    {
        return $this->collection($articleCategoryContent->attachables, new ImageIndexTransformer());
    }
}
