<?php

namespace App\Transformers\Sysadmin\User;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'title' => $user->title,
        ];
    }
}
