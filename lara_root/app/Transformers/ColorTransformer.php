<?php

namespace App\Transformers;

use App\Models\Color;
use League\Fractal\TransformerAbstract;

class ColorTransformer extends TransformerAbstract
{
    protected array $defaultIncludes = [

    ];

    protected array $availableIncludes = [

    ];

    public function transform(Color $color): array
    {
        return [
            'foreground' => $color->foreground,
            'background' => $color->background,
        ];
    }
}
