<?php

namespace App\Serializers;

use League\Fractal\Serializer\ArraySerializer;

class SimpleArraySerializer extends ArraySerializer
{
    public function includeData($data, $includedData)
    {
        return $includedData;
    }
}
