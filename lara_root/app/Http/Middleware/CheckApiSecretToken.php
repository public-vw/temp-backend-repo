<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckApiSecretToken
{
    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->header('Api-Secret-Token', '');
        $apiToken = config('routes.api_secret_token', '');

        if ($apiToken !== '' && $token !== $apiToken) {
            //TODO: report to the firewall
            return response()->noContent()->setStatusCode(404);
        }

        return $next($request);
    }
}
