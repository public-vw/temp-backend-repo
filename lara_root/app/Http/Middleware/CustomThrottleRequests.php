<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Symfony\Component\HttpFoundation\Response;

class CustomThrottleRequests extends ThrottleRequests
{
    public function handle($request, Closure $next, $maxAttempts = 60, $decayMinutes = 1, $prefix = ''): Response
    {
        $ip = get_ip($request);
        $token = $request->header('Build-Token');

        // List of IPs to bypass
        $bypassIps = config('front-app.white-ips');

        // Bypass token
        $bypassToken = config('front-app.build-token');

        if (in_array($ip, $bypassIps) || $token === $bypassToken) {
            return $next($request);
        }

        return parent::handle($request, $next, $maxAttempts, $decayMinutes, $prefix);
    }
}
