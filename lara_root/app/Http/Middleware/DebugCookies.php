<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class DebugCookies
{
    public function handle(Request $request, Closure $next): Response
    {
        $cookies = $request->cookies->all();

        foreach ($cookies as $name => $value) {
            Log::info("Cookie Name: {$name}, Cookie Value: {$value}");
        }
        dump('Cookies: ', $cookies);

        return $next($request);
    }
}
