<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckFrontendBuildToken
{
    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->header('Build-Token');

        if ($token !== config('front-app.build-token')) {
            return response()->json(null, 404);
        }

        return $next($request);
    }
}
