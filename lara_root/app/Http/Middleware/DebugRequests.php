<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class DebugRequests
{
    public function handle(Request $request, Closure $next): Response
    {
        Log::info('Requests: ', $request->all());
        dump('Requests: ', $request->headers->all());
        return $next($request);
    }
}
