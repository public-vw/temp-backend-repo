<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class DebugHeaders
{
    public function handle(Request $request, Closure $next): Response
    {
        Log::info('Headers: ', $request->headers->all());
        dump('Headers: ', $request->headers->all());
        return $next($request);
    }
}
