<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    //TODO: this should be fixed! the auth should not be passed throw csrf (the xsrf cookie is not set somehow)
    protected $except = [
        'auth/session/*',
        'api/v1/*',
    ];
}
