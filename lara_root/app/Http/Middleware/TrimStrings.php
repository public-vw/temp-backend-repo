<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TrimStrings as Middleware;

class TrimStrings extends Middleware
{
    /**
     * The names of the attributes that should not be trimmed.
     *
     * @var array<int, string>
     */
    protected $except = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    protected $except_patterns = [
        '/^content\.\d+\.children\.\d+\.text$/',
    ];

    protected function transform($key, $value)
    {
        foreach ($this->except_patterns as $pattern) {
            if (preg_match($pattern, $key)) {
                return $value;
            }
        }

        return parent::transform($key, $value);
    }
}
