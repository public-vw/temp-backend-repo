<?php

namespace App\Http\Controllers\InterfaceZone\SecureAPI\V1;

use App\Http\Controllers\ApiController;
use App\Models\Article;
use App\Models\Comment;
use App\Requests\InterfaceZone\API\V1\Comment\CreateRequest;
use App\Requests\InterfaceZone\API\V1\Comment\UpdateRequest;
use App\Transformers\InterfaceZone\Comment\IndexTransformer;

class CommentController extends ApiController
{
    public function getYoursComment($article_id): \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
    {
        $article = Article::findOrFail($article_id);

        $this->authorize('viewAny', Comment::class);
        $this->authorize('view', $article);

        $query = $article
            ->comments()
            ->orderBy('created_at', 'desc')
            ->get();

        return $this->responseByAllFinalFractal($query, new IndexTransformer());
    }

    public function store(CreateRequest $request, $article_id): \Illuminate\Http\JsonResponse
    {
        $article = Article::findOrFail($article_id);

        $this->authorize('create', Comment::class);
        $this->authorize('view', $article);

        $query = $article->comments()->create($request->validated());
        $query->fill([
            'user_id' => $request->user()->id,
            #TODO: change this with setting of website
            'status' => config_key('enums.comments.status', 'accepted'),
        ])->save();

        return self::responseJson(['id' => $query->id]);
    }

    public function update(UpdateRequest $request, Comment $comment): \Illuminate\Http\JsonResponse|array
    {
        $this->authorize('update', $comment);

        $comment->update($request->validated());

        return self::success(__('interface_zone.comment.update.success'));
    }
}
