<?php

namespace App\Http\Controllers\InterfaceZone\SecureAPI\V1;

use App\Http\Controllers\ApiController;
use App\Models\Like;
use App\Requests\InterfaceZone\API\V1\Like\CheckRequest;
use App\Requests\InterfaceZone\API\V1\Like\ToggleRequest;
use App\Transformers\InterfaceZone\Like\IndexTransformer;
use Illuminate\Http\Request;

class LikeController extends ApiController
{
    protected string $model = Like::class;

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $likes = $request->user()->likes()->orderByDesc('updated_at')->getQuery();

        return $this->responseByCursorFractal($likes, new IndexTransformer());
    }

    public function toggle(ToggleRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', $this->model);

        $input = $request->validated();
        $query = [
            'likable_type' => config2class($input['model_type']),
            'likable_id' => $input['model_id'],
        ];

        $like = $request->user()->likes()->firstOrCreate($query);

        $final_status = $like->toggle();

        return self::responseJson(['id' => $like->id, 'final_status' => $final_status]);
    }

    public function check(CheckRequest $request): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('viewAny', $this->model);

        $liked = $request->user()->likes()->where([
            'likable_type' => model2class($request->input('model_type')),
            'likable_id' => $request->input('model_id'),
        ])->exists();

        return self::responseJson($liked ?? false);
    }
}
