<?php

namespace App\Http\Controllers\InterfaceZone\SecureAPI\V1;

use App\Http\Controllers\ApiController;
use App\Notifications\SendEmailOtp;
use App\Notifications\SendSmsOtp;
use App\Requests\Auth\API\V1\VerifyOtpRequest;
use App\Services\Otp\Otp;
use Carbon\Carbon;

class VerifyController extends ApiController
{
    public function email(Otp $otp_service)
    {
        $user = auth()->user();
        abort_if(! is_null($user->email_verified_at), '409', 'Email already verified');

        $otp = $otp_service->makeOtp($user);

        $tracking_code = $otp_service->makeTrackingCode('email', $otp, $user);
        $user->notify(new SendEmailOtp($otp));

        return response()->json([
            'developer_code' => encrypt($otp), #TODO: [LUNCH] remove this
            'tracking_code' => $tracking_code,
        ]);
    }

    public function mobile(Otp $otp_service)
    {
        $user = auth()->user();
        abort_if(! is_null($user->mobile_verified_at), '409', 'Mobile already verified');

        $otp = $this->makeOtp($user);

        $tracking_code = $otp_service->makeTrackingCode('mobile', $otp, $user);
        $user->notify(new SendSmsOtp($otp));

        return response()->json([
            'developer_code' => encrypt($otp), #TODO: [LUNCH] remove this
            'tracking_code' => $tracking_code,
        ]);
    }

    public function verify(VerifyOtpRequest $request, Otp $otp_service): \Illuminate\Http\JsonResponse
    {
        $track = $otp_service->verification($request->tracking_code, $request->otp);

        $track['user'][$track['field'].'_verified_at'] = Carbon::now();
        $track['user']->save();

        return response()->json(['message' => ucwords($track['field']) . ' verified successfully.']);
    }
}
