<?php

namespace App\Http\Controllers\InterfaceZone\SecureAPI\V1;

use App\Http\Controllers\ApiController;
use App\Requests\InterfaceZone\API\V1\User\UpdateRequest;
use App\Transformers\InterfaceZone\User\ProfileTransformer;
use Illuminate\Http\Request;

class ProfileController extends ApiController
{
    public function show(Request $request): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $user = $request->user();
        $this->authorize('view', $user);

        return $this->responseByOneFractal($user, new ProfileTransformer());
    }

    public function update(UpdateRequest $request): \Illuminate\Http\JsonResponse|array
    {
        $user = $request->user();

        $this->authorize('update', $user);

        $user->update($request->validated());

        return self::success(__('interface_zone.profile.update.success'));
    }
}
