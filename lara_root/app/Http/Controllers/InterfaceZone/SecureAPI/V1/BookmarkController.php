<?php

namespace App\Http\Controllers\InterfaceZone\SecureAPI\V1;

use App\Http\Controllers\ApiController;
use App\Models\Bookmark;
use App\Requests\InterfaceZone\API\V1\Bookmark\CheckRequest;
use App\Requests\InterfaceZone\API\V1\Bookmark\ToggleRequest;
use App\Transformers\InterfaceZone\Bookmark\IndexTransformer;
use Illuminate\Http\Request;

class BookmarkController extends ApiController
{
    protected string $model = Bookmark::class;

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $bookmarks = $request->user()->bookmarks()->orderByDesc('updated_at')->getQuery();

        return $this->responseByCursorFractal($bookmarks, new IndexTransformer());
    }

    public function toggle(ToggleRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', $this->model);

        $input = $request->validated();
        $query = [
            'bookmarkable_type' => config2class($input['model_type']),
            'bookmarkable_id' => $input['model_id'],
        ];

        $bookmark = $request->user()->bookmarks()->firstOrCreate($query);

        $final_status = $bookmark->toggle();

        return self::responseJson(['id' => $bookmark->id, 'final_status' => $final_status]);
    }

    public function check(CheckRequest $request): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('viewAny', $this->model);

        $booked = $request->user()->bookmarks()->where([
            'bookmarkable_type' => model2class($request->input('model_type')),
            'bookmarkable_id' => $request->input('model_id'),
        ])->exists();

        return self::responseJson($booked ?? false);
    }
}
