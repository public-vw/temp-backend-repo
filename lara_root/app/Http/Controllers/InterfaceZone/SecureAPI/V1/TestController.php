<?php

namespace App\Http\Controllers\InterfaceZone\SecureAPI\V1;

use App\Http\Controllers\ApiController;

class TestController extends ApiController
{
    public function ping(): \Illuminate\Http\JsonResponse|array
    {
        return self::success('Any Authed User | PONG!');
    }
}
