<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\Bookmark;
use App\Requests\InterfaceZone\API\V1\Bookmark\CheckRequest;
use App\Requests\InterfaceZone\API\V1\Bookmark\ToggleRequest;
use App\Transformers\InterfaceZone\Bookmark\IndexTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class BookmarkController extends ApiController
{
    protected string $model = Bookmark::class;

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $userIp = $request->ip();
        $cacheKey = "bookmarks:{$userIp}";

        $bookmarkIds = Cache::get($cacheKey, []);
        $bookmarks = Bookmark::whereIn('id', $bookmarkIds)->orderByDesc('updated_at')->newQuery();

        return $this->responseByCursorFractal($bookmarks, new IndexTransformer());
    }

    public function toggle(ToggleRequest $request): \Illuminate\Http\JsonResponse
    {
        $userIp = $request->ip();
        $input = $request->validated();
        $query = [
            'bookmarkable_type' => config2class($input['model_type']),
            'bookmarkable_id' => $input['model_id'],
        ];

        $bookmarkIds = Cache::get("bookmarks:{$userIp}", []);

        $bookmark = Bookmark::where('booked', true)->firstOrCreate($query);

        if (in_array($bookmark->id, $bookmarkIds)) {
            $bookmarkIds = array_diff($bookmarkIds, [$bookmark->id]);
            $finalStatus = false;
        } else {
            $bookmarkIds[] = $bookmark->id;
            $finalStatus = true;
        }
        $bookmark->toggle();

        Cache::put("bookmarks:{$userIp}", $bookmarkIds, now()->addHours(4));

        $cacheKey = "bookmarks:{$userIp}:{$bookmark->id}";
        Cache::put($cacheKey, $finalStatus, now()->addHours(4));

        return self::responseJson(['id' => $bookmark->id, 'final_status' => $finalStatus]);
    }

    public function check(CheckRequest $request): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $userIp = $request->ip();
        $input = $request->validated();
        $query = [
            'bookmarkable_type' => config2class($input['model_type']),
            'bookmarkable_id' => $input['model_id'],
        ];

        $bookmark = Bookmark::where($query)->first();
        $booked = Cache::get("bookmarks:{$userIp}:{$bookmark?->id}", false);

        return self::responseJson($booked ?? false);
    }
}
