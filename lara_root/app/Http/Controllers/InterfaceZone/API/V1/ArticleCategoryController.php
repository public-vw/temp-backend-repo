<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\ArticleCategory;
use App\Models\Url;
use App\Requests\InterfaceZone\API\V1\ArticleCategory\IndexRequest;
use App\Requests\InterfaceZone\API\V1\ArticleCategory\ShowRequest;
use App\Transformers\InterfaceZone\ArticleCategory\IndexTransformer;
use App\Transformers\InterfaceZone\ArticleCategory\ShowTransformer;

class ArticleCategoryController extends ApiController
{
    protected string $model = ArticleCategory::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $query = new $this->model();
        $query = $request->parent_id ? $query->where('parent_id', $request->parent_id) : $query->whereNull('parent_id');
        $query = $query->published();
        $query = $query->orderBy('order')->orderBy('updated_at', 'desc');

        return $this->responseByCursorFractal($query, new IndexTransformer());
    }

    public function show(ArticleCategory $articleCategory): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        abort_if(! $articleCategory->published, 404);
        return $this->responseByOneFractal($articleCategory, new ShowTransformer());
    }

    public function showByUrl(ShowRequest $request): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $requested_urls = [$request->url, '/'.$request->url()];
        $urlable = Url::whereIn('url', $requested_urls)->firstOrFail();
        $urlable = $urlable->urlable;
        abort_if(! $urlable->published, 404);

        return $this->responseByOneFractal($urlable, new ShowTransformer());
    }
}
