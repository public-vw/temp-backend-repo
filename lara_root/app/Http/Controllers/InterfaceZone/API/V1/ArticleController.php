<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Url;
use App\Requests\InterfaceZone\API\V1\Article\ShowRequest;
use App\Transformers\InterfaceZone\Article\IndexTransformer;
use App\Transformers\InterfaceZone\Article\ShowTransformer;
use Illuminate\Http\Request;

class ArticleController extends ApiController
{
    public function index(ArticleCategory $articleCategory, Request $request): \Illuminate\Http\JsonResponse
    {
        abort_if(! $articleCategory->published, 404);

        $query = $articleCategory->activeArticles();

        return $this->responseByCursorFractal($query, new IndexTransformer());
    }

    public function show(Article $article): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        return $this->responseByOneFractal($article, new ShowTransformer());
    }

    public function showByUrl(ShowRequest $request): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $urlable = Url::where('url', $request->url)->firstOrFail();
        $urlable = $urlable->urlable;
        abort_if(! $urlable->published, 404);

        return $this->responseByOneFractal($urlable, new ShowTransformer());
    }
}
