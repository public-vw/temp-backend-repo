<?php

//TODO: this is a front-end needed api and we must move it to sysadmin or delete it before lunch

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Transformers\InterfaceZone\All\ArticleTransformer as ArticleIndexTransformer;
use App\Transformers\InterfaceZone\All\CategoryTransformer as ArticleCategoryIndexTransformer;

class AllController extends ApiController
{
    protected int $perPageDefault = 3;

    public function categories(): \Illuminate\Http\JsonResponse
    {
        $query = ArticleCategory::published();
        $query = $query->orderBy('parent_id')->orderBy('order')->orderBy('updated_at', 'desc');

        return $this->responseByCursorFractal($query, new ArticleCategoryIndexTransformer());
    }
    public function articles(): \Illuminate\Http\JsonResponse
    {
        $query = Article::published();
        $query = $query->orderBy('updated_at', 'desc');

        return $this->responseByCursorFractal($query, new ArticleIndexTransformer());
    }
}
