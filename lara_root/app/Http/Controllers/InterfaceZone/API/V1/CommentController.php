<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\Article;
use App\Requests\InterfaceZone\API\V1\Comment\IndexRequest;
use App\Transformers\InterfaceZone\Comment\IndexTransformer;

class CommentController extends ApiController
{
    public function index(IndexRequest $request, $article_id)
    {
        $article = Article::findOrFail($article_id);

        return $this->responseByAllFinalFractal($article->comments, new IndexTransformer());
    }
}
