<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\Article;
use App\Models\Menu;
use App\Transformers\InterfaceZone\Article\IndexTransformer as ArticleIndexTransformer;
use Illuminate\Http\Request;

class FooterController extends ApiController
{
    protected string $model = Menu::class;

    // TODO: needs cleaner implementation
    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        switch ($request->place) {
            case 'last_articles':
                $articles = Article::published()->orderBy('updated_at', 'desc')->limit(3)->get();
                return $this->responseByAllFinalFractal($articles, new ArticleIndexTransformer());
            case 'twitter':
                // TODO: implement to connect to twitter
                return self::responseJson(['data' => []]);

            default:
        }
        return self::fail('place not found');
    }
}
