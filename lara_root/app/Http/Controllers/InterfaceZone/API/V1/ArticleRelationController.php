<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\ArticleRelation;
use App\Requests\InterfaceZone\API\V1\ArticleRelation\IndexRequest;
use App\Transformers\InterfaceZone\ArticleRelation\IndexTransformer;

class ArticleRelationController extends ApiController
{
    protected string $model = ArticleRelation::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $input = $request->validated();

        $query = new $this->model();
        $query = $query
            ->where([
                'relatable_type' => config2class($input['model_type']),
                'relatable_id' => $input['model_id'],
                'relation_type' => config_key('enums.article_relations.types', $input['relation_type']),
            ])->orderBy('order');

        return $this->responseByCursorFractal($query, new IndexTransformer());
    }
}
