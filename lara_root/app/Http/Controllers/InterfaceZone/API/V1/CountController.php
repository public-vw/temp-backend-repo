<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\Like;
use App\Requests\InterfaceZone\API\V1\Count\LikeRequest;

class CountController extends ApiController
{
    public function likes(LikeRequest $request): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $likes_count = Like::where([
            'likable_type' => model2class($request->input('model_type')),
            'likable_id' => $request->input('model_id'),
        ])->count();

        return self::responseJson(['count' => $likes_count]);
    }
}
