<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\Like;
use App\Requests\InterfaceZone\API\V1\Like\CheckRequest;
use App\Requests\InterfaceZone\API\V1\Like\ToggleRequest;
use App\Transformers\InterfaceZone\Like\IndexTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class LikeController extends ApiController
{
    protected string $model = Like::class;

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $userIp = $request->ip();
        $cacheKey = "likes:{$userIp}";

        $likeIds = Cache::get($cacheKey, []);
        $likes = Like::whereIn('id', $likeIds)->orderByDesc('updated_at')->newQuery();

        return $this->responseByCursorFractal($likes, new IndexTransformer());
    }

    public function toggle(ToggleRequest $request): \Illuminate\Http\JsonResponse
    {
        $userIp = $request->ip();
        $input = $request->validated();
        $query = [
            'likable_type' => config2class($input['model_type']),
            'likable_id' => $input['model_id'],
        ];

        $likeIds = Cache::get("likes:{$userIp}", []);

        $like = Like::where('liked', true)->firstOrCreate($query);

        if (in_array($like->id, $likeIds)) {
            $likeIds = array_diff($likeIds, [$like->id]);
            $finalStatus = false;
        } else {
            $likeIds[] = $like->id;
            $finalStatus = true;
        }
        $like->toggle();

        Cache::put("likes:{$userIp}", $likeIds, now()->addHours(4));

        $cacheKey = "likes:{$userIp}:{$like->id}";
        Cache::put($cacheKey, $finalStatus, now()->addHours(4));

        return self::responseJson(['id' => $like->id, 'final_status' => $finalStatus]);
    }

    public function check(CheckRequest $request): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $userIp = $request->ip();
        $input = $request->validated();
        $query = [
            'likable_type' => config2class($input['model_type']),
            'likable_id' => $input['model_id'],
        ];

        $like = Like::where($query)->first();
        $liked = Cache::get("likes:{$userIp}:{$like?->id}", false);

        return self::responseJson($liked ?? false);
    }
}
