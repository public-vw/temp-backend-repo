<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use Carbon\Carbon;

class TestController extends ApiController
{
    public function ping()
    {
        return response('Interface | PONG!');
    }
    public function time()
    {
        return Carbon::now();
    }
}
