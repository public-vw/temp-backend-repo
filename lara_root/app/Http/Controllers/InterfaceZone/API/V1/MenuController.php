<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\Menu;
use App\Models\Panel;
use App\Requests\InterfaceZone\API\V1\Menu\IndexRequest;
use App\Transformers\InterfaceZone\Menu\IndexTransformer;

class MenuController extends ApiController
{
    protected string $model = Menu::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $panel = Panel::where('title', 'interface')->first();
        $placeholder = $panel->menuPlaceholders()->where('slug', $request->place)->first();
        $query = $placeholder->menus()->where('active', true)->where('parent_id', null)->get();

        return $this->responseByAllFinalFractal($query, new IndexTransformer());
    }
}
