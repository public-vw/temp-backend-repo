<?php

namespace App\Http\Controllers\InterfaceZone\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\StaticPage;
use App\Models\Url;
use App\Requests\InterfaceZone\API\V1\StaticPage\ShowRequest;
use App\Transformers\InterfaceZone\StaticPage\ShowTransformer;

class StaticPageController extends ApiController
{
    public function show(StaticPage $staticPage): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        return $this->responseByOneFractal($staticPage, new ShowTransformer());
    }

    public function showByUrl(ShowRequest $request): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $urlable = Url::where('url', $request->url)->firstOrFail();
        $urlable = $urlable->urlable;
        abort_if(! $urlable->published, 404);

        return $this->responseByOneFractal($urlable, new ShowTransformer());
    }
}
