<?php

namespace App\Http\Controllers\InterfaceZone;

use App\Http\Controllers\ApiController;

abstract class _Controller extends ApiController
{
    protected $view;
    protected array $perPages = [5, 10, 25, 50];

    public function __construct()
    {
        $this->view = config('routes.interface.view', 'interface');
    }
}
