<?php

namespace App\Http\Controllers\InterfaceZone;

use Illuminate\Routing\Controller;

class WelcomeController extends Controller
{
    public function index()
    {
        return view('interface_zone.home');
    }
}
