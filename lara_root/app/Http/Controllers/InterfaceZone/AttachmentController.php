<?php

namespace App\Http\Controllers\InterfaceZone;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Url;
use Illuminate\Http\Request;

class AttachmentController extends Controller
{
    public function showByUrl(Request $request, string $url): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $request->validate([
            'box' => 'sometimes|in:' . implode(',', config_keys_all('enums.attachments.image_sizes.16_9')),
        ]);

        $urlable = Url::where('urlable_type', Attachment::class)->where('url', '/' . $url)->firstOrFail();

        $attachment_record = $urlable->urlable;

        $file_content = $attachment_record->getFromStorage($request->input('box', config('enums.attachments.image_sizes_default')));

        $headers = [
            'Content-Type' => $attachment_record->mimetype,
        ];

        (bool) $attachment_record->noindex !== true ?: $headers['X-Robots-Tag'] = 'noindex';

        return response($file_content, 200)->withHeaders($headers);
    }
}
