<?php

namespace App\Http\Controllers;

use App\Serializers\SimpleArraySerializer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Gate;
use League\Fractal\Manager;
use League\Fractal\Pagination\Cursor;
use League\Fractal\TransformerAbstract;
use Spatie\Fractalistic\Fractal;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected array $perPages = [5, 10, 25, 50];

    public function __construct()
    {
        if (method_exists(static::class, 'setRoleView')) {
            $this->setRoleView();
        }
    }

    protected static function success($message): array
    {
        return self::alert('success', $message);
    }

    protected static function fail($message): array
    {
        return self::alert('danger', $message);
    }

    protected static function alert($type, $message): array
    {
        return [
            'alert' => $type,
            'message' => $message,
        ];
    }

    protected static function smartRedirect($request, $route_header, $obj = null)
    {
        switch ($request->get('after_action')) {
            case 'stay':
                return redirect()->back();
                break;
            case 'back':
                return redirect()->route($route_header . '.index');
                break;
            case 'edit':
                return redirect()->route($route_header . '.edit', [$obj]);
                break;
        }
    }

    protected function checkPermission($spesific_action = null)
    {
        if (auth()->guest()) {
            return false;
        }

        $user = auth()->user();
        $dbt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        [$guard, $panel, $table, $action] = $this->parseTrace($dbt[1]);
        $term = sprintf('%s.%s', $table, $spesific_action ?? $action);
        if (config('app.permissions_debug')) {
            dump($guard, $panel, $table, $action, $term);
        }
        $territories = ['all', 'branch', 'own'];

        $all_perms = array_map(function ($item) use ($term) {
            return $term . '.' . $item;
        }, $territories);
        $all_perms = array_combine($territories, $all_perms);

        if (config('app.permissions_debug')) {
            foreach ($all_perms as $tperm) {
                dump("{$tperm} : " . ($user->hasPermissionTo($tperm, $guard) ? 'yes' : 'no'));
            }
            dump('any : ' . (Gate::forUser($user)->any($all_perms) ? 'yes' : 'no'));
            dd('none : ' . (Gate::forUser($user)->none($all_perms) ? 'yes' : 'no'));
        }
        foreach ($all_perms as $territory => $perm) {
            if ($user->hasPermissionTo($perm, $guard)) {
                return $territory;
            }
        }

        abort(403, 'Permission Unauthorized');
    }

    protected function responseByCursorFractal(Builder $builder, TransformerAbstract $fractalManager): JsonResponse
    {
        $request = Request::capture();
        $paginator = $builder->cursorPaginate($this->getPerPage($request));
        $cursor = new Cursor(
            null,
            $this->getCursorParameter($paginator->previousPageUrl()),
            $this->getCursorParameter($paginator->nextPageUrl()),
        );
        $fractal = Fractal::create()
            ->collection($paginator, $fractalManager)
            ->withCursor($cursor);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function responseByOneFractal(?Model $item, TransformerAbstract $fractalManager): \Illuminate\Foundation\Application|\Illuminate\Http\Response|JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        if (is_null($item)) {
            return response()->json([], 204);
        }

        $fractal = Fractal::create()->item($item, $fractalManager);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function responseByPaginatorFractal(Builder $builder, TransformerAbstract $fractalManager): JsonResponse
    {
        $request = Request::capture();
        $paginator = $builder->Paginate($this->getPerPage($request));
        $cursor = new Cursor(
            $paginator->currentPage(),
            $paginator->previousPageUrl(),
            $paginator->nextPageUrl(),
            $paginator->total(),
        );
        $fractal = Fractal::create()
            ->collection($paginator, $fractalManager)
            ->withCursor($cursor);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function responseByAllFractal(Builder $builder, TransformerAbstract $fractalManager): JsonResponse
    {
        $fractal = Fractal::create()->collection($builder->get(), $fractalManager);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function responseByAllFinalFractal(?Collection $item, TransformerAbstract $fractalManager): JsonResponse|Response
    {
        if (is_null($item)) {
            return response()->json([], 204);
        }

        $fractal = Fractal::create()->collection($item, $fractalManager);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function activateFractalIncludeExclude(Fractal $fractal): \League\Fractal\Scope
    {
        $request = Request::capture();
        $manager = new Manager();
        $manager->setSerializer(new SimpleArraySerializer());
        $manager->parseIncludes($request->get('include', ''));
        $manager->parseExcludes($request->get('exclude', ''));
        return $manager->createData($fractal->getResource());
    }

    protected function getPerPage(Request $request): mixed
    {
        return $this->perPages[$request->get('per_page_mode', $this->perPageDefault ?? 0)];
    }

    private function parseTrace($dbt)
    {
        $methodTranslations = [
            'index' => 'view',
            'show' => 'view',
            'list' => 'view',
            'create' => 'create',
            'store' => 'create',
            'edit' => 'edit',
            'update' => 'edit',
            'destroy' => 'delete',
        ];

        $is_api = false;
        preg_match('/API\\\\V\d+\\\\/', $dbt['class'], $is_api);
        $dbt['class'] = preg_replace('/API\\\\V\d+\\\\/', '', $dbt['class']);

        $class = explode('\\', $dbt['class']);
        $panel = strtolower($class[3]); //Admin, Client, ...
        $class = str_replace('Controller', '', $class[4]);
        $table = (new $dbt['class']())->table ?? model2table($class);
        $action = $methodTranslations[$dbt['function']] ?? $dbt['function'];

        return [$is_api ? 'api' : 'web', $panel, $table, $action];
    }

    private function getCursorParameter(?string $cursorUrl): ?string
    {
        return preg_match('/\?cursor=(.*)/', $cursorUrl, $out) ? $out[1] : null;
    }
}
