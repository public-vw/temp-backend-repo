<?php

namespace App\Http\Controllers\Sysadmin;

use App\DataTables\Sysadmin\RoleDataTable;
use App\Http\BackpackRequests\Sysadmin\RoleRequest;
use App\Models\Panel;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends _Controller
{
    protected $model = Role::class;

    public function index(RoleDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $panels = Panel::all();
        return view("{$this->view}.items.{$this->table}.create")->withPanels($panels);
    }

    public function store(RoleRequest $request)
    {
        $item = Role::create($request->validated());
        $item->update(['panel_id' => $request->validated()['panel_id']]);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->name])
            ));
    }

    public function edit(Request $request, Role $role)
    {
        $panels = Panel::all();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withPanels($panels)
            ->withItem($role);
    }

    public function update(RoleRequest $request, Role $role)
    {
        $role->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $role)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $role->id])
            ));
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $role->name])
            ));
    }

    public function permissions(Role $role)
    {
        $models = getModels();
        $tables = array_map('model2table', $models);

        return view("{$this->view}.items.{$this->table}.permissions")
            ->withTables($tables)
            ->withRole($role);
    }

    public function updatePermissions(Request $request, Role $role)
    {
        foreach ($request->permissions as $key => $value) {
            foreach (['all','branch','own'] as $territory) {
                $perm = "{$key}.{$territory}";
                $perm === $value ?
                    $role->givePermissionTo($perm)
                    : $role->revokePermissionTo($perm);
            }
        }

        return response()->json($request->all());
    }
}
