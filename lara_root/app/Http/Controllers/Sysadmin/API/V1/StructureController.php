<?php

namespace App\Http\Controllers\Sysadmin\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\Panel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Output\BufferedOutput;

class StructureController extends ApiController
{
    # Low-level entities
    public function createTransformer(Request $request): string
    {
        $panels = $this->getPanels();
        $models = getModels();

        $inputs = $request->validate([
            'panel' => 'required|string|in:' . implode(',', $panels),
            'model' => 'required|string',
        ]);
        return $this->runArtisan('transformer', $inputs);
    }

    public function createApiController(Request $request): string
    {
        $panels = $this->getPanels();
        $models = getModels();

        $inputs = $request->validate([
            'panel' => 'required|string|in:' . implode(',', $panels),
            'model' => 'required|string',
        ]);
        return $this->runArtisan('api:controller', $inputs);
    }

    ##   Generic entities

    public function createApiRoute(Request $request): string
    {
        $panels = $this->getPanels();

        $inputs = $request->validate([
            'panel' => 'required|string|in:' . implode(',', $panels),
            'model' => 'required|string',
        ]);
        return $this->runArtisan('api:route', $inputs);
    }

    public function createApiRequest(Request $request): string
    {
        $panels = $this->getPanels();
        $models = getModels();

        $inputs = $request->validate([
            'panel' => 'required|string|in:' . implode(',', $panels),
            'model' => 'required|string',
        ]);
        return $this->runArtisan('api:request', $inputs);
    }

    public function createPolicy(Request $request): string
    {
        $models = getModels();

        $inputs = $request->validate([
            'model' => 'required|string',
        ]);
        return $this->runArtisan('policy', $inputs);
    }

    public function createFactory(Request $request): string
    {
        $models = getModels();

        $inputs = $request->validate([
            'model' => 'required|string|in:' . implode(',', $models),
        ]);
        return $this->runArtisan('factory', $inputs);
    }

    public function createFakeSeeder(Request $request): string
    {
        $models = getModels();

        $inputs = $request->validate([
            'model' => 'required|string|in:' . implode(',', $models),
        ]);
        return $this->runArtisan('fake:seeder', $inputs);
    }

    public function createSeeder(Request $request): string
    {
        $inputs = $request->validate([
            'model' => 'required|string',
        ]);
        return $this->runArtisan('seeder', $inputs);
    }

    public function createMigration(Request $request): string
    {
        $models = getModels();

        $inputs = $request->validate([
            'model' => 'required|string|in:' . implode(',', $models),
        ]);
        return $this->runArtisan('migration', $inputs);
    }

    ## Web entities

    public function createModel(Request $request): string
    {
        $models = getModels();

        $inputs = $request->validate([
            'model' => 'required|string|notIn:' . implode(',', $models),
        ]);
        return $this->runArtisan('model', $inputs);
    }

    # High-level (package) entities

    public function createPermission(Request $request): string
    {
        return 'not working yet! needs refactor';

        $inputs = $request->validate([
            'model' => 'required|string',
        ]);
        return $this->runArtisan('permission', $inputs);
    }

    public function createRoute(Request $request): string
    {
        $panels = $this->getPanels();

        $inputs = $request->validate([
            'panel' => 'required|string|in:' . implode(',', $panels),
            'model' => 'required|string',
        ]);
        return $this->runArtisan('route', $inputs);
    }

    # Helpers

    public function createApi(Request $request): array
    {
        $inputs = $request->validate([
            'panel' => 'required|string|in:' . implode(',', ['sysadmin', 'staff', 'interface']),
            'model' => 'required|string',
        ]);

        $out = [];
        $out['api_controller'] = $this->runArtisan('api:controller', $inputs);
        $out['api_transformer'] = $this->runArtisan('transformer', $inputs);
        $out['api_route'] = $this->runArtisan('api:route', $inputs);
        $out['api_request'] = $this->runArtisan('api:request', $inputs);
        $out['policy'] = $this->runArtisan('policy', $request->only(['model']));

        return $out;
    }

    public function createEntity(Request $request): array
    {
        $inputs = $request->validate([
            'model' => 'required|string',
        ]);

        $out = [];
        $out['model'] = $this->runArtisan('model', $inputs);
        $out['migration'] = $this->runArtisan('migration', $inputs);
        $out['factory'] = $this->runArtisan('factory', $inputs);
        $out['seeder_fake'] = $this->runArtisan('fake:seeder', $inputs);
        $out['seeder'] = $this->runArtisan('seeder', $inputs);
        $out['policy'] = $this->runArtisan('policy', $inputs);

        return $out;
    }
    public function getPanels()
    {
        $panels = Panel::all(['title'])->pluck('title')->toArray();
        $panels[] = 'sysadmin';
        return $panels;
    }

    # Helpers
    protected function runArtisan(string $command, array $params): string
    {
        $output = new BufferedOutput();

        $params = implode(' ', $params);
        $command = "structure:{$command} {$params}";
        Artisan::call(command: $command, outputBuffer: $output);

        return $output->fetch();
    }
}
