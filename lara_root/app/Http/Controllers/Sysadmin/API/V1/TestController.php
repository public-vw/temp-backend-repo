<?php

namespace App\Http\Controllers\Sysadmin\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\User;
use App\Transformers\Sysadmin\User\IndexTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class TestController extends ApiController
{
    public function ping()
    {
        return response('Sysadmin | PONG!');
    }

    public function test(Request $request)
    {
//        $territory = $this->checkPermission();

        $query = User::all();

        $fractal = Fractal::create()->collection($query, new IndexTransformer());
        return response()->json($fractal);
    }
}
