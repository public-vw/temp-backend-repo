<?php

namespace App\Http\Controllers\Sysadmin;

use App\DataTables\Sysadmin\UserDataTable;
use App\Http\BackpackRequests\Sysadmin\UserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class UserController extends _Controller
{
    protected $model = User::class;
    protected $titleField = 'username';

    public function index(UserDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(UserRequest $request)
    {
        $values = $request->validated();

        $values['password'] = isset($values['password']) ? Hash::make($values['password']) : Hash::make(Str::random(10));
        $values['random_color'] = (string) rand(0, count(config_keys_all('enums.random_colors')) - 1);

        $item = User::create($values);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(User $user)
    {
        return view("{$this->view}.items.{$this->table}.show")
            ->withUser($user);
    }

    public function edit(Request $request, User $user)
    {
        return view("{$this->view}.items.{$this->table}.edit")
            ->withUser($user);
    }

    public function update(UserRequest $request, User $user)
    {
        $values = $request->validated();
        if (isset($values['password'])) {
            $values['password'] = Hash::make($values['password']);
        } else {
            unset($values['password']);
        }
        $user->update($values);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $user)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $user->id])
            ));
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $user->id])
            ));
    }

    public function roles(User $user)
    {
        $roles = Role::all();

        return view("{$this->view}.items.{$this->table}.roles")
            ->withRoles($roles)
            ->withUser($user);
    }

    public function updateRoles(Request $request, User $user)
    {
        $user->roles()->detach();
        foreach ($request->roles as $role_id) {
            $user->assignRole(Role::find($role_id));
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $user)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $user->id])
            ));
    }

    public function permissions(User $user)
    {
        $models = getModels();
        $tables = array_map('model2table', $models);

        return view("{$this->view}.items.{$this->table}.permissions")
            ->withTables($tables)
            ->withUser($user);
    }

    public function updatePermissions(Request $request, User $user)
    {
        foreach ($request->permissions as $key => $value) {
            foreach (['all','branch','own'] as $territory) {
                $perm = "{$key}.{$territory}";
                $perm === $value ?
                    $user->givePermissionTo($perm)
                    : $user->revokePermissionTo($perm);
            }
        }

        return response()->json($request->all());
    }

    public function setImpersonate(User $user)
    {
        Session::put('impersonate', auth()->user());
        auth()->login($user);
        return redirect()->route('auth.switch_to_panel');
    }
}
