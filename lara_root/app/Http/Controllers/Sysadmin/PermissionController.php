<?php

namespace App\Http\Controllers\Sysadmin;

use App\DataTables\Sysadmin\PermissionDataTable;
use App\Http\BackpackRequests\Sysadmin\PermissionRequest;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends _Controller
{
    protected $model = Permission::class;

    public function index(PermissionDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(PermissionRequest $request)
    {
        $item = Permission::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->name])
            ));
    }

    public function show($id)
    {
    }

    public function edit(Request $request, Permission $permission)
    {
        return view("{$this->view}.items.{$this->table}.edit")
            ->withItem($permission);
    }

    public function update(PermissionRequest $request, Permission $permission)
    {
        $permission->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $permission)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $permission->id])
            ));
    }

    public function destroy(Permission $permission)
    {
        $permission->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $permission->name])
            ));
    }
}
