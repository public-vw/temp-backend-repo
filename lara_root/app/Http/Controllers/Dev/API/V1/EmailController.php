<?php

namespace App\Http\Controllers\Dev\API\V1;

use App\Http\Controllers\ApiController;
use App\Models\User;
use Illuminate\Http\Request;

class EmailController extends ApiController
{
    public function unverifyEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'username' => 'required|string|exists:users,username',
        ]);

        $user = User::where('email', $request->email)
            ->where('username', $request->username)
            ->firstOrFail();
        $user->email_verified_at = null;
        $user->save();
        return self::responseJson(['message' => 'Email unverified successfully.']);
    }
}
