<?php

namespace App\Http\Controllers\Dev\API\V1;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class OtpController extends ApiController
{
    public function getOtp(Request $request)
    {
        $request->validate([
            'developer_code' => 'required',
        ]);

        $code = decrypt($request->developer_code);
        return self::responseJson(['otp' => $code]);
    }
}
