<?php

namespace App\Http\Controllers\Dev\API\V1;

use App\Http\Controllers\ApiController;

class TestController extends ApiController
{
    public function ping(): array|\Illuminate\Http\JsonResponse
    {
        return self::success('Dev | PONG!');
    }
}
