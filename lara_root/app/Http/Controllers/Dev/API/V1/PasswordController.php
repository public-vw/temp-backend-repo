<?php

namespace App\Http\Controllers\Dev\API\V1;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class PasswordController extends ApiController
{
    public function getResetPasswordToken(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
        ]);

        $url = cache($request->input('email'), 'url not found');

        return self::responseJson(['url' => $url]);
    }
}
