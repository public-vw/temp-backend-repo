<?php

namespace App\Http\Controllers\FrontendBuildTime;

use App\Http\Controllers\ApiController;
use App\Models\SeoDetail;
use App\Requests\FrontendBuildTime\SeoDetail\ShowRequest;

class SeoDetailController extends ApiController
{
    protected string $model = SeoDetail::class;

    public function show(ShowRequest $request): \Illuminate\Http\JsonResponse
    {
        $class = model2class($request->model_type);
        $model = new $class();
        $record = $model::find($request->model_id);

        //TODO: check for some entities that have no PUBLISH state (maybe static pages)
        abort_if(! $record->published, 404, 'Record not published!');

        $seoDetail = $record->final_content->seo;

        return response()->json($seoDetail);
    }
}
