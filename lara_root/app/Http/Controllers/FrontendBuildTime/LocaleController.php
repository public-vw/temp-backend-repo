<?php

namespace App\Http\Controllers\FrontendBuildTime;

use App\Http\Controllers\ApiController;
use App\Models\SettingGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class LocaleController extends ApiController
{
    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'place' => 'required|in:visitor,staff',
            'app' => 'sometimes|in:interface,panel',
        ]);

        $output['config'] = SettingGroup::get('languages.'.$request->input('app', 'interface').'.'.$request->place)->allSettings();

        Lang::setLocale($output['config']['lang']);
        $output['data'] = Lang::get('frontend/'.$request->input('app', 'interface').'/'.$request->place);

        return response()->json($output);
    }
}
