<?php

namespace App\Http\Controllers\FrontendBuildTime;

use App\Http\Controllers\ApiController;
use App\Models\Setting;
use App\Models\SettingGroup;
use Illuminate\Http\Request;

class SettingGroupController extends ApiController
{
    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $output = [];

        $groups = SettingGroup::with('settings')
            ->where('for_frontend', true)
            ->whereNull('parent_id')
            ->get();

        foreach ($groups as $group) {
            $output[$group->slug] = $this->getNestedSettings($group);
        }

        return response()->json($output);
    }

    protected function getNestedSettings(SettingGroup $group): array
    {
        $nested = [];

        $settings = $group->settings()->where('for_frontend', true)->get();

        foreach ($settings as $setting) {
            $nested[$setting->slug] = $this->getValues($setting);
        }

        $childGroups = $group->children()->where('for_frontend', true)->get();

        if ($childGroups->isNotEmpty()) {
            foreach ($childGroups as $childGroup) {
                $nested[$childGroup->slug] = $this->getNestedSettings($childGroup);
            }
        }

        return $nested;
    }

    protected function getValues(Setting $setting): array|string
    {
        $value = $setting->value;
        (strpos($setting->type, '_list') > 0) || settype($value, $setting->type);

        return strpos($setting->type, '_list') > 0 ? json_decode($setting->value, true) : $value;
    }
}
