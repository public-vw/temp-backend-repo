<?php

namespace App\Http\Controllers\FrontendBuildTime;

use App\Http\Controllers\ApiController;
use App\Models\Url;
use App\Requests\InterfaceZone\API\V1\Url\IndexRequest;
use App\Transformers\InterfaceZone\Url\IndexTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class UrlController extends ApiController
{
    public const PUBLISHABLE_MODELS = ['Article', 'ArticleCategory'];

    protected string $model = Url::class;
    protected int $perPageDefault = 3;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $config = Str::plural(Str::snake($request->get('model')));
        $modelName = 'App\\Models\\' . $request->get('model');

        $query = $this->getQuery($modelName);

        if ($this->hasStatus($request)) {
            $query = $this->filterActiveStatus($query, $modelName, $config);
        }

        return $this->responseByCursorFractal($query, new IndexTransformer());
    }

    protected function getQuery(string $modelName): mixed
    {
        $query = new $this->model();
        $query = $query->where('urlable_type', $modelName);
        return $query->orderBy('urlable_id');
    }

    protected function hasStatus(IndexRequest $request): bool
    {
        return in_array($request->get('model'), self::PUBLISHABLE_MODELS);
    }

    protected function filterActiveStatus(Builder $query, string $modelName, string $config): mixed
    {
        $query->whereHasMorph('urlable', [$modelName], function ($query) {
            return $query->published();
        });

        return $query;
    }
}
