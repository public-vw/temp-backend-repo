<?php

namespace App\Http\Controllers\Staff;

use App\Events\RecordUpdateOccurred;
use App\Http\Controllers\ApiController;
use App\Jobs\JCheckJobService;
use App\Models\Setting;
use App\Models\SettingGroup;
use App\Requests\Staff\Setting\CreateRequest;
use App\Requests\Staff\Setting\IndexRequest;
use App\Requests\Staff\Setting\UpdateRequest;
use App\Transformers\Staff\Setting\IndexTransformer;

class SettingController extends ApiController
{
    protected string $model = Setting::class;
    protected int $perPageDefault = 3;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $input = $request->validated();

        $settings = $this->model::query();

        if (isset($input['group_id'])) {
            $settings = $settings->where('group_id', $input['group_id']);
        }
        if (isset($input['slug'])) {
            $groupIds = SettingGroup::where('slug', 'LIKE', "%{$input['slug']}%")->pluck('id');

            $settings = Setting::where(function ($query) use ($input, $groupIds) {
                $query->whereIn('group_id', $groupIds)
                    ->orWhere('slug', 'LIKE', "%{$input['slug']}%");
            });
        }

        return $this->responseByPaginatorFractal($settings, new IndexTransformer());
    }

    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', $this->model);

        $input = $request->validated();

        $group = SettingGroup::findOrFail($input['group_id']);
        $query = $group->settings()->create($input);

        return self::responseJson(['id' => $query->id]);
    }

    public function update(UpdateRequest $request, Setting $setting): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $setting);

        $setting->update($request->validated());

        RecordUpdateOccurred::dispatch($setting);

        // check queue requested
        JCheckJobService::dispatchIf($setting->full_slug === 'server.queue' && $setting->value === '1');

        return self::success(__('admin.setting.update.success'));
    }

    public function destroy(Setting $setting): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $setting);

        $setting->delete();

        return self::success(__('admin.setting.delete.success'));
    }
}
