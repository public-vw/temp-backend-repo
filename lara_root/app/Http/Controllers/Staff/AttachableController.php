<?php

namespace App\Http\Controllers\Staff;

use App\Events\RecordUpdateOccurred;
use App\Http\Controllers\ApiController;
use App\Models\Attachable;
use App\Models\Attachment;
use App\Models\AttachmentType;
use App\Requests\Staff\Attachable\CreateRequest;
use App\Requests\Staff\Attachable\UpdateRequest;
use App\Services\ImageProcessor\ImageProcessor;
use App\Transformers\Staff\Attachable\ShowTransformer;

class AttachableController extends ApiController
{
    protected string $model = Attachable::class;

    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', $this->model);

        $input = $request->validated();

        $image = $request->file('file');
        $processor = new ImageProcessor($image);
        [$original, $sizes, $blur] = array_values($processor->process());

        $md5 = md5($original->get());

        $origin_attachment = Attachment::where([
            'md5' => $md5,
        ])->first();

        $attachment = $origin_attachment ? $origin_attachment :
            Attachment::create(
                [
                    'storage_disk' => 'temporary',
                    'file_ext' => config('enums.attachments.image_ext'),
                    'properties' => ['blur' => $blur],
                    'md5' => $md5,
                    'media' => config_key('enums.attachments.medias', 'image'),
                    'file_name' => null,
                    'user_id' => auth()->user()->id,
                ]
            );

        if (! $origin_attachment) {//has no original image, then should make hard images copy
            if (! $attachment->putToStorage($original->get(), extension: $this->extractFileExtension($original))) {
                $attachment->delete();
                return self::fail(__('upload.failed.original'));
            }

            foreach ($sizes as $label => $size) {
                if (! $attachment->putToStorage($size->encode(), $label, config('enums.attachments.image_ext'))) {
                    $attachment->delete();
                    return self::fail(__('upload.failed.' . $label));
                }
            }
            $attachment->fill([
                'uri' => $request->input('uri', null),
                'noindex' => (bool) $request->input('noindex', false),
                'original_name' => __('attachments.raw_data'),
                'original_ext' => $this->extractFileExtension($original),
                'file_size' => $original->getSize(),
                'mimetype' => $original->getMimeType(),
            ])->save();

            $attachment->addUrl();
        }

        $attachment_type = AttachmentType::where('slug', $input['attachment_type_slug'])->firstOrFail(['id']);

        $attachable = new Attachable();
        $attachable->fill([
            'attachable_type' => model2class($input['model_type']),
            'attachable_id' => $input['model_id'],
            'type_id' => $attachment_type->id,
        ]);
        $attachable->attachment()->associate($attachment->id);
        $attachable->save();

        return $this->responseByOneFractal($attachable, new ShowTransformer());
    }

    public function update(UpdateRequest $request, Attachable $attachable): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $attachable);

        $attachable->update($request->validated());

        ! $request->has(['noindex']) ?: $attachable->attachment()->update([
            'noindex' => (int) $request->input('noindex'),
        ]);
        ! $request->has(['uri']) ?: $attachable->attachment()->update([
            'uri' => (int) $request->input('uri'),
        ]);

        RecordUpdateOccurred::dispatch($attachable);

        return self::success(__('admin.attachable.update.success'));
    }

    public function destroy(Attachable $attachable): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $attachable);

        $attachable->delete();

        return self::success(__('admin.attachable.delete.success'));
    }

    protected function extractFileExtension(array|\Illuminate\Http\UploadedFile|null $image): mixed
    {
        $file_ext = $image->getMimeType();
        preg_match('/image\/(\w+)/', $file_ext, $file_ext);
        return $file_ext[1];
    }
}
