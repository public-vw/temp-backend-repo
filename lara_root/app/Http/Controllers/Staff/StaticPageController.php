<?php

namespace App\Http\Controllers\Staff;

use App\Events\RecordUpdateOccurred;
use App\Events\StaticPageCreated;
use App\Http\Controllers\ApiController;
use App\Models\StaticPage;
use App\Requests\Staff\StaticPage\CreateRequest;
use App\Requests\Staff\StaticPage\IndexRequest;
use App\Requests\Staff\StaticPage\UpdateRequest;
use App\Transformers\Staff\StaticPage\IndexTransformer;
use App\Transformers\Staff\StaticPage\ShowTransformer;
use Illuminate\Support\Facades\DB;

class StaticPageController extends ApiController
{
    protected string $model = StaticPage::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $query = new $this->model();
        $query = $query->newQuery();

        if ($request->has('static_page_id')) {
            $query = $query->where(['static_page_id' => $request->get('static_page_id')]);
        }

        if ($request->has('text')) {
            $query = $query->where(function ($query) use ($request) {
                return $query->where('heading', 'LIKE', '%' . $request->get('text') . '%')
                    ->orWhere('raw_content', 'LIKE', '%' . $request->get('text') . '%');
            });
        }

        $query = $query->orderByDesc('updated_at');

        return $this->responseByPaginatorFractal($query, new IndexTransformer());
    }

    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', $this->model);

        $static_page = null;
        $new_content = null;

        DB::transaction(function () use ($request, &$static_page, &$new_content) {
            $static_page = StaticPage::create($request->validated());

            $new_content = $static_page->contents()->create([
                'version' => 1,
            ]);

            StaticPageCreated::dispatch($static_page);
        });

        return self::responseJson([
            'static_page_id' => $static_page->id,
            'content_id' => $new_content->id,
        ]);
    }

    public function show(StaticPage $staticPage): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('view', $staticPage);

        return $this->responseByOneFractal($staticPage, new ShowTransformer());
    }

    public function update(UpdateRequest $request, StaticPage $staticPage): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $staticPage);

        $staticPage->update($request->validated());

        RecordUpdateOccurred::dispatch($staticPage);

        return self::success(__('admin.staticPage.update.success'));
    }

    public function destroy(StaticPage $staticPage): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $staticPage);

        $staticPage->delete();

        return self::success(__('admin.staticPage.delete.success'));
    }
}
