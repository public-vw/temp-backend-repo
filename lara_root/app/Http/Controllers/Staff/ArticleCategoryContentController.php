<?php

namespace App\Http\Controllers\Staff;

use App\Events\ContentStatusChanged;
use App\Events\RecordUpdateOccurred;
use App\Http\Controllers\ApiController;
use App\Models\ArticleCategory;
use App\Models\ArticleCategoryContent;
use App\Requests\Staff\ArticleCategoryContent\CreateRequest;
use App\Requests\Staff\ArticleCategoryContent\IndexRequest;
use App\Requests\Staff\ArticleCategoryContent\UpdateRequest;
use App\Requests\Staff\ArticleCategoryContent\UpdateStatusRequest;
use App\Transformers\Staff\ArticleCategoryContent\IndexTransformer;
use App\Transformers\Staff\ArticleCategoryContent\ShowTransformer;
use Illuminate\Support\Facades\DB;

class ArticleCategoryContentController extends ApiController
{
    protected string $model = ArticleCategoryContent::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $query = new $this->model();
        $query = $query->newQuery();

        if ($request->has('category_id')) {
            $query = $query->where(['article_category_id' => $request->get('category_id')]);
        }

        if ($request->has('text')) {
            $query = $query->where(function ($query) use ($request) {
                return $query->where('heading', 'LIKE', '%' . $request->get('text') . '%')
                    ->orWhere('description', 'LIKE', '%' . $request->get('text') . '%')
                    ->orWhere('summary', 'LIKE', '%' . $request->get('text') . '%');
            });
        }

        return $this->responseByPaginatorFractal($query, new IndexTransformer());
    }

    # to be called when the frontend requests for make new article content with next version number
    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', $this->model);

        $input = $request->validated();

        $article_category = ArticleCategory::findOrFail($input['article_category_id']);

        $content = null;

        DB::transaction(function () use ($article_category, &$content) {
            $last_content = $article_category->getLastVersion();

            $content = $article_category->contents()->create([
                'version' => $article_category->getNextNewVersionNumber(),
                'title' => $last_content->title,
                'heading' => $last_content->heading,
                'content' => $last_content->content,
                'raw_content' => $last_content->raw_content,
                'summary' => $last_content->summary,
                'description' => $last_content->description,
            ]);

            if ($last_content->seoDetails()->count() > 0) {
                foreach ($last_content->seoDetails as $seo_detail) {
                    $content->seoDetails()->create([
                        'type' => $seo_detail->type,
                        'data' => $seo_detail->data,
                    ]);
                }
            }

            if ($last_content->attachments()->count() > 0) {
                foreach ($last_content->attachments as $attachment) {
                    $content->attachments()->attach($attachment->id);
                }
            }
        });

        return self::responseJson(['content_id' => $content->id]);
    }

    public function show(ArticleCategoryContent $articleCategoryContent): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('view', $articleCategoryContent);

        return $this->responseByOneFractal($articleCategoryContent, new ShowTransformer());
    }

    public function update(UpdateRequest $request, ArticleCategoryContent $articleCategoryContent): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $articleCategoryContent);

        $articleCategoryContent->update($request->validated());

        RecordUpdateOccurred::dispatch($articleCategoryContent);

        return self::success(__('admin.article_category_content.update.success'));
    }

    public function destroy(ArticleCategoryContent $articleCategoryContent): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $articleCategoryContent);

        $articleCategoryContent->delete();

        return self::success(__('admin.article_category_content.delete.success'));
    }

    public function changeStatus(UpdateStatusRequest $request, ArticleCategoryContent $articleCategoryContent): \Illuminate\Http\JsonResponse
    {
        $this->authorize('updateStatus', $articleCategoryContent);

        ContentStatusChanged::dispatch(
            ...$articleCategoryContent->updateStatus($request->status),
        );

        return self::success(__('admin.article_category_content.update_status.success'));
    }
}
