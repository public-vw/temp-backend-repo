<?php

namespace App\Http\Controllers\Staff;

use App\Events\RecordUpdateOccurred;
use App\Http\Controllers\ApiController;
use App\Requests\Staff\ProfileUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends ApiController
{
    public function edit(Request $request): \Illuminate\Http\JsonResponse
    {
        return self::responseJson([
            'user' => $request->user(),
        ]);
    }

    public function update(ProfileUpdateRequest $request): \Illuminate\Http\JsonResponse
    {
        $request->user()->fill($request->validated());

        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        $request->user()->save();

        RecordUpdateOccurred::dispatch($request->user());

        return self::success('profile-updated');
    }

    public function destroy(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current-password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return self::success('Account Deleted');
    }
}
