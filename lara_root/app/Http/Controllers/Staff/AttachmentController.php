<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\ApiController;
use App\Models\Attachment;

class AttachmentController extends ApiController
{
    protected string $model = Attachment::class;

    public function destroy(Attachment $attachment): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $attachment);

//        TODO:check if attachment is not used in any other attachables
        //$attachable->delete();

        return self::fail(__('admin.not_implemented_yet'));
//        return self::success(__('admin.attachment.delete.success'));
    }
}
