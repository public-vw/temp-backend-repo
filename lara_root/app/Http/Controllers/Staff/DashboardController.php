<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\ApiController;

class DashboardController extends ApiController
{
    public function index(): \Illuminate\Http\JsonResponse
    {
        $branch = trim(runShell('git branch --show-current'));
        $version = runShell('git rev-list --max-count=1 '.$branch);
        $details = runShell('git rev-list --format="%s | %ar" --max-count=4 '.$branch);

        return self::responseJson([
            'branch' => $branch,
            'details' => $details,
            'version' => $version,
        ]);
    }
}
