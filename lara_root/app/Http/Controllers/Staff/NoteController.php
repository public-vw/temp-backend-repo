<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\ApiController;
use App\Models\Note;
use App\Requests\_CustomApiRequest;
use App\Requests\Staff\Note\CreateRequest;
use App\Requests\Staff\Note\IndexRequest;
use App\Requests\Staff\Note\UpdateRequest;
use App\Transformers\Staff\Note\IndexTransformer;

class NoteController extends ApiController
{
    protected string $model = Note::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $model = $this->getNotable($request);

        $query = $model->notes;

        return $this->responseByAllFinalFractal($query, new IndexTransformer());
    }

    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $notable = $this->getNotable($request);

        $this->authorize('createNote', $notable);

        $query = $notable->notes()->create($request->validated());
        $query->user()->associate(auth()->user())->save();

        return self::responseJson(['id' => $query->id]);
    }

    public function update(UpdateRequest $request, Note $note): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $note);

        $note->update($request->validated());

        return self::success(__('staff.note.update.success'));
    }

    public function destroy(Note $note): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $note);

        $note->delete();

        return self::success(__('staff.note.delete.success'));
    }

    protected function getNotable(_CustomApiRequest $request)
    {
        $class = model2class($request->model_type);
        $model = new $class();
        return $model::find($request->model_id);
    }
}
