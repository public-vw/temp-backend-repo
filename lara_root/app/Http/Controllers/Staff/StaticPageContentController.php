<?php

namespace App\Http\Controllers\Staff;

use App\Events\ContentStatusChanged;
use App\Events\RecordUpdateOccurred;
use App\Http\Controllers\ApiController;
use App\Models\StaticPage;
use App\Models\StaticPageContent;
use App\Requests\Staff\StaticPageContent\CreateRequest;
use App\Requests\Staff\StaticPageContent\IndexRequest;
use App\Requests\Staff\StaticPageContent\UpdateRequest;
use App\Requests\Staff\StaticPageContent\UpdateStatusRequest;
use App\Transformers\Staff\StaticPageContent\IndexTransformer;
use App\Transformers\Staff\StaticPageContent\ShowTransformer;
use Illuminate\Support\Facades\DB;

class StaticPageContentController extends ApiController
{
    protected string $model = StaticPageContent::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $query = new $this->model();
        $query = $query->newQuery();

        if ($request->has('static_page_id')) {
            $query = $query->where(['static_page_id' => $request->get('static_page_id')]);
        }

        if ($request->has('text')) {
            $query = $query->where(function ($query) use ($request) {
                return $query->where('heading', 'LIKE', '%' . $request->get('text') . '%')
                    ->orWhere('raw_content', 'LIKE', '%' . $request->get('text') . '%');
            });
        }

        return $this->responseByPaginatorFractal($query, new IndexTransformer());
    }

    # to be called when the frontend requests for make new article content with next version number
    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', $this->model);

        $input = $request->validated();

        $static_page = StaticPage::findOrFail($input['static_page_id']);

        $content = null;

        DB::transaction(function () use ($static_page, &$content) {
            $last_content = $static_page->getLastVersion();

            $content = $static_page->contents()->create([
                'version' => $static_page->getNextNewVersionNumber(),
                'title' => $last_content->title,
                'heading' => $last_content->heading,
                'reading_time' => $last_content->reading_time,
                'content' => $last_content->content,
                'raw_content' => $last_content->raw_content,
            ]);

            foreach ($last_content->seoDetails as $seo_detail) {
                $content->seoDetails()->create([
                    'type' => $seo_detail->type,
                    'data' => $seo_detail->data,
                ]);
            }

            foreach ($last_content->attachments as $attachment) {
                $content->attachments()->attach($attachment->id);
            }
        });

        return self::responseJson(['content_id' => $content->id]);
    }

    public function show(StaticPageContent $staticPageContent): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('view', $staticPageContent);

        return $this->responseByOneFractal($staticPageContent, new ShowTransformer());
    }

    public function update(UpdateRequest $request, StaticPageContent $staticPageContent): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $staticPageContent);

        $staticPageContent->update($request->validated());

        RecordUpdateOccurred::dispatch($staticPageContent);

        return self::success(__('admin.static_page_content.update.success'));
    }

    public function destroy(StaticPageContent $staticPageContent): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $staticPageContent);

        $staticPageContent->delete();

        return self::success(__('admin.static_page_content.delete.success'));
    }

    public function changeStatus(UpdateStatusRequest $request, StaticPageContent $staticPageContent): \Illuminate\Http\JsonResponse
    {
        $this->authorize('updateStatus', $staticPageContent);

        ContentStatusChanged::dispatch(
            ...$staticPageContent->updateStatus($request->status),
        );

        return self::success(__('admin.static_page_content.update_status.success'));
    }
}
