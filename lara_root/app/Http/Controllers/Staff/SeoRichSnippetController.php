<?php

namespace App\Http\Controllers\Staff;

use App\Events\RecordUpdateOccurred;
use App\Http\Controllers\ApiController;
use App\Models\SeoRichSnippet;
use App\Requests\_CustomApiRequest;
use App\Requests\Staff\SeoRichSnippet\ShowRequest;
use App\Requests\Staff\SeoRichSnippet\UpdateByContentRequest;
use App\Requests\Staff\SeoRichSnippet\UpdateByParentRequest;

class SeoRichSnippetController extends ApiController
{
    protected string $model = SeoRichSnippet::class;

    public function show(ShowRequest $request): \Illuminate\Http\JsonResponse
    {
        $model = $this->getRichSnippetable($request);

        $seoRichSnippet = $model?->rich_snippets ?? generalRichSnippets();

        return self::responseJson($seoRichSnippet);
    }

    public function updateByContent(UpdateByContentRequest $request): \Illuminate\Http\JsonResponse
    {
        $richsnippetable = $this->getRichSnippetable($request);

        if (is_null($richsnippetable)) {
            $richSnippets = SeoRichSnippet::whereNull('richsnippetable_type');

            $this->authorize('updateAny', $this->model);
        } else {
            $this->authorize('updateSeo', $richsnippetable->final_content);

            $richSnippets = $richsnippetable->final_content->richSnippets();
        }

        foreach ($request->data as $type => $data) {
            $type_key = config_key('enums.rich_snippets.types', $type);
            abort_if($type_key === '', '406', 'Wrong rich snippet type :' . $type);
            $richSnippets->updateOrCreate(['type' => $type_key], ['data' => $data]);
        }

//        JRevalidateRequest::dispatch($richsnippetable->container, $richsnippetable->url);

        RecordUpdateOccurred::dispatch($richsnippetable, 'Rich Snippet Updated By Content');

        return self::success(__('admin.rich_snippet.update.success'));
    }

    public function updateByParent(UpdateByParentRequest $request): \Illuminate\Http\JsonResponse
    {
        $richsnippetable = $this->getRichSnippetable($request);

        if (is_null($richsnippetable)) {
            $richSnippets = SeoRichSnippet::whereNull('richsnippetable_type');

            $this->authorize('updateAny', $this->model);
        } else {
            $this->authorize('updateSeo', $richsnippetable->final_content);

            $richSnippets = $richsnippetable->final_content->richSnippets();
        }

        foreach ($request->data as $type => $data) {
            $type_key = config_key('enums.rich_snippets.types', $type);
            abort_if($type_key === '', '406', 'Wrong rich snippet type :' . $type);
            $richSnippets->updateOrCreate(['type' => $type_key], ['data' => $data]);
        }

//        JRevalidateRequest::dispatch($richsnippetable, $richsnippetable->url);

        RecordUpdateOccurred::dispatch($richsnippetable, 'Rich Snippet Updated By Parent');

        return self::success(__('admin.rich_snippet.update.success'));
    }

    protected function getRichSnippetable(_CustomApiRequest $request)
    {
        if ($request->model_type === 'Website') {
            return null;
        }

        $class = model2class($request->model_type);
        $model = new $class();
        return $model::find($request->model_id);
    }
}
