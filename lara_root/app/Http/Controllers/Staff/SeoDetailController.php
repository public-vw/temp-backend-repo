<?php

namespace App\Http\Controllers\Staff;

use App\Events\RecordUpdateOccurred;
use App\Http\Controllers\ApiController;
use App\Models\SeoDetail;
use App\Requests\_CustomApiRequest;
use App\Requests\Staff\SeoDetail\ShowRequest;
use App\Requests\Staff\SeoDetail\UpdateByContentRequest;
use App\Requests\Staff\SeoDetail\UpdateByParentRequest;

class SeoDetailController extends ApiController
{
    protected string $model = SeoDetail::class;

    public function show(ShowRequest $request): \Illuminate\Http\JsonResponse
    {
        $model = $this->getSeoable($request);

        $seoDetail = $model->seo;

        return self::responseJson($seoDetail);
    }

    public function updateByContent(UpdateByContentRequest $request): \Illuminate\Http\JsonResponse
    {
        $seoable = $this->getSeoable($request);

        $this->authorize('updateSeo', $seoable);

        foreach ($request->data as $type => $data) {
            $type_key = config_key('enums.seo_details.types', $type);
            abort_if($type_key === '', '406', 'Wrong seo details type :' . $type);
            $seoable->seoDetails()->updateOrCreate(['type' => $type_key], ['data' => $data]);
        }

//        JRevalidateRequest::dispatch($seoable->container, $seoable->url);

        RecordUpdateOccurred::dispatch($seoable, 'Seo Details Updated By Content');

        return self::success(__('admin.seoDetail.update.success'));
    }

    public function updateByParent(UpdateByParentRequest $request): \Illuminate\Http\JsonResponse
    {
        $seoable = $this->getSeoable($request);

        $this->authorize('updateSeo', $seoable->final_content);

        foreach ($request->data as $type => $data) {
            $type_key = config_key('enums.seo_details.types', $type);
            abort_if($type_key === '', '406', 'Wrong seo details type :' . $type);
            $seoable->final_content->seoDetails()->updateOrCreate(['type' => $type_key], ['data' => $data]);
        }

//        JRevalidateRequest::dispatch($seoable, $seoable->url);

        RecordUpdateOccurred::dispatch($seoable, 'Seo Details Updated By Parent');

        return self::success(__('admin.seoDetail.update.success'));
    }

    protected function getSeoable(_CustomApiRequest $request)
    {
        $class = model2class($request->model_type);
        $model = new $class();
        return $model::find($request->model_id);
    }
}
