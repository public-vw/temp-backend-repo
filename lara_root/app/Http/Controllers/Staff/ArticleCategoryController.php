<?php

namespace App\Http\Controllers\Staff;

use App\Events\ArticleCategoryCreated;
use App\Events\RecordUpdateOccurred;
use App\Http\Controllers\ApiController;
use App\Models\ArticleCategory;
use App\Requests\Staff\ArticleCategory\CreateRequest;
use App\Requests\Staff\ArticleCategory\IndexRequest;
use App\Requests\Staff\ArticleCategory\UpdateRequest;
use App\Transformers\Staff\ArticleCategory\IndexTransformer;
use App\Transformers\Staff\ArticleCategory\ShowTransformer;
use Illuminate\Support\Facades\DB;

class ArticleCategoryController extends ApiController
{
    protected string $model = ArticleCategory::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', ArticleCategory::class);

        $query = new $this->model();
        $query = $query->newQuery();

        if ($request->has('parent_id')) {
            $query = $query->where(['parent_id' => $request->get('parent_id')]);
        }

        $query = $query->orderByDesc('updated_at');

        return $this->responseByPaginatorFractal($query, new IndexTransformer());
    }

    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', ArticleCategory::class);

        $category = null;
        $new_content = null;

        DB::transaction(function () use ($request, &$category, &$new_content) {
            $category = ArticleCategory::create($request->validated());

            $new_content = $category->contents()->create([
                'version' => 1,
            ]);

            ArticleCategoryCreated::dispatch($category);
        });

        return self::responseJson([
            'category_id' => $category->id,
            'content_id' => $new_content->id,
        ]);
    }

    public function show(ArticleCategory $articleCategory): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('view', $articleCategory);

        return $this->responseByOneFractal($articleCategory, new ShowTransformer());
    }

    public function update(UpdateRequest $request, ArticleCategory $articleCategory): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $articleCategory);

        $articleCategory->update($request->validated());

        RecordUpdateOccurred::dispatch($articleCategory);

        return self::success(__('admin.articleCategory.update.success'));
    }

    public function destroy(ArticleCategory $articleCategory): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $articleCategory);

        $articleCategory->delete();

        return self::success('admin.articleCategory.delete.success');
    }
}
