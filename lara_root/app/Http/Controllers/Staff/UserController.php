<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\ApiController;
use App\Models\User;
use App\Requests\Staff\User\IndexRequest;
use App\Transformers\Staff\User\IndexTransformer;

class UserController extends ApiController
{
    protected string $model = User::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $input = $request->validated();

        $query = User::query();

        if (isset($input['status'])) {
            $query = $query->where([
                'status' => config_key('enums.users.status', $input['status']),
            ]);
        }

        if (isset($input['role'])) {
            $query = $query->role($input['role']);
        }

        return $this->responseByCursorFractal($query, new IndexTransformer());
    }

//    TODO: update below functions to make them work. routing is only for the `index` now
    /*
        public
        function store(CreateRequest $request): \Illuminate\Http\JsonResponse
        {
            $this->authorize('create', $this->model);

            $query = User::create($request->validated());

            return self::responseJson(['id' => $query->id]);
        }

        public
        function show(User $user): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
        {
            $this->authorize('view', $user);

            return $this->responseByOneFractal($user, new ShowTransformer());
        }

        public
        function update(UpdateRequest $request, User $user): \Illuminate\Http\JsonResponse
        {
            $this->authorize('update', $user);

            $user->update($request->validated());

            RecordUpdateOccurred::dispatch($user);

            return self::success(__('admin.user.update.success'));
        }

        public
        function destroy(User $user): \Illuminate\Http\JsonResponse
        {
            $this->authorize('delete', $user);

            $user->delete();

            return self::success(__('admin.user.delete.success'));
        }*/
}
