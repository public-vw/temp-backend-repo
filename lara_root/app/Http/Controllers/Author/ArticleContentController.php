<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\ApiController;
use App\Models\Article;
use App\Models\ArticleContent;
use App\Requests\Author\ArticleContent\CreateRequest;
use App\Requests\Author\ArticleContent\IndexRequest;
use App\Requests\Author\ArticleContent\UpdateRequest;
use App\Requests\Author\ArticleContent\UpdateStatusRequest;
use App\Transformers\Author\ArticleContent\IndexTransformer;
use App\Transformers\Author\ArticleContent\ShowTransformer;
use Illuminate\Support\Facades\DB;

class ArticleContentController extends ApiController
{
    protected string $model = ArticleContent::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $query = new $this->model();
        $query = $query->newQuery();

        $query = $query->whereHas('container', function ($query) {
            $query->where('author_id', auth()->user()->id);
        });

        if ($request->has('article_id')) {
            $query = $query->where(['article_id' => $request->get('article_id')]);
        }

        if ($request->has('text')) {
            $query = $query->where(function ($query) use ($request) {
                return $query->where('heading', 'LIKE', '%' . $request->get('text') . '%')
                    ->orWhere('raw_content', 'LIKE', '%' . $request->get('text') . '%')
                    ->orWhere('summary', 'LIKE', '%' . $request->get('text') . '%')
                    ->orWhere('inspiration', 'LIKE', '%' . $request->get('text') . '%');
            });
        }

        return $this->responseByPaginatorFractal($query, new IndexTransformer());
    }

    # to be called when the frontend requests for make new article content with next version number
    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', $this->model);

        $input = $request->validated();

        $article = Article::findOrFail($input['article_id']);

        $content = null;

        DB::transaction(function () use ($article, &$content) {
            $last_content = $article->getLastVersion();

            $content = $article->contents()->create([
                'version' => $article->getNextNewVersionNumber(),
                'heading' => $last_content->heading,
                'reading_time' => $last_content->reading_time,
                'content' => $last_content->content,
                'raw_content' => $last_content->raw_content,
                'summary' => $last_content->summary,
            ]);

            foreach ($last_content->seoDetails as $seo_detail) {
                $content->seoDetails()->create([
                    'type' => $seo_detail->type,
                    'data' => $seo_detail->data,
                ]);
            }

            foreach ($last_content->attachments as $attachment) {
                $content->attachments()->attach($attachment->id);
            }
        });

        return self::responseJson(['content_id' => $content->id]);
    }

    public function show(ArticleContent $articleContent): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('view', $articleContent);

        return $this->responseByOneFractal($articleContent, new ShowTransformer());
    }

    public function update(UpdateRequest $request, ArticleContent $articleContent): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $articleContent);

        $articleContent->update($request->validated());

        return self::success(__('author.article_content.update.success'));
    }

    public function destroy(ArticleContent $articleContent): \Illuminate\Http\JsonResponse
    {
        $this->authorize('delete', $articleContent);

        $articleContent->delete();

        if ($articleContent->article->contents()->count() === 0) {
            $articleContent->article->delete();
        }

        return self::success(__('author.articleContent.delete.success'));
    }

    public function changeStatus(UpdateStatusRequest $request, ArticleContent $articleContent): \Illuminate\Http\JsonResponse
    {
        $this->authorize('updateStatus', $articleContent);

        $articleContent->setStatus($request->status);

        return self::success(__('author.article_content.update_status.success'));
    }
}
