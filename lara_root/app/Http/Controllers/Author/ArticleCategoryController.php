<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\ApiController;
use App\Models\ArticleCategory;
use App\Requests\Author\ArticleCategory\IndexRequest;
use App\Transformers\Author\ArticleCategory\IndexTransformer;
use App\Transformers\Author\ArticleCategory\ShowTransformer;

class ArticleCategoryController extends ApiController
{
    protected string $model = ArticleCategory::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', ArticleCategory::class);

        $query = new $this->model();
        $query = $query->newQuery();
        $query = $query->orderByDesc('updated_at');

        if ($request->has('parent_id')) {
            $query = $query->where(['parent_id' => $request->get('parent_id')]);
        }

        return $this->responseByPaginatorFractal($query, new IndexTransformer());
    }

    public function show(ArticleCategory $articleCategory): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('view', $articleCategory);

        return $this->responseByOneFractal($articleCategory, new ShowTransformer());
    }
}
