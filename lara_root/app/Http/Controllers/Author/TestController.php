<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\ApiController;

class TestController extends ApiController
{
    public function ping(): \Illuminate\Http\JsonResponse
    {
        return self::success('Author | PONG!');
    }
}
