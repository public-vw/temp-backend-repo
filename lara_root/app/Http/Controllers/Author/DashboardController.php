<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\ApiController;

class DashboardController extends ApiController
{
    public function index(): \Illuminate\Http\JsonResponse
    {
        return self::success('author.dashboard.welcome');
    }
}
