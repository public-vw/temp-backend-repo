<?php

namespace App\Http\Controllers\Author;

use App\Events\ArticleCreated;
use App\Http\Controllers\ApiController;
use App\Models\Article;
use App\Requests\Author\Article\CreateRequest;
use App\Requests\Author\Article\IndexRequest;
use App\Requests\Author\Article\UpdateRequest;
use App\Transformers\Author\Article\IndexTransformer;
use App\Transformers\Author\Article\ShowTransformer;
use Illuminate\Support\Facades\DB;

class ArticleController extends ApiController
{
    protected string $model = Article::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', Article::class);

        $query = new $this->model();
        $query = $query->newQuery();

        if ($request->has('category_id')) {
            $query = $query->where(['category_id' => $request->get('category_id')]);
        }

        if ($request->has('author_id')) {
            $query = $query->where(['author_id' => $request->get('author_id')]);
        }

        return $this->responseByPaginatorFractal($query, new IndexTransformer());
    }

    public function store(CreateRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('create', Article::class);

        $article = null;
        $new_content = null;

        DB::transaction(function () use ($request, &$article, &$new_content) {
            $article = Article::create($request->validated());

            $new_content = $article->contents()->create([
                'version' => 1,
            ]);

            ArticleCreated::dispatch($article);
        });

        return self::responseJson([
            'article_id' => $article->id,
            'content_id' => $new_content->id,
        ]);
    }

    public function show(Article $article): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('view', $article);

        return $this->responseByOneFractal($article, new ShowTransformer());
    }

    public function update(UpdateRequest $request, Article $article): \Illuminate\Http\JsonResponse
    {
        $this->authorize('update', $article);

        $article->update($request->validated());

        return self::success(__('author.article.update.success'));
    }
}
