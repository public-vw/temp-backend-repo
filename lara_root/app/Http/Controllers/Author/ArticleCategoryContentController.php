<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\ApiController;
use App\Models\ArticleCategoryContent;
use App\Requests\Author\ArticleCategoryContent\IndexRequest;
use App\Transformers\Staff\ArticleCategoryContent\IndexTransformer;
use App\Transformers\Staff\ArticleCategoryContent\ShowTransformer;

class ArticleCategoryContentController extends ApiController
{
    protected string $model = ArticleCategoryContent::class;

    public function index(IndexRequest $request): \Illuminate\Http\JsonResponse
    {
        $this->authorize('viewAny', $this->model);

        $query = new $this->model();
        $query = $query->newQuery();

        if ($request->has('category_id')) {
            $query = $query->where(['article_category_id' => $request->get('category_id')]);
        }

        if ($request->has('text')) {
            $query = $query->where(function ($query) use ($request) {
                return $query->where('heading', 'LIKE', '%' . $request->get('text') . '%')
                    ->orWhere('description', 'LIKE', '%' . $request->get('text') . '%')
                    ->orWhere('summary', 'LIKE', '%' . $request->get('text') . '%');
            });
        }

        return $this->responseByPaginatorFractal($query, new IndexTransformer());
    }

    public function show(ArticleCategoryContent $articleCategoryContent): \Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $this->authorize('view', $articleCategoryContent);

        return $this->responseByOneFractal($articleCategoryContent, new ShowTransformer());
    }
}
