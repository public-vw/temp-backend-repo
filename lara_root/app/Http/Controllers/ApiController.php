<?php

namespace App\Http\Controllers;

use App\Serializers\SimpleArraySerializer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use League\Fractal\Manager;
use League\Fractal\Pagination\Cursor;
use League\Fractal\TransformerAbstract;
use Spatie\Fractalistic\Fractal;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected array $perPages = [8, 16, 32, 64];

    protected static function responseJson($data, $status = 200): JsonResponse
    {
        //TODO: check if this is a better approach
//        $fractal = \Spatie\Fractal\Fractal::create()->collection($query, new IndexTransformer());
        return response()->json($data)->setStatusCode($status);
    }

    protected static function emptyResponse($status = 204): Response
    {
        return response()->noContent()->setStatusCode($status);
    }

    protected static function success($message, $status = 200): JsonResponse
    {
        return self::alert('success', $message, $status);
    }

    protected static function fail($message, $status = 400): JsonResponse
    {
        return self::alert('danger', $message, $status);
    }

    protected static function alert($type, $message, $status = 200): JsonResponse
    {
        return response()->json([
            'alert' => $type,
            'message' => $message,
        ])->setStatusCode($status);
    }

    protected function responseByCursorFractal(Builder $builder, TransformerAbstract $fractalManager): JsonResponse
    {
        $paginator = $builder->cursorPaginate($this->getPerPage(Request::capture()));
        $cursor = new Cursor(
            null,
            $this->getCursorParameter($paginator->previousPageUrl()),
            $this->getCursorParameter($paginator->nextPageUrl()),
        );
        $fractal = Fractal::create()
            ->collection($paginator, $fractalManager)
            ->withCursor($cursor);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function responseByOneFractal(?Model $item, TransformerAbstract $fractalManager): \Illuminate\Foundation\Application|\Illuminate\Http\Response|JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        if (is_null($item)) {
            return response()->json([], 204);
        }

        $fractal = Fractal::create()->item($item, $fractalManager);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function responseByPaginatorFractal(Builder $builder, TransformerAbstract $fractalManager): JsonResponse
    {
        $request = Request::capture();
        $per_page = $this->getPerPage($request);
        $paginator = $builder->Paginate($per_page);
        $cursor = new Cursor(
            $paginator->currentPage(),
            $paginator->previousPageUrl(),
            $paginator->nextPageUrl(),
            $paginator->total(),
        );
        $fractal = Fractal::create()
            ->collection($paginator, $fractalManager)
            ->withCursor($cursor)
            ->addMeta(['per_page' => $per_page]);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function responseByAllFractal(Builder $builder, TransformerAbstract $fractalManager): JsonResponse
    {
        $fractal = Fractal::create()->collection($builder->get(), $fractalManager);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function responseByAllFinalFractal(?Collection $item, TransformerAbstract $fractalManager): JsonResponse|Response
    {
        if (is_null($item)) {
            return response()->json([], 204);
        }

        $fractal = Fractal::create()->collection($item, $fractalManager);

        $transformedData = $this->activateFractalIncludeExclude($fractal);

        return response()->json($transformedData);
    }

    protected function activateFractalIncludeExclude(Fractal $fractal): \League\Fractal\Scope
    {
        $request = Request::capture();
        $manager = new Manager();
        $manager->setSerializer(new SimpleArraySerializer());
        $manager->parseIncludes($request->get('include', ''));
        $manager->parseExcludes($request->get('exclude', ''));
        return $manager->createData($fractal->getResource());
    }

    protected function getPerPage(Request $request): mixed
    {
        return $this->perPages[$request->get('per_page_mode', $this->perPageDefault ?? 0)];
    }

    private function getCursorParameter(?string $cursorUrl): ?string
    {
        return preg_match('/\?cursor=(.*)/', $cursorUrl, $out) ? $out[1] : null;
    }
}
