<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use PragmaRX\Google2FA\Google2FA;

class GeneralController extends ApiController
{
    public function switchToPanel(Request $request)
    {
        if (! auth()->check()) {
            return redirect()->route('auth.register');
        }
        return redirect()->route('sysadmin.dashboard');

        $user = auth()->user();

        $roles = $user->webRoles();

        if (count($roles) === 0) {
            #TODO: add event on this kind of user, such an odd event is this
            return redirect()->route('public.homepage');
        }
        if (
            count($roles) === 1
        ) {
            $panel = Role::findByName($roles[0])->panel ?? 'sysadmin';
            return redirect()->route(Route::has("{$panel->title}.home") ? "{$panel->title}.home" : 'public.homepage');
        }

        $panels = $user->allowedPanels();

        return view('auth.switch_to_panel')
            ->withUser($user)
            ->withPanels($panels);
    }

    public function showAuthTwo(Request $request)
    {
        return view('auth.auth2');
    }

    public function resendAjaxAuthTwo(Request $request)
    {
        return true;
    }

    public function verifyAuthTwo(Request $request)
    {
        $user = session('auth2.user');

        if (empty($user)) {
            return redirect()->route('public.homepage');
        }

        $google2fa = new Google2FA();
        if ($google2fa->verifyKey($user->google_secret_key, $request->gtoken)) {
            auth()->login($user);
            return redirect()->route('auth.switch_to_panel');
        }

        return redirect()->back()
            ->withResult(self::fail(
                __('Google Token Mismatch!')
            ));
    }

    public function verifyAjaxAuthTwo(Request $request)
    {
        $user = session('auth2.user');

        if (empty($user)) {
            return \response('Empty User', 501);
        }

        $google2fa = new Google2FA();
        if ($google2fa->verifyKey($user->google_secret_key, $request->gtoken)) {
            auth()->login($user);
            return \response('Logged In', 200);
        }

        return \response('Token Mismatch', 502);
    }

    public function stopImpersonate()
    {
        $user = Session::get('impersonate');
        abort_if(! $user, 404);

        Session::forget('impersonate');
        auth()->login($user);
        return redirect()->route('auth.switch_to_panel');
    }

    # bugfix: googlebot saw http500 on endpoint
    public function resetPassword()
    {
        return redirect('/', 301);
    }
}
