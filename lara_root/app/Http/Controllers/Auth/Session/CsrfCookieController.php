<?php

namespace App\Http\Controllers\Auth\Session;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Sanctum\Http\Controllers\CsrfCookieController as SanctumCsrfCookieController;

class CsrfCookieController extends SanctumCsrfCookieController
{
    public function show(Request $request): JsonResponse
    {
        return new JsonResponse(null, 200);
    }
}
