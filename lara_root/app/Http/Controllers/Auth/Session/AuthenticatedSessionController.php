<?php

namespace App\Http\Controllers\Auth\Session;

use App\Http\Controllers\ApiController;
use App\Requests\Auth\Session\LoginRequest;
use App\Transformers\InterfaceZone\User\SafeShowTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AuthenticatedSessionController extends ApiController
{
    public function show(): \Illuminate\Foundation\Application|Response|\Illuminate\Http\JsonResponse|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        if (auth()->guest()) {
            return self::fail('unauthorized', 401);
        }

        return $this->responseByOneFractal(auth()->user(), new SafeShowTransformer());
    }

    public function store(LoginRequest $request): \Illuminate\Http\JsonResponse
    {
        $request->authenticate();

        $request->session()->regenerate();

        return self::success(__('login.successful'));
    }

    public function destroy(Request $request): \Illuminate\Http\JsonResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return self::success(__('logout.successful'));
    }
}
