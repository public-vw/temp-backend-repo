<?php

namespace App\Http\Controllers\Auth\Session;

use App\Http\Controllers\ApiController;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends ApiController
{
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $request_all = $request->validate([
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:' . User::class],
            'mobile' => ['sometimes', 'string', 'max:15', 'unique:' . User::class],
            'password' => ['required', Rules\Password::defaults()],
        ]);
        $request_all['password'] = Hash::make($request->password);

        $user = User::create($request_all);

        event(new Registered($user));

        Auth::login($user);

        return self::success('auth.registered');
    }
}
