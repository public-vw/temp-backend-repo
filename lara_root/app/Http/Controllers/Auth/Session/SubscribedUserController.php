<?php

namespace App\Http\Controllers\Auth\Session;

use App\Events\SubscribedForEmail;
use App\Http\Controllers\ApiController;
use App\Models\User;
use App\Requests\Auth\Session\SubscriptionRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SubscribedUserController extends ApiController
{
    public function store(SubscriptionRequest $request): \Illuminate\Http\JsonResponse
    {
        $input = $request->validated();

        $user = User::firstOrCreate(
            [
                'email' => $input['email'],
            ],
            [
                'firstname' => 'EmailSubscriber-' . Carbon::now()->format('Ymd-his'),
                'password' => Hash::make(Str::random(8)),
            ]
        );

        $user->subscriptions()->firstOrCreate([
            'type' => $input['type'] ? config_key('enums.subscriptions.types', $input['type']) : config('enums.subscriptions.type_default'),
        ]);

        event(new SubscribedForEmail($user));

        return self::success('auth.subscribed');
    }
}
