<?php

namespace App\Http\Controllers\Bots;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Telegram\TelegramBot;

class WebhookController extends ApiController
{
    public function index(Request $request, string $token): \Illuminate\Http\Response
    {
        abort_if(config('tbot.webhook.token') !== $token, 404);

        $tbot = new TelegramBot();
        return $tbot->handleWebhook($request);
    }
}
