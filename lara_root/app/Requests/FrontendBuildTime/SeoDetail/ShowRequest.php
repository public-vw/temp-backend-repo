<?php

namespace App\Requests\FrontendBuildTime\SeoDetail;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class ShowRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type' => 'required|in:' . implode(',', $model_types),
            'model_id' => [
                'required',
                'integer',
                Rule::exists(model2table($this->model_type), 'id'),
            ],
        ];
    }

    public function messages(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type.in' => 'The selected model type is invalid. Options: ' . implode(', ', $model_types),
        ];
    }

    public function getModelTypes(): mixed
    {
        $model_types = config('morphs.seoable');
        alterArray($model_types, 'config2model');
        alterArray($model_types, function (&$value) {
            return str_replace('Content', '', $value);
        });
        return $model_types;
    }
}
