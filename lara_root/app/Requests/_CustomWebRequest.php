<?php

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class _CustomWebRequest extends FormRequest
{
    protected $model;
    protected $mainEntity;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $this->prepareProperties();
        $rules = $this->renderRules();

//        if ($this->method() == 'PUT') {
        foreach ($rules as $key => $rule) {
            if (isset($this->mainEntity->id) && strpos($rule, 'unique') !== false) {
                $rules[$key] .= ",{$this->mainEntity->id},id";
            }
            if (strpos($rule, 'COMB_UNIQUE') !== false) {
                $fields = [];
                preg_match('/.*?\[(.*?)]/', $rules[$key], $fields);
                $base = str_replace(",[{$fields[1]}]", '', $rules[$key]);
                $fields = explode(',', $fields[1]);

                $rules[$key] = $base;
                $rules[$key] .= ',' . array_shift($fields) . ',';
                $rules[$key] .= ($this->mainEntity->id ?? 'NULL') . ',id';

                while ($field = array_shift($fields)) {
                    $rules[$key] .= ",{$field},";
                    $rules[$key] .= $this->mainEntity[$field] ?? 'NULL';
                }
                $rules[$key] = str_replace('COMB_UNIQUE', 'unique', $rules[$key]);
            }
        }

        return $rules;
    }

    abstract protected function prepareProperties();

    protected function renderRules()
    {
        $rules = $this->model::$validation;

        // removes * rule for safety (resolves input injection)
        $trailing_rules = $rules['*'] ?? '';
        unset($rules['*']);

        // adds trailing rules to each rule
        foreach ($rules as &$rule) {
            $rule = $trailing_rules . ($rule ? '|' . $rule : '');
        }

        return $rules;
    }
}
