<?php

namespace App\Requests\Auth\API\V1;

use App\Requests\_CustomApiRequest;

class VerifyOtpRequest extends _CustomApiRequest
{
    public function rules()
    {
        return [
            'otp' => 'required|string|size:' . config('otp.length'),
            'tracking_code' => 'required|string|size:' . config('auth.tracking_code.length'),
        ];
    }
}
