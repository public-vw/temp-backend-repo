<?php

namespace App\Requests\Auth\Session;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class SubscriptionRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'email' => 'required|string|email|max:255',
            'type' => 'required|string',
            'model_type' => [
                $this->type !== 'general' ? 'required' : 'sometimes',
                'string',
                'in:' . implode(',', $model_types),
            ],
            'model_id' => [
                $this->type !== 'general' ? 'required' : 'sometimes',
                'integer',
                Rule::exists(model2table($this->model_type), 'id'),
            ],
        ];
    }

    public function getModelTypes(): mixed
    {
        $model_types = config('morphs.subscribable');
        alterArray($model_types, 'config2model');
        return $model_types;
    }

    public function messages(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type.in' => 'The selected model type is invalid. Options: ' . implode(', ', $model_types),
        ];
    }
}
