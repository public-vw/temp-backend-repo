<?php

namespace App\Requests\Sysadmin;

use App\Models\Structure;
use App\Requests\_CustomWebRequest;

class StructureRequest extends _CustomWebRequest
{
    protected function prepareProperties()
    {
        $this->model = Structure::class;
        $this->mainEntity = $this->structure;
    }
}
