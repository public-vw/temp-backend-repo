<?php

namespace App\Requests\InterfaceZone\API\V1\Menu;

use App\Models\Panel;
use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'place' => [
                'required',
                Rule::exists('menu_placeholders', 'slug')
                    ->where(function ($query) {
                        $panel_ids = Panel::where('title', 'interface')->get(['id'])->toArray();
                        return $query->whereIn('panel_id', $panel_ids);
                    }),
            ],
        ];
    }
}
