<?php

namespace App\Requests\InterfaceZone\API\V1\Url;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model' => 'required|string|in:'.implode(',', $model_types),
        ];
    }

    public function messages(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model.in' => 'The selected model is invalid. Options: ' . implode(', ', $model_types),
        ];
    }
    public function getModelTypes(): mixed
    {
        $model_types = config('morphs.urlable');
        alterArray($model_types, 'config2model');
        alterArray($model_types, function (&$value) {
            return str_replace('Content', '', $value);
        });
        return $model_types;
    }
}
