<?php

namespace App\Requests\InterfaceZone\API\V1\User;

use App\Requests\_CustomApiRequest;

class UpdateRequest extends _CustomApiRequest
{
    public function rules()
    {
        return [
            'firstname' => 'sometimes|string',
            'lastname' => 'sometimes|string',
            'username' => 'sometimes|string|unique:users,username,' . $this->user()->id . ',id',
            'nickname' => 'sometimes|string|unique:users,nickname,' . $this->user()->id . ',id',
            'ref_code' => 'sometimes|string|unique:users,ref_code,' . $this->user()->id . ',id',
            'telegram_uid' => 'sometimes|nullable|numeric|digits:11',

            'required_field' => 'required_without_all:firstname,lastname,username,nickname,ref_code,telegram_uid',
        ];
    }

    public function messages()
    {
        return [
            'required_field.required_without_all' => 'At least one field is required. Available fields: firstname, lastname, username, nickname, ref_code, telegram_uid',
        ];
    }
}
