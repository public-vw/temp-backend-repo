<?php

namespace App\Requests\InterfaceZone\API\V1\Comment;

use App\Requests\_CustomApiRequest;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'content' => 'required|string|min:5',
        ];
    }
}
