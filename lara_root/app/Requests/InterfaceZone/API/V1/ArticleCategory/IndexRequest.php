<?php

namespace App\Requests\InterfaceZone\API\V1\ArticleCategory;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'parent_id' => 'sometimes|integer|exists:article_categories,id',
        ];
    }
}
