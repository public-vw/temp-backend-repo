<?php

namespace App\Requests\InterfaceZone\API\V1\Article;

use App\Requests\_CustomApiRequest;

class ShowRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'url' => 'required|string|regex:/^[a-z\/\-0-9]+\.html$/',
        ];
    }
}
