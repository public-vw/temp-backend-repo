<?php

namespace App\Requests\InterfaceZone\API\V1\ArticleRelation;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type' => 'required|in:' . implode(',', $model_types),
            'model_id' => [
                'required',
                'integer',
                Rule::exists(model2table($this->model_type), 'id'),
            ],
            'relation_type' => 'required|in:' . implode(',', config('enums.article_relations.types')),
        ];
    }

    public function getModelTypes(): mixed
    {
        $model_types = config('morphs.relatable');
        alterArray($model_types, 'config2model');
        return $model_types;
    }

    public function messages(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type.in' => 'The selected model type is invalid. Options: ' . implode(', ', $model_types),
            'relation_type.in' => 'The selected relation type is invalid. Options: ' . implode(', ', config('enums.article_relations.types')),
        ];
    }
}
