<?php

namespace App\Requests\InterfaceZone\API\V1\StaticPage;

use App\Requests\_CustomApiRequest;

class ShowRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'url' => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    if (! ($value === '/' || preg_match('/^[a-z\/\-0-9]+\.html$/', $value))) {
                        $fail('The ' . $attribute . ' is invalid.');
                    }
                },
            ],
        ];
    }
}
