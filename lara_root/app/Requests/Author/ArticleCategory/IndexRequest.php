<?php

namespace App\Requests\Author\ArticleCategory;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'parent_id' => 'sometimes|nullable|integer|exists:article_categories,id',
        ];
    }
}
