<?php

namespace App\Requests\Author\Attachable;

use App\Requests\_CustomApiRequest;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'noindex' => 'sometimes|boolean',
            'attributes' => 'sometimes|nullable',//TODO:xss issue, filter objects with editorjs concepts
        ];
    }
}
