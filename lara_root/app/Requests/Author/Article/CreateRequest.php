<?php

namespace App\Requests\Author\Article;

use App\Requests\_CustomApiRequest;

class CreateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'category_id' => 'sometimes|nullable|integer|exists:article_categories,id',
        ];
    }
}
