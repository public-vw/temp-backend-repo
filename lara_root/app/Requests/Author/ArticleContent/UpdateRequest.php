<?php

namespace App\Requests\Author\ArticleContent;

use App\Requests\_CustomApiRequest;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $article = $this->route('article_content')->container;
        $category = $article->category;

        return [
            'heading' => 'sometimes|nullable|string|max:255',
            'content' => 'sometimes|nullable|array',//TODO:xss issue, filter objects with editorjs concepts
            'reading_time' => 'sometimes|integer|min:0|max:100',
            'summary' => 'sometimes|nullable|string',
            'inspiration' => 'sometimes|nullable|string',
        ];
    }

    protected function prepareForValidation(): void
    {
        if (! $this->has('content')) {
            return;
        }

        $content = $this->input('content');
        if (is_array($content)) {
            $content = $this->replaceNullWithEmptyString($content, 'text');
        }

        $this->merge(['content' => $content]);
    }

    private function replaceNullWithEmptyString($array, $target_key)
    {
        array_walk_recursive($array, function (&$item, $key) use ($target_key) {
            if ($key === $target_key && is_null($item)) {
                $item = '';
            }
        });

        return $array;
    }
}
