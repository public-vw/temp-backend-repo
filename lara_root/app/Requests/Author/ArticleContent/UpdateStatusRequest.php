<?php

namespace App\Requests\Author\ArticleContent;

use App\Requests\_CustomApiRequest;

class UpdateStatusRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'status' => 'sometimes|string|in:' . implode(',', ['review', 'ready', 'request-review']),
        ];
    }
}
