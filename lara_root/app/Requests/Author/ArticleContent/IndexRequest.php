<?php

namespace App\Requests\Author\ArticleContent;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'article_id' => 'sometimes|integer|exists:articles,id',
            'text' => 'sometimes|string',
        ];
    }
}
