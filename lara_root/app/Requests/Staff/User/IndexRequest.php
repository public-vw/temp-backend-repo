<?php

namespace App\Requests\Staff\User;

use App\Models\Role;
use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'role' => 'sometimes|string|exists:roles,name',
            'status' => 'sometimes|string|in:' . implode(',', config('enums.users.status')),
        ];
    }

    public function messages(): array
    {
        return [
            'role.exists' => 'The selected role is invalid. Options: ' . implode(', ', Role::pluck('name')->toArray()),
            'status.in' => 'The selected status is invalid. Options: ' . implode(', ', config('enums.users.status')),
        ];
    }
}
