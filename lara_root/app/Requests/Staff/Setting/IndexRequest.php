<?php

namespace App\Requests\Staff\Setting;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'group_id' => 'sometimes|exists:setting_groups,id',
            'slug' => 'sometimes',
        ];
    }
}
