<?php

namespace App\Requests\Staff\Setting;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'title' => 'sometimes|string',
            'slug' => [
                'sometimes',
                'string',
                Rule::unique('settings', 'slug')
                    ->where('group_id', $this->group_id)
                    ->ignore($this->id),
            ],
            'group_id' => 'sometimes|integer|exists:setting_groups,id',
            'value' => 'sometimes|' . ($this->type ?? $this->setting->type),
            'order' => 'sometimes|integer',
            'lang' => 'sometimes|string|in:en,fa',
            'direction' => 'sometimes|string|in:' . implode(',', config('enums.settings.directions')),
            'type' => 'sometimes|string|in:' . implode(',', config('enums.settings.types')),
            'mode' => 'sometimes|string|in:' . implode(',', config('enums.settings.modes')),
            'description' => 'sometimes|string',
            'hidden' => 'sometimes|boolean',
            'for_frontend' => 'sometimes|boolean',
        ];
    }

    public function messages(): array
    {
        return [
            'lang.in' => 'The lang field is invalid. Options: ' . implode(',', ['en', 'fa']),
            'direction.in' => 'The direction field is invalid. Options: ' . implode(',', config('enums.settings.directions')),
            'type.in' => 'The type field is invalid. Options: ' . implode(',', config('enums.settings.types')),
            'mode.in' => 'The mode field is invalid. Options: ' . implode(',', config('enums.settings.modes')),
        ];
    }
}
