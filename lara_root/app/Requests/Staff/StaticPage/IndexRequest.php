<?php

namespace App\Requests\Staff\StaticPage;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'title' => 'sometimes|string|max:150',
            'text' => 'sometimes|string',
        ];
    }
}
