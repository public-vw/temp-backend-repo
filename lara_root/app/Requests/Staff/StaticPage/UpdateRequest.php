<?php

namespace App\Requests\Staff\StaticPage;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'title' => [
                'sometimes',
                'string',
                'max:150',
                Rule::unique('static_pages', 'title')
                    ->ignore($this->route('static_page')->id),
            ],
        ];
    }
}
