<?php

namespace App\Requests\Staff\StaticPage;

use App\Requests\_CustomApiRequest;

class CreateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:150|unique:static_pages,title',
        ];
    }
}
