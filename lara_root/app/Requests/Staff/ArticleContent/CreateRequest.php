<?php

namespace App\Requests\Staff\ArticleContent;

use App\Requests\_CustomApiRequest;

class CreateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'article_id' => 'required|integer|exists:articles,id',
        ];
    }
}
