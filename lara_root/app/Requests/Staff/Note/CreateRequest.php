<?php

namespace App\Requests\Staff\Note;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type' => 'required|in:' . implode(',', $model_types),
            'model_id' => [
                'required',
                'integer',
                Rule::exists(model2table($this->model_type), 'id'),
            ],
            'platejs_id' => 'required|string',
            'platejs_parent_id' => 'sometimes|nullable|string',
            'content' => 'required|array',
        ];
    }

    public function getModelTypes(): mixed
    {
        $model_types = config('morphs.notable');
        alterArray($model_types, 'config2model');
        return $model_types;
    }

    public function messages(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type.in' => 'The selected model type is invalid. Options: ' . implode(', ', $model_types),
        ];
    }
}
