<?php

namespace App\Requests\Staff\Note;

use App\Requests\_CustomApiRequest;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'platejs_id' => 'sometimes|string',
            'platejs_parent_id' => 'sometimes|nullable|string',
            'content' => 'sometimes|array',
        ];
    }
}
