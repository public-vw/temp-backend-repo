<?php

namespace App\Requests\Staff\StaticPageContent;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'static_page_id' => 'sometimes|integer|exists:static_pages,id',
            'text' => 'sometimes|string',
        ];
    }
}
