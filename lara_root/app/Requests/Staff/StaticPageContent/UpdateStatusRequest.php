<?php

namespace App\Requests\Staff\StaticPageContent;

use App\Requests\_CustomApiRequest;

class UpdateStatusRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'status' => 'sometimes|string|in:' . implode(',', config('enums.contents.status')),
        ];
    }
}
