<?php

namespace App\Requests\Staff\StaticPageContent;

use App\Requests\_CustomApiRequest;

class CreateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'static_page_id' => 'required|integer|exists:static_pages,id',
        ];
    }
}
