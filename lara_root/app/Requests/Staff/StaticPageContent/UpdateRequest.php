<?php

namespace App\Requests\Staff\StaticPageContent;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $static_page = $this->route('static_page_content')->container;

        return [
            'uri' => [
                'sometimes',
                'nullable',
                'string',
                Rule::unique('article_contents', 'uri')
                    ->whereNot('static_page_id', $static_page->id),
            ],
            'heading' => 'sometimes|nullable|string|max:255',
            'content' => 'sometimes|nullable|array',//TODO:xss issue, filter objects with editorjs concepts
            'data' => 'sometimes|nullable|array',//TODO:xss issue, filter objects with editorjs concepts
            'reading_time' => 'sometimes|integer|min:0|max:100',
        ];
    }

    protected function prepareForValidation(): void
    {
        if (! $this->has('content')) {
            return;
        }

        $content = $this->input('content');
        if (is_array($content)) {
            $content = $this->replaceNullWithEmptyString($content, 'text');
        }

        $this->merge(['content' => $content]);
    }

    private function replaceNullWithEmptyString($array, $target_key)
    {
        array_walk_recursive($array, function (&$item, $key) use ($target_key) {
            if ($key === $target_key && is_null($item)) {
                $item = '';
            }
        });

        return $array;
    }
}
