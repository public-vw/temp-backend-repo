<?php

namespace App\Requests\Staff\ArticleCategory;

use App\Requests\_CustomApiRequest;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'parent_id' => 'sometimes|nullable|integer|exists:article_categories,id',
            'order' => 'sometimes|integer|min:0',
        ];
    }
}
