<?php

namespace App\Requests\Staff\ArticleCategory;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'parent_id' => 'sometimes|nullable|exists:article_categories,id',
        ];
    }
}
