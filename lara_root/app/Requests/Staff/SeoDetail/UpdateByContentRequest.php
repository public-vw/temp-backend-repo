<?php

namespace App\Requests\Staff\SeoDetail;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class UpdateByContentRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type' => 'required|in:' . implode(',', $model_types),
            'model_id' => [
                'required',
                'integer',
                Rule::exists(model2table($this->model_type), 'id'),
            ],
            'data' => 'required|array',
        ];
    }

    public function messages(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type.in' => 'The selected model type is invalid. Options: ' . implode(', ', $model_types),
        ];
    }

    public function getModelTypes(): mixed
    {
        $model_types = config('morphs.seoable');
        alterArray($model_types, 'config2model');
        return $model_types;
    }
}
