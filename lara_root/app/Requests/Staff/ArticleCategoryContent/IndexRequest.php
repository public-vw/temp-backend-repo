<?php

namespace App\Requests\Staff\ArticleCategoryContent;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'category_id' => 'sometimes|integer|exists:article_categories,id',
            'text' => 'sometimes|string',
        ];
    }
}
