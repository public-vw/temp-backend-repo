<?php

namespace App\Requests\Staff\ArticleCategoryContent;

use App\Requests\_CustomApiRequest;

class CreateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'article_category_id' => 'required|integer|exists:article_categories,id',
            'title' => 'required|string|max:255',
        ];
    }
}
