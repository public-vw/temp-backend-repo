<?php

namespace App\Requests\Staff\ArticleCategoryContent;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $category = $this->route('article_category_content')->container;
        $parent = $category->parent ?? $category->publishedParent();

        return [
            'uri' => [
                'sometimes',
                'nullable',
                'string',
                Rule::unique('article_category_contents', 'uri')
                    ->where(function ($query) use ($parent, $category) {
                        return $query->whereIn('article_category_id', function ($subQuery) use ($parent) {
                            $subQuery->select('id')
                                ->from('article_categories')
                                ->where('parent_id', $parent?->id);
                        })->where('article_category_id', '<>', $category->id); // Excluding the current category's contents
                    }),
            ],
            'heading' => 'sometimes|nullable|string|max:255',
            'title' => 'sometimes|nullable|string|max:255',
            'summary' => 'sometimes|nullable|string',
            'description' => 'sometimes|nullable|string',
        ];
    }
}
