<?php

namespace App\Requests\Staff\Article;

use App\Requests\_CustomApiRequest;

class IndexRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'category_id' => 'sometimes|nullable|exists:article_categories,id',
            'author_id' => 'sometimes|nullable|exists:users,id',
        ];
    }
}
