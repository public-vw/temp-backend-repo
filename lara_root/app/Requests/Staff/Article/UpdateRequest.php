<?php

namespace App\Requests\Staff\Article;

use App\Requests\_CustomApiRequest;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        return [
            'author_id' => 'sometimes|nullable|integer|exists:users,id',
            'category_id' => 'sometimes|nullable|integer|exists:article_categories,id',
        ];
    }
}
