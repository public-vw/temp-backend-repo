<?php

namespace App\Requests\Staff\SeoRichSnippet;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class UpdateByParentRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $model_types = $this->getModelTypes();
        $modelType = $this->input('model_type');

        // Common rules
        $rules = [
            'model_type' => 'required|in:' . implode(',', $model_types),
            'data' => 'required|array',
        ];

        // Conditional rules for model_id
        if ($modelType !== 'Website') {
            $rules['model_id'] = [
                'required',
                'integer',
                Rule::exists(model2table($modelType), 'id'),
            ];
        } else {
            $rules['model_id'] = 'nullable';
        }

        return $rules;
    }

    public function messages(): array
    {
        $model_types = $this->getModelTypes();

        return [
            'model_type.in' => 'The selected model type is invalid. Options: ' . implode(', ', $model_types),
        ];
    }

    public function getModelTypes(): mixed
    {
        $model_types = config('morphs.richsnippetable');
        alterArray($model_types, 'config2model');
        alterArray($model_types, function (&$value) {
            return str_replace('Content', '', $value);
        });
        return $model_types;
    }
}
