<?php

namespace App\Requests\Staff\Attachable;

use App\Models\AttachmentType;
use App\Models\ModelAttachmentType;
use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $model_types = $this->getModelTypes();

        return [
            //TODO: max size should be adjustable by attachmentType
            //TODO: mimes should accept audio and video also
            'file' => 'required|mimes:jpg,jpeg,png,gif,webp|max:200480',

            'model_type' => 'required|in:' . implode(',', $model_types),
            'model_id' => [
                'required',
                'integer',
                Rule::exists(model2table($this->model_type), 'id'),
            ],
            'attachment_type_slug' => [
                'required',
                'string',
                Rule::exists('attachment_types', 'slug')
                    ->where(function ($query) {
                        $attachment_type_ids = ModelAttachmentType::where('model_name', $this->model_type)->get(['attachment_type_id'])->toArray();
                        return $query->whereIn('id', $attachment_type_ids);
                    }),
            ],
        ];
    }

    public function getModelTypes(): mixed
    {
        $model_types = config('morphs.attachmentable');
        alterArray($model_types, 'config2model');
        return $model_types;
    }

    public function messages(): array
    {
        $model_types = $this->getModelTypes();

        $attachment_type_ids = ModelAttachmentType::where('model_name', $this->model_type)->get(['attachment_type_id'])->toArray();
        $slugs = AttachmentType::whereIn('id', $attachment_type_ids)->pluck('slug')->toArray();

        return [
            'model_type.in' => 'The selected model type is invalid. Options: ' . implode(', ', $model_types),
            'attachment_type_slug.exists' => 'The selected attachment type does not exists for this model_type.'
                .'Options: '. implode(', ', $slugs),
        ];
    }
}
