<?php

namespace App\Requests\Staff\Attachable;

use App\Requests\_CustomApiRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends _CustomApiRequest
{
    public function rules(): array
    {
        $attachment = $this->attachable->attachment;

        return [
            'noindex' => 'sometimes|boolean',
            'uri' => [
                'sometimes',
                'string',
                Rule::unique('attachments')->where(function ($query) use ($attachment) {
                    $media = $attachment->media;
                    return $query->where('media', $media);
                })->ignore($attachment?->id),
            ],
            'attributes' => 'sometimes|nullable',//TODO:xss issue, filter objects with editorjs concepts
        ];
    }
}
