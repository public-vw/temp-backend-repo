<?php

namespace App\Listeners;

use App\Events\ContentActivated;
use App\Jobs\JRevalidateRequest;
use App\Libraries\Revalidation\UrlBase;

class CreateUrlIfNeeded
{
    public function handle(ContentActivated $event): void
    {
        $container = $event->content->container;

        $old_url = $container->url;
        if ($container->addUrl()) { # generates new url (if differs from previous)
            $new_url = $container->url;
        }

        if ($old_url !== $new_url) {
            JRevalidateRequest::dispatch(new UrlBase($container::class, $old_url));
            JRevalidateRequest::dispatch(new UrlBase($container::class, $new_url));
        }
    }
}
