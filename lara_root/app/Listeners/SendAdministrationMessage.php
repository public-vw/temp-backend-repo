<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;

class SendAdministrationMessage
{
    use InteractsWithQueue;

    public function handle(object $event): void
    {
        $eventClass = Str::kebab(last(explode('\\', $event::class)));
        if (config('tbot.listen_to.' . $eventClass, true) === false) {
            return;
        }

        \App\Events\TelegramAlertRequested::dispatch(
            config('tbot.supervisor_telegram_uid'),
            $event->level,
            $event->title,
            implode_assoc($event->payload, ': ', PHP_EOL)
        );
    }
}
