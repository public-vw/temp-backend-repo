<?php

namespace App\Listeners;

use App\Events\TelegramAlertRequested;
use App\Jobs\JSendTelegramMessage;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;

class SendTelegramAlert
{
    use InteractsWithQueue;

    protected TelegramAlertRequested $event;
    protected Carbon $time;

    public function handle(TelegramAlertRequested $event): void
    {
        $this->event = $event;
        $this->time = Carbon::now();

        JSendTelegramMessage::dispatch(
            $event->chat_id,
            $this->getMessage()
        );
    }

    protected function getMessage(): string
    {
        $message = [
            'nokey_project' => $this->event->level->value . ' ' . config('app.telegram_title'),
            'nokey_gap1' => '',
            'nokey_hastags' => implode(PHP_EOL, $this->hashtags()),
            'nokey_gap2' => '',
            'IP' => get_ip(),
            'Date' => $this->time->toDateString(),
            'Time' => $this->time->toTimeString(),
            'User' => auth()->user()?->username ?? 'guest',
//            'URL'               => $this->url,
            'nokey_separator_1' => '---------------',
            'nokey_title' => $this->event->title,
            'nokey_gap3' => '',
            'Data' => PHP_EOL . $this->event->data,
        ];

        return implode_assoc($message, ': ', PHP_EOL);
    }

    protected function hashtags(): array
    {
        $return[] = '#'.Str::slug('site_'.config('app.telegram_title'), '_');
        $return[] = '#'.Str::slug('level_'.$this->event->level->name, '_');
        $return[] = '#'.Str::slug('title_'.$this->event->title, '_');
        $return[] = '#'.Str::slug('date_'.$this->time->toDateString(), '_');

        return $return;
    }
}
