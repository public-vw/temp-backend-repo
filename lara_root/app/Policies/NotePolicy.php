<?php

namespace App\Policies;

use App\Models\Note;
use App\Models\User;

class NotePolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, Note $note): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, Note $note): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return $note->user_id === $user->id || $user->hasRole('sysadmin|admin');
    }

    public function delete(User $user, Note $note): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, Note $note): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, Note $note): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return $user->hasRole('sysadmin|admin');
    }
}
