<?php

namespace App\Policies;

use App\Models\User;

class UserPolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
