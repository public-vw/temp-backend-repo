<?php

namespace App\Policies;

use App\Models\Menu;
use App\Models\User;

class MenuPolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, Menu $menu): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, Menu $menu): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, Menu $menu): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, Menu $menu): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, Menu $menu): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
