<?php

namespace App\Policies;

use App\Models\ArticleContent;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class ArticleContentPolicy
{
    public function createNote(User $user, Model $notable): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function updateSeo(User $user, Model $seoable): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, ArticleContent $articleContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, ArticleContent $articleContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function updateStatus(User $user, ArticleContent $articleContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, ArticleContent $articleContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, ArticleContent $articleContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, ArticleContent $articleContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
