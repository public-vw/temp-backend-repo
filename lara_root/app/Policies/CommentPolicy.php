<?php

namespace App\Policies;

use App\Models\Comment;
use App\Models\User;

class CommentPolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, Comment $comment): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, Comment $comment): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return $comment->user_id === $user->id || $user->hasRole('sysadmin|admin');
    }

    public function delete(User $user, Comment $comment): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, Comment $comment): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, Comment $comment): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return $user->hasRole('sysadmin|admin');
    }
}
