<?php

namespace App\Policies;

use App\Models\Like;
use App\Models\User;

class LikePolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, Like $like): bool
    {
        return $user->id === $like->user_id;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, Like $like): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, Like $like): bool
    {
        return $user->id === $like->user_id;
    }

    public function restore(User $user, Like $like): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, Like $like): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
