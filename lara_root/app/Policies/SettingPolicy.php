<?php

namespace App\Policies;

use App\Models\Setting;
use App\Models\User;

class SettingPolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, Setting $setting): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, Setting $setting): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, Setting $setting): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, Setting $setting): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, Setting $setting): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
