<?php

namespace App\Policies;

use App\Models\StaticLink;
use App\Models\User;

class StaticLinkPolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, StaticLink $staticLink): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, StaticLink $staticLink): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, StaticLink $staticLink): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, StaticLink $staticLink): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, StaticLink $staticLink): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
