<?php

namespace App\Policies;

use App\Models\StaticPageContent;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class StaticPageContentPolicy
{
    public function createNote(User $user, Model $notable): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function updateSeo(User $user, Model $seoable): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, StaticPageContent $staticPageContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, StaticPageContent $staticPageContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function updateStatus(User $user, StaticPageContent $staticPageContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, StaticPageContent $staticPageContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, StaticPageContent $staticPageContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, StaticPageContent $staticPageContent): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
