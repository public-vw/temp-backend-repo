<?php

namespace App\Policies;

use App\Models\MenuPlaceholder;
use App\Models\User;

class MenuPlaceholderPolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, MenuPlaceholder $menuPlaceholder): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, MenuPlaceholder $menuPlaceholder): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, MenuPlaceholder $menuPlaceholder): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, MenuPlaceholder $menuPlaceholder): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, MenuPlaceholder $menuPlaceholder): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
