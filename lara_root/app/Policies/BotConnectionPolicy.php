<?php

namespace App\Policies;

use App\Models\BotConnection;
use App\Models\User;

class BotConnectionPolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, BotConnection $botConnection): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, BotConnection $botConnection): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, BotConnection $botConnection): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, BotConnection $botConnection): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, BotConnection $botConnection): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
