<?php

namespace App\Policies;

use App\Models\SeoRichSnippet;
use App\Models\User;

class SeoRichSnippetPolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, SeoRichSnippet $seoRichSnippet): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, SeoRichSnippet $seoRichSnippet): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function updateAny(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, SeoRichSnippet $seoRichSnippet): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, SeoRichSnippet $seoRichSnippet): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, SeoRichSnippet $seoRichSnippet): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
