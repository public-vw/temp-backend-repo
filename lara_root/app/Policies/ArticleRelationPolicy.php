<?php

namespace App\Policies;

use App\Models\ArticleRelation;
use App\Models\User;

class ArticleRelationPolicy
{
    public function viewAny(User $user): bool
    {
        return $user->hasState('active');
    }

    public function view(User $user, ArticleRelation $articleRelation): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function create(User $user): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function update(User $user, ArticleRelation $articleRelation): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function delete(User $user, ArticleRelation $articleRelation): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function restore(User $user, ArticleRelation $articleRelation): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }

    public function forceDelete(User $user, ArticleRelation $articleRelation): bool
    {
        if (! $user->hasState('active')) {
            return false;
        }

        return true;
    }
}
