import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    // Only for debug purpose
    server: {
        host: '0.0.0.0',
        port: 3000 // or any port you want to use
    },

    plugins: [
        laravel({
            input: [
                // // Auth
                // 'resources/assets/auth/css/app.css',
                // 'resources/assets/auth/js/app.js',
                //
                // // Sysadmin
                // 'resources/assets/sysadmin/css/app.css',
                // 'resources/assets/sysadmin/js/app.js',
                //
                // // Admin
                // 'resources/assets/admin/css/app.css',
                // 'resources/assets/admin/js/app.js',
                //
                // // Developer
                // 'resources/assets/dev/css/app.css',
                // 'resources/assets/dev/js/app.js',
                //
                // // Author
                // 'resources/assets/author/css/app.css',
                // 'resources/assets/author/js/app.js',
                //
                // // Seo
                // 'resources/assets/seo/css/app.css',
                // 'resources/assets/seo/js/app.js',
                //
                // // Reader
                // 'resources/assets/reader/css/app.css',
                // 'resources/assets/reader/js/app.js',
            ],
            refresh: true,
        }),
    ],
});
