<?php

namespace Tests\Feature\GraphQL;

use Myblogs\Support\Arr;

trait EnablesQueries
{
    public function getEnvironmentSetup($app)
    {
        parent::getEnvironmentSetUp($app);

        if ($this->enabledQueries) {
            foreach (Arr::wrap($this->enabledQueries) as $key) {
                $app['config']->set('Myblogs.graphql.resources.'.$key, true);
            }
        }
    }
}
