<?php

use Myblogs\Facades\Blink;

class StarterKitPostInstall
{
    public $registerCommands = [
        StarterKitTestCommand::class,
    ];

    public function handle($console)
    {
        $console->call('Myblogs:test:starter-kit-command');
    }
}

class StarterKitTestCommand extends \Illuminate\Console\Command
{
    protected $name = 'Myblogs:test:starter-kit-command';

    public function handle()
    {
        Blink::put('post-install-hook-run', true);
    }
}
