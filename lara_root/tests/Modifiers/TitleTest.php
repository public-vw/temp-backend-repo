<?php

namespace Tests\Modifiers;

use Myblogs\Modifiers\Modify;
use Tests\TestCase;

class TitleTest extends TestCase
{
    /** @test */
    public function it_converts_to_a_title(): void
    {
        $string = 'create your first PR to Myblogs CMS';

        $this->assertSame('Create Your First PR to Myblogs CMS', $this->modify($string));
    }

    private function modify($value)
    {
        return Modify::value($value)->title()->fetch();
    }
}
