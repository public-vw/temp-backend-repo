<?php

namespace Tests\Fieldtypes;

use Myblogs\Fields\Field;
use Myblogs\Fieldtypes\Checkboxes;
use Tests\TestCase;

class CheckboxesTest extends TestCase
{
    use CastsMultipleBooleansTests, MultipleLabeledValueTests;

    private function field($config)
    {
        $ft = new Checkboxes;

        return $ft->setField(new Field('test', array_merge($config, ['type' => $ft->handle()])));
    }
}
