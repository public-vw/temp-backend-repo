<?php

namespace Tests\Fieldtypes;

use Myblogs\Fields\Field;
use Myblogs\Fieldtypes\Radio;
use Tests\TestCase;

class RadioTest extends TestCase
{
    use CastsBooleansTests, LabeledValueTests;

    private function field($config)
    {
        $ft = new Radio;

        return $ft->setField(new Field('test', array_merge($config, ['type' => $ft->handle()])));
    }
}
