<?php

namespace Tests\Auth;

use Myblogs\Auth\File\User;
use Tests\PreventSavingStacheItemsToDisk;
use Tests\TestCase;

/** @group user-repo */
class StacheUserRepositoryTest extends TestCase
{
    use PreventSavingStacheItemsToDisk;
    use UserRepositoryTests;

    public function userClass()
    {
        return User::class;
    }

    public function fakeUserClass()
    {
        return FakeStacheUser::class;
    }
}

class FakeStacheUser extends \Myblogs\Auth\File\User
{
    public function initials()
    {
        return 'FAKEINITIALS';
    }
}
