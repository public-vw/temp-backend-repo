<?php

namespace Tests\Search;

use Mockery;
use Myblogs\Search\Algolia\Index;
use Myblogs\Search\Algolia\Query;
use Tests\TestCase;

class AlgoliaQueryTest extends TestCase
{
    /** @test */
    public function it_adds_scores()
    {
        $index = Mockery::mock(Index::class);
        $index->shouldReceive('searchUsingApi')->with('foo')->once()->andReturn(collect([
            ['reference' => 'a'],
            ['reference' => 'b'],
            ['reference' => 'c'],
        ]));

        $query = new Query($index);

        $this->assertEquals([
            ['reference' => 'a', 'search_score' => 3],
            ['reference' => 'b', 'search_score' => 2],
            ['reference' => 'c', 'search_score' => 1],
        ], $query->getSearchResults('foo')->all());
    }
}
