<?php

namespace Tests\Antlers\Fixtures\Addon\Modifiers;

use Mockery\MockInterface;
use Myblogs\Modifiers\Modifier;
use Myblogs\Support\Str;

class IsBuilder extends Modifier
{
    protected static $handle = 'is_builder';

    public function index($value)
    {
        if ($value instanceof MockInterface) {
            $className = get_class($value);

            if (Str::endsWith($className, 'Myblogs_Contracts_Query_Builder')) {
                return 'Myblogs\Contracts\Query\Builder';
            }
        }

        return 'Definitely\Did\Not\Get\The\Mocked\Builder';
    }
}
