<?php

namespace Tests\View\Antlers;

use Myblogs\Facades\Antlers;
use Tests\TestCase;

class ParserTest extends TestCase
{
    use ParserTests;

    protected function resolveApplicationConfiguration($app)
    {
        parent::resolveApplicationConfiguration($app);
        $app['config']->set('Myblogs.antlers.version', 'regex');
    }

    private function renderString($template, $data = [])
    {
        return (string) $this->parser()->parse($template, $data);
    }

    private function parser()
    {
        return Antlers::parser();
    }
}
