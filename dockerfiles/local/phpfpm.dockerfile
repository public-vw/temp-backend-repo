FROM bitnami/php-fpm:8.2

RUN apt update
RUN apt install apt-utils
RUN apt install -y build-essential
RUN apt install -y vim iputils-ping
RUN apt install -y autoconf gcc make curl php-curl
RUN apt install -y php-http php-pecl-http net-tools

# Installing the Redis extension
RUN curl -L -o /tmp/redis.tar.gz https://github.com/phpredis/phpredis/archive/refs/tags/5.3.7.tar.gz \
    && tar xfz /tmp/redis.tar.gz \
    && rm -r /tmp/redis.tar.gz \
    && mv phpredis-* phpredis \
    && cd phpredis \
    && phpize \
    && ./configure \
    && make \
    && make install
#RUN echo "extension=redis.so" > /opt/bitnami/php/etc/conf.d/redis.ini

# Installing the Xdebug extension
RUN curl -L -o /tmp/xdebug.tar.gz https://xdebug.org/files/xdebug-3.2.2.tgz \
    && tar -xzf /tmp/xdebug.tar.gz \
    && rm /tmp/xdebug.tar.gz \
    && cd xdebug-3.2.2 \
    && phpize \
    && ./configure --enable-xdebug \
    && make \
    && make install
#RUN echo "zend_extension=xdebug.so" > /opt/bitnami/php/etc/conf.d/xdebug.ini

# Installing Imagick and GD
RUN apt-get install -y libmagickwand-dev libmagickcore-dev
RUN curl -L -o /tmp/imagick.tar.gz https://github.com/Imagick/imagick/archive/master.tar.gz \
    && tar xfz /tmp/imagick.tar.gz \
    && rm /tmp/imagick.tar.gz \
    && mv imagick-* imagick \
    && cd imagick \
    && phpize \
    && ./configure \
    && make \
    && make install
#RUN pecl install imagick
#RUN echo "extension=imagick.so" > /opt/bitnami/php/etc/conf.d/imagick.ini

# Installing GD dependencies
RUN apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev
# In Bitnami, you'll need to manually configure and enable GD

# Cleaning up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/pear /tmp/* /var/tmp/*

USER root
