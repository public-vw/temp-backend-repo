# Laravel 10 Pack
## Getting Started
use Script approach or manual approach
### Script Approach
simply run `./shell_scripts/starter.sh` and follow the instructions

### Manual Approach
copy general env file
decide if this is a development env or production env

mkdir -p data/db data/minio
chmod 777 -R data

### Config minio
it should work with defaults, if you only go to the minio management panel and create the 3 buckets
- permanent
- temporary
- data

## minIO painpoints
+ env: FILESYSTEM_DRIVER = s3
+ use_path_style_endpoint=true

(caused a lot of not-working-issues, took me a while to figure this out)

## XDEBUG adjustment with postman and intellijIDEA
1. ide confings
* Go to Preferences -> Languages & Frameworks -> PHP -> Debug | Xdebug.
* Set the Debug port to 9001.

2. in the Run/Debug configuration
* add new 'PHP Remote Debug'
* create a server, with http://localhost:8251 XDebug, and map /lara_root with /app
* IDE key=INDOCKER

3. in the postman
* add XDEBUG_SESSION_START=INDOCKER to the request header

## Use as base on a new project
1. create a new project repo in github|gitlab
2. initialize the project locally
`git clone <repo address>`
`git switch --create master`
`touch README.md`
`git add .`
`git commit -m "creates master branch"`
`git push -u origin master`
3. add 'Laravel 10 pack' to the project as a new branch
`git remote add larapack-origin git@datarivers.gitlab.com:own-projects-2023/lara-10-site-pack.git;`
`git fetch larapack-origin;`
`git checkout -b global-pack larapack-origin/master;`
`git checkout master;`

## Add remote repo of Gitlab to the server
`git clone https://username:password@gitlab.com/repo.git`

## Fire-up New Domain
### DNS connection
point a cloudflare dns record to the server

## Commands & Tools
### using artisan
`./artisan.sh <command data>`

### using composer
`./composer.sh <command data>`

### using mail server
the port can be changed in the general env file

http://localhost:8025 
### using dump server
`./artisan.sh dump-server`

### using phpMyAdmin
#### starting the pma server
`bash ./pma/start.sh`
#### viewing the browser
the port can be changed in the general env file

http://localhost:8080
#### stopping the pma server
bash ./pma/stop.sh

## RClone configuration

### Create a new config file
run the config command in simple mode (not advanced mode)
`docker compose run rclone config`
* remember to use **NO** when asked for the authentication method

### Adjust specific gdrive folder in config
`root_folder_id = <the folder id>`

## DB Backup and Restore

### Move data from server to local
Backup on server:
`./shell_scripts/db_backup.sh -n -u`
* **-n:** don't keep local backup
* **-u:** upload to remote source

Restore on local:
`./shell_scripts/db_restore.sh -d -r`
* **-d:** drop all local tables
* **-r:** restore data from remote source