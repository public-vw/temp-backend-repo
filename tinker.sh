#!/bin/bash

DEBUG=false
ARGS=""
XDEBUG_IDEKEY="INDOCKER"

# Parse command-line arguments for debug flag
for arg in "$@"
do
  case $arg in
    -d|--debug)
      DEBUG=true
      shift
      ;;
    *)
      ARGS="$ARGS $arg"
      shift
      ;;
  esac
done

(
  source ./.env

  if [ "$DEBUG" = true ]; then
    $DOCKER_COMPOSE exec -e XDEBUG_MODE=debug -e XDEBUG_CONFIG="idekey=$XDEBUG_IDEKEY" \
     phpfpm php artisan tinker $ARGS
  else
    $DOCKER_COMPOSE exec phpfpm php artisan tinker $ARGS
  fi
)