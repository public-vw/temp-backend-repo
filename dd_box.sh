#!/bin/bash

reset
(
  source ./.env
  $DOCKER_COMPOSE exec -u $(id -u):$(id -g) phpfpm php artisan dump-server $@
)