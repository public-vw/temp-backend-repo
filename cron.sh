#!/bin/bash

(
  source ./.env
  $DOCKER_COMPOSE exec phpfpm php artisan schedule:run >> ./logs/cron.log
)