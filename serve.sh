#!/bin/bash

(
  source ./.env
  $DOCKER_COMPOSE exec phpfpm php artisan serve --port=8000 --host=0.0.0.0
)