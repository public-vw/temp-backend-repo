#!/bin/bash

reset

(
  source ./.env
  # shellcheck disable=SC2145
  $DOCKER_COMPOSE exec phpfpm sh -c "./vendor/bin/pest $@"
)